	  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url('backend/'.$this->controller.'/ads_list')?>"><?php echo __('Advertisment List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$ads_data->id)?__('Edit'):__('Create').__('Advertisment')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-12">
     			<?php echo message_warning($this)?>
     	</div>
     	<div class="col-6">
     		<div class="card">
			      <div class="card-header"><?php echo (@$ads_data->id)?__('Edit'):__('Create').__('Advertisment')?></div>
			      <div class="card-body">
			       	<?php echo form_open('',array('name'=>'create-ads-form'))?>
			          <div class="form-group">
			            <label for=""><?php echo __('Advertisment Name')?> : </label>	    
			            <?php echo form_input(array('name'=>'name','value'=>@$ads_data->name,'class'=>'form-control'))?>
			          </div>
			          <div class="form-group">
			            <label for=""><?php echo __('Advertisment Description')?> : </label>
			            <?php echo form_textarea(array('name'=>'description','value'=>@$ads_data->description,'rows'=>2,'class'=>'form-control'))?>
			          </div>

			          <div class="form-group">
			            <label for=""><?php echo __('Duration')?> : </label>	    
			            <?php echo form_input(array('name'=>'duration','value'=>@$duration,'class'=>'form-control'))?>
			          </div>

			          <div class="form-group">
			          	<label for=""><?php echo __('Status')?> : </label>
			          	<?php echo form_dropdown('active',array('0'=>__('Unactive','backend/default'),'1'=>__('Active','backend/default')),@$ads_data->active,'class="form-control"')?>
			          </div>

			       
			          
			          <?php echo form_button(array('type'=>'submit','content'=>__('Submit','backend/default'),'class'=>'btn btn-primary pull-right'))?>
			          <!-- <a class="btn btn-primary btn-block" href="index.html">Login</a> -->
			        <?php echo form_close();?>
			        
			      </div>
    		</div>
     	</div>
     </div>