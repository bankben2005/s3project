<ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo base_url('backend/admin_dashboard')?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text"><?php echo __('Dashboard')?></span>
          </a>
        </li>
        <!-- <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="charts.html">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Charts</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="tables.html">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Tables</span>
          </a>
        </li> -->
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-desktop"></i>
            <span class="nav-link-text"><?php echo __('Channel Management')?></span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="<?php echo base_url('backend/admin_channel/channel_list')?>"><?php echo __('Channels')?></a>
            </li>
            <li style="display: none;">
              <a href="<?php echo base_url('backend/admin_channel/channel_categories_list')?>"><?php echo __('Channel Categories')?></a>
            </li>

            <li>
              <a href="<?php echo base_url('backend/admin_channel/channel_ordinal_list')?>">
                <?php echo __('Channel Ordinal')?>
              </a>
            </li>
          </ul>
        </li>


        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="<?php echo base_url('backend/admin_youtube')?>">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text"><?php echo __('Youtube categories management')?></span>
          </a>
        </li>

        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseAPI" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-desktop"></i>
            <span class="nav-link-text"><?php echo __('API Management')?></span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseAPI">
            <li>
              <a href="<?php echo base_url('backend/admin_api/youtube_api')?>"><?php echo __('Youtube API')?></a>

              <a href="<?php echo base_url('backend/admin_api/youtube_api_unactive')?>"><?php echo __('Youtube API Unactive')?></a>
            
              <a href="<?php echo base_url('backend/admin_channel/video_download_api')?>"><?php echo __('Video Download API')?></a>
            </li>
            
          </ul>
        </li>






        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion" style="display: none;">
            <i class="fa fa-fw fa-bullhorn"></i>
            <span class="nav-link-text"><?php echo __('Advertisment Management')?></span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="<?php echo base_url('backend/admin_advertisment/ads_list')?>"><?php echo __('Advertisment')?></a>
            </li>
            <li>
              <a href="<?php echo base_url('backend/admin_advertisment/ads_zone_list')?>"><?php echo __('Advertisment Zone')?></a>
            </li>
            <li>
              <a href="<?php echo base_url('backend/admin_advertisment/ads_banner_list')?>"><?php echo __('Advertisment Banner')?></a>
            </li>
            <!-- <li>
              <a href="blank.html">Blank Page</a>
            </li> -->
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels" style="display: none;">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-sitemap"></i>
            <span class="nav-link-text">Menu Levels</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="#">Second Level Item</a>
            </li>
            <li>
              <a href="#">Second Level Item</a>
            </li>
            <li>
              <a href="#">Second Level Item</a>
            </li>
            <li>
              <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Third Level</a>
              <ul class="sidenav-third-level collapse" id="collapseMulti2">
                <li>
                  <a href="#">Third Level Item</a>
                </li>
                <li>
                  <a href="#">Third Level Item</a>
                </li>
                <li>
                  <a href="#">Third Level Item</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link" style="display: none;">
          <a class="nav-link" href="#">
            <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Link</span>
          </a>
        </li>
      </ul>