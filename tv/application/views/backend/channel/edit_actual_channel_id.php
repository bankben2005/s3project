    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        <?php 
          if(isset($vdoData->parent_id) && isset($vdoData->channel_id)) {
        ?>
          <a href="<?php echo base_url('backend/admin_channel/video_download_api/' . $vdoData->parent_id . '/' . $vdoData->channel_id)?>"><?php echo __('Video Download API')?></a>
          <?php } else {?>
             <a href="<?php echo base_url('backend/admin_channel/video_download_api')?>"><?php echo __('Video Download API')?></a>
          <?php }?>
        </li>
        <li class="breadcrumb-item active"><?php echo __('EditDownload').__('Channel')?></li>
      </ol>
      <?php
     
      ?>
     <!-- Eof Breadcrumbs-->

     <div class="row">
      <div class="col-12">
          <?php echo message_warning($this)?>
      </div>
        
        <div class="col-12">
              <?php echo form_open_multipart('',array('name'=>'edit_actual_channel_form'))?>
                  <div class="row">
                    <div class="col-lg-6">
                    
                      <div class="form-group">
                            <label><?php echo __('Channel Name')?> :</label>
                            <?php echo form_input(array('name'=>'channel_name','value' => isset($vdoData->channel_name) ? $vdoData->channel_name : "",'class'=>'form-control','required'=>'required'))?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Channel Description')?> :</label>
                            <?php echo form_textarea(array('name'=>'channel_desc','value' => isset($vdoData->channel_desc) ? $vdoData->channel_desc : "" ,'class'=>'form-control','rows'=>2))?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Channel URL')?> :</label>
                            <?php echo form_textarea(array('name'=>'channel_url','value' => isset($vdoData->channel_url) ? $vdoData->channel_url : "" ,'class'=>'form-control','rows'=>2))?>
                        </div>

                        <div class="form-group">
                            <label><?php echo __('Channel Category')?> :</label>
                            <?php echo form_dropdown('channel_category',@$select_category, isset($vdoData->channel_category) ? $vdoData->channel_category : "",'class="form-control" onchange="changeSelectCategory(this)"')?>
                        </div>

                    <?php
                    // echo '<PRE>';
                    // print_r($data);
                    ?>
                      

                        <div class="form-group">
                          <?php //echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
                          <?php echo form_button(array(
                            'type'=>'submit',
                            'class'=>'btn btn-success pull-right',
                            'content'=>__('Submit','backend/default')
                          ))?>
                        </div>
                    </div>
                    <div class="col-lg-6">

                    </div>

                    <div class="col-12 mb-3">
                      
                    </div>

                  </div>


              <?php echo form_close()?>
        </div>
    </div>