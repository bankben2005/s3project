    <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('backend/admin_channel/channel_list')?>"><?php echo __('Channel List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$channel_data->id)?__('Edit'):__('Create').__('Channel')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
      <div class="col-12">
          <?php echo message_warning($this)?>
      </div>
        
        <div class="col-12">
          
        <?php echo form_open_multipart('',array('name'=>'create-channel-form'))?>
        <?php echo form_input(array(
          'type'=>'hidden',
          'name'=>'channel_id',
          'value'=>@$channel_data->id
        ))?>
        <div class="row">
      <div class="col-6">
        <div class="card">
            <!-- <center><img src="<?php echo getTVChannelLogo($channel_data->logo)?>" style="width:80px;height: 80px;"></center> -->
            <div class="card-header"><?php echo (@$channel_data->id)?__('Edit'):__('Create').__('Channel')?></div>
            <div class="card-body">

                            <div class="form-group">
                              <?php //print_r($channel_data)?>
                                <label><?php echo __('Channel Logo')?> : gif|jpg|png</label>
                                <div class="clearfix"></div>
                                    <?php if($channel_data->logo && file_exists('uploaded/tv/logo/'.$channel_data->logo)){?>
                                                <?php //echo 'aaaa';?>
                                        <div class="view view-first" style="margin:5px 0px;">
                                                            <img src="<?php echo base_url().'uploaded/tv/logo/'.$channel_data->logo;?>" style="width:100%;height: 100%;">
                                                             <div class="mask">
                                                              <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                                 <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('backend/'.$this->controller.'/deleteChannelLogo/'.$channel_data->id);?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                             </div>
                                        </div>
                                           
                                                
                                  <?php }?>
                                            
                                <div class="clearfix"></div>
                                            
                                <div class="input-group">
                                                <span class="input-group-btn">
                                                 <span class="btn btn-primary btn-file">
                                                         <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('channel_logo', '', '');?>
                                                     </span>
                                                </span>
                                                 <input type="text" class="form-control" readonly>
                                </div>
                        </div>
                        <div class="form-group">
                            <!-- <label><?php ?></label> -->
                            <label><?php echo __('Channel Name')?> :</label>
                            <?php echo form_input(array('name'=>'name','value'=>@$channel_data->name,'class'=>'form-control'))?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Channel Description')?> :</label>
                            <?php echo form_textarea(array('name'=>'description','value'=>@$channel_data->description,'class'=>'form-control','rows'=>2))?>
                        </div>

                        <div class="form-group">
                            <label><strong><?php echo __('Band Type')?> : </strong></label>
                            <?php echo form_dropdown('band_type',array(
                              'KU'=>__('KU-Band'),
                              'C'=>__('C-Band'),
                              'ALL'=>__('ALL')
                            ),@$channel_data->band_type,'class="form-control"')?>
                        </div>

                        <?php //print_r($this->account_data);?>
                        <?php if(in_array($this->account_data['data']['email'], ['kridsada@psisat.com','staff@psisat.com'])){?>
                        
                        <!-- <div class="form-group">
                            <label><strong><?php echo __('MV Request Streaming')?> : </strong></label>
                            <?php echo form_input(array(
                              'name'=>'mv_request_streaming',
                              'value'=>@$channel_data->mv_request_streaming,
                              'class'=>'form-control'
                            ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('MV Streaming Temp')?> : </strong></label>
                          <?php echo form_textarea(array(
                            'name'=>'mv_streaming_temp',
                            'value'=>@$channel_data->mv_streaming_temp,
                            'class'=>'form-control',
                            'rows'=>3
                          ))?>
                        </div> -->

                        <div class="form-group">
                          <label><strong><?php echo __('Direct streaming link')?> : </strong></label>
                          <?php echo form_input([
                            'name'=>'direct_streaming_link',
                            'class'=>'form-control',
                            'value'=>@$channel_data->direct_streaming_link
                          ])?>
                        </div>


                        <?php }?>

                     

                      <?php //print_r($internet_tv_status)?>
                        <div class="form-group">
                          <label><strong><?php echo __('Internet tv status')?> : </strong></label>
                          <?php echo form_dropdown('internet_tv_status',array(
                            '0'=>__('Unactive'),
                            '1'=>__('Active')
                          ),@$internet_tv_status,'class="form-control"')?>
                        </div>


                        <div class="form-group">
                          <label><strong><?php echo __('Show channel list')?> : </strong></label>
                          <?php echo form_dropdown('only_internet_tv',[
                            '0'=>__('Show on satellite and internet tv'),
                            '1'=>__('Show only internet tv')
                          ],@$channel_data->only_internet_tv,'class="form-control"')?>
                        </div>

                        <div class="form-group" style="display: none;">
                          <label><strong><?php echo __('Channel ASCII Code')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'channel_ascii_code',
                            'class'=>'form-control',
                            'value'=>@$channel_data->channel_ascii_code
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><?php echo __('Status','backend/default')?> : </label>
                          <?php echo form_dropdown('active',array('0'=>__('Unactive','backend/default'),'1'=>__('Active','backend/default')),@$channel_data->active,'class="form-control"')?>
                        </div>
                  </div>
            </div>
        </div>
        <div class="col-6">

            <div class="form-group">
              <label><strong><?php echo __('Map update channel (O5)')?> : </strong></label>

              <?php echo form_dropdown('ofive_map_channel',@$select_map_channel,@$channel_data->ofive_map_channel,'class="form-control"')?>
            </div>
            <div class="card">
                <div class="card-header"><?php echo __('Channel Categories')?></div>
                <div class="card-body">
                        <ul class="list-group">
                          <?php foreach($channel_categories as $key => $value){?>
                            <li class="list-group-item"><?php echo $value->name;?>
                                
                                <?php echo form_checkbox(array('name'=>'check_cat[]','value'=>$value->id,'class'=>'pull-right','checked'=>(in_array($value->id, $channel_categories_list))?TRUE:FALSE))?>
                            </li>
                          <?php }?>
                        </ul>
                </div>
            </div>
        </div>
        <div class="col-12">
            <?php echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
        </div>
        </div>
        <?php echo form_close();?>
        </div>
        
    </div>


    <!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo __('Image Preview')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <img src="" id="imagepreview" class="img-fluid" >
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>