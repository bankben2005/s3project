    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('backend/admin_channel/video_download_api')?>"><?php echo __('Video Download API')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('CreateDownload').__('Link')?></li>
      </ol>
      <?php
      
      ?>
     <!-- Eof Breadcrumbs-->

     <div class="row">
      <div class="col-12">
          <?php echo message_warning($this)?>
      </div>
        
        <div class="col-12">
              <?php echo form_open_multipart('',array('name'=>'create_download_form'))?>
                  <div class="row">
                    <div class="col-lg-6">
                    
                      <div class="form-group">
                            <label><?php echo __('URL')?> :</label>
                            <?php echo form_input(array('name'=>'links','value' => "",'class'=>'form-control','required'=>'required'))?>
                        </div>
                        <div class="form-group">
                          <label><?php echo __('Status','backend/default')?> : </label>
                          <?php echo form_dropdown('active',array(
                            
                            '1'=>__('ตั้งค่าเป็น Default Link','backend/default'),
                            '0'=>__('Unactive','backend/default')
                          ),'','class="form-control"')?>
                        </div>

                    <?php
                    // echo '<PRE>';
                    // print_r($data);
                    ?>
                      

                        <div class="form-group">
                          <?php //echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
                          <?php echo form_button(array(
                            'type'=>'submit',
                            'class'=>'btn btn-success pull-right',
                            'content'=>__('Submit','backend/default')
                          ))?>
                        </div>
                    </div>
                    <div class="col-lg-6">

                    </div>

                    <div class="col-12 mb-3">
                      
                    </div>

                  </div>


              <?php echo form_close()?>
        </div>
    </div>