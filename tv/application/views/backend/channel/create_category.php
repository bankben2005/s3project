	  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url('backend/admin_channel/channel_categories_list')?>"><?php echo __('Categories List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$category_data->id)?__('Edit'):__('Create').__('Channel Category')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-12">
     			<?php echo message_warning($this)?>
     	</div>
     	<div class="col-6">
     		<div class="card">
			      <div class="card-header"><?php echo (@$category_data->id)?__('Edit'):__('Create').__('Channel Category')?></div>
			      <div class="card-body">
			       	<?php echo form_open('',array('name'=>'create-cat-form'))?>
			          <div class="form-group">
			            <label for=""><?php echo __('Category Name')?> : </label>
			            <!-- <input class="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email"> -->
			            <?php echo form_input(array('name'=>'name','value'=>@$category_data->name,'class'=>'form-control'))?>
			          </div>
			          <div class="form-group">
			            <label for=""><?php echo __('Category Description')?> : </label>
			            <!-- <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Password"> -->
			            <?php echo form_textarea(array('name'=>'description','value'=>@$category_data->description,'rows'=>2,'class'=>'form-control'))?>
			          </div>
			          
			          <?php echo form_button(array('type'=>'submit','content'=>__('Submit','backend/default'),'class'=>'btn btn-primary pull-right'))?>
			          <!-- <a class="btn btn-primary btn-block" href="index.html">Login</a> -->
			        <?php echo form_close();?>
			        
			      </div>
    		</div>
     	</div>
     </div>