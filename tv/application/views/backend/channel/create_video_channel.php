    <!-- Breadcrumbs-->
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('backend/admin_channel/video_download_api')?>"><?php echo __('Video Download API')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('CreateDownload').__('Channel')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
      <div class="col-12">
          <?php echo message_warning($this)?>
      </div>
        
        <div class="col-12">
              <?php echo form_open_multipart('',array('name'=>'import_channel_form'))?>
                  <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <label><strong><?php echo __('Import Channel File (.json)')?> : </strong></label>
                        <div class="input-group">
                            <span class="input-group-btn">
                                <span class="btn btn-primary btn-file">
                                        <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('channel_file','','');?>
                                    </span>
                            </span>
                            <input type="text" class="form-control" readonly>
                        </div>
                        
                      </div>

                      <div class="form-group">
                            <label><?php echo __('Alias Name')?> :</label>
                            <?php echo form_input(array('name'=>'alias_name','class'=>'form-control','required'=>'required'))?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Description')?> :</label>
                            <?php echo form_textarea(array('name'=>'description','class'=>'form-control','rows'=>2))?>
                        </div>

                   
                        <div class="form-group">
                          <label><?php echo __('Status','backend/default')?> : </label>
                          <?php echo form_dropdown('active',array(
                            
                            '1'=>__('Active','backend/default'),
                            '0'=>__('Unactive','backend/default')
                          ),'','class="form-control"')?>
                        </div>

                        <div class="form-group">
                          <?php //echo form_submit('_submit',__('Submit','backend/default'),'class="btn btn-success pull-right"')?>
                          <?php echo form_button(array(
                            'type'=>'submit',
                            'class'=>'btn btn-success pull-right',
                            'content'=>__('Submit','backend/default')
                          ))?>
                        </div>
                    </div>
                    <div class="col-lg-6">

                    </div>

                    <div class="col-12 mb-3">
                      
                    </div>

                  </div>


              <?php echo form_close()?>
        </div>
    </div>