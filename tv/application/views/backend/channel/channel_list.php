<style type="text/css">
	.modal-dialog  {width:900px;}

</style>
  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Channel List')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
        <div class="col-12">
          	<!-- Example DataTables Card-->
		      <div class="card mb-3">
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('Channel List')?>

		          <a href="<?php echo base_url('backend/'.$this->controller.'/createChannel')?>" class="btn btn-success pull-right"><?php echo __('Create Channel')?></a>
		      	</div>
		        <div class="card-body">
		          <div class="table-responsive">
		            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		              <thead>
		                <tr>
		                  <th><?php echo __('Logo')?></th>
		                  <th><?php echo __('Name')?></th>
		                  <th><?php echo __('Country')?></th>
		                  <th><?php echo __('Band Type')?></th>
		                  <!-- <th><?php echo __('Channel ascii code')?></th> -->
		                  <th><?php echo __('Status')?></th>
		                  <th></th>
		                </tr>
		              </thead>
		              <tbody>
		              		<?php foreach($channels as $key => $row){?>
		              			<tr>
		              				<td>
		              					<img src="<?php echo getTVChannelLogo($row->logo)?>" style="width:50px;height: 50px;">
		              				</td>
		              				<td><?php echo $row->name;?></td>
		              				<td><?php echo $row->country_iso2?></td>
		              				<td>
		              					
		              					<span class="badge badge-info"><?php echo $row->band_type?></span>
		              				</td>
		              				<td>
		              					<?php if($row->active){?>
		              						<span class="badge badge-success"><?php echo __('Active','backend/default')?></span>
		              					<?php }else{?>
		              						<span class="badge badge-danger"><?php echo __('Unactive','backend/default')?></span>
		              					<?php }?>
		              				</td>
		              				
		              				<td>
		              					<!-- <a href="<?php echo base_url('backend/'.$this->controller.'/editChannel/'.$row->id)?>" class="btn btn-primary btn-sm" onclick="editChannel(this)"><i class="fa fa-eye"></i></a> -->
		              					<!-- <a href="javascript:void(0);" data-id="<?php echo $row->id?>" data-toggle="modal" data-target="#channelDetailModal" class="btn-view btn btn-secondary btn-sm">
		              						<i class="fa fa-eye"></i>
		              					</a> -->
		              					<?php 
		              					if($this->account_data['data']['email'] == 'kridsada@psisat.com'){
		              					?>

		              					<a href="<?php echo base_url('backend/'.$this->controller.'/editChannelComponent/'.$row->id)?>" class="btn btn-primary btn-sm"><i class="fa fa-align-justify"></i> Channel Components</a>
		              					<?php }?>
		              					<a href="<?php echo base_url('backend/'.$this->controller.'/editChannel/'.$row->id)?>" class="btn btn-info btn-sm"><i class="fa fa-pencil"></i></a>
		              					<!-- <a href="javascript:void(0);" onclick="if(confirm('ต้องการลบใช่หรือไม่') == true){window.location.href='<?php echo base_url('backend/'.$this->controller.'/deleteChannel/'.$row->id);?>'}"  class="btn btn-danger btn-sm">
		              						<i class="fa fa-trash"></i>
		              					</a> -->
		              				</td>
		              			</tr>

		              		<?php }?>
		              </tbody>

		            </table>
		           </div>
		        </div>
		      </div>

        </div>
      </div>

      <div class="modal fade" id="channelDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">--</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
          		
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
            <!-- <a class="btn btn-primary" href="<?php echo base_url('Welcome/logout')?>"><?php echo __('OK','backend/default')?></a> -->
          </div>
        </div>
      </div>
    </div>