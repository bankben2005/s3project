<style type="text/css">
	.modal-dialog  {width:900px;}

	.table > tbody > tr{
		cursor: move;
	}

</style>
  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Channel Ordinal List')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->


     <div class="row">
     	<div class="col-lg-12">
     		<?php echo message_warning($this)?>
     	</div>
     </div>
     <div class="row">
        <div class="col-12">
          	<!-- Example DataTables Card-->
		      <div class="card mb-3">
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('Channel List')?>

		          <!-- <a href="<?php echo base_url('backend/'.$this->controller.'/createChannel')?>" class="btn btn-success pull-right"><?php echo __('Create Channel')?></a> -->
		      	</div>
		        <div class="card-body">
		          <?php echo form_open('',array('name'=>'channel-ordinal-list','onsubmit'=>'submitOridinalList(event)'))?>
		          <div class="table-responsive">
		            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		              <thead>
		                <tr>
		                  <th><?php echo __('Logo')?></th>
		                  <th><?php echo __('Name')?></th>
		                  <th><?php echo __('C-Band Ordinal')?></th>
		                  <th><?php echo __('KU-Band Ordinal')?></th>
		                  <th><?php echo __('Mobile App Ordinal')?></th>
		                  <th><?php echo __('Band Type')?></th>
		                  <th><?php echo __('Status')?></th>
		                  <th></th>
		                  
		                </tr>
		              </thead>
		              <tbody>
		              	<?php foreach($channels as $key => $row){?>
		              			<tr>
		              				<td>
		              					<img src="<?php echo getTVChannelLogo($row->logo)?>" style="width:50px;height: 50px;">
		              				</td>
		              				<td><a href="<?php echo base_url('backend/admin_channel/editChannel/'.$row->id)?>" target="_blank"><?php echo $row->name;?></a></td>
		              				<td>
		              					<?php echo form_input([
		              						'type'=>'hidden',
		              						'name'=>'channel_id[]',
		              						'value'=>$row->id
		              					])?>
		              					<?php echo form_input(array(
		              						'name'=>'cband_ordinal[]',
		              						'class'=>'form-control',
		              						'value'=>$row->cband_ordinal,
		              						'data-channelid'=>$row->id,
		              						'onkeypress'=>'validate(event)'
		              					))?>
		              				</td>
		              				<td>
		              					<?php echo form_input(array(
		              						'name'=>'kuband_ordinal[]',
		              						'class'=>'form-control',
		              						'value'=>$row->kuband_ordinal,
		              						'data-channelid'=>$row->id,
		              						'onkeypress'=>'validate(event)'
		              					))?>
		              				</td>
		              				<td>
		              					<?php echo form_input(array(
		              						'name'=>'mobile_app_ordinal[]',
		              						'class'=>'form-control',
		              						'value'=>$row->mobile_app_ordinal,
		              						'data-channelid'=>$row->id,
		              						'onkeypress'=>'validate(event)'
		              					))?>
		              				</td>
		              				<td>
		              					<span class="badge badge-info"><?php echo $row->band_type?></span>
		              				</td>
		              				<td>
		              					<?php if($row->active){?>
		              						<span class="badge badge-success"><?php echo __('Active','backend/default')?></span>
		              					<?php }else{?>
		              						<span class="badge badge-danger"><?php echo __('Unactive','backend/default')?></span>
		              					<?php }?>
		              				</td>
		              				<td>
		              					<?php echo form_input(array(
		              						'type'=>'hidden',
		              						'name'=>'hide_band_type[]',
		              						'value'=>$row->band_type
		              					))?>
		              				</td>
		              			</tr>
		              	<?php }?>

		              </tbody>
		            </table>
		          </div>
		          <div class="row mt-3">
		          	<div class="col-lg-12">
		          		<div class="form-group">
		          			<?php echo form_button(array(
		          				'type'=>'submit',
		          				'class'=>'btn btn-success float-right',
		          				'content'=>__('Save','backend/default')
		          			))?>
		          		</div>
		          	</div>
		          </div>
		          <?php echo form_close()?>
		        </div>
		    </div>
		</div>
	</div>