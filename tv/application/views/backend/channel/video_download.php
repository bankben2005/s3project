
<style type="text/css">
	.modal-dialog  {width:900px;}

	.table > tbody > tr{
		cursor: move;
	}

table td {
  word-wrap: break-word;
  max-width: 400px;
}
#example td {
  white-space:inherit;
}

#dataTable.table{
	/*width:auto;table-layout:fixed*/
}

.table_fixed{
	width:auto;
	table-layout:fixed;
}
/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 50px;
  height: 24px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 16px;
  width: 16px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

.modal {
    display:    none;
    position:   fixed;
    z-index:    1000;
    top:        0;
    left:       0;
    height:     100%;
    width:      100%;
    background: rgba( 255, 255, 255, .8 ) 
                url('http://i.stack.imgur.com/FhHRx.gif') 
                50% 50% 
                no-repeat;
}

/* When the body has the loading class, we turn
   the scrollbar off with overflow:hidden */
body.loading .modal {
    overflow: hidden;   
}

/* Anytime the body has the loading class, our
   modal element will be visible */
body.loading .modal {
    display: block;
}
</style>
<?php 
	// echo '<PRE>';
	// print_r($select_category);
	// exit();
	?>
  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Video Download API')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->


	 <div class="row">
        <div class="col-12">
			
          	<!-- Example DataTables Card-->
		      <div class="card mb-3">

            <!-- insert download link -->    
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('จัดการลิงค์ภายนอก')?> <a href="<?php echo base_url('backend/'.$this->controller.'/create_download_link')?>" class="btn btn-success pull-right"><?php echo __('เพิ่มลิงค์ภายนอก')?></a><BR> 
            </div>


            <div class="card-body">
            <?php echo form_open('',array('name'=>'download-link','id'=>'download-link'))?>
		          <div class="table-responsive">
		            <table class="table table-bordered" id="dataTableLinkLog" width="100%" cellspacing="0">
		              <thead>
		                <tr>
                      <th width="5%"><?php echo __('ID')?></th>
		                  <th width="50%"><?php echo __('URL')?></th>
		                  <th width="30%"><?php echo __('Status')?></th>
		                  <th width="15%"></th>
		                  
		                </tr>
		              </thead>
		              <tbody id="outboundLink">
                  
		              </tbody>
		            </table>
		          </div>
		          <div class="row mt-3">
		          	<div class="col-lg-12">
		          		<div class="form-group">
		          			<?php echo form_button(array(
		          				'type'=>'Button',
		          				'class'=>'btn btn-success float-right',
								'content'=>__('Save','backend/default'),
								'onclick'=> 'submitOridinalList()'
		          			))?>
		          		</div>
		          	</div>
		          </div>
		          <?php  echo form_close()?>
		        </div>
            <!-- eof insert download link-->
          
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('จัดการลิงค์ภายในจากสตรีมไฟล์')?> <a href="<?php echo base_url('backend/'.$this->controller.'/create_video_channel')?>" class="btn btn-success pull-right"><?php echo __('เพิ่มสตรีมไฟล์')?></a><BR> 
				  <div class="card-body">
          
		          <div class="table-responsive">
		            <table class="table " id="dataTableListFile" width="100%" cellspacing="0">
		              
		              <tbody>
					  <?php
					//  echo '<PRE>';
					//  print_r($steam_files);
					//  exit(); 
					  ?>
		              		<?php foreach($steam_files as $keyObj => $fileObj){?>
		              			<tr>
		              				<td width="60%">
									  	<input type="radio"  class="groupOfStream" id="stream<?php echo $keyObj?>" data-sub="<?php echo $fileObj->file_name?>"" data-id="<?php echo $fileObj->id?>" name="groupOfStream" value="<?php echo $fileObj->file_name?>">
										<label for="stream<?php echo $keyObj?>"><?php echo $fileObj->file_name?></label>
									</td>
									<td  align="right" width="40%">
									<form role="form"  action="<?php echo base_url('backend/'.$this->controller.'/download_video_channel/'.$fileObj->id.'/')?>" method="post">
									
										<?php if($fileObj->active){?>
		              						<span class="badge badge-success"><?php echo __('Active','backend/default')?></span>
		              					<?php }else{?>
		              						<span class="badge badge-danger"><?php echo __('Unactive','backend/default')?></span>
		              					<?php }?>
										
										<a href="<?php echo base_url('backend/'.$this->controller.'/edit_video_channel/'.$fileObj->id.'/')?>" class="btn btn-info btn-sm "><i class="fa fa-pencil"></i></a>
										<a href="<?php echo base_url('backend/'.$this->controller.'/delete_video_channel/'.$fileObj->id.'/')?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
										<!-- <a href="#" onclick="download_video_channel(<?php echo $fileObj->id;?>)" class="btn btn-warning btn-sm" title="download json file"><i class="fa fa-download"></i></a>
									 -->
												<button class="btn btn-warning btn-sm"><i class="fa fa-download"></i></button>
   										 </form> 
									</td>
		              			</tr>

		              		<?php }?>
		              </tbody>

		            </table>
		           </div>
		        </div>         
		      	</div>

				  <div class="card-body" style="padding-bottom:10px !important;">
				 
					
					<!-- ปุ่ม sync -->
					<a id="syncChannel" onclick="ajaxsyncStreamChannel($('#syncChannel').attr('data-id'))" href="#" data-id="0" class="btn btn-danger pull-right ml-2"><?php echo __('เช็คลิงค์ต้นทางของช่อง')?></a>	
					<!-- eof ปุ่ม sync  -->
					
					<!-- ปุ่มเพิ่มช่องรายการ -->
					<a id="insertChannel" onclick="ajaxInsertStreamChannel($('#insertChannel').attr('data-id'))" href="#" data-id="0" class="btn btn-warning text-white pull-right ml-2"><?php echo __('เพิ่มช่องรายการ')?></a>	
					<!-- eof ปุ่มเพิ่มช่องรายการ  -->


					<?php echo form_button(array(
						'type'=>'Button',
						'class'=>'btn btn-success float-right',
						'content'=>__('Save','backend/default'),
						'onclick'=> 'submit_eventEditInBoundManageLink()',
						'data-parentid'=>0,
						'id' => 'eventEditInBoundManageLink'
					))?>
			

				  </div>
		        <div class="card-body">
				

				  <div class="table-responsive">
		            <table class="table table-bordered" id="dataTable" cellspacing="0">
		              <thead>
		                <tr>
		                  <!-- <th><?php echo __('ID')?></th> -->
		                  <th><?php echo __('ชื่อ')?></th>
		                  <th><?php echo __('URL')?></th>
                      	  <th><?php echo __('หมวดหมู่')?></th>
						  <th><?php echo __('สถานะของช่อง')?></th>
						  <th><?php echo __('คำอธิบายสถานะ')?></th>
						  <th id="orderCol"><?php echo __('การเรียงลำดับ')?></th>
						  <th></th>
		         
		                </tr>
		              </thead>
		              <tbody id="bodyData" >
		              	
		              </tbody>

		            </table>
		           </div>
		        </div>
		      </div>

        </div>
      </div>

	  <div class="row mt-1 mb-1">
			<div class="col-lg-12">
				<div class="form-group">
						<!-- ปุ่ม sync -->
					<a id="syncChannel" onclick="ajaxsyncStreamChannel($('#syncChannel').attr('data-id'))" href="#" data-id="0" class="btn btn-danger pull-right ml-2"><?php echo __('เช็คลิงค์ต้นทางของช่อง')?></a>	
					<!-- eof ปุ่ม sync  -->
					
					<!-- ปุ่มเพิ่มช่องรายการ -->
					<a id="insertChannel" onclick="ajaxInsertStreamChannel($('#insertChannel').attr('data-id'))" href="#" data-id="0" class="btn btn-warning text-white pull-right ml-2"><?php echo __('เพิ่มช่องรายการ')?></a>	
					<!-- eof ปุ่มเพิ่มช่องรายการ  -->



					<?php echo form_button(array(
						'type'=>'Button',
						'class'=>'btn btn-success float-right',
						'content'=>__('Save','backend/default'),
						'onclick'=> 'submit_eventEditInBoundManageLink()',
						'data-parentid'=>0,
						'id' => 'eventEditInBoundManageLink'
					))?>
				</div>
		
			</div>
	  </div>

      <div class="modal fade" id="channelDetailModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">--</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">
          		
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
            <!-- <a class="btn btn-primary" href="<?php echo base_url('Welcome/logout')?>"><?php echo __('OK','backend/default')?></a> -->
          </div>
        </div>
      </div>
    </div>
	<div class="modal" id="ajaxLoading"><!-- Place at bottom of page --></div>
	<?php
	$select_category = json_encode($select_category);
	?>						
	<script type= "text/javascript">
		var selectOpt = <?php echo $select_category?>;
	</script>