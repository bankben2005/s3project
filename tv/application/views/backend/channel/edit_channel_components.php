    <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('backend/admin_channel/channel_list')?>"><?php echo __('Channel List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Edit Channel Components')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
      <div class="col-12">
          <?php echo message_warning($this)?>
      </div>
        
        <div class="col-12">
          
              <?php echo form_open_multipart('',array('name'=>'edit-channel-components-form'))?>
              
              <div class="row">
                  <div class="col-12">
                    <div class="card">
                        <!-- <center><img src="<?php echo getTVChannelLogo($channel_data->logo)?>" style="width:80px;height: 80px;"></center> -->
                        <div class="card-header"><?php echo __('Edit Channel Components')?></div>
                        <div class="card-body">

                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                      <label><strong><?php echo __('Band Type')?> : </strong></label>
                                      <?php echo form_dropdown('band_type',array(
                                        'ALL'=>'ALL',
                                        'C'=>"C-band",
                                        'KU'=>'KU-band'
                                      ),@$channel_data->band_type,'class="form-control"')?>
                                    </div>
                                </div>
                                <div class="col-lg-8">

                                </div>
                            </div>

                            <div class="row">
                              <div class="col-lg-12">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                  <li class="nav-item">
                                    <a class="nav-link active" id="cband-tab" data-toggle="tab" href="#cband" role="tab" aria-controls="home" aria-selected="true">C-Band</a>
                                  </li>
                                  <li class="nav-item">
                                    <a class="nav-link" id="kuband-tab" data-toggle="tab" href="#kuband" role="tab" aria-controls="profile" aria-selected="false">KU-Band</a>
                                  </li>
                                  
                                </ul>
                                <div class="tab-content  mt-3" id="myTabContent">

                                  <!-- start CBAND TAB -->
                                  <div class="tab-pane fade show active" id="cband" role="tabpanel" aria-labelledby="cband-tab">
                                      <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                              <label><strong>FRQ : </strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'cband_frq',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>SYM : </strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'cband_sym',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>SERVICE ID</strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'cband_service_id',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">

                                            <div class="form-group">
                                              <label><strong>VDO PID</strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'cband_vdo_pid',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>ADO PID</strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'cband_ado_pid',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>POL</strong></label>
                                              <?php echo form_dropdown('cband_pol',array(
                                                'V'=>'V',
                                                'H'=>'H'
                                              ),'','class="form-control"')?>
                                            </div>
                                            

                                        </div>
                                        <div class="col-lg-4">
                                            

                                        </div>

                                      </div>

                                  </div>
                                  <!-- eof cband tab-->

                                  <!-- Start KU-band tab -->
                                  <div class="tab-pane fade" id="kuband" role="tabpanel" aria-labelledby="kuband-tab">
                                      <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                              <label><strong>FRQ : </strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'kuband_frq',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>SYM : </strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'kuband_sym',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>SERVICE ID</strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'kuband_service_id',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">

                                            <div class="form-group">
                                              <label><strong>VDO PID</strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'kuband_vdo_pid',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>ADO PID</strong></label>
                                              <?php echo form_input(array(
                                                'name'=>'kuband_ado_pid',
                                                'class'=>'form-control'
                                              ))?>
                                            </div>

                                            <div class="form-group">
                                              <label><strong>POL</strong></label>
                                              <?php echo form_dropdown('kuband_pol',array(
                                                'V'=>'V',
                                                'H'=>'H'
                                              ),'','class="form-control"')?>
                                            </div>
                                            

                                        </div>
                                        <div class="col-lg-4">
                                            

                                        </div>

                                      </div>
                                  </div>
                                  <!-- eof kuband tab-->
                                  
                                </div>

                              </div>
                            </div>

                        </div>
                    </div>
                  </div>
              </div>

              <?php echo form_close()?>
        </div>
    </div>