<style type="text/css">
	.modal-dialog  {width:900px;}
	small[data-bv-result="INVALID"]{
		color:red;
	}
</style>
  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url('backend/'.$this->controller);?>"><?php echo __('Category List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$youtube_category->id)?__('Edit category'):__('Create category')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-lg-12">
     		<?php echo message_warning($this)?>
     	</div>
     </div>
     <div class="row">
        <div class="col-12">
          	<!-- Example DataTables Card-->
		      <div class="card mb-3">
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo (@$youtube_category->id)?__('Edit category'):__('Create category')?>
		      	</div>
		        <div class="card-body">
		        	<?php echo form_open_multipart('',array('name'=>'create-youtube-category'))?>
		        		<div class="row">
		        			<div class="col-lg-4">
		        				<div class="form-group">
		        					<label><strong><?php echo __('Category Name')?> : </strong></label>
		        					<?php echo form_input(array(
		        						'name'=>'name',
		        						'class'=>'form-control',
		        						'value'=>@$youtube_category->name
		        					))?>
		        				</div>

		        				<div class="form-group">
		        					<label><strong><?php echo __('Category Description')?> : </strong></label>
		        					<?php echo form_textarea(array(
		        						'name'=>'description',
		        						'class'=>'form-control',
		        						'value'=>@$youtube_category->description,
		        						'rows'=>4
		        					))?>
		        				</div>

		        				<div class="form-group">
		        					<label><strong><?php echo __('Search Keyword')?> : </strong></label>
		        					<?php echo form_input(array(
		        						'name'=>'search_keyword',
		        						'class'=>'form-control',
		        						'value'=>@$youtube_category->search_keyword
		        					))?>
		        				</div>

		        				<div class="form-group">
		        					<label><strong><?php echo __('Ordinal')?> : </strong></label>
		        					<?php echo form_input(array(
		        						'name'=>'ordinal',
		        						'class'=>'form-control',
		        						'value'=>@$youtube_category->ordinal
		        					))?>
		        				</div>

		        				<div class="form-group">
		        					<label><strong><?php echo __('Status')?> : </strong></label>
		        					<?php echo form_dropdown('active',array(
		        						'1'=>__('Active','default'),
		        						'0'=>__('Unactive','default')
		        					),@$youtube_category->active,'class="form-control"')?>
		        				</div>

		        				
		        			</div>
		        			<div class="col-lg-4">

		        				<div class="form-group">
			                        <label><?php echo __('Cover Image')?> : gif|jpg|png</label>
			                        <div class="clearfix"></div>
			                            <?php if(@$youtube_category->cover_image && file_exists('uploaded/youtube_category/'.$youtube_category->id.'/'.$youtube_category->cover_image)){?>
			                                        
			                                <div class="view view-first" style="margin:5px 0px;">
			                                                    <img src="<?php echo base_url().'uploaded/youtube_category/'.$youtube_category->id.'/'.$youtube_category->cover_image;?>" style="width:100%;height: 100%;">
			                                                     <div class="mask">
			                                                      <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
			                                                         <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('backend/').$this->controller.'/delete_image/'.$youtube_category->id;?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
			                                                     </div>
			                                </div>
			                                   
			                                        
			                          <?php }?>
			                                    
			                        <div class="clearfix"></div>
			                                    
			                        <div class="input-group">
			                                        <span class="input-group-btn">
			                                         <span class="btn btn-primary btn-file">
			                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('cover_image', '', '');?>
			                                             </span>
			                                        </span>
			                                         <input type="text" class="form-control" readonly>
			                        </div>
			                </div>

		        			</div>
		        			<div class="col-lg-4">

		        			</div>
		        		</div>

		        		<div class="row">
		        			<div class="col-lg-8">
		        				<div class="form-group">
		        					<?php echo form_button(array(
		        						'type'=>'submit',
		        						'class'=>'btn btn-success float-right',
		        						'content'=>__('Submit','default')
		        					))?>
		        				</div>

		        			</div>
		        			<div class="col-lg-4">

		        			</div>
		        		</div>	

		        	<?php echo form_close()?>
		        </div>
		      </div>
		</div>
	</div>


	<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo __('Image Preview')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <img src="" id="imagepreview" class="img-fluid" >
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>