<style type="text/css">
	.modal-dialog  {width:900px;}

</style>
  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Category List')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
        <div class="col-12">
          	<!-- Example DataTables Card-->
		      <div class="card mb-3">
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('Category List')?>


		          <a href="<?php echo base_url('backend/'.$this->controller.'/createCategory')?>" class="btn btn-success pull-right"><?php echo __('Create Category')?></a>

		          <a href="javascript:void(0);" class="btn btn-info pull-right mr-1"><?php echo __('Edit ordinal')?></a>
		      	</div>
		        <div class="card-body">
		          <div class="table-responsive">
		            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		              <thead>
		                <tr>
		                  <th><?php echo __('Image')?></th>
		                  <th><?php echo __('Category Name')?></th>
		                  <th><?php echo __('Search Keyword')?></th>
		                  <th><?php echo __('Ordinal')?></th>
		                  <th><?php echo __('Updated')?></th>
		                  <th><?php echo __('Status')?></th>
		                  <th></th>
		                </tr>
		              </thead>
		              <tbody>
		              	<?php foreach($youtube_categories as $key => $row){?>
		              		<tr>
		              			<td><a href="javascript:void(0);" class="pop"><img src="<?php echo getYoutubeCategoryCoverImage($row)?>" width="50" height="50"></a></td>
		              			<td><?php echo $row->name?></td>
		              			<td><?php echo $row->search_keyword?></td>
		              			<td>
		              				<span class="show_ordinal"><?php echo $row->ordinal?></span>
		              			</td>
		              			<td><?php echo date('d/m/Y H:i:s',strtotime($row->updated))?></td>
		              			<td>
		              				<?php if($row->active){?>
		              					<span class="badge badge-success"><?php echo __('Active','default')?></span>
		              				<?php }else{?>
		              					<span class="badge badge-error"><?php echo __('Unactive','default')?></span>
		              				<?php }?>
		              			</td>
		              			<td>
		              				<a href="<?php echo base_url('backend/'.$this->controller.'/editCategory/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>

		              				<a href="javascript:void(0)" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>

		              			</td>

		              		</tr>

		              	<?php }?>
		              </tbody>
		             </table>
		          </div>
		        </div>
		    </div>
		</div>
	</div>

	<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo __('Image Preview')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <img src="" id="imagepreview" class="img-fluid" >
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>
