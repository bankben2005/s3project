	  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url('backend/admin_api/youtube_api')?>"><?php echo __('Youtube API List')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo (@$youtube_api_keys->id)?__('Edit'):__('Create').__('Youtube API')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-12">
     			<?php echo message_warning($this)?>
     	</div>
     	<div class="col-12">
     		<div class="card">
			      <div class="card-header"><?php echo (@$youtube_api_keys->id)?__('Edit'):__('Create').__('Youtube API')?></div>
			      <div class="card-body">
                    <?php echo form_open('',array('name'=>'create-youtube-api-form'))?>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label><strong><?php echo __('API KEYS')?> : </strong></label>
                                    <?php echo form_input(array(
                                        'name'=>'api_key',
                                        'class'=>'form-control',
                                        'value'=>@$youtube_api_keys->api_key,
                                        'required'=>'required'
                                    ))?>
                                </div>

                                <div class="form-group">
                                    <label><strong><?php echo __('Gmail Account')?> : </strong></label>
                                    <?php echo form_input(array(
                                        'type'=>'email',
                                        'name'=>'gmail_account',
                                        'class'=>'form-control',
                                        'value'=>@$youtube_api_keys->gmail_account,
                                        'required'=>'required'
                                    ))?>
                                </div>

                                <div class="form-group">
                                    <label><strong><?php echo __('Gmail Account Password')?> : </strong></label>
                                    <?php echo form_input(array(
                                        'type'=>'password',
                                        'name'=>'gmail_password',
                                        'class'=>'form-control',
                                        'value'=>@$youtube_api_keys->gmail_password
                                    ))?>
                                </div>

                                <div class="form-group">
                                    <label><strong><?php echo __('Project Name')?> : </strong></label>
                                    <?php echo form_input(array(
                                        'name'=>'project_name',
                                        'class'=>'form-control',
                                        'value'=>@$youtube_api_keys->project_name
                                    ))?>
                                </div>

                                <div class="form-group">
                                    <label><strong><?php echo __('Status')?> : </strong></label>
                                    <?php echo form_dropdown('active',[
                                        '1'=>__('Active','default'),
                                        '0'=>__('Unactive','default')

                                    ],@$youtube_api_keys->active,'class="form-control"')?>
                                </div>

                                <div class="form-group">
                                    <?php echo form_button(array(
                                        'type'=>'submit',
                                        'class'=>'btn btn-success float-right',
                                        'content'=>__('Submit','default')
                                    ))?>
                                </div>

                            </div>
                            <div class="col-lg-4">

                            </div>
                        </div>
                    <?php echo form_close()?>

                  </div>

            </div>
        </div>
    </div>
