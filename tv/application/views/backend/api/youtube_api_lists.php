<style type="text/css">
	/*.modal-dialog  {width:900px;}*/
	.modal-dialog.modal-medium {
	    max-width: 800px;
	    margin: 30px auto;
	}
</style>
  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url();?>"><?php echo __('Dashboard','backend/default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Yotube API List')?></li>
      </ol>
     <!-- Eof Breadcrumbs-->

     <div class="row">
     	<div class="col-lg-12">
     		<?php echo message_warning($this)?>
     	</div>
     </div>

     <div class="row">
        <div class="col-12">
          	<!-- Example DataTables Card-->
		      <div class="card mb-3">
		        <div class="card-header">
		          <i class="fa fa-table"></i> <?php echo __('Yotube API List')?>



		          <a href="<?php echo base_url('backend/'.$this->controller.'/createYoutubeAPI')?>" class="btn btn-success pull-right"><?php echo __('Create Youtube API')?></a>

		          <a href="javascript:void(0)" data-toggle="modal" data-target="#createMultipleAPIKey" class="btn btn-success pull-right mr-3"><?php echo __('Create Multiple Youtube API')?>
		          	
		          	<a href="javascript:void(0)" class="btn btn-success float-right mr-3" data-toggle="modal" data-target="#importAPIKeyFromXLSXFile"><?php echo __('Import from xlsx file')?></a>
		          </a>
		      	</div>
		        <div class="card-body">

              <div class="row mb-3">
                <div class="col-lg-12" id="search-panel">
                  <div class="card">
                    <div class="card-header"><i class="fa fa-search"></i> </div>
                    <div class="card-body">
                      <?php echo form_open('',['name'=>'search-form'])?>
                        <div class="row">
                          <div class="col-lg-3">
                            <div class="form-group">
                              <?php echo form_input([
                                'name'=>'gmail_account',
                                'class'=>'form-control',
                                'value'=>@$search_criteria['gmail_account'],
                                'placeholder'=>__('Gmail account..')
                              ])?>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="form-group">
                              <?php echo form_dropdown('over_limit_status',[
                                ''=>__('Select Status'),
                                '0'=>__('Available'),
                                '1'=>__('Over Limited')
                              ],@$search_criteria['over_limit_status'],'class="form-control"')?>
                            </div>
                          </div>
                          <div class="col-lg-3">
                            <div class="form-group">

                            </div>
                          </div>
                          <div class="col-lg-3">


                            <div class="form-group text-right">
                              <?php if(isset($search_criteria)){?>
                              <a href="<?php echo base_url('backend/'.$this->controller.'/'.$this->method)?>" class="btn btn-secondary btn-sm"><i class="fa fa-close"></i> <?php echo __('Reset')?></a>
                            <?php }?>

                              <?php echo form_button([
                                'type'=>'submit',
                                'class'=>'btn btn-success btn-sm',
                                'content'=>'<i class="fa fa-search"></i> '.__('Search')
                              ])?>
                            </div>
                          </div>
                        </div>
                      <?php echo form_close()?>

                    </div>
                  </div>
                </div>
              </div>

              <div class="alert alert-success">
                <strong><?php echo __('TOTAL')?></strong> : <?php echo number_format($total_key)?> <?php echo __('keys')?>
              </div>
		          <div class="table-responsive">
		            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
		              <thead>
		                <tr>
		                  <th><?php echo __('API KEY')?></th>
		                  <th><?php echo __('Over limit status')?></th>
		                  <th><?php echo __('Gmail Account')?></th>
                      <th><?php echo __('Gmail Password')?></th>
		                  <!-- <th><?php echo __('Last updated by')?></th>
		                  <th><?php echo __('Last updated os')?></th> -->
		                  <th><?php echo __('Created')?></th>
		                  <th><?php echo __('Updated')?></th>
		                  <th><?php echo __('Status')?></th>
		                  <th></th>
		                </tr>
		              </thead>
		              <tbody>
		              	<?php foreach($youtube_api as $key => $row){?>
		              		<tr>
		              			<td><?php echo $row->api_key?></td>
		              			<td>
		              				<?php if($row->over_limit_status){?>
		              					<a href="javascript:void(0)" data-overlimit="1" data-apiid="<?php echo $row->id?>" onclick="changeYoutubeOverLimitStatus(this)"><span class="badge badge-danger"><?php echo __('Over Limited')?></span></a>
		              				<?php }else{?>
		              					<a href="javascript:void(0)" data-overlimit="0" onclick="changeYoutubeOverLimitStatus(this)" data-apiid="<?php echo $row->id?>"><span class="badge badge-success"><?php echo __('Available')?></span></a>
		              				<?php }?>
		              			</td>

		              			<td><?php echo $row->gmail_account?></td>
                        <td><?php echo $row->gmail_password?></td>
		              			<!-- <td><?php echo $row->last_updated_by?></td>
		              			<td><?php echo $row->last_updated_os?></td> -->
		              			<td><?php echo $row->created?></td>
		              			<td><?php echo $row->updated?></td>
		              			<td>
		              				<?php if($row->active){?>
		              					<span class="badge badge-success"><?php echo __('Active')?></span>
		              				<?php }else{?>
		              					<span class="badge badge-danger"><?php echo __('Unactive')?></span>
		              				<?php }?>
		              					
		              			</td>
		              			<td>
		              				<a href="https://www.googleapis.com/youtube/v3/videos?id=7lCDEYXw3mM&key=<?php echo $row->api_key?>&part=snippet,contentDetails,statistics,status" target="_blank" class="btn btn-info btn-sm"><i class="fa fa-link"></i> <?php echo __('TEST API')?></a>
		              				<a href="<?php echo base_url('backend/'.$this->controller.'/editYoutubeAPI/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>

		              				<?php if($this->account_data['data']['email'] == 'kridsada@psisat.com'){?>
		              				<a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('<?php echo __('Are you sure to delete?')?>') == true){window.location.href='<?php echo base_url("backend/".$this->controller."/deleteYoutubeAPI/".$row->id)?>'}"><i class="fa fa-trash"></i></a>
		              				<?php }?>
		              			</td>



		              		</tr>
		              	<?php }?>
		              </tbody>
		            </table>
                <div class="row">
                  <div class="col-lg-12">
                    <?php echo $pages?>
                  </div>
                </div>
		          </div>
		        </div>
		      </div>
		</div>
	</div>


<!-- Modal -->
<div class="modal fade" id="createMultipleAPIKey" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-medium" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Create Multiple API Key')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',['name'=>'create-multiple-key'])?>
      <div class="modal-body">
        
        	<div class="row">
        		<div class="col-lg-6">
        			<div class="form-group">
        				<label><strong><?php echo __('Gmail Account')?> : </strong></label>
        				<?php echo form_input([
        					'type'=>'email',
        					'class'=>'form-control',
        					'name'=>'gmail_account',
        					'required'=>'required'
        				])?>
        			</div>
        		</div>
        		<div class="col-lg-6">
        			<div class="form-group">
        				<label><strong><?php echo __('Gmail Password')?> : </strong></label>
        				<?php echo form_input([
        					'name'=>'gmail_password',
        					'class'=>'form-control'
        				])?>
        			</div>
        		</div>

        	</div>

        	<div class="row">
        		<div class="col-lg-12">
        			<a href="javascript:void(0)" class="btn btn-success" onclick="addKeyRows()"><i class="fa fa-plus"></i> <?php echo __('Add Key')?></a>
        		</div>
        	</div>

        	<div id="key_row" class="mt-2">

        	</div>

        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="importAPIKeyFromXLSXFile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Import API Key from xlsx file')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open_multipart(base_url('backend/'.$this->controller.'/importAPIKeyFromXLSXFile'),array('name'=>'import-excel-form'))?>
      <?php //echo form_open_multipart(base_url($this->controller.'/importJobByExcelFileTSB'),array('name'=>'import_excel-form'))?>
      <div class="modal-body">

            <center  class="mb-3"><a href="<?php echo base_url('uploaded/key_excel/example/example.xlsx')?>" target="_blank" class="text-success"><u><?php echo __('Example File')?></u></a></center>
        
            <!-- <input type="file" id="input_excel" name="import_excel"   class="file-loading" /> -->
            <?php echo form_input(array(
                'type'=>'file',
                'id'=>'input_excel',
                'name'=>'import_excel',
                'class'=>'file-loading'
            ))?>
       
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
        <?php echo form_button(array(
            'type'=>'button',
            'class'=>'btn btn-secondary',
            'data-dismiss'=>'modal',
            'content'=>__('Close','default')
        ))?>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
            'type'=>'submit',
            'class'=>'btn btn-primary',
            'content'=>__('Save','default')
        ))?>
      </div>
       <?php echo form_close()?>
    </div>
  </div>
</div>