<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_channel extends Backend_controller {
    private $http_status_codes = array(
        0 => '1) The Client cannot connect to the server in 60 second
              2) The Client cannot receive the response within the timeout period (60 second)
              3) The Request was "stopped(aborted)" by the Client.',
        100 => 'Informational: Continue',
        101 => 'Informational: Switching Protocols',
        102 => 'Informational: Processing',
        200 => 'Successful: OK',
        201 => 'Successful: Created',
        202 => 'Successful: Accepted',
        203 => 'Successful: Non-Authoritative Information',
        204 => 'Successful: No Content',
        205 => 'Successful: Reset Content',
        206 => 'Successful: Partial Content',
        207 => 'Successful: Multi-Status',
        208 => 'Successful: Already Reported',
        226 => 'Successful: IM Used',
        300 => 'Redirection: Multiple Choices',
        301 => 'Redirection: Moved Permanently',
        302 => 'Redirection: Found but url not available this time',
        303 => 'Redirection: See Other',
        304 => 'Redirection: Not Modified',
        305 => 'Redirection: Use Proxy',
        306 => 'Redirection: Switch Proxy',
        307 => 'Redirection: Temporary Redirect',
        308 => 'Redirection: Permanent Redirect',
        400 => 'Client Error: Bad Request',
        401 => 'Client Error: Unauthorized',
        402 => 'Client Error: Payment Required',
        403 => 'Client Error: Forbidden',
        404 => 'Client Error: Not Found',
        405 => 'Client Error: Method Not Allowed',
        406 => 'Client Error: Not Acceptable',
        407 => 'Client Error: Proxy Authentication Required',
        408 => 'Client Error: Request Timeout',
        409 => 'Client Error: Conflict',
        410 => 'Client Error: Gone',
        411 => 'Client Error: Length Required',
        412 => 'Client Error: Precondition Failed',
        413 => 'Client Error: Request Entity Too Large',
        414 => 'Client Error: Request-URI Too Long',
        415 => 'Client Error: Unsupported Media Type',
        416 => 'Client Error: Requested Range Not Satisfiable',
        417 => 'Client Error: Expectation Failed',
        418 => 'Client Error: I\'m a teapot',
        419 => 'Client Error: Authentication Timeout',
        420 => 'Client Error: Enhance Your Calm',
        420 => 'Client Error: Method Failure',
        422 => 'Client Error: Unprocessable Entity',
        423 => 'Client Error: Locked',
        424 => 'Client Error: Failed Dependency',
        424 => 'Client Error: Method Failure',
        425 => 'Client Error: Unordered Collection',
        426 => 'Client Error: Upgrade Required',
        428 => 'Client Error: Precondition Required',
        429 => 'Client Error: Too Many Requests',
        431 => 'Client Error: Request Header Fields Too Large',
        444 => 'Client Error: No Response',
        449 => 'Client Error: Retry With',
        450 => 'Client Error: Blocked by Windows Parental Controls',
        451 => 'Client Error: Redirect',
        451 => 'Client Error: Unavailable For Legal Reasons',
        494 => 'Client Error: Request Header Too Large',
        495 => 'Client Error: Cert Error',
        496 => 'Client Error: No Cert',
        497 => 'Client Error: HTTP to HTTPS',
        499 => 'Client Error: Client Closed Request',
        500 => 'Server Error: Internal Server Error',
        501 => 'Server Error: Not Implemented',
        502 => 'Server Error: Bad Gateway',
        503 => 'Server Error: Service Unavailable',
        504 => 'Server Error: Gateway Timeout',
        505 => 'Server Error: HTTP Version Not Supported',
        506 => 'Server Error: Variant Also Negotiates',
        507 => 'Server Error: Insufficient Storage',
        508 => 'Server Error: Loop Detected',
        509 => 'Server Error: Bandwidth Limit Exceeded',
        510 => 'Server Error: Not Extended',
        511 => 'Server Error: Network Authentication Required',
        598 => 'Server Error: Network read timeout error',
        599 => 'Server Error: Network connect timeout error',
    );
    
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
                        $this->load->helper(array(
                            'our'
                        ));
                    
                
    }

    public function index(){
    	$this->channel_list();
    }
    public function channel_list(){
        //echo 'aaaa';exit;
    	$this->loadDataTableCSS();
    	$this->loadDataTableJS();

    	$this->template->javascript->add(base_url('assets/js/backend/channel/channel_list.js'));


    	$data = array(
    		'channels' => $this->getAllChannels()
    	);

    	//print_r($data);exit;
    	$this->template->content->view('backend/channel/channel_list',$data);
        $this->template->publish();
    }
    public function create_video_channel(){

        $this->loadValidator();
        $this->loadImageUploadStyle();

        
        $this->template->javascript->add(base_url('assets/js/backend/channel/create_video_channel.js'));
        $this->loadImageUploadScript();

        if($this->input->post(NULL,FALSE)){
       
            $query = $this->db->insert('vdo_download_api',array(
            
                'alias_name'=>$this->input->post('alias_name'),
                'description'=>$this->input->post('description'),
                'active' => $this->input->post('active'),
                'created'=>date('Y-m-d H:i:s')

            ));

            if($query){
                $insert_id = $this->db->insert_id();

                $this->uploadDownloadChannelFiles(array(
                    'channel_id'=>$insert_id
                ));

                $txtSuccess = "Create channel success";
                $this->msg->add($txtSuccess,'success');
                redirect($this->uri->uri_string());


            }
        }
   
        $data = array(
            
        );

        $this->template->content->view('backend/channel/create_video_channel',$data);
        $this->template->publish();
    }

    /* delete video download json  */
    public function delete_video_channel($id=null){

        /* if recieve post data from frontend then delete it */
        if($id > 0){

            /* if recieve post data from frontend then delete it */
            $this->db->update('vdo_download_api',array(
                'deleted_at'=>date('Y-m-d H:i:s'),
            ),array('id'=>$id));  
            /* eof recieve post data from frontend then delete it */
        }
        /* eof recieve post data from frontend then delete it */
        
        $this->video_download_api();

    }
     /* download video download json  */
     public function download_video_channel($id=null){

        /* if recieve post data from frontend then delete it */
        if($id > 0){
            //echo $id;exit();
            /* if recieve post data from frontend then delete it */

            $queryFileContent = $this->db->select('*')->from('vdo_download_api')->where('id',$id)->where('deleted_at',null)->get();
            if($queryFileContent->num_rows() > 0){
                $this->load->helper('download');
                $results = $queryFileContent->result();
                $filename = $results[0]->filename;
                $pth    =   file_get_contents(base_url()."./assets/json/" . $filename);
                header('Content-Type: application/json');
                $pth = json_decode($pth);
                /* requirment:remove obj->channel_id before send  */
                if(isset($pth->Channels)){
                    if(!empty($pth->Channels)){
                        foreach($pth->Channels as $chK => $chV){
                            # code ..
                            unset($pth->Channels[$chK]->channel_id);
                        }
                    }
                }
                /* eof requirment:remove obj->channel_id before send*/
              
                $pth=  json_encode($pth,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);

                $nme    =  "stream_" . date('Ymd_H:i:s').'.json';
                force_download($nme, $pth);     
            }

            /* eof recieve post data from frontend then delete it */
        }
        /* eof recieve post data from frontend then delete it */
        
       // $this->video_download_api();

    }

    public function edit_video_channel($id=null){

        $this->loadValidator();
        $this->loadImageUploadStyle();

        
        $this->template->javascript->add(base_url('assets/js/backend/channel/edit_video_channel.js'));
        $this->loadImageUploadScript();

        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;
            $this->db->update('vdo_download_api',array(
                'alias_name'=>$this->input->post('alias_name'),
                'description'=>$this->input->post('description'),
                'active'=>$this->input->post('active'),
            ),array('id'=>$id));  

            $this->msg->add('Update success','success');
            redirect($this->uri->uri_string());
        }
        /* eof recieve post data from frontend then */

        /* get video data and send it to front */
        $vdoDownLoadData = array();
        $vdoDownLoadData = $this->getVdoDownloadData($id);
        $data = array(
            "vdoData" => count($vdoDownLoadData) > 0  ?  reset($vdoDownLoadData) : array()
        );
        /* eof video data and send it to front*/
        
        $this->template->content->view('backend/channel/edit_video_channel',$data);
        $this->template->publish();
    }

    public function create_download_link($id=null){
        $this->loadValidator();
        $this->loadImageUploadStyle();

        
        $this->template->javascript->add(base_url('assets/js/backend/channel/create_stream_channel.js'));
        $this->loadImageUploadScript();

        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            /* find default link  */
            $active = $this->input->post('active');
            $numRows = $this->findDefaultLink();
          
           // if($numRows == 0 ) {
                $query = $this->db->insert('download_history_log',array(
                    'links'=>$this->input->post('links'),
                    'active' => $this->input->post('active'),
                    'created'=>date('Y-m-d H:i:s'),
                    'updated'=>date('Y-m-d H:i:s')
                )); 
                $insert_id = $this->db->insert_id();

                # if set status is active update other to unactive
                if($active == 1){
                    $queryDownloadHisLog = $this->db->select('*')
                    ->from('download_history_log')
                    ->where('active',1)
                    ->where('id !=', $insert_id)
                    ->where('deleted_at',null)
                    ->order_by('id','asc')
                    ->get();
                 
                    if($queryDownloadHisLog->num_rows() > 0){
                        foreach ($queryDownloadHisLog->result() as $key => $value) {
                            # update other id set to 1
                            $update_id  = $value->id;
                            $this->db->update('download_history_log',array(
                                'active'=>0
                              ),array('id'=>$update_id));
                            }
                    }
                }

                $this->msg->add('Insert success','success');
                redirect($this->uri->uri_string());
            // }else{
            //     $this->msg->add("Default ลิงค์สามารถมีได้แค่ URL เดียว , กรุณาปิดการใช้งานลิงค์อื่นก่อน", 'error');
            //     redirect($this->uri->uri_string());
            // }

        }
        /* eof recieve post data from frontend then */

        /* get link data and send it to front */
        $data = array(
           
        );
        /* eof link data and send it to front*/
        
        $this->template->content->view('backend/channel/create_download_link',$data);
        $this->template->publish();
    }

    public function findDefaultLink(){
        $queryDefault = $this->db->select('*')->from('download_history_log')->where('active',1)->where('deleted_at',null)->get();
        $numRow  = $queryDefault->num_rows();
       
        if($numRow > 0){
            return $numRow;
        }else{
            return 0;
        }
    }

    public function create_stream_channel($id=null){

        $this->loadValidator();
        $this->loadImageUploadStyle();

        
        $this->template->javascript->add(base_url('assets/js/backend/channel/create_stream_channel.js'));
        $this->loadImageUploadScript();

        /* if recieve post data from frontend then */
        if($this->input->post(NULL,FALSE)){
            $stream_content ="";
            $parent_id = $id;
            $steam_files =  $this->getVdoDownloadData($parent_id);
            // echo '<PRE>';
            // print_r($parent_id);exit();
            $steam_files = $this->prepareStreamFile($steam_files);
            $stream_content = isset($steam_files[0]->content) ? $steam_files[0]->content : array();
            $channel_arr = array();

             /* query get file  */
             $parent_id = $id;
             $QueryGETFile =  $this->db->select('*')
             ->from('vdo_download_api')
             ->where('id',$parent_id)
             ->get();
             /* eof query get file */
             
            
         
             /* find actual channel id in json object */
             if(!empty($stream_content)){
                 $stream_obj = json_decode($stream_content);
                 $channel_arr = $stream_obj->Channels;
                // $found_key = count($channel_arr) > 0 ? count($channel_arr) - 1 : "";
               
                 $found_key = $this->calc_attribute_in_array($channel_arr, 'channel_id', 'max');

                 //rearrage stream obj
                 if($found_key != ""){
                    $next_channel_id = $found_key + 1 ;
                    
                    /* set total  */
                    $this->setStreamObjTotal($stream_obj,count($channel_arr) + 1);
                    /* push object to array */
                   
                    // insert new order
                    //* if frontend send dropdown value = SelectCategory then change it's to null
                    $SelectCategory = "";
                    if($this->input->post('channel_category') != "SelectCategory"){
                        $SelectCategory = $this->input->post('channel_category');
                    }
                    

                    $pushObj;
                    $pushObj->channel_id = $next_channel_id; 
                    $pushObj->channel_order = 1; 
                    $pushObj->channel_name  = $this->input->post('channel_name');
                    $pushObj->channel_category = $SelectCategory;
                    $pushObj->channel_description = $this->input->post('channel_desc');
                    $pushObj->channel_url = $this->input->post('channel_url');
                    //eof rearrange all channel_order if insert new one
                    array_push($stream_obj->Channels,$pushObj);

                    
                    //rearrage before create stream obj
                    $stream_obj->Channels = $this->reArrageChannelStreamByOrders($stream_obj->Channels, 'channel_order');
                    //eof rearrage  before create stream obj
                   
                    // eof insert new order
                    //rearrange all channel_order if insert new one
                    $countArrayObjForSetChOrder = count($channel_arr) + 1;
                    
                    
                    //start with channel_order 1 because the new one is 1
                    $count = 1;
                    //eof

                    // update all channel order 
                    $this->updateAllChannelOrder($stream_obj , $next_channel_id , $count ,$countArrayObjForSetChOrder , 'create');
                    
                    //eof update all channel order
                    //rearrage after create stream obj
                    $stream_obj->Channels = $this->reArrageChannelStreamByOrders($stream_obj->Channels, 'channel_order');
                   
                    
                    //eof rearrage  before create stream obj
                  
                    header('Content-Type: application/json');
                    $newStreamContent = json_encode($stream_obj,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                    $filename = $QueryGETFile->row()->filename;
                    file_put_contents('./assets/json/'.$filename, $newStreamContent );
    
                    $this->msg->add('Insert success','success');
                    redirect($this->uri->uri_string());
                 }
                 
                 /*set value */ 
                
                 
             }
             /* eof find  actual channel id in json object */
        }
        /* eof recieve post data from frontend then */

        /* get video data and send it to front */
        $vdoDownLoadData = array();
        $vdoDownLoadData = $this->getVdoDownloadData($id);
        $data = array(
            "vdoData" => count($vdoDownLoadData) > 0  ?  reset($vdoDownLoadData) : array()
        );

         /* select category */
         $data['select_category'] = $this->getSelectVdoDownloadCategory();
         /* eof select category */

        /* eof video data and send it to front*/
        
        $this->template->content->view('backend/channel/create_stream_channel',$data);
        $this->template->publish();
    }
    public  function calc_attribute_in_array($array, $prop, $func) {
        $result = array_map(function($o) use($prop) {
                                return $o->$prop;
                            },
                            $array);
    
        if(function_exists($func)) {
            return $func($result);
        }
        return false;
        
    }
    public function updateAllChannelOrder($stream_obj , $next_channel_id , $count , $countArrayObjForSetChOrder , $type){
          # update channel order of stream object
          # $type = create , update , delete 
       
          foreach($stream_obj->Channels as $chKey => $chVal){
            if($type == 'create' || $type == 'update'){
                if($stream_obj->Channels[$chKey]->channel_id != $next_channel_id){
                    ++ $count;
                    if($count <= $countArrayObjForSetChOrder){
                        $stream_obj->Channels[$chKey]->channel_order = $count;
                    }
                }
            }
            else{
              
                    ++ $count;
                    if($count <= $countArrayObjForSetChOrder){
                        $stream_obj->Channels[$chKey]->channel_order = $count;
                    }
            }
        }
    
        //eof update channel order of stream object
    }
    public function reArrageChannelStreamByOrders($arr, $col, $dir = SORT_ASC){
        /* เรียงลำดับ array จาก  channel_order */
        $sort_col = array();
        foreach ($arr as $key=> $row) {
            $row = (array)$row;
            $sort_col[$key]['channel_order'] = $row[$col];
            //convert to array
            $arr[$key] = (array)$arr[$key];  

        }
      
        array_multisort( array_column( $arr, 'channel_order' ), SORT_ASC, SORT_NUMERIC, $arr );
        //convert $arr[$key] = object
        foreach ($arr as $key=> $row) {
            $arr[$key] = (object)$arr[$key];
        }
        //eof convert $arr[$key] = object
        // start reset 
        $arr = $this->resetObjFromJsonFile($arr);
       
        //
        return $arr;
    }
    
    private function resetObjFromJsonFile($arr){
        //rearrage all object channel arr
         // array stream object should be send from $stream_obj->Channels
        foreach ($arr as $key=> $row) {
            
            $channel_id = $arr[$key]->channel_id;
            $channel_order = $arr[$key]->channel_order;
            $channel_name = $arr[$key]->channel_name;
            $channel_category = $arr[$key]->channel_category;
            $channel_description = $arr[$key]->channel_description;
            $channel_url = $arr[$key]->channel_url;
            $channel_sync_time =$arr[$key]->channel_sync_time;

            if(isset($arr[$key]->channel_sync_time)){
                $channel_sync_time =$arr[$key]->channel_sync_time;
            }
            if(isset($arr[$key]->channel_desc)){
                $channel_desc =$arr[$key]->channel_desc;
            }
            if(isset($arr[$key]->channel_status)){
                $channel_status =$arr[$key]->channel_status;
            }
            if(isset($arr[$key]->channel_code)){
                $channel_code =$arr[$key]->channel_code;
            }
            if(isset($arr[$key]->channel_codeinfo)){
                $channel_codeinfo =$arr[$key]->channel_codeinfo;
            }
           
            unset($arr[$key]->channel_id);
            unset($arr[$key]->channel_order);
            unset($arr[$key]->channel_name);
            unset($arr[$key]->channel_category);
            unset($arr[$key]->channel_description);
            unset($arr[$key]->channel_url);
            unset($arr[$key]->channel_sync_time);
            // unset($arr[$key]->channel_description);
            unset($arr[$key]->channel_status);
            unset($arr[$key]->channel_code);
            unset($arr[$key]->channel_codeinfo);

            $arr[$key]->channel_id = $channel_id;
            $arr[$key]->channel_order = $channel_order;
            $arr[$key]->channel_name = $channel_name;
            $arr[$key]->channel_category = $channel_category;
            $arr[$key]->channel_description = $channel_description;
            $arr[$key]->channel_url = $channel_url;
            $arr[$key]->channel_sync_time = $channel_sync_time;
        
            $arr[$key]->channel_status =$channel_status;
            $arr[$key]->channel_code = $channel_code;
            $arr[$key]->channel_codeinfo = $channel_codeinfo;

        }

        return $arr;
    }
    public function setStreamObjTotal($stream_obj,$nextKey){
        $stream_obj->totals = $nextKey;
    }
    public function createChannel(){

        $this->loadValidator();
        $this->loadImageUploadStyle();

        
        $this->template->javascript->add(base_url('assets/js/backend/channel/createchannel.js'));
        $this->loadImageUploadScript();





        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;
            $query = $this->db->insert('tvchannels',array(
                'channel_number'=>$this->input->post('channel_ascii_code'),
                'name'=>$this->input->post('name'),
                'description'=>$this->input->post('description'),
                'country_iso2'=>'TH',
                'band_type'=>$this->input->post('band_type'),
                'created'=>date('Y-m-d H:i:s')
            ));

            if($query){
                $insert_id = $this->db->insert_id();

                $this->uploadChannelLogo(array(
                    'channel_id'=>$insert_id
                ));

                $txtSuccess = "Create channel success";
                $this->msg->add($txtSuccess,'success');
                redirect($this->uri->uri_string());


            }



        }


        
        $data = array(
            
        );

        $this->template->content->view('backend/channel/createchannel',$data);
        $this->template->publish();
    }
    public function editChannel($id=null){
            if(!$this->getChannelData($id)){
                redirect(base_url('backend/'.$this->controller.'/channel_list'));
            }else{
                $this->__createChannel($id);
            }
    }
    public function deleteChannel($id=0){
        if(!$this->getChannelData($id)){
            redirect(base_url('backend/'.$this->controller.'/channel_list'));
        }else{
            $this->db->delete('TVChannels',array('id'=>$id));
            $this->msg->add(__('Delete channel success','backend/channel/create_channel'),'success');
            redirect(base_url('backend/'.$this->controller.'/channel_list'));
        }
    }

    public function deleteChannelLogo($channel_id = 0){

        /* check channel exist */
        $query = $this->db->select('*')
        ->from('tvchannels')
        ->where('id',$channel_id)
        ->get();
        /* eof check channel exist*/

        if($query->num_rows() > 0){

            $path = './uploaded/tv/logo/'.$query->row()->logo;
            /* check if with path if file exist delete file first */
            if(file_exists($path)){
                unlink($path);
            }
            /* eof check file exist */


            /* set channel logo field to empty */
            $this->db->update('tvchannels',array(
                'logo'=>''
            ),array('id'=>$channel_id));

            $this->msg->add(__('Delete channel logo success'),'success');
            redirect('backend/'.$this->controller.'/editChannel/'.$channel_id);

        }else{
            redirect('backend/'.$this->controller.'/channel_list');
        }
    }
    private function __createChannel($id=null){

            $this->loadImageUploadStyle();
            $this->loadImageUploadScript();

            $row = $this->getChannelData($id);
            //echo 'aaaa';exit;

            $center_channel_json = $this->getSelectOfiveChannelList();

            //print_r($center_channel_json);


            if($id){
                $this->template->javascript->add(base_url('assets/js/backend/channel/create_channel.js'));
            }

            $internet_tv_status = 0;

            if($row->mv_status || $row->direct_streaming_status){
                    $internet_tv_status = 1;
            }

            //print_r($row);exit;


            if($this->input->post(NULL,FALSE)){
                /* update channel */
                if($id){

                    //echo 'aaa';exit;
                    $query = $this->db->update('tvchannels',array(
                        'name'=>$this->input->post('name'),
                        'description'=>$this->input->post('description'),
                        'active'=>$this->input->post('active'),
                        'only_internet_tv'=>$this->input->post('only_internet_tv'),
                        //'mv_request_streaming'=>$this->input->post('mv_request_streaming'),
                        //'mv_streaming_temp'=>$this->input->post('mv_streaming_temp'),

                        //'mv_status'=>$this->input->post('mv_status'),
                        //'direct_streaming_status'=>($this->input->post('mv_status'))?1:0,

                        //'channel_ascii_code'=>$this->input->post('channel_ascii_code'),
                        'direct_streaming_link'=>($this->input->post('direct_streaming_link') && $this->input->post('direct_streaming_link') != '')?$this->input->post('direct_streaming_link'):'',
                        'band_type'=>$this->input->post('band_type'),
                        'ofive_map_channel'=>$this->input->post('ofive_map_channel')
                    ),array('id'=>$id));

                    /* update internet tv status  */
                    if($this->input->post('internet_tv_status') == 1){
                        if($row->direct_streaming_status){
                            $this->db->update('tvchannels',[
                                'mv_status'=>0
                            ],['id'=>$id]);
                        }

                        if($row->direct_streaming_link && !$row->direct_streaming_status){
                            $this->db->update('tvchannels',[
                                'direct_streaming_status'=>1
                            ],['id'=>$id]);
                        }

                        if(!$row->direct_streaming_status && !$row->direct_streaming_link && $mv_request_streaming){
                            $this->db->update('tvchannels',[
                                'mv_status'=>1
                            ],['id'=>$id]);
                        }
                    }else{
                        $this->db->update('tvchannels',[
                            'mv_status'=>0,
                            'direct_streaming_status'=>0
                        ],['id'=>$id]); 
                    }


                    if($query){
                        /* update tv channels category list */

                            /* check upload logo */
                            $this->uploadUpdateChannelLogo(array(
                                'channel_id'=>$id
                            ));
                            /* eof check upload logo*/

                            // 1. remove old record from fv channels category list
                            $this->db->delete('tvchannelcategorylists',array('tvchannels_id'=>$id));

                            // 2. insert new config
                            foreach ($this->input->post('check_cat') as $key => $value) {
                                # code...
                                    $this->db->insert('tvchannelcategorylists',array('tvchannels_id'=>$id,'tvchannelcategories_id'=>$value));
                            }
                            $this->msg->add(__('Edit channel successful!!','backend/channel/create_channel'),'success');
                            redirect(base_url('backend/'.$this->controller.'/editChannel/'.$id));
                    }

                }else{
                    //echo 'aaaa';exit;
                    $query = $this->db->insert('tvchannels',array('name'=>$this->input->post('name'),'description'=>$this->input->post('description')));
                    if($query){
                        /* insert tv channel category list */
                        foreach ($this->input->post('check_cat') as $key => $value) {
                            # code...
                            $this->db->insert('tvchannelcategorylists',array('tvchannels_id'=>$id,'tvchannelcategories_id'=>$value));
                        }
                        $this->msg->add(__('Create channel successful!!','backend/channel/create_channel'),'success');
                            redirect(base_url('backend/'.$this->controller.'/channel_list'));

                    }

                }
            }

            $data = array(
                'channel_data' => $row,
                'channel_categories'=>$this->getAllChannelCategoriesAvailable(),
                'channel_categories_list'=>$this->getChannelCategoriesList($row->id),
                'select_map_channel'=>$center_channel_json,
                'internet_tv_status'=>$internet_tv_status
            );

            // print_r($data);exit;

            $this->template->content->view('backend/channel/create_channel',$data);
            $this->template->publish();

    }

    public function channel_categories_list(){
    	$this->loadDataTableCSS();
    	$this->loadDataTableJS();

    	$data = array(
    		'channel_categories' => $this->getAllChannelCategories()
    	);

    	$this->template->content->view('backend/channel/channel_categories_list',$data);
    	$this->template->publish();
    }

    public function createCategory(){
    		$this->__createCategory();
    }
    public function editCategory($id=0){
        if(!$this->getCategoryData($id)){
            redirect(base_url('backend/'.$this->controller.'/channel_categories_list'));
        }else{
            $this->__createCategory($id);
        }
    }
    public function deleteCategory($id=0){
        if(!$this->getCategoryData($id)){
            redirect(base_url('backend/'.$this->controller.'/channel_categories_list'));
        }else{
            $this->db->delete('tvchannelcategories',array('id'=>$id));
            $this->msg->add(__('Delete channel category success','backend/channel/create_category'),'success');
            redirect(base_url('backend/'.$this->controller.'/channel_categories_list'));
        }
    }

    public function editChannelComponent($id=0){
        $query = $this->db->select('*')
        ->from('tvchannels')
        ->where('id',$id)
        ->get();

        if($query->num_rows() <= 0){
            redirect('backend/'.$this->controller.'/channel_list');
        }


        $data = array(

        );

        $this->template->content->view('backend/channel/edit_channel_components',$data);
        $this->template->publish();
    }

    public function edit_actual_channel_id($parent_id , $channel_id){
        
        $this->loadValidator();
        $this->loadImageUploadStyle();

        
        $this->template->javascript->add(base_url('assets/js/backend/channel/edit_actual_channel_id.js'));
        $this->loadImageUploadScript();
       
        /* if recieve post data from frontend then */
        $stream_content ="";
        $steam_files =  $this->getVdoDownloadData($parent_id);
        $steam_files = $this->prepareStreamFile($steam_files);
        $stream_content = isset($steam_files[0]->content) ? $steam_files[0]->content : array();
        $channel_arr = array();
        if($this->input->post(NULL,FALSE)){
            //* if frontend send dropdown value = SelectCategory then change it's to null
           
            /* query get file  */
            $QueryGETFile =  $this->db->select('*')
    		->from('vdo_download_api')
    		->where('id',$parent_id)
            ->get();
            /* eof query get file */
            
            /* find actual channel id in json object */
            if(!empty($stream_content)){
                $stream_obj = json_decode($stream_content);
                $channel_arr = $stream_obj->Channels;
                $found_key = $this->findKey_ChannelInStreamfile($channel_id,$channel_arr);
               
                /*set value */ 
                $stream_obj->Channels[$found_key]->channel_name  = $this->input->post('channel_name');
                $stream_obj->Channels[$found_key]->channel_description = $this->input->post('channel_desc');
                $stream_obj->Channels[$found_key]->channel_url = $this->input->post('channel_url');
                $stream_obj->Channels[$found_key]->channel_category = $SelectCategory;
                /*eof set value */
                
                header('Content-Type: application/json');
                $newStreamContent = json_encode($stream_obj,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                $filename = $QueryGETFile->row()->filename;
                file_put_contents('./assets/json/'.$filename, $newStreamContent );
                
                $this->msg->add('Update success','success');
                redirect($this->uri->uri_string());
                
            }
            /* eof find  actual channel id in json object */
        }
        /* eof recieve post data from frontend then */

        /* get channel data by id */
        $found_key = "";
        $stream_content ="";
        $steam_files =  $this->getVdoDownloadData($parent_id);
        $steam_files = $this->prepareStreamFile($steam_files);
        $stream_content = isset($steam_files[0]->content) ? $steam_files[0]->content : array();
      
        if(!empty($stream_content)){
            /* find actual channel id in json object */
            $stream_obj = json_decode($stream_content);
            $channel_arr = $stream_obj->Channels;
            $found_key = $this->findKey_ChannelInStreamfile($channel_id,$channel_arr);
           
            /* eof find  actual channel id in json object */
         
            
            /* set data and send it to frontend */
            $vdoDownLoadData->parent_id = $parent_id;
            $vdoDownLoadData->channel_id = $channel_arr[$found_key]->channel_id;
            $vdoDownLoadData->channel_name  = $channel_arr[$found_key]->channel_name;
            $vdoDownLoadData->channel_description  = $channel_arr[$found_key]->channel_desc;
            $vdoDownLoadData->channel_url   = $channel_arr[$found_key]->channel_url;
            $vdoDownLoadData->channel_category   = $channel_arr[$found_key]->channel_category;
            /* eof set data and send it to frontend  */

        }
         /* eof get channel data by id */
        
        /* get video data and send it to front */
         $data = array(
             "vdoData" => isset($vdoDownLoadData)  ?  $vdoDownLoadData : array()
         );
        /* eof get video data */
         /* select category */
         $data['select_category'] = $this->getSelectVdoDownloadCategory();
         /* eof select category */

        
        $this->template->content->view('backend/channel/edit_actual_channel_id',$data);
        $this->template->publish();
        
    
    }
    public function delete_link_data(){
     
        /* if recieve post data from frontend then */
       
        $channel_arr = array();
        if($this->input->post(NULL,FALSE)){
              $stream_content ="";
              $parent_id = $this->input->post('parent_id');
              $status_id = $this->input->post('status_id');
              $this->db->update('download_history_log',array(
                'deleted_at'=>date('Y-m-d H:i:s')
              ),array('id'=>$parent_id));
             
              # if status = 1  then update other to 1
              if($status_id == 1){
                $queryDownloadHisLog = $this->db->select('*')
                ->from('download_history_log')
                ->where('active',0)
                ->where('deleted_at',null)
                ->order_by('id','asc')
                ->limit(1)
                ->get();
             
                if($queryDownloadHisLog->num_rows() > 0){
                
                    foreach ($queryDownloadHisLog->result() as $key => $value) {
                        # update other id set to 1
                        $update_id  = $value->id;
                        $this->db->update('download_history_log',array(
                            'active'=>1
                          ),array('id'=>$update_id));

                    }
                }
              }
              echo json_encode(array('status'=>true));
          
        }
        /* eof recieve post data from frontend then */
   
    
  }

  

    public function delete_actual_channel_id(){
     
          /* if recieve post data from frontend then */
         
          $channel_arr = array();
          if($this->input->post(NULL,FALSE)){
                $stream_content ="";
                $parent_id = $this->input->post('parent_id');
                $channel_id =$this->input->post('channel_id');
            
                $steam_files =  $this->getVdoDownloadData($parent_id);
                $steam_files = $this->prepareStreamFile($steam_files);
                $stream_content = isset($steam_files[0]->content) ? $steam_files[0]->content : array();
                /* query get file  */
                $QueryGETFile =  $this->db->select('*')
                ->from('vdo_download_api')
                ->where('id',$parent_id)
                ->get();
                /* eof query get file */
            
                /* find actual channel id in json object */
                if(!empty($stream_content)){
                $stream_obj = json_decode($stream_content);
                
                $channel_arr = $stream_obj->Channels;
                
                $found_key = $this->findKey_ChannelInStreamfile($channel_id,$channel_arr);
                
                /*delete value */
                array_splice($stream_obj->Channels , $found_key , 1);
                /*eof delete value */
                
                /* set total  */
                $this->setStreamObjTotal($stream_obj,count($stream_obj->Channels));
              
                $countArrayObjForSetChOrder = count($stream_obj->Channels);
                //rearrage when delete
                $this->updateAllChannelOrder($stream_obj , "" , 0 ,$countArrayObjForSetChOrder , 'delete');
                    
                $stream_obj->Channels = $this->reArrageChannelStreamByOrders($stream_obj->Channels, 'channel_order');
              
                $newStreamContent = json_encode($stream_obj,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                //$newStreamContent = json_encode($stream_obj);
                $filename = $QueryGETFile->row()->filename;
                file_put_contents('./assets/json/'.$filename, $newStreamContent );
            }
            /* eof find  actual channel id in json object */
          }
          /* eof recieve post data from frontend then */
        echo json_encode(array('status'=>true));
      
    }

    /* find array key of channel(id) in stream file(.json) */
    public function findKey_ChannelInStreamfile($channel_id,$channel_arr){
        $found_key = array_search($channel_id, array_column($channel_arr, 'channel_id'));
        return $found_key;
    }
    /* eof  find array key of channel(id) in stream file(.json) */

    function searchForId($id, $array) {
        foreach ($array as $key => $val) {
            //print_r($array);exit();
            if ($val->channel_id == $id) {
                return $key;
            }
        }
        return null;
     }
    public function ajaxGetChannelData(){
        $data_receive = array(
            'channel_id' => $this->input->post('channel_id')
        );

        $channel_data = $this->getChannelData($data_receive['channel_id']);

        echo json_encode(array('status'=>true,'channel_data'=>$channel_data));
        // print_r($channel_data);
    }

    public function ajaxGetChannelListData(){
        $channel_id = $this->input->post('channel_id');
        $data_receive = array(
            'channel_id' => $channel_id
        );

        /* get channel data by id */
        $stream_content ="";
        $steam_files =  $this->getVdoDownloadData($channel_id);
        $steam_files = $this->prepareStreamFile($steam_files);
        /* eof get channel data by id */

        echo json_encode(array('status'=>true,'channel_data'=>json_encode($steam_files)));
        //echo json_encode(array('status'=>true,'channel_data'=>$channel_data));
    }


    public function ajaxGetDownloadLinkData(){
        $parent_id = $this->input->post('id');
        $data = array(
            'id' => $id
        );

        /* get download data by id */
        $stream_content ="";
        $download_link_data =  $this->getAllDownloadLinkData($parent_id,null);
 
        /* eof download data by id */

        echo json_encode(array('status'=>true,'channel_data'=>json_encode($download_link_data)));

    }
    public function getAllDownloadLinkData($id,$active){
        $query = $this->db->select('*')
        ->from('download_history_log')
        ->where("deleted_at",null);
        if($id != null)
        {
            $query = $query->where('id',$id);
        }
        if($active != null)
        {
            $query = $query->where('active',"1");
        }
        $results = $query->get();
        if($results->num_rows() > 0){
            return $results->result();
        }else{
            return 0;
        }
    }
    
    public function ajaxsyncStreamChannel(){
        $parent_id  = $this->input->post('parent_id');
        if($this->input->post(NULL,FALSE)){
            $stream_content ="";
         
            $steam_files =  $this->getVdoDownloadData($parent_id);
        
            $steam_files = $this->prepareStreamFile($steam_files);
            /* get only one content from stream file */
            foreach($steam_files as $_getStream => $_getActuallyStream){
                if($_getActuallyStream->id == $parent_id){
                    $stream_content = isset($_getActuallyStream->content) ? $_getActuallyStream->content : array();
                }
            }
            /* eof get only one content from stream file */

            $channel_arr = array();

             /* query get file  */
            
             $QueryGETFile =  $this->db->select('*')
             ->from('vdo_download_api')
             ->where('id',$parent_id)
             ->get();

             
             /* eof query get file */
             /* find actual channel id in json object */
             if(!empty($stream_content)){
                 $stream_obj = json_decode($stream_content);
             
                /* push object to array */
                $count = 0;
                    
                //Add whatever options here. The CURLOPT_URL is left out intentionally.
                //It will be added in later from the url array.
                $optionArray = array(
                
                    CURLOPT_USERAGENT        => 'Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:47.0) Gecko/20100101 Firefox/47.0',//Pick your user agent.
                    CURLOPT_RETURNTRANSFER   => TRUE,
                    CURLOPT_TIMEOUT          => 60,
                    CURLOPT_CONNECTTIMEOUT   => 60,
                    CURLOPT_MAXREDIRS        => 20
                
                );
                
                //Create an array of your urls.
                foreach($stream_obj->Channels as $keyChannel => $valueChannel){
                    $urlArray[$valueChannel->channel_id] = $valueChannel->channel_url;
                }
  
                //Play around with this number and see what works best.
                //This is how many urls it will try to do at one time.
                $nThreads = count($urlArray);
                //$nThreads = 50;
                //To use run the function.
                $results = $this->multi_thread_curl($urlArray, $optionArray, $nThreads);
        
            if(!empty($results)){
                    foreach($stream_obj->Channels as $keyChannel => $valueChannel){
                        $status = 0;
                        $channel_id = $valueChannel->channel_id;
                        $code = $results[$channel_id]['code'];
                        $code_info = $results[$channel_id]['code_info'];
                        $time_sync = date('Y-m-d H:i:s');

                        if($code >= 200 && $code < 302){
                            $status = 1;
                        }else{
                            $status = 0;
                        }
                        
                        /** set http status  */
                        # future use
                        // $valueChannel->channel_status = $status;
                        // $valueChannel->channel_code  = $code; 
                        // $valueChannel->channel_codeinfo  = $code_info; 
                        // $valueChannel->channel_sync_time = $time_sync;
            
                    }
             
                  
                    /*eof push object to json */

                    $newStreamContent = json_encode($stream_obj,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                    $filename = $QueryGETFile->row()->filename;
                    file_put_contents('./assets/json/'.$filename, $newStreamContent );
                    /*set value */ 
                
                 
             }
             /* eof find  actual channel id in json object */
            }
        }
    }
    
    public function multi_thread_curl($urlArray, $optionArray, $nThreads) {

        //Group your urls into groups/threads.
        $curlArray = array_chunk($urlArray, $nThreads, $preserve_keys = true);
      
        //Iterate through each batch of urls.
        $ch = 'ch_';
        foreach($curlArray as $threads) {      
      
            //Create your cURL resources.
            foreach($threads as $thread=>$value) {
      
            ${$ch . $thread} = curl_init();
      
              curl_setopt_array(${$ch . $thread}, $optionArray); //Set your main curl options.
              curl_setopt(${$ch . $thread}, CURLOPT_URL, $value); //Set url.

              curl_setopt(${$ch . $thread}, CURLOPT_RETURNTRANSFER, true);
      
              }
      
            //Create the multiple cURL handler.
            $mh = curl_multi_init();
      
            //Add the handles.
            foreach($threads as $thread=>$value) {
      
            curl_multi_add_handle($mh, ${$ch . $thread});
      
            }
      
            $active = null;
      
            //execute the handles.
            do {
      
            $mrc = curl_multi_exec($mh, $active);
      
            } while ($mrc == CURLM_CALL_MULTI_PERFORM);
      
            while ($active && $mrc == CURLM_OK) {
      
                if (curl_multi_select($mh) != -1) {
                    do {
      
                        $mrc = curl_multi_exec($mh, $active);
      
                    } while ($mrc == CURLM_CALL_MULTI_PERFORM);
                }
      
            }
      
            //Get your data and close the handles.
            foreach($threads as $thread=>$value) {
                $code = curl_getinfo(${$ch.$thread}, CURLINFO_HTTP_CODE); 
                $info = curl_getinfo(${$ch.$thread});

                $results[$thread]['content'] = curl_multi_getcontent(${$ch . $thread});
                $results[$thread]['code'] = $code; 
                $results[$thread]['code_info'] = $this->http_status_codes[$code];
             
                curl_multi_remove_handle($mh, ${$ch . $thread});
      
            }
            
            //Close the multi handle exec.
            curl_multi_close($mh);
      
        }
      
      
        return $results;
      
      } 
      
      
      


    private function getSelectVdoDownloadCategory(){
		$arrReturn = []; 
    
		$arrReturn[''] = __('SelectCategory','');
        
		$query = $this->db->select('*')
		->from('vdo_download_category')
		->where('active',1)
		->order_by('category_name','asc')
        ->get();
      
		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arrReturn[$value->category_code] = $value->category_name;
			}
		}

		return $arrReturn;

    }
    
    public function ajaxSetChannelOrdinal(){
        $post_data  = $this->input->post();

        /* set data for cband */
        foreach ($post_data['cband_ordinal'] as $key => $value) {
            # code...
            if(@$value['ordinal'] != "" && @$value['oridnal'] != '0'){
                /* update channel cband ordinal */
                $this->db->update('tvchannels',array(
                    'cband_ordinal'=>$value['ordinal']
                ),array('id'=>$value['channel_id']));
            }
        }


        /* set data for kuband*/
        foreach ($post_data['kuband_ordinal'] as $key => $value) {
            # code...
            if(@$value['ordinal'] != "" && @$value['oridnal'] != '0'){
                /* update channel cband ordinal */
                $this->db->update('tvchannels',array(
                    'kuband_ordinal'=>$value['ordinal']
                ),array('id'=>$value['channel_id']));
            }
        }

        foreach ($post_data['mobile_app_ordinal'] as $key => $value) {
            # code...
            if(@$value['ordinal'] != "" && @$value['ordinal'] != '0'){
                $this->db->update('tvchannels',array(
                    'mobile_app_ordinal'=>$value['ordinal']
                ),array('id'=>$value['channel_id']));
            }
        }


        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data
        ));
    }

    public function channel_ordinal_list(){

        //$this->loadDataTableCSS();
        //$this->loadDataTableJS();
        $this->template->javascript->add('https://code.jquery.com/ui/1.12.0/jquery-ui.min.js');
        $this->loadSweetAlert();
        $this->template->javascript->add(base_url('assets/js/backend/channel/channel_ordinal_list.js?'.time()));

        /* get all channel list */


        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;
            $post_data = $this->input->post();

                //print_r($this->input->post());exit;

                    /* set data for cband */
                foreach ($post_data['cband_ordinal'] as $key => $value) {
                    # code...
                    if(@$value != "" && @$value != '0'){
                        /* update channel cband ordinal */
                        $this->db->update('tvchannels',array(
                            'cband_ordinal'=>$value
                        ),array('id'=>$post_data['channel_id'][$key]));
                    }
                }


                /* set data for kuband*/
                foreach ($post_data['kuband_ordinal'] as $key => $value) {
                    # code...
                    if(@$value != "" && @$value != '0'){
                        /* update channel cband ordinal */
                        $this->db->update('tvchannels',array(
                            'kuband_ordinal'=>$value
                        ),array('id'=>$post_data['channel_id'][$key]));
                    }
                }

                foreach ($post_data['mobile_app_ordinal'] as $key => $value) {
                    # code...
                    if(@$value != "" && @$value != '0'){
                        $this->db->update('tvchannels',array(
                            'mobile_app_ordinal'=>$value
                        ),array('id'=>$post_data['channel_id'][$key]));
                    }
                }


                $this->msg->add('Update success','success');
                redirect($this->uri->uri_string());


        }


        $data = array(
            'channels' => $this->getAllChannelsActive()
        );


        $this->template->content->view('backend/channel/channel_ordinal_list',$data);
        $this->template->publish();

    }


    public function video_download_api(){

        $this->loadDataTableCSS();
    	$this->loadDataTableJS();
        $this->template->javascript->add('https://code.jquery.com/ui/1.12.0/jquery-ui.min.js');
        // $this->template->javascript->add('https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js');
        
        // $this->template->css->add('https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css');
        //cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js
    
   
    
        //echo '<PRE>';
        //print_r($this->input->post());
       // exit();
        /* get stream file from server  */
        //$steam_files = getDirectory('assets/json/');
        $stream_content ="";
        $steam_files =  $this->getVdoDownloadData(null);
        $steam_files = $this->prepareStreamFile($steam_files);
        /* eof get stream file from server */
       
        $data = array(
            
            'stream_content' => json_decode($stream_content),
            'steam_files'=>$steam_files,
            "select_category" => $this->getSelectVdoDownloadCategory()
        );

        /*channel category */
    
        /*eof channelcategory  */
      
        $this->template->content->view('backend/channel/video_download',$data);
        $this->template->javascript->add(base_url('assets/js/backend/channel/video_download.js'));

        $this->template->publish();

    }

    public function update_link(){
        $arr_post = $this->input->post('arr_post');
        if(!empty($arr_post)){
            foreach($arr_post as $k => $value){
                /* update channel with logo */
                $this->db->update('download_history_log',array(
                    'links'=>$value['url'],
                    'active'=>$value['form_status']
                ),array('id'=>$value['id']));
            }

            return json_encode(array('status'=>true));
        }
        return json_encode(array('status'=>false));
       
    }

    public function update_channel_streamfile(){
        $arr_post = $this->input->post('arr_post');
       // print_r($arr_post);exit();
        if(!empty($arr_post)){
           
            $parent_id  = $this->input->post('parent_id');
            // print_r($parent_id);exit();
                $stream_content ="";
             
                $steam_files =  $this->getVdoDownloadData($parent_id);
            
                $steam_files = $this->prepareStreamFile($steam_files);
                /* get only one content from stream file */
             //   print_r($steam_files);exit();
                foreach($steam_files as $_getStream => $_getActuallyStream){
                  
                    if($_getActuallyStream->id == $parent_id){
                        $stream_content = isset($_getActuallyStream->content) ? $_getActuallyStream->content : array();
                      
                    }
                }
                   /* find actual channel id in json object */
                if(!empty($stream_content)){
                    $stream_obj = json_decode($stream_content);
                }
               
                /* eof get only one content from stream file */
    
                $channel_arr = array();
               
                 /* query get file  */
                
                 $QueryGETFile =  $this->db->select('*')
                 ->from('vdo_download_api')
                 ->where('id',$parent_id)
                 ->get();

                 
            if(!empty($QueryGETFile->result()) && !empty($stream_obj)){
                $arr_post = json_decode($arr_post);
                foreach($stream_obj->Channels as $keyChannel => $valueChannel){
                    $status = 0;
                    $channel_id = $valueChannel->channel_id;
                    
                    /** set http status  */
                    $valueChannel->channel_id = $channel_id;
                    $valueChannel->channel_order = (int)$arr_post[$channel_id]->channel_order;
                    $valueChannel->channel_name = $arr_post[$channel_id]->channel_name;
                    $valueChannel->channel_url  = $arr_post[$channel_id]->channel_url;

                    $SelectCategory = "";
                    if($arr_post[$channel_id]->channel_category != "SelectCategory"){
                        $SelectCategory = $arr_post[$channel_id]->channel_category;
                    }
                    $valueChannel->channel_category  = $SelectCategory;
                
                }

                $countArrayObjForSetChOrder = count($stream_obj->Channels);
                //$this->updateAllChannelOrder($stream_obj , "" , 0 ,$countArrayObjForSetChOrder , 'delete');
                    
                $stream_obj->Channels = $this->reArrageChannelStreamByOrders($stream_obj->Channels, 'channel_order');
                   
                /*eof push object to json */

                $newStreamContent = json_encode($stream_obj,JSON_UNESCAPED_SLASHES|JSON_PRETTY_PRINT);
                $filename = $QueryGETFile->row()->filename;
                file_put_contents('./assets/json/'.$filename, $newStreamContent );

                // $this->msg->add('Insert success','success');
                // redirect($this->uri->uri_string());
                return json_encode(array('status'=>true ));
             
             /*set value */ 
            
             
         }
         return json_encode(array('status'=>true ));
           
        }
        return json_encode(array('status'=>false));
    }
    private function prepareStreamFile($steam_files){
       // $steam_files =  $this->getVdoDownloadData(null);
        if(!empty($steam_files)){
            foreach($steam_files as $stream_key => $stream_obj){
                /* declare parent id */
                $steam_files[$stream_key]->parent_id = isset($stream_obj->id) ? $stream_obj->id : 0;
                /* eof declare parent id  */

                /* file_name use alias name  */
                $steam_files[$stream_key]->file_name = $stream_obj->alias_name;
                
                /* hard code relative path file url*/
                $steam_files[$stream_key]->file_url = empty($stream_obj->file_url) ? "assets/json/" : $stream_obj->file_url;
                /* eof */
                
                $steam_files[$stream_key]->content = file_get_contents(base_url('assets/json/' .  $stream_obj->filename));
                // $stream_content= file_get_contents(base_url('assets/json/' .  $stream_obj->filename));
            }
        }

        return $steam_files;
    }
    private function __createCategory($id=null){
    	
    	$row = $this->getCategoryData($id);

    	if($this->input->post(NULL,FALSE)){
                $data = array(
                    'name'=>$this->input->post('name'),
                    'description'=>$this->input->post('description')
                );
            if($row){
                /* edit */
                $this->db->update('tvchannelcategories',$data,array('id'=>$row->id));
                $this->msg->add(__('Edit category successful!!','backend/channel/create_category'),'success');

            }else{
                $this->db->insert('tvchannelcategories',$data);
                $this->msg->add(__('Create category successful!!','backend/channel/create_category'),'success');

                /* create*/
            }

    	}

    	$data = array(
    		'category_data' => $row
    	);

    	// print_r($data);exit;

    	$this->template->content->view('backend/channel/create_category',$data);
    	$this->template->publish();

    }

    private function getCategoryData($category_id){
    		$query = $this->db->select('*')
    		->from('tvchannelcategories')
    		->where('id',$category_id)
    		->get();

    		if($query->num_rows() > 0){
    			return $query->row();
    		}else{
    			return false;
    		}
    }

    private function getChannelData($channel_id){
            $query = $this->db->select('*')
            ->from('tvchannels')
            ->where('id',$channel_id)
            ->get();
            if($query->num_rows() > 0){
                return $query->row();
            }else{
                // $row = new StdClass();
                // $row->{'id'} = 0;
                // $row->{'logo'} = '';
                // return $row;
                return false;
            }
    }

    private function getVdoDownloadData($channel_id){
        $query = $this->db->select('*')
        ->from('vdo_download_api')
        ->where('deleted_at',null);
        
        if($channel_id > 0){
            $results = $query->where('id',$channel_id);
        }
        $results = $query->get();
        //print_r($results->result());exit();
        if($results->num_rows() > 0){
            //return $results->row();
            return $results->result();
        }else{
            // $row = new StdClass();
            // $row->{'id'} = 0;
            // $row->{'logo'} = '';
            // return $row;
            return false;
        }
}

    private function getDownloadLinkData($channel_id){
        $query = $this->db->select('*')
        ->from('download_history_log')
        ->where('deleted_at',null);
        
        // if($channel_id > 0){
        //     $results = $query->where('id',$channel_id);
        // }
        $results = $query->get();
        return $results->result();

        //print_r($results->result());exit();
        //     //return $results->row();
        //     return $results->result();
        // }else{
        //     // return $row;
        //     return false;
        // }
    }

    private function getAllChannels(){
    	$query = $this->db->select('*')
    	->from('tvchannels')
        ->order_by('channel_number')
    	->get();

        // print_r($query->result());exit;

    	return $query->result();

    }

    private function getAllChannelsActive(){
        $query = $this->db->select('*')
        ->from('tvchannels')
        ->where('active',1)
        ->order_by('mobile_app_ordinal')
        ->get();

        return $query->result();
    }
    private function getAllChannelCategories(){
    	$query = $this->db->select('*')
    	->from('tvchannelcategories')
    	->get();

    	return $query->result();
    }
    private function getAllChannelCategoriesAvailable(){
        $query = $this->db->select('*')
        ->from('tvchannelcategories')
        ->where('active',1)
        ->get();
        return $query->result();
    }

    private function getChannelCategoriesList($channel_id=0){
        //print_r($channel_id);exit;
        $arReturn = array();
        $query = $this->db->select('*')
        ->from('tvchannelcategorylists')
        ->where('tvchannels_id',$channel_id)
        ->get();

        foreach ($query->result() as $key => $value) {
            # code...
            array_push($arReturn, $value->tvchannelcategories_id);
        }
        return $arReturn;

        // print_r($arReturn);exit;


    }
   

    public function exportChannelClick(){
        $post_data = $this->input->post();

        $query = $this->db->select('*,tvchannels.name,tvchannelviews.created as viewtime')
        ->from('tvchannelViews')
        ->join('tvchannels','tvchannelviews.tvchannels_id = tvchannels.id')
        ->where('tvchannelviews.tvchannels_id',$post_data['channel_id'])
        ->order_by('tvchannelviews.created','desc')
        ->get();

        //print_r($query->result());exit;

        // print_r($questionnaireanswer);exit;
                $filename = "TVChannelViewReport(".$query->result()[0]->name.').xlsx';

                require_once 'assets/plugin/PHPExcel/Classes/PHPExcel.php';


                // Create new PHPExcel object
                $objPHPExcel = new PHPExcel();

                //exit;

                // Set document properties
                $objPHPExcel->getProperties()->setCreator("PSIFIXIT")
                                             ->setLastModifiedBy("PSIFIXIT Admin")
                                             ->setTitle($filename)
                                             ->setSubject("Excel file tv channel view")
                                             ->setDescription("Excel file tv")
                                             ->setKeywords("TvChannelView Report")
                                             ->setCategory("Report");

                //print_r($arDataOrder);


                $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:D1')->setCellValue('A1',$filename);
                $objPHPExcel->getActiveSheet()->getStyle('A1:D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

                $objPHPExcel->setActiveSheetIndex(0)
                            ->setCellValue('A2','#')
                            ->setCellValue('B2','TVChannel')
                            ->setCellValue('C2','Customer IP')
                            ->setCellValue('D2','ViewTime');

                $count = 1;
                $table_row = 3;
                foreach ($query->result() as $key => $value) {
                   
                                $objPHPExcel->setActiveSheetIndex(0)
                                ->setCellValue('A'.$table_row.'',@$count)
                                ->setCellValue('B'.$table_row.'',@$value->name)
                                ->setCellValue('C'.$table_row.'',@$value->client_ip)
                                ->setCellValue('D'.$table_row.'',@date('d/m/Y H:i:s',strtotime($value->viewtime)));
                                $count++;
                                $table_row++;
                }


                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('A')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('B')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('C')
                ->setAutoSize(true);
                $objPHPExcel->getActiveSheet()
                ->getColumnDimension('D')
                ->setAutoSize(true);

                // Rename worksheet
                $objPHPExcel->getActiveSheet()->setTitle('TVChannelViewReport');


                // Set active sheet index to the first sheet, so Excel opens this as the first sheet
                $objPHPExcel->setActiveSheetIndex(0);


                // Redirect output to a client’s web browser (Excel2007)
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment;filename="'.$filename.'"');
                header('Cache-Control: max-age=0');
                // If you're serving to IE 9, then the following may be needed
                header('Cache-Control: max-age=1');

                // If you're serving to IE over SSL, then the following may be needed
                header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
                header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
                header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
                header ('Pragma: public'); // HTTP/1.0

                $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
                $objWriter->save('php://output');
                exit;

    }

    private function uploadChannelLogo($data = array()){
        $channel_id = $data['channel_id'];

                            if($_FILES['channel_logo']['error'] == 0){
                                if(!is_dir('uploaded/tv/logo')){
                                        mkdir('uploaded/tv/logo',0777,true);
                                }
                                                     
                                $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                                $config['upload_path'] = 'uploaded/tv/logo/';
                                $config['allowed_types'] = 'gif|jpg|png';
                                $config['max_size'] = '2000';
                                $this->load->library('upload',$config);
                                    if(!$this->upload->do_upload('channel_logo')){
                                        $error = array('error' => $this->upload->display_errors());
                                        $this->msg->add($error['error'], 'error');
                                           
                                        /* remove data which saved */
                                        $this->db->delete('tvchannels',array(
                                            'id'=>$channel_id
                                        )); 
                                        /* eof remove data wich saved*/
                                        redirect($this->uri->uri_string());
                                    }else{
                                            $data_upload = array('upload_data' => $this->upload->data());
                                            // $employee_update = new M_employee($employee->getId());
                                            // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                                            // $employee_update->save();

                                            /* update channel with logo */
                                            $this->db->update('tvchannels',array(
                                                'logo'=>$data_upload['upload_data']['file_name'],
                                                'updated'=>date('Y-m-d H:i:s')
                                            ),array('id'=>$channel_id));


                                    }

                            }


    }

    private function uploadDownloadChannelFiles($data = array()){
        $channel_id = $data['channel_id'];

                            if($_FILES['channel_file']['error'] == 0){
                                if(!is_dir('assets/json/')){
                                        mkdir('assets/json/',0777,true);
                                }
                                // print_r($_FILES['channel_file']);exit();           
                                $config['file_name'] = "stream_".md5(strtotime(date('Y-m-d H:i:s')));
                                $config['upload_path'] = 'assets/json/';
                                $config['allowed_types'] = 'json';
                                // $config['sess_match_useragent'] = FALSE;
                                /* config maxsize of upload file 25mb */
                                $config['max_size'] = '25600';
                                $this->load->library('upload',$config);
                                    //print_r($this->upload->data());exit();
                                    
                                    if(!$this->upload->do_upload('channel_file')){
                                        $error = array('error' => $this->upload->display_errors());
                                        $this->msg->add($error['error'], 'error');
                                           
                                        /* remove data which saved */
                                        $this->db->delete('vdo_download_api',array(
                                            'id'=>$channel_id
                                        )); 
                                        /* eof remove data wich saved*/
                                        redirect($this->uri->uri_string());
                                    }else{
                                            $data_upload = array('upload_data' => $this->upload->data());
                                            // $employee_update = new M_employee($employee->getId());
                                            // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                                            // $employee_update->save();

                                            /* update channel with filename */
                                            $this->db->update('vdo_download_api',array(
                                                'filename'=>$data_upload['upload_data']['file_name'],
                                                'updated'=>date('Y-m-d H:i:s')
                                            ),array('id'=>$channel_id));


                                    }

                            }


    }

    private function uploadUpdateChannelLogo($data = array()){

                        $channel_id = $data['channel_id'];

                            if($_FILES['channel_logo']['error'] == 0){
                                if(!is_dir('uploaded/tv/logo')){
                                        mkdir('uploaded/tv/logo',0777,true);
                                }
                                                     
                                $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                                $config['upload_path'] = 'uploaded/tv/logo/';
                                $config['allowed_types'] = 'gif|jpg|png';
                                $config['max_size'] = '2000';
                                $this->load->library('upload',$config);
                                    if(!$this->upload->do_upload('channel_logo')){
                                        $error = array('error' => $this->upload->display_errors());
                                        $this->msg->add($error['error'], 'error');
                                           
                                        $this->msg->add('เกิดข้อผิดพลาดในการอัพโหลด กรุณาลองใหม่อีกครั้ง','error');
                                        /* eof remove data wich saved*/
                                        redirect($this->uri->uri_string());
                                    }else{
                                            $data_upload = array('upload_data' => $this->upload->data());
                                            
                                            /* unlink older file */
                                            $channel_data = $this->getChannelData($channel_id);

                                            $older_path = './uploaded/tv/logo/'.$channel_data->logo;
                                            if(file_exists($older_path)){
                                                unlink($older_path);
                                            }




                                            /* update channel with logo */
                                            $this->db->update('tvchannels',array(
                                                'logo'=>$data_upload['upload_data']['file_name'],
                                                'updated'=>date('Y-m-d H:i:s')
                                            ),array('id'=>$channel_id));


                                    }

                            }


    }

    private function getSelectOfiveChannelList(){

        $ofive_url = 'http://ofivetv.psisat.com/welcome/getAllOfiveChannel';

        // create curl resource 
        $ch = curl_init(); 

        // set url 
        curl_setopt($ch, CURLOPT_URL, $ofive_url); 

        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        // $output contains the output string 
        $output = curl_exec($ch); 

        // close curl resource to free up system resources 
        curl_close($ch);      


        $response = json_decode($output);

        $arrSelect = [];
        $arrSelect[""] = "-- Select Channel --";
        foreach ($response->channels as $key => $value) {
            # code...
            $arrSelect[$value->channel_id] = $value->channel_name;

        }

        return $arrSelect;


    }

}