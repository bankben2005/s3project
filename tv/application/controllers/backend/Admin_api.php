<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_api extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
                        $this->load->helper(array(
                            'our'
                        ));

                        // print_r($this->account_data);exit;
                        $this->load->library([
                            'pagination'
                        ]);
                         $this->page_num = 10;
    }

    public function youtube_api($page = null){

        

       

    	// $this->loadDataTableCSS();
        $this->loadImageUploadStyle();
        $this->loadBootstrapFileuploadMasterStyle();



    	// $this->loadDataTableJS();
        $this->loadImageUploadScript();
        $this->loadBootstrapFileuploadMasterScript();
    	$this->loadSweetAlert();

        $this->template->javascript->add(base_url('assets/js/backend/jquery.base64.js'));
    	$this->template->javascript->add(base_url('assets/js/backend/api/youtube_api_lists.js?'.time()));


        $youtube_api_keys = new M_youtube_api_keys();
        $youtube_api_keys->where('active',1);
        //->get();

        if($this->input->get('q')){
            $criteria_decode = base64_decode($this->input->get('q'));

            parse_str($criteria_decode,$criteria);
            //print_r($criteria);

            if($criteria['gmail_account']){
                $youtube_api_keys->where('gmail_account',$criteria['gmail_account']);
            }

            if(is_numeric($criteria['over_limit_status'])){
                $youtube_api_keys->where('over_limit_status',$criteria['over_limit_status']);
            }
            $this->setData('search_criteria',$criteria);
        }


        $clone = $youtube_api_keys->get_clone();
        $key_count = $clone->count();
        $youtube_api_keys->order_by('id','asc');
        $youtube_api_keys->limit(10,$page);
        $youtube_api_keys->get();

        //echo 'key count = '.$key_count.'<br> page = '.$page;
        //echo $youtube_api_keys->check_last_query();
        $this->setData('youtube_api',$youtube_api_keys);
        $this->setData('total_key',$key_count);
        $clone->get();

        $this->config_page($key_count,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url('backend/'.$this->controller.'/youtube_api/');
        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());


        //print_r($this->getData());exit;
        $data = $this->getData();
        //$data['youtube_api'] = $youtube_api_keys;

    	$this->template->content->view('backend/api/youtube_api_lists',$data);
    	$this->template->publish();
    }

    public function youtube_api_unactive($page = NULL){
        // $this->loadDataTableCSS();
        // $this->loadDataTableJS();
        // $this->loadSweetAlert();
        $this->template->javascript->add(base_url('assets/js/backend/jquery.base64.js'));
        $this->template->javascript->add(base_url('assets/js/backend/api/youtube_api_lists_unactive.js?'.time()));



        $youtube_api_keys = new M_youtube_api_keys();
        $youtube_api_keys->where('active',0);

        if($this->input->get('q')){
            $criteria_decode = base64_decode($this->input->get('q'));

            parse_str($criteria_decode,$criteria);

            if($criteria['search_by_status']){
                switch ($criteria['search_by_status']) {
                    case 'Blocked':
                        $youtube_api_keys->where('remark','has been block');
                    break;
                    case 'KeyInvalid':
                        $youtube_api_keys->where('remark','key invalid');
                    break;
                    case 'NotConfigured':
                        $youtube_api_keys->where('remark','access not configured');
                    break;
                    
                    default:
                        # code...
                    break;
                }
            }

        }


        $clone = $youtube_api_keys->get_clone();
        $key_count = $clone->count();
        $youtube_api_keys->order_by('updated','desc');
        $youtube_api_keys->limit(10,$page);
        $youtube_api_keys->get();

        //echo 'key count = '.$key_count.'<br> page = '.$page;
        //echo $youtube_api_keys->check_last_query();
        $this->setData('youtube_api',$youtube_api_keys);
        $this->setData('total_key',$key_count);
        $clone->get();

        $this->config_page($key_count,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url('backend/'.$this->controller.'/youtube_api_unactive/');
        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());




        $data = $this->getData();

        $this->template->content->view('backend/api/youtube_api_unactive_lists',$data);
        $this->template->publish();
    }

    public function createYoutubeAPI(){
    	$this->__createYoutubeAPI();
    }

    public function editYoutubeAPI($id){
    	/* check youtube api by id */

    	$queryCheck = $this->db->select('id')
    	->from('youtube_api_keys')
    	->where('id',$id)
    	->get();

    	if($queryCheck->num_rows() <= 0){
    		redirect('backend/'.$this->controller.'/youtube_api');
    	}

    	$this->__createYoutubeAPI($id);

    }

    public function deleteYoutubeAPI($id){

    	$queryCheck = $this->db->select('id')
    	->from('youtube_api_keys')
    	->where('id',$id)
    	->get();

    	if($queryCheck->num_rows() <= 0){
    		redirect('backend/'.$this->controller.'/youtube_api');
    	}

    	$this->db->delete('youtube_api_keys',[
    		'id'=>$id
    	]);

    	$this->msg->add(__('Delete youtube api key success','backend/api/youtube_api_lists'),'success');
    	redirect('backend/'.$this->controller.'/youtube_api');
    }

    public function ajaxChangeOverLimitStatus(){
    	$post_data = $this->input->post();

    	$this->db->update('youtube_api_keys',[
    		'over_limit_status'=>($post_data['over_limit_status'] == '1')?0:1,
    		'updated'=>date('Y-m-d H:i:s')
    	],['id'=>$post_data['api_id']]);


    	echo json_encode(array(
    		'status'=>true,
    		'post_data'=>$post_data
    	));
    }
    public function ajaxSaveMultipleKey(){
        $post_data = $this->input->post();

        foreach ($post_data['key'] as $key => $value) {
            # code...
            if($value != ''){
                $this->db->insert('youtube_api_keys',[
                    'api_key'=>$value,
                    'over_limit_status'=>0,
                    'gmail_account'=>$post_data['gmail_account'],
                    'gmail_password'=>$post_data['gmail_password'],
                    'project_name'=>$post_data['project_name'][$key],
                    'created'=>date('Y-m-d H:i:s'),
                    'active'=>1
                ]);
            }
        }

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    public function importAPIKeyFromXLSXFile(){
        set_time_limit(0);

        if($_FILES['import_excel']['error'] == 0){
           $file_name = $this->uploadImportKeyByExcelFile();
            //echo $file_name;exit;

            /** PHPExcel */
            require_once APPPATH.'/third_party/PHPExcel/Classes/PHPExcel.php';

            /** PHPExcel_IOFactory - Reader */
            include APPPATH.'/third_party/PHPExcel/Classes/PHPExcel/IOFactory.php';


            $inputFileName = "./uploaded/key_excel/".$file_name; 

            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);  
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);  
            $objReader->setReadDataOnly(true);  
            $objPHPExcel = $objReader->load($inputFileName);  

          

            $objWorksheet = $objPHPExcel->getActiveSheet();

            //print_r($objWorksheet);exit;
            $highestRow = $objWorksheet->getHighestRow();
            $highestColumn = $objWorksheet->getHighestColumn();
            $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); 

           
            $rows = array();
            for($row=2;$row<=$highestRow;++$row){
                for($col=0;$col<=$highestColumnIndex;++$col){
                    $objWorksheetValue = $objWorksheet->getCellByColumnAndRow($col,$row)->getValue();
                    if($objWorksheetValue != ''){
                        array_push($rows, $objWorksheetValue);
                    }
                }
                //array_push($rows, 'endrow');
            } 

            $excel_rows = array_chunk($rows, 4);

            //print_r($excel_rows);exit;

            if(count($excel_rows) <= 0){
                $this->msg->add(__('Not found excel rows','backend/api/youtube_api_lists'),'error');
                redirect('backend/'.$this->controller.'/youtube_api');

            }
            if(count($excel_rows[0]) == 4){

                /* check email invalid */
                if (!filter_var($excel_rows[0][0], FILTER_VALIDATE_EMAIL)) {
                    // invalid emailaddress
                    $this->msg->add(__('File column is invalid,Please check file column from example file'),'error');
                    redirect('backend/'.$this->controller.'/youtube_api');
                }
                /* eof check email invalid */

                foreach ($excel_rows as $key => $value) {
                    # code...
                    $this->db->insert('youtube_api_keys',[
                        'api_key'=>trim($value[3]),
                        'over_limit_status'=>0,
                        'gmail_account'=>$value[0],
                        'gmail_password'=>$value[1],
                        'project_name'=>$value[2],
                        'created'=>date('Y-m-d H:i:s'),
                        'active'=>1
                    ]);
                }


                $this->msg->add(__('Import youtube api key success','backend/api/youtube_api_lists'),'success');
                redirect('backend/'.$this->controller.'/youtube_api');

            }


        }

    }

    private function __createYoutubeAPI($id=null){

    	// $this->loadValidator();
    	// $this->template->javascript->add(base_url('assets/js/backend/api/create_youtube_api.js'));


    	$youtube_api_keys = $this->db->select('*')
    	->from('youtube_api_keys')
    	->where('id',$id)
    	->get();

    	if($this->input->post(NULL,FALSE)){
    		if($id){
    			/* only update */
    			$this->db->update('youtube_api_keys',[
    				'api_key'=>$this->input->post('api_key'),
    				'gmail_account'=>$this->input->post('gmail_account'),
    				'gmail_password'=>$this->input->post('gmail_password'),
    				'project_name'=>$this->input->post('project_name'),
                    'active'=>$this->input->post('active'),
    				'updated'=>date('Y-m-d H:i:s')
    			],['id'=>$id]);

    			$this->msg->add(__('Edit youtube api complete','backend/api/create_youtube_api'),'success');
    			redirect($this->uri->uri_string());


    		}else{
    			/* create new record */

    			$this->db->insert('youtube_api_keys',[
    				'api_key'=>$this->input->post('api_key'),
    				'gmail_account'=>$this->input->post('gmail_account'),
    				'gmail_password'=>$this->input->post('gmail_password'),
    				'project_name'=>$this->input->post('project_name'),
                    'active'=>$this->input->post('active'),
    				'created'=>date('Y-m-d H:i:s')
    			]);
    			$this->msg->add(__('Create youtube api complete','backend/api/create_youtube_api'),'success');
    			redirect($this->uri->uri_string());

    		}
    	}


    	$data = array(
    		'youtube_api_keys'=>$youtube_api_keys->row()
    	);

    	$this->template->content->view('backend/api/create_youtube_api',$data);
    	$this->template->publish();

    }


    private function getYoutubeAPIKEYS(){
    	$query = $this->db->select('*')
    	->from('youtube_api_keys')
        ->where('active',1)
    	->get();

    	return $query;
    }


    private function getYoutubeAPIKEYSUnactive(){
        $query = $this->db->select('*')
        ->from('youtube_api_keys')
        ->where('active',0)
        ->get();

        return $query;
    }
    private function uploadImportKeyByExcelFile(){

        $file_name = "";
        if(!is_dir('uploaded/key_excel')){
            mkdir('uploaded/key_excel',0777,true);
        }
                                                     
        $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
        $config['upload_path'] = 'uploaded/key_excel/';
        $config['allowed_types'] = 'xls|xlsx';
        $config['max_size'] = '10000';
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload('import_excel')){
            $error = array('error' => $this->upload->display_errors());
            $this->msg->add($error['error'], 'error');
        }else{
            $data_upload = array('upload_data' => $this->upload->data());
            $file_name = $data_upload['upload_data']['file_name'];
        }

        return $file_name;
    }
}