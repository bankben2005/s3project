<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Admin_youtube extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
                        $this->load->helper(array(
                            'our'
                        ));
    }

    public function index(){

        $this->template->javascript->add(base_url('assets/js/backend/youtube/category_list.js'));
        /* get category list */
        $query = $this->db->select('*')
        ->from('youtube_categories')
        ->get();
        /* eof category list */

    	$data = array(
            'youtube_categories'=>$query->result()
    	);


    	$this->template->content->view('backend/youtube/category_list',$data);
    	$this->template->publish();


    }

    public function createCategory(){
    	$this->__createCategory();
    }

    public function editCategory($category_id = 0){

    	/* check category */
    	$query = $this->db->select('id')
        ->from('youtube_categories')
        ->where('id',$category_id)
        ->get();

        if($query->num_rows() <= 0){
            redirect('backend/'.$this->controller);
        }
    	/* eof check category */

        $this->__createCategory($category_id);
    }

    private function __createCategory($id = null){

        $this->loadValidator();

        $this->loadImageUploadStyle();
        $this->loadImageUploadScript();


    	if($id){
    		$this->template->javascript->add(base_url('assets/js/backend/youtube/edit_category.js'));
    	}else{
    		$this->template->javascript->add(base_url('assets/js/backend/youtube/create_category.js'));
    	}

        $str_query = "select * ";
        $str_query .= "from youtube_categories";
        $str_query .= " where id = '".$id."'";

        //echo $str_query;exit;

        $query = $this->db->query($str_query);

    	if($this->input->post(NULL,FALSE)){
    		//print_r($this->input->post());exit;
            if($id){
                $this->db->update('youtube_categories',array(
                    'name'=>$this->input->post('name'),
                    'description'=>$this->input->post('description'),
                    'search_keyword'=>$this->input->post('search_keyword'),
                    'ordinal'=>$this->input->post('ordinal'),
                    'active'=>$this->input->post('active'),
                    'updated'=>date('Y-m-d H:i:s')
                ),array('id'=>$id));

                if($_FILES['cover_image']['error'] == 0){
                    $this->uploadYoutubeCategoryCoverImage(array(
                        'id'=>$id,
                        'type'=>'edit'
                    ));
                }

                $this->msg->add(__('Edit youtube category success'),'success');
                redirect($this->uri->uri_string());
            }else{
                $this->db->insert('youtube_categories',array(
                    'name'=>$this->input->post('name'),
                    'description'=>$this->input->post('description'),
                    'search_keyword'=>$this->input->post('search_keyword'),
                    'ordinal'=>$this->input->post('ordinal'),
                    'active'=>$this->input->post('active'),
                    'created'=>date('Y-m-d H:i:s')
                ));

                if($_FILES['cover_image']['error'] == 0){
                    $this->uploadYoutubeCategoryCoverImage(array(
                        'id'=>$this->db->insert_id(),
                        'type'=>'create'
                    ));

                }

                $this->msg->add(__('Create youtube category success'),'success');
                redirect($this->uri->uri_string());
            }
    	}

        //print_r($query->row());exit;

    	$data = array(
            'youtube_category'=>$query->row()
    	);


    	$this->template->content->view('backend/youtube/create_category',$data);
    	$this->template->publish();




    }

    private function uploadYoutubeCategoryCoverImage($data = array()){
                $category_id = $data['id'];

                if(!is_dir('uploaded/youtube_category/'.$category_id.'')){
                    mkdir('uploaded/youtube_category/'.$category_id.'',0777,true);
                }
                clearDirectory('uploaded/youtube_category/'.$category_id.'/');
                                                     
                $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
                $config['upload_path'] = 'uploaded/youtube_category/'.$category_id.'/';
                $config['allowed_types'] = 'gif|jpg|png';
                $config['max_size'] = '2000';
                $this->load->library('upload',$config);
                if(!$this->upload->do_upload('cover_image')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->msg->add($error['error'], 'error');

                    if(file_exists('uploaded/youtube_category/'.$category_id)){
                            rmdir('uploaded/youtube_category/'.$category_id);
                    }
                    if($data['type'] == 'create'){
                        $this->db->delete('youtube_categories',array(
                            'id'=>$category_id
                        ));
                    }
                    if($data['type']=='edit'){
                        redirect(base_url('backend/'.$this->controller.'/editCategory/'.$category_id)); 
                    }else{
                        redirect(base_url('backend/'.$this->controller.'/createCategory'));
                    }
                }else{
                    $data_upload = array('upload_data' => $this->upload->data());

                    $this->db->update('youtube_categories',array(
                        'cover_image'=>$data_upload['upload_data']['file_name']
                    ),array('id'=>$category_id));
                    
                }
    }

}