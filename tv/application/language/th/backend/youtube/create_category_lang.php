<?php

$lang['Category List'] = "Category List";
$lang['Create category'] = "Create category";
$lang['Category Name'] = "Category Name";
$lang['Category Description'] = "Category Description";
$lang['Search Keyword'] = "Search Keyword";
$lang['Ordinal'] = "Ordinal";
$lang['Status'] = "Status";
$lang['Edit category'] = "Edit category";
$lang['Cover Image'] = "Cover Image";
$lang['pls_select_file'] = "pls_select_file";
$lang['confirm_delete_image'] = "confirm_delete_image";
$lang['Image Preview'] = "Image Preview";
