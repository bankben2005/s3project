<?php

$lang['Category List'] = "Category List";
$lang['Create Category'] = "Create Category";
$lang['Image'] = "Image";
$lang['Category Name'] = "Category Name";
$lang['Search Keyword'] = "Search Keyword";
$lang['Updated'] = "Updated";
$lang['Status'] = "Status";
$lang['Ordinal'] = "Ordinal";
$lang['Edit ordinal'] = "Edit ordinal";
$lang['Image Preview'] = "Image Preview";
