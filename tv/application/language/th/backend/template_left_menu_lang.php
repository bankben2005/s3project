<?php

$lang['Dashboard'] = "แผงควบคุม";
$lang['Channel Management'] = "จัดการรายการ";
$lang['Channels'] = "ช่องรายการ";
$lang['Channel Categories'] = "หมวดหมู่รายการ";
$lang['Advertisment'] = "รายการโฆษณา";
$lang['Advertisment Management'] = "จัดการโฆษณา";
$lang['Advertisment Zone'] = "รายการโซนโฆษณา";
$lang['Advertisment Banner'] = "รายการแบนเนอร์โฆษณา";
$lang['Channel Ordinal'] = "การจัดเรียงลำดับช่องรายการ";
$lang['Youtube categories management'] = "จัดการหมวดหมู่ Youtube";
$lang['API Management'] = "API Management";
$lang['Youtube API'] = "Youtube API";
$lang['Youtube API Unactive'] = "Youtube API Unactive";
$lang['Video Download API'] = "Video Download API";
