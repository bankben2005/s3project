<?php

$lang['Channel List'] = "ช่องรายการ";
$lang['Logo'] = "โลโก้";
$lang['Name'] = "ชื่อ";
$lang['Country'] = "ประเทศ";
$lang['Status'] = "สถานะ";
$lang['Create Channel'] = "เพิ่มช่องรายการ";
$lang['Band Type'] = "Band Type";
$lang['KU-Band'] = "KU-Band";
$lang['C-Band'] = "C-Band";
$lang['Channel ascii code'] = "Channel ascii code";
