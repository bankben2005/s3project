<?php

$lang['Channel Ordinal List'] = "รายการจัดเรียงช่องรายการ";
$lang['Channel List'] = "รายการจัดเรียงช่องรายการ";
$lang['Create Channel'] = "Create Channel";
$lang['Logo'] = "โลโก้";
$lang['Name'] = "ช่อง";
$lang['C-Band Ordinal'] = "C-Band (ลำดับ)";
$lang['KU-Band Ordinal'] = "KU-Band (ลำดับ)";
$lang['Status'] = "สถานะ";
$lang['Band Type'] = "Band Type";
$lang['Mobile App Ordinal'] = "การจัดลำดับบนมือถือ";
