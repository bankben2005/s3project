<?php

$lang['Video Download API'] = "Video Download API";
$lang['EditDownload'] = "EditDownload";
$lang['Channel'] = "Channel";
$lang['Alias Name'] = "Alias Name";
$lang['Description'] = "Description";
$lang['Channel Name'] = "Channel Name";
$lang['Channel Description'] = "Channel Description";
$lang['Channel URL'] = "Channel URL";
$lang['Channel Category'] = "Channel Category";
