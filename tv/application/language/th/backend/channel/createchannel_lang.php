<?php

$lang['Channel List'] = "Channel List";
$lang['Create'] = "Create";
$lang['Channel'] = "Channel";
$lang['Channel Logo'] = "Channel Logo";
$lang['pls_select_file'] = "pls_select_file";
$lang['Channel Name'] = "Channel Name";
$lang['Channel Description'] = "Channel Description";
$lang['Band Type'] = "Band Type";
$lang['KU-Band'] = "KU-Band";
$lang['C-Band'] = "C-Band";
$lang['Channel ASCII Code'] = "Channel ASCII Code";
$lang['ALL'] = "ALL";
