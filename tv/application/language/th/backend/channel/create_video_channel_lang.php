<?php

$lang['Video Download'] = "Video Download";
$lang['Create'] = "Create";
$lang['Channel'] = "Channel";
$lang['Channel Logo'] = "Channel Logo";
$lang['pls_select_file'] = "pls_select_file";
$lang['Channel Name'] = "Channel Name";
$lang['Channel Description'] = "Channel Description";
$lang['Band Type'] = "Band Type";
$lang['KU-Band'] = "KU-Band";
$lang['C-Band'] = "C-Band";
$lang['ALL'] = "ALL";
$lang['Channel ASCII Code'] = "Channel ASCII Code";
$lang['Video Download API'] = "Video Download API";
$lang['Channel File'] = "Channel File";
$lang['Channel File (.json)'] = "Channel File (.json)";
$lang['Channel Alias Name'] = "Channel Alias Name";
$lang['Import Channel File (.json)'] = "Import Channel File (.json)";
$lang['Description'] = "Description";
$lang['Alias Name'] = "Alias Name";
$lang['CreateDownload'] = "CreateDownload";
