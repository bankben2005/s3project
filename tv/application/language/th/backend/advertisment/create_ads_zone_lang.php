<?php

$lang['Advertisment Zone List'] = "Advertisment Zone List";
$lang['Create'] = "Create";
$lang['Advertisment Zone'] = "Advertisment Zone";
$lang['Advertisment Zone Name'] = "Advertisment Zone Name";
$lang['Advertisment Zone Description'] = "Advertisment Zone Description";
$lang['Width Spec'] = "Width Spec";
$lang['Height Spec'] = "Height Spec";
$lang['Status'] = "Status";
$lang['Create advertisment zone success'] = "Create advertisment zone success";
$lang['Advertisment'] = "Advertisment";
$lang['Edit'] = "Edit";
$lang['Update advertisment zone success'] = "Update advertisment zone success";
