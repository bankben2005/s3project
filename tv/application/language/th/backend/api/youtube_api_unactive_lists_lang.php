<?php

$lang['Yotube API List'] = "Yotube API List";
$lang['Create Youtube API'] = "Create Youtube API";
$lang['API KEY'] = "API KEY";
$lang['Over limit status'] = "Over limit status";
$lang['Gmail Account'] = "Gmail Account";
$lang['Last updated by'] = "Last updated by";
$lang['Last updated os'] = "Last updated os";
$lang['Updated'] = "Updated";
$lang['Status'] = "Status";
$lang['Yotube API Unactive List'] = "Yotube API Unactive List";
$lang['TOTAL'] = "TOTAL";
$lang['keys'] = "keys";
$lang['Gmail Password'] = "Gmail Password";
$lang['Created'] = "Created";
$lang['Remark'] = "Remark";
$lang['Search'] = "Search";
$lang['Select Status'] = "Select Status";
$lang['Blocked'] = "Blocked";
$lang['Key Invalid'] = "Key Invalid";
$lang['Not Configured'] = "Not Configured";
$lang['Over Limited'] = "Over Limited";
$lang['Unactive'] = "Unactive";
$lang['TEST API'] = "TEST API";
$lang['Are you sure to delete?'] = "Are you sure to delete?";
$lang['Available'] = "Available";
