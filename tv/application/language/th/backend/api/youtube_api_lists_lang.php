<?php

$lang['Yotube API List'] = "Yotube API List";
$lang['Create Youtube API'] = "Create Youtube API";
$lang['API KEY'] = "API KEY";
$lang['Over limit status'] = "Over limit status";
$lang['Gmail Account'] = "Gmail Account";
$lang['Last updated by'] = "Last updated by";
$lang['Updated'] = "Updated";
$lang['Over Limited'] = "Over Limited";
$lang['Available'] = "Available";
$lang['Are you sure to delete?'] = "Are you sure to delete?";
$lang['Delete youtube api key success'] = "Delete youtube api key success";
$lang['TEST API'] = "TEST API";
$lang['Status'] = "Status";
$lang['Active'] = "Active";
$lang['Last updated os'] = "Last updated os";
$lang['Create Multiple Youtube API'] = "Create Multiple Youtube API";
$lang['Create Multiple API Key'] = "Create Multiple API Key";
$lang['Gmail Password'] = "Gmail Password";
$lang['Add Key'] = "Add Key";
$lang['Created'] = "Created";
$lang['Import from xlsx file'] = "Import from xlsx file";
$lang['Import API Key from xlsx file'] = "Import API Key from xlsx file";
$lang['Example File'] = "Example File";
$lang['Import youtube api key success'] = "Import youtube api key success";
$lang['TOTAL'] = "TOTAL";
$lang['keys'] = "keys";
$lang['Select Status'] = "Select Status";
$lang['Gmail account..'] = "Gmail account..";
$lang['Search'] = "Search";
$lang['Reset'] = "Reset";
