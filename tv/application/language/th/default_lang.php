<?php

$lang['Please set permission for this user,before access to backend'] = "กรุณาตั้งค่าสิทธิการเข้าถึงเมนูให้ User ก่อนเข้าใช้งานระบบ";
$lang['Authentication fail,Please try again.'] = "อีเมล์หรือหรัสผ่านไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง";
$lang['Success'] = "สำเร็จ";
$lang['Active'] = "Active";
$lang['Unactive'] = "Unactive";
$lang['Submit'] = "Submit";
$lang['upload_no_file_selected'] = "upload_no_file_selected";
$lang['Close'] = "Close";
$lang['Save'] = "Save";
$lang['File column is invalid,Please check file column from example file'] = "File column is invalid,Please check file column from example file";
$lang['upload_no_filepath'] = "upload_no_filepath";
$lang['upload_invalid_filetype'] = "upload_invalid_filetype";

$lang['Default ลิงค์สามารถมีได้แค่ URL เดียว , กรุณาปิดการใช้งานลิงค์อื่น'] = "Default ลิงค์สามารถมีได้แค่ URL เดียว , กรุณาปิดการใช้งานลิงค์อื่น";
$lang['Default ลิงค์สามารถมีได้แค่ URL เดียว , กรุณาปิดการใช้งานลิงค์อื่นก่อน'] = "Default ลิงค์สามารถมีได้แค่ URL เดียว , กรุณาปิดการใช้งานลิงค์อื่นก่อน";
