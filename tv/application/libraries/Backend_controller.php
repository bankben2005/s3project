<?php
     class Backend_controller extends CI_Controller{
            
             public $meta_title = '';
             public $meta_keywords = '';
             public $meta_description = '';
             public $data = array();
             public $page_num = 10;
             public $controller = '';
             public $method = '';
             public $language = 'th';
             public $default_language;
             public $site_setting = '';
             public $account_data = array();
             private $conn_mysql;
             public $event_ability;
         public function __construct() {
                    parent::__construct();
                    $CI = & get_instance();
                    $this->setController($CI->router->class);
                    $this->setMethod($CI->router->method);

                    $this->load->library(array('Template','Msg', 'Lang_controller'));
                    $this->load->helper(array('lang', 'our', 'inflector','html','cookie','url'));
                    
                    $this->template->set_template('backend/template');

                    $this->_checkAlready_signin();
                    $this->_getAccountData();

                    //$this->checkMenuPermission();

                    // print_r($this->session->userdata('member_data'));
                    $this->_load_js();
                    $this->_load_css();

                    //print_r($this->session->userdata());

                    //print_r($this->session->userdata('member_data'));
        }

        /**
         * load javascript
         */
        public function _load_js() {
            $this->template->javascript->add(base_url('assets/vendor/jquery/jquery.min.js'));
            $this->template->javascript->add(base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'));
            $this->template->javascript->add(base_url('assets/js/sb-admin.min.js'));
        }
        /**
         * load style sheet
         */
        public function _load_css() {            
            $this->template->stylesheet->add(base_url('assets/vendor/bootstrap/css/bootstrap.min.css'));
            $this->template->stylesheet->add(base_url('assets/vendor/font-awesome/css/font-awesome.min.css'));
            
            $this->template->stylesheet->add(base_url('assets/css/sb-admin.css'));
        }

        public function loadSweetAlert(){
            $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
        }

        public function loadDataTableCSS(){
            $this->template->stylesheet->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.css'));
        }
        public function loadDataTableJS(){
            $this->template->javascript->add(base_url('assets/vendor/datatables/jquery.dataTables.js'));
            $this->template->javascript->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.js'));
            $this->template->javascript->add(base_url('assets/js/sb-admin-datatables.min.js'));
        }

        public function loadImageUploadStyle(){
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_common.css'));
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_hover.css'));

        }
        public function loadImageUploadScript(){
            $this->template->javascript->add(base_url('assets/js/fileupload_script.js'));
        }

        public function loadValidator(){
        $this->template->stylesheet->add(base_url('assets/css/bootstrapvalidator/bootstrapValidator.min.css'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/bootstrapValidator.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/language/th_TH.js'));
        }

        protected function loadBootstrapFileuploadMasterScript(){
         //$this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/plugins/sortable.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/fileinput.min.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/locales/th.js'));

        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/themes/explorer-fa/theme.js'));
        $this->template->javascript->add(base_url('assets/js/bootstrap-fileupload-master/themes/fa/theme.js'));
        }
        protected function loadBootstrapFileuploadMasterStyle(){
            $this->template->stylesheet->add(base_url('assets/css/bootstrap-fileupload-master/fileinput.min.css'));

            $this->template->stylesheet->add(base_url('assets/js/bootstrap-fileupload-master/themes/explorer-fa/theme.css'));
        }

        public function _checkAlready_signin(){
            if(!$this->session->userdata('user_id')){
                redirect(base_url());
            }
        }
        public function _getAccountData(){
            if($this->session->userdata('member_data') && empty($this->account_data)){
                $this->account_data = $this->session->userdata('member_data');
            }

        }
        public function getData($key = null) {
            if (is_null($key) || empty($key))
                return $this->data;
            else
                return $this->data[$key];
        }

        public function setData($key, $data) {
            $this->data[$key] = $data;
        }
        public function getController() {
            return $this->controller;
        }

        public function setController($controller) {
            $this->setData('controller', $controller);
            $this->controller = $controller;
        }

        public function getMethod() {
            return $this->method;
        }

        public function setMethod($method) {
            $this->setData('method', $method);
            $this->method = $method;
        }

        public function config_page($total, $cur_page) {

        
                $config['base_url'] = base_url();
                $config['total_rows'] = $total;
                $config['per_page'] = $this->page_num;

                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['num_tag_open'] = '<li class="page-item">';
                $config['num_tag_close'] = '</li>';

                $config['prev_tag_open'] = '<li class="paging_btn">';
                $config['prev_tag_close'] = '</li>';
                $config['prev_link'] = '« ' . __('Previous', 'b2c_default');

                $config['cur_tag_open'] = '<li class="active"><a class="page-link">';
                $config['cur_tag_close'] = '</a></li>';
                $config['next_link'] = __('Next', 'b2c_default') . ' »';
                $config['next_tag_open'] = '<li class="paging_btn">';
                $config['next_tag_close'] = '</li>';
                $config['num_links'] = 5;

                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['first_link'] = __('First', 'b2c_default');
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['last_link'] = __('Last', 'b2c_default');
                $config['cur_page'] = $cur_page;
                $this->data['config_page'] = $config;
                $this->setData('config_page', $config);
            }



}