$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('form[name="edit_channel_form]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                
                name:{
                    validators:{
                        notEmpty: {
                            message:''
                        }
                    }
                },
                description:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                }
            }
    });


    // $('form[name="create-channel-form"]').submit(function(e){
    //     e.preventDefault();

    // });

});