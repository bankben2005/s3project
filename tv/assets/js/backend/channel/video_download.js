$(document).ready(function(){
    /**set download link data */
    var status = ajaxGetDownloadLinkData();
   
    if(status == 0){
      $('#dataTableLinkLog').data.reload();
    }
    /* end */

    /* set default datatable value */
    setDefaultDTValue();
    /* eof set default datatable value */
});
var base_url = $('input[name="base_url"]').val();
/* set default dt value */
function setDefaultDTValue(){
    
    var parent_id = $('input[name=groupOfStream]:checked').data('id');
    
    if(parent_id > 0){
        /* config base url  */
        var base_url = $('input[name="base_url"]').val();	
        /* set dt value */
        ajaxGetContentData(parent_id,base_url);
        /* eof set dt value */
    }
}
/* eof set default dt value */

/* arrange by dom text */
/* Create an array with the values of all the input boxes in a column */
$.fn.dataTable.ext.order['dom-text'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val();
    } );
}


/* Create an array with the values of all the input boxes in a column, parsed as numbers */
$.fn.dataTable.ext.order['dom-text-numeric'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('input', td).val() * 1;
    } );
}
/* Create an array with the values of all the select options in a column */
$.fn.dataTable.ext.order['dom-select'] = function  ( settings, col )
{
    return this.api().column( col, {order:'index'} ).nodes().map( function ( td, i ) {
        return $('select', td).val();
    } );
}
/* eof arrange by dom text  */

	/* ajax get content data */
    function ajaxGetContentData(channel_id , base_url){

        var dataTable_ = $('#dataTable').DataTable( {
        "bDestroy": true,
        "bPaginate":false,
        "bFilter":false,
        "bInfo" : false,
        "responsive": true ,
        "columns": [
              
                { "width": "20%" },
                { "width": "30%" },
                { "width": "15%" },
                { "width": "10%" },
                { "width": "10%" },
                { "width": "10%" ,"orderDataType": "dom-text-numeric" },
                { "width": "5%" , "className" : "text-center" },
            ],
            "columnDefs": [ 
                {
                    "targets": 0,
                    "orderable": false,
                  
                } ,
                
                {
                    "targets": 1,
                    "orderable": false,
                  
                } ,
                {
                    "targets": 2,
                    "orderable": false,
                  
                } ,
                {
                    "targets": 3,
                    "orderable": false,
                   
                } ,
                {
                    "targets": 4,
                    "orderable": false
                } ,
                {
                    "targets": 5,
                    "orderable": false
                } ,
                {
                    "targets": 6,
                    "orderable": false
                }  
            ],
            
            "order": [[ 5, "asc" ]]
            
        });
        dataTable_.clear();
        /* set id on insert channel  */
        $('#syncChannel').attr('data-id',channel_id);
        /* eof set id on insert channel  */
    
    
        /* set id on insert channel  */
        $('#insertChannel').attr('data-id',channel_id);
        /* eof set id on insert channel  */
        
        /* set append parent id to eventEditInBoundManageLink  */
        $('#eventEditInBoundManageLink').attr('data-parentid',channel_id);
        /* eof set append parent id to eventEditInBoundManageLink   */

      

        if($('#dataTable tbody .dataTables_empty').length){
            $('.dataTables_empty').hide()
        }
        $('#dataTable > #bodyData').html("");
        var html = "";
            $.ajax({
                url: base_url+"backend/admin_channel/ajaxGetChannelListData",
                type: "post",
                data: {'channel_id':channel_id} ,
                async:true,
                dataType:'json',
                success: function (response) {
                    console.log('==response==');
                    console.log(response);
                    if(response.channel_data){
                        var channel_data = JSON.parse(response.channel_data);
                        //var content = JSON.parse(channel_data);
                        if(channel_data[0].content){
                            var prepareContent = channel_data[0].content;
                            var parent_id  = channel_data[0].parent_id;
                            var content = JSON.parse(prepareContent);
                            $.each(content.Channels, function(i, item) {
                                 /* HTML BUILDER EDIT AND DELETE BUTTON */
                                 var html ="";
                                //  html += '<a href="';
                                //  html += base_url + 'backend/admin_channel/edit_actual_channel_id/'+ parent_id + '/' + item.channel_id +'/"';
                                //  html += 'class="btn btn-info btn-sm "><i class="fa fa-pencil"></i>';
                                //  html += '</a>&nbsp;';
                                 html += '<a href="#" onclick="delete_actual_data('+parent_id+','+item.channel_id+')"';
                             
                                 html += 'class="btn btn-danger btn-sm "><i class="fa fa-trash"></i>';
                                 html += '</a>';
                                //channel name
                                var channel_name  = "";
                                if(item.channel_name == null || item.channel_name == undefined){
                                    channel_name = "";
                                }else{
                                    //channel_name = '<input type="text" name="channel_name[]" value="'+item.channel_name+'" class="form-control" required="required">';
                                    channel_name = '<textarea name="channel_name[]" data-channel_name_channel_id="'+item.channel_id+'"   class="form-control" required="required"  rows="3">'+item.channel_name+'</textarea>';
                                    // channel_name = '<input name="channel_name[]" class="form-control" required="required"   value="'+item.channel_name+'"  >';
                                }
                                //eof channel name
                                
                                //channel_url
                                var channel_url  = "";
                                if(item.channel_url == null || item.channel_url == undefined){
                                    channel_url = "";
                                }else{
                                    //channel_url = '<input type="text" name="channel_url[]" value="'+item.channel_url+'" class="form-control" required="required">';
                                    channel_url = '<textarea name="channel_url[]" data-channel_url_channel_id="'+item.channel_id+'"   class="form-control" rows="3" required="required">'+item.channel_url+'</textarea>';
                                }
                                //eof channel_url

                                var channel_category_id = item.channel_category;
                                var channel_category = "";
                                 //channel category
                                 if(item.channel_category == null || item.channel_category == undefined){
                                     channel_category = "";
                                 }else{
                                      
                                     channel_category +=  '<select name="channel_category[]" data-channel_category_channel_id="'+item.channel_id+'"  class="form-control" >';
                                     $.each(selectOpt, function(i, item) {
                                        var selected = "";
                                        if(item === channel_category_id){
                                            var selected = "selected";
                                        }
                                        channel_category += '<option value="'+item+'" '+selected+'>'+item+'</option>';
                                    });  
                                }
                                 //eof channel category
                                // status label
                                var statusLabel = "";
                                 if(item.channel_status == null || item.channel_status == undefined){
                                    item.channel_status = "";
                                }else{
                                    if(item.channel_status == 1){
                                        statusLabel += "<span style='font-size:50% !important;' class='badge badge-success'>Success</span>";
                                    }else{
                                        statusLabel += "<span style='font-size:50% !important;' class='badge badge-danger'>URL Not Available</span>";
                                    }
                                }
                                // eof status label
                                // time sync
                                var time_sync = "";
                                if(item.channel_sync_time == null || item.channel_sync_time == undefined){
                                    time_sync =  "";
                                }else{
                                    time_sync = "<BR> time sync: <BR><span style='font-size:75% !important;'> " +item.channel_sync_time+ "</span>";
                                }
                                //eof time sync
    
                                // a description of http status
                                var codeDescLabel = "";
                                if(item.channel_codeinfo == null || item.channel_codeinfo == undefined){
                                    item.channel_codeinfo = "";
                                }else{
                                    codeDescLabel += "<span class='badge badge-warning'>HTTP Status: " + item.channel_code + "</span><br>";
                                    codeDescLabel += "<u>Description:</u> <br>" + item.channel_codeinfo;
                                
                                }
                                // eof description of http status

                                //hard code in order
                                var in_orders ="";
                                // console.log(item.channel_order);
                                if(item.channel_order == null || item.channel_order == undefined){
                                    in_orders = "";
                                }else{
                                    in_orders = '<input type="text" name="channel_order[]"  data-order_channel_id="'+item.channel_id+'"  class="form-control" value="'+item.channel_order+'" readonly="readonly" onkeypress="validate(event)">';
                                }
                                //eof hard code in order 
    
                                 /* eof HTML BUILDER EDIT AND DELETE BUTTON */
    
                                 /* Datatable add new row */
                                dataTable_.row.add( [
                                    channel_name,
                                    channel_url,
                                    channel_category,
                                    statusLabel + time_sync,
                                    codeDescLabel,
                                    in_orders,
                                    html
                                ] ).draw( false );
                                /* eof Datatable add new row */
                            });
                        }
                    }
                    
                    $("#dataTable").append(html);
                    // $('#dataTable.table').addClass("table_fixed");
                    $('#bodyData > tr > td > input[name="channel_order[]').addClass("sort");
                   // you will get response from your php page (what you echo or print)                 
    
                },
                error: function (request, status, error) {
                console.log(request.responseText);
            }
    
    
        });
    }
      // remove class sorting
      $('#orderCol').removeClass("sorting_asc");
    /* eof ajax get content data */

    

/* ajax GetDownloadLinkData get content data */
function ajaxGetDownloadLinkData(id = null){
    
    var status = "";
    var dataTable_ = $('#dataTableLinkLog').DataTable( {
        "bDestroy": true,
        "bPaginate":false,
        "bFilter":false,
        "bInfo" : false,
      
        "columns": [
            { "width": "10%" },
            { "width": "65%" },
            { "width": "15%" },
            { "width": "10%" , "className" : "text-center" },
        ]
    });
    dataTable_.clear();
    
    if($('#dataTableLinkLog tbody .dataTables_empty').length){
        $('.dataTables_empty').hide()
    }
    $('#dataTableLinkLog > #bodyData').html("");
    var html = "";
        $.ajax({
            url: base_url+"backend/admin_channel/ajaxGetDownloadLinkData",
            type: "post",
            data: {'id':id} ,
            async:true,
            dataType:'json',
            success: function (response) {
                console.log('==response==');
                
                if(response.channel_data){
                    var channel_data = JSON.parse(response.channel_data);
                    console.log(channel_data);
                    if(channel_data != 0){
                     $.each(channel_data, function(i, item) {
                             /* HTML BUILDER EDIT AND DELETE BUTTON */
                             var html ="";

                             html += '<a href="#" onclick="delete_link_data('+item.id+')"';
                         
                             html += 'class="btn btn-danger btn-sm "><i class="fa fa-trash"></i>';
                             html += '</a>';

                             var form_url = '<input type"text" name="_url[]" data-id="'+item.id+'" class="form-control" value="'+item.links+'" >';
                             var form_status = '<select name="form_status[]" id="form_status_'+item.id+'" onchange="chaneTypeDD('+item.id+')" class="form-control">';
                             var active_selected  = "";
                             var unactive_selected = "";
                             /* status selected */
                             if(item.active == 1){
                                active_selected  = "selected";
                             }else{
                                active_selected  = "";
                             }

                             if(item.active == 0){
                                unactive_selected = "selected";
                             }else{
                                unactive_selected  = "";
                             }
                             /* eof status selected */
                             form_status += '<option value="1" '+active_selected+'>ตั้งค่าเป็น Default link</option>';
                             form_status += '<option value="0" '+unactive_selected+'>ไม่เปิดใช้งาน</option>';
                             form_status += '</select>';
                             /* eof HTML BUILDER EDIT AND DELETE BUTTON */

                             /* Datatable add new row */
                            dataTable_.row.add( [
                                item.id,
                                form_url,
                                form_status,
                                html
                            ] ).draw( false );
                            /* eof Datatable add new row */
                        });

                       
                    }else{
                        $("#dataTableLinkLog").DataTable().destroy();
                    }
                    
                    $("#dataTableLinkLog").append(html);
                }
              
                
               // you will get response from your php page (what you echo or print)                 

            },
            error: function (request, status, error) {
            console.log(request.responseText);
        }


    });

    
}

/**
 * 
 * change type status
 *  
 */
function chaneTypeDD(id){
    var sendId = "form_status_" + id;
    var sendValue = $('#'+sendId).val();
    // if formstatus that send is active then other dropdown need to change status 
    if(sendValue == 1){
        var selectFormStatus = $("#outboundLink > tr > td > select[name='form_status[]']");
        $.each(selectFormStatus, function( index, value ) {
            var selectID = value.id;
            if( selectID != sendId ){    
                $('#'+selectID).val(0).trigger('change');
            }
        });
    }
    // eof
}


 /* eof */
/* eof ajax get content data */



function ajaxInsertStreamChannel(channel_id){
    if(channel_id == 0){
        alert("Plese Select Stream file");
    }else{
        var base_url = $('input[name="base_url"]').val();
        window.location.href= base_url + 'backend/admin_channel/create_stream_channel/' + channel_id;
    }
}

function ajaxsyncStreamChannel(parent_id){
    if(parent_id == 0){
        alert("Plese Select Stream file");
    }else{
        var base_url = $('input[name="base_url"]').val();
    
        /* ajax sync channel  */
        alert('Function unavailable');
        return;
      
        $.ajax({
                url: base_url+"backend/admin_channel/ajaxsyncStreamChannel",
                type: "post",
                data: {'parent_id':parent_id} ,
                async:true,
                
                timeout: 1000000,
                beforeSend: function() { $('#ajaxLoading').show(); },
                complete: function() { $('#ajaxLoading').hide(); },
                success: function (response) {
                    console.log('==response==');
                    /* config base url  */
                    var base_url = $('input[name="base_url"]').val();	
                    /* set dt value */
                    ajaxGetContentData(parent_id,base_url);
                    alert('Sync Success');
                // you will get response from your php page (what you echo or print) 
                                

                },
                error: function(xmlhttprequest, textstatus, message) {
                    if(textstatus==="timeout") {
                        alert("got timeout");
                    } else {
                        alert(textstatus);
                    }
                }
        });
        /* eof ajax sync channel */
    }
}
/**
 * delete actual data
 */

 function delete_actual_data(parent_id,channel_id){
    var base_url = $('input[name="base_url"]').val();
    $.ajax({
            url: base_url+"backend/admin_channel/delete_actual_channel_id",
            type: "post",
            data: {'parent_id' : parent_id , 'channel_id':channel_id} ,
            async:true,
            dataType:'json',
            success: function (response) {
                console.log('==response==');
                /* config base url  */
                var base_url = $('input[name="base_url"]').val();	
                /* set dt value */
                ajaxGetContentData(parent_id,base_url);

                alert('Delete Success');
               // you will get response from your php page (what you echo or print)                 

            },
            error: function (request, status, error) {
            console.log(request.responseText);
        }


    });
 }

 function download_video_channel(parent_id){
  
        var base_url = $('input[name="base_url"]').val();
        $.ajax({
                url: base_url+"backend/admin_channel/download_video_channel",
                type: "post",
                data: {'parent_id' : parent_id} ,
                async:true,
              
                success: function (response) {
                    console.log('==response==');
                    /* config base url  */
                    var base_url = $('input[name="base_url"]').val();	
                    /* set dt value */
    
                    window.location.href = base_url+"backend/admin_channel/download_video_channel";
                   // you will get response from your php page (what you echo or print)                 
    
                },
                error: function (request, status, error) {
                console.log(request.responseText);
                console.log(status);
                console.log(error);
            }
    
    
        });
 
    
 }


 /**
 * delete actual data
 */

function delete_link_data(parent_id){
    var base_url = $('input[name="base_url"]').val();
    var status_id = $('#form_status_' + parent_id).val();
    $.ajax({
            url: base_url+"backend/admin_channel/delete_link_data",
            type: "post",
            data: {'parent_id' : parent_id , 'status_id' : status_id } ,
            async:true,
            dataType:'json',
            success: function (response) {
                console.log('==response==');
                /* config base url  */
                var base_url = $('input[name="base_url"]').val();	
                /* set dt value */
                ajaxGetDownloadLinkData(null);

                alert('Delete Success');
               // you will get response from your php page (what you echo or print)                 

            },
            error: function (request, status, error) {
            console.log(request.responseText);
        }


    });
 }

 
 /* eof delete actual data */
$('input[type=radio][name=groupOfStream]').change(function() {
    var base_url = $('input[name="base_url"]').val();
    var channel_id =  $(this).data('id');
    
    /* ajax get content data */
    ajaxGetContentData(channel_id , base_url);
    /* eof ajax get content data */

});


function submitOridinalList(){
 console.log($('#download-link'));

 var links = $('#download-link').find('input[name="_url[]"]');
 console.log()
 var arr_post = {};
 var count_default = 0;
 $('#download-link').find('input[name="_url[]"]').each(function() {
    var id = $(this).attr("data-id");
    var url = $(this).val();
    var form_status = $("#form_status_" + id).val();
    if(form_status == 1){
        count_default += 1;
    }
     
    arr_post[id] = {"id":id,"url":url,"form_status":form_status};
 });
 

 if(count_default > 1)
 {
     alert("default link สามารถมีได้ตัวเดียวเท่านั้น");
     return;
 }


 //alert(5);
 var base_url = $('input[name="base_url"]').val();
 $.ajax({
         url: base_url+"backend/admin_channel/update_link",
         type: "post",
         data: {'arr_post' : arr_post } ,
         async:true,
         dataType:'text',
         success: function (response , status) {
             console.log('==response==');
            
             console.log(response);
             /* config base url  */
             var base_url = $('input[name="base_url"]').val();	
             if(status == "success"){
                 alert("Update Success");
             }else{
                 alert("Update Fail");
             }
             /* set dt value */
                        

         },
         error: function (request, status, error) {
             console.log(status);
         console.log(request.responseText);
     }


 });
}

function submit_eventEditInBoundManageLink(){
    var parent_id = $('#eventEditInBoundManageLink').attr("data-parentid");
    
    if(parent_id == 0){
        alert('กรุณาเลือก Stream File');
    }else{
        var arr_post = [];
        $('#bodyData > tr > td > textarea[name="channel_name[]"]').each(function(i , item) {
            // channel_url
            var channel_id = $(this).attr("data-channel_name_channel_id");

            /* channel_url element */
            var channel_url_ele = $('#bodyData > tr > td > textarea[name="channel_url[]"]').filter("[data-channel_url_channel_id="+channel_id+"]");
            var channel_url = $(channel_url_ele).val()
            /* eof channel_url */

             /* channel_category element */
             var channel_category_ele = $('#bodyData > tr > td > select[name="channel_category[]"]').filter("[data-channel_category_channel_id="+channel_id+"]");
             var channel_category = $(channel_category_ele).val()
             /* eof channel_category */

              /* channel_order element */
              var channel_order_ele = $('#bodyData > tr > td > input[name="channel_order[]"]').filter("[data-order_channel_id="+channel_id+"]");
              var channel_order = $(channel_order_ele).val()
              /* eof channel_order */
            
           
             console.log(channel_id)
             arr_post[channel_id] = {"id":channel_id ,"channel_name" : $(this).val() , "channel_url" : channel_url , "channel_category" : channel_category , "channel_order" : channel_order};
         });

      
       
           
         //ajax start
         var base_url = $('input[name="base_url"]').val();
         $.ajax({
                 url: base_url+"backend/admin_channel/update_channel_streamfile",
                 type: "post",
                 data: { 'parent_id' : parent_id ,'arr_post' : JSON.stringify(arr_post)  } ,
                 async:true,
                 dataType:'text',
                 success: function (response , status) {
                     console.log('==response==');
                    
                     console.log(response);
                     /* config base url  */
                     var base_url = $('input[name="base_url"]').val();	
                     /* set dt value */
                     ajaxGetContentData(parent_id,base_url);	
                     if(status == "success"){
                         alert("Update Success");
                     }else{
                         alert("Update Fail");
                     }
                     /* set dt value */
                                
        
                 },
                 error: function (request, status, error) {
                     console.log(status);
                 console.log(request.responseText);
             }
        
        
         });
         // eof ajax


    }

}

$('#bodyData').sortable({
	stop: function( event, ui ) {
        console.log(ui);
        $('input[name="channel_order[]"]', ui.item.parent()).each(function (i) {

            $(this).val(i+1);
            console.log(i);
           
      });
  	}
});

$('#bodyData > tr > td > input[name="channel_order[]"]').sortable({
    items: ".sort"
});

function validate(evt) {
    var theEvent = evt || window.event;
    var key = theEvent.keyCode || theEvent.which;
    key = String.fromCharCode( key );
    var regex = /[0-9]|\./;
    if( !regex.test(key) ) {
      theEvent.returnValue = false;
      if(theEvent.preventDefault) theEvent.preventDefault();
    }
  }
  