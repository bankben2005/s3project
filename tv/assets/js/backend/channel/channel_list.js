function editChannel(element){
	var channel_id = $(element).data('id');
	console.log('channel_id = '+channel_id);
}
function deleteChannel(element){
	var channel_id = $(element).data('id');
}

$('.btn-view').click(function(){
	var base_url = $('input[name="base_url"]').val();
	var channel_id = $(this).data('id');
	console.log('channel_id = '+channel_id);

	 	$.ajax({
		        url: base_url+"backend/admin_channel/ajaxGetChannelData",
		        type: "post",
		        data: {'channel_id':channel_id} ,
		        async:true,
                dataType:'json',
		        success: function (response) {
		        	console.log('==response==');
		        	console.log(response);
		        	if(response.status){
		        		var txt_body = "";
		        		txt_body += '<div class="col-12 text-center">';
		        		txt_body += '<img src="http://apiservice.psisat.com/uploaded/tv/logo/'+response.channel_data.logo+'" width="80" height="80" />';
		        		txt_body += '<p><strong>Name :</strong> '+response.channel_data.name+'</p>';
		        		txt_body += '<p><strong>Description :</strong> '+response.channel_data.description+'</p>';
		        		txt_body += '<p><strong>Channel number :</strong> '+response.channel_data.channel_number+'</p>';
		        		txt_body += '<p><strong>Streamer :</strong> '+response.channel_data.streamer+'</p>';
		        		txt_body += '<p><strong>Country :</strong> '+response.channel_data.country_iso2+'</p>';
		        		txt_body += '</div>';

		        		$('.modal-title').html('').html(response.channel_data.name);
		        		$('.modal-body').html('').html(txt_body);

		        		 //location.reload();
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});
});


