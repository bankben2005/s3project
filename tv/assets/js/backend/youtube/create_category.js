$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('form[name="create-youtube-category"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                search_keyword:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                ordinal:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        integer: {
                            message: ''
                        }
                    }
                }
                
            }
    });

});

$('.pop').click(function(){
        var imgurl = $(this).parent().find('img').attr('src');
        console.log(imgurl);
        $('#imagepreview').attr('src', imgurl); // here asign the image to the modal when the user click the enlarge link
        $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function


});