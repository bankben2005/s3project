$(document).ready(function(){
	

	$('form[name="search-form-unactive"]').submit(function(e){
		e.preventDefault();
		var form_data = $(this).serialize();

		console.log(form_data);
		var encodedString = $.base64.encode(form_data);
		//console.log(encodedString);

		var current_location = window.location.href.split('?')[0];
		current_location += '?q='+encodedString;
		window.location.href = current_location;

	});

});