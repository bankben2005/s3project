$("#input_excel").fileinput({ 
    language: "th",
    uploadUrl: '',
    uploadAsync: false,
    maxFileSize: 10000,
    allowedFileExtensions: ["xls", "xlsx"],
    uploadExtraData: {
        gallary_id: ""        
    },
    dropZoneEnabled:false,    
    showUpload: false,
    showClose: false,
    showCaption: true,
    showBrowse: true,
    showUpload:false,
    showUploadedThumbs: true,
    showPreview: true
}).on('filebatchuploaderror', function(event, data, msg) {
	console.log('filebatch uploade rror event = '+event+' data = '+data+' msg = '+msg);
}).on('filebatchpreupload',function(event,data,msg){
    console.log('file batchpreupload event = '+event+' data = '+data+' msg = '+msg);
}).on('filebatchuploadcomplete',function(event,data,msg){
    console.log('filebatch upload complete = '+event+' data = '+data+' msg = '+msg);
});


$('#input_excel').on('fileselect', function(event, numFiles, label) {
    $('button.fileinput-upload-button').attr('disabled','disabled');
});


$(document).ready(function(){
	$('form[name="create-multiple-key"]').submit(function(e){
		e.preventDefault();
		submitAddMultipleKey();
	});

	$('form[name="search-form"]').submit(function(e){
		e.preventDefault();
		var form_data = $(this).serialize();

		console.log(form_data);
		var encodedString = $.base64.encode(form_data);
		//console.log(encodedString);

		var current_location = window.location.href.split('?')[0];
		current_location += '?q='+encodedString;
		window.location.href = current_location;

	});

});

function changeYoutubeOverLimitStatus(element){

	var base_url = $('input[name="base_url"]').val();
	var element = $(element);
	var overlimit_status = element.data('overlimit');
	var api_id = element.data('apiid');

	console.log(element);
	

	var post_data = {
		'over_limit_status':overlimit_status,
		'api_id':api_id
	};

		$.ajax({
		        url: base_url+"backend/admin_api/ajaxChangeOverLimitStatus",
		        type: "post",
		        data: post_data,
		        async:true,
                dataType:'json',
		        success: function (response) {
		        	// console.log(response);
		        	// return false;
		        	if(response.status){

		        		if(response.post_data.over_limit_status === '0'){
		        			element.parent().find('a').attr('data-overlimit','1');
		        			element.parent().find('a > span').remove();
		        			element.parent().find('a').append('<span class="badge badge-danger">Over Limited</span>');
		        		}else{
		        			element.parent().find('a').attr('data-overlimit','0');
		        			element.parent().find('a > span').remove();
		        			element.parent().find('a').append('<span class="badge badge-success">Available</span>');


		        		}

		        		//location.reload();
		        				// swal({
              //                       title: 'สำเร็จ!!',
              //                       text: 'แก้ไขข้อมูลสำเร็จ',
              //                       icon: "success",
              //                   }).then(function(){
              //                       location.reload();
              //                   });
		        		
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});





}

function addKeyRows(){

	var row_to_add = '<div class="clearfix"></div><div class="row">';
			row_to_add += '<div class="col-lg-5">';
				row_to_add += '<div class="form-group">';
					row_to_add += '<input type="text" name="key[]" class="form-control" placeholder="key" />';
				row_to_add += '</div>';
			row_to_add += '</div>';
			row_to_add += '<div class="col-lg-5">';
				row_to_add += '<div class="form-group">';
					row_to_add += '<input type="text" name="project_name[]" class="form-control" placeholder="project name" />';
				row_to_add += '</div>'; 
			row_to_add += '</div>';
			row_to_add += '<div class="col-lg-2">';
					row_to_add += '<a href="javascript:void(0)" class="btn btn-danger btn-block" onclick="removeRow(this)"><i class="fa fa-remove"></i></a>';
			row_to_add += '</div>';
		row_to_add += '</div>';

	$('div#key_row').append(row_to_add);
}


function removeRow(element){
	console.log(element);
	var element = $(element);
	element.parent().parent().remove();
}
function submitAddMultipleKey(){
	var form_data = $('form[name="create-multiple-key"]').serialize();
	var base_url = $('input[name="base_url"]').val();

		$.ajax({
		        url: base_url+"backend/admin_api/ajaxSaveMultipleKey",
		        type: "post",
		        data: form_data ,
		        async:true,
                dataType:'json',
		        success: function (response) {
		        	console.log('==response==');
		        	console.log(response);
		        	if(response.status){
		        				$('#createMultipleAPIKey').modal('hide');
		        		 		swal({
                                    title: 'Success!!',
                                    text: 'Create youtube api key success',
                                    icon: "success",
                                }).then(function(){
                                    location.reload();
                                });
		        	}
		           // you will get response from your php page (what you echo or print)                 

		        },
		        error: function (request, status, error) {
                console.log(request.responseText);
            }


    	});
	//console.log(form_data);
}