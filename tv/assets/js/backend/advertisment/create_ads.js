$('input[name="duration"]').daterangepicker({
    	"autoApply": true,
        timePicker: false,
        timePickerIncrement: 30,
        minDate:moment(),
        locale: {
            format: 'DD-MM-YYYY'
        }
}, function(start, end, label) {
  console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});