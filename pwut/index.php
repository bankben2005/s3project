<?php
include "Mobile_Detect.php"; 


class DetectAndRedirect{

    private $detect;

    function __construct(){
        $this->detect = new Mobile_Detect(); 

        // print_r($this->detect);exit;
        echo $this->url_exists('splus://');exit;
        

    }

    function checkDetectAndRedirect(){
        //header("location: splus://");exit;
        $checkAlreadyApplication = $this->getCurlSplus(); 



        // print_r($checkAlreadyApplication);exit;

        if($checkAlreadyApplication){
            header("location: splus://");exit;
        }else{
            if($this->detect->isiOS()){
            header("location: https://apps.apple.com/us/app/s-hybrid/id1502370631", true, 302);exit;
            }else if($this->detect->isAndroidOS()){
                header("location: https://play.google.com/store/apps/details?id=com.softtechnetwork.psis4", true, 302);exit;
            }
        }

    }

    function getCurlSplus(){
        $url = 'splus://'; 

        $ch = curl_init();  
 
        curl_setopt($ch,CURLOPT_URL,$url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    //  curl_setopt($ch,CURLOPT_HEADER, false); 
     
        $output=curl_exec($ch); 

        $httpCode = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);  // Try to get the last url
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);      // Get http status from last url

        print_r($httpCode);exit;

        if(curl_exec($ch) === false){
            $info = curl_getinfo($ch);
            print_r($info);exit;
            //return false;
            echo 'Curl error: ' . curl_error($ch);exit;
            if(curl_error($ch) != ''){
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }

    }
    function url_exists($url) {
            $result = 'false';
            $url = filter_var($url, FILTER_VALIDATE_URL);
            
            /* Open curl connection */
            $handle = curl_init($url);
            
            /* Set curl parameter */
            curl_setopt_array($handle, array(
                CURLOPT_FOLLOWLOCATION => TRUE,     // we need the last redirected url
                CURLOPT_NOBODY => TRUE,             // we don't need body
                CURLOPT_HEADER => FALSE,            // we don't need headers
                CURLOPT_RETURNTRANSFER => FALSE,    // we don't need return transfer
                CURLOPT_SSL_VERIFYHOST => FALSE,    // we don't need verify host
                CURLOPT_SSL_VERIFYPEER => FALSE     // we don't need verify peer
            ));
        
            /* Get the HTML or whatever is linked in $url. */
            $response = curl_exec($handle);
            
            $httpCode = curl_getinfo($handle, CURLINFO_EFFECTIVE_URL);  // Try to get the last url
            $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);      // Get http status from last url
            
            /* Check for 200 (file is found). */
            if($httpCode == 200) {
                $result = 'true';
            }
            
            return $result;
            
            /* Close curl connection */
            curl_close($handle);
    }

}




$DetectAndRedirect = new DetectAndRedirect();
$DetectAndRedirect->checkDetectAndRedirect();

?>

<!DOCTYPE html>
<html lang="th">

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">  
   
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
 
	<meta name="apple-mobile-web-app-capable" content="yes">
 
     <!-- Site Metas -->
    <title>S PLUS application</title>  
    <meta name="keywords" content="s+ , splus , application , psi , s4 , s2plus , s2">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Site Icons -->
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="images/apple-touch-icon.png">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <!-- Site CSS -->
    <link rel="stylesheet" href="style.css">
    <!-- ALL VERSION CSS -->
    <link rel="stylesheet" href="css/versions.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="css/responsive.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">
    
    <style>
@import url('https://fonts.googleapis.com/css?family=Kanit:100,200,300&display=swap');
    </style>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="app_version" data-spy="scroll" data-target="#navbarApp" data-offset="98">

    <!-- LOADER -->
    <!-- <div id="preloader">
        <img class="preloader" src="images/loaders/loader-app.gif" alt="">
    </div> end loader -->
    <!-- END LOADER -->
	<div class="top-bar">
		
    <header class="header header_style_01">
        <nav class="navbar header-nav navbar-expand-lg">
            <div class="container">
				<a class="navbar-brand" href="index.html"><img src="images/logos/logo-app.png" alt="image"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarApp" aria-controls="navbarApp" aria-expanded="false" aria-label="Toggle navigation">
					<span></span>
					<span></span>
					<span></span>
				</button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarApp">
                    <ul class="navbar-nav">
                        <li><a class="nav-link active" href="#home">หน้าแรก</a></li>
                        <li><a class="nav-link" href="#features">คุณสมบัติ</a></li>
                        <li><a class="nav-link" href="#screenshots">ภาพหน้าจอ</a></li>
                        <li><a class="nav-link" href="#purchase">ดาวน์โหลด</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>
	
	
    <div id="home" class="parallax first-section" style="background-image:url('uploads/bg-img.jpg');">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="big-tagline">
                        <h2>เป็นมากกว่ารีโมท</h2>
                        <p class="lead">S PLUS HYBRID แอพพลิเคชั่นที่ใช้งานแทนรีโมท พร้อมคุณสมบัติการสั่งงาน Youtube และ Video on demand ในกล่องรับสัญญาณฯ ผ่านไวไฟ</p>
                        <a data-scroll href="https://play.google.com/store/apps/details?id=com.softtechnetwork.psis4" class="btn btn-light btn-radius btn-brd"><i class="fa fa-play" aria-hidden="true"></i> GooglePlay </a>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a data-scroll href="https://apps.apple.com/us/app/s-hybrid/id1502370631" class="btn btn-light btn-radius btn-brd"><i class="fa fa-apple" aria-hidden="true"></i> AppStore</a>
                    </div>
                </div>
                <div class="app_iphone_01 wow slideInUp hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.2s">
                    <img src="uploads/app_iphone_01.png" alt="" class="img-fluid">
                </div>
                <div class="app_iphone_02 wow slideInUp hidden-xs hidden-sm" data-wow-duration="1s" data-wow-delay="0.5s">
                    <img src="uploads/app_iphone_02.png" alt="" class="img-fluid">
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->

    <div id="features" class="section wb">
        <div class="container">
            <div class="section-title text-center">
                <h3>คุณสมบัติการใช้งาน</h3>
               <!-- <p class="lead">ฟีเจอร์การใช้งาน</p>-->
            </div><!-- end title -->

            <div id="default" class="row clearfix zenith_slider">
                  
				<!--The First row-->  
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 left-row">
					<ul class="features-left">
						<li>
							<i>
							<img class="icon50" src="uploads/sat_icon.png"/>
							</i>
							<div class="fl-inner">
								<h4>ทีวีดาวเทียม (Satellite TV)</h4>
								<p>สามารถสั่งงานเพื่อดูทีวีผ่าระบดาวเทียม ทำให้การรับชมช่องรายการทีวีคมชัด ไม่มีสะดุด</p>
							</div>
						</li><!-- .highlight .left-row -->

						<li>
							<i>
							<img class="icon50" src="uploads/iptv_icon.png"/>
							</i>
							<div class="fl-inner">
								<h4>อินเตอร์เน็ตทีวี (IPTV)</h4>
								<p>ไม่จำเป็นต้องใช้เสาอากาศหรือจานดาวเทียม ก็สามารถดูทีวีได้ผ่านระบบอินเตอร์เน็ต</p>
							</div>
						</li><!-- .highlight .left-row -->
						
						
					</ul>
				</div><!-- .row .left-row -->
			  

				<!--The Second row-->
				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 right-row">
					<ul class="features-right">
						<li>
							<i>
							<img class="icon50" src="uploads/youtube_icon.png"/>
							</i>						 
							<div class="fr-inner">
								<h4>ยูทูป (Youtube)</h4>
								<p>ดูยูทูปบนทีวีได้เต็มจอ เลือกและค้นหารายการได้อย่างง่ายดาย เหมือนใช้แอพพลิเคชั่นของยูทูปเอง
								</p>
							</div>
						</li><!-- .highlight .left-row -->
						<li>
							<i><img class="icon50" src="uploads/wifi_icon.png"/></i>
							<div class="fr-inner">
								<h4>เชื่อมต่อง่ายผ่านระบบไวไฟ (Wifi Connected)</h4>
								<p>เชื่อมต่อและควบคุมกล่องรับสัญญาณฯ ผ่านระบบ Wifi และสามารถเชื่อมต่อได้หลายอุปกรณ์</p>
							</div>
						</li><!-- .highlight .left-row -->
					</ul>
				</div><!-- .row .left-row -->
			  
			</div><!--Highlights close-->
        </div><!-- end container -->
    </div><!-- end section -->


    <div class="parallax section noover" style="background-color: white;">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-7 col-sm-12 col-xs-14">
                    <div class="customwidget text-left">
                        <h1>เทคโนโลยีการเชื่อมต่อและใช้งานแอพพลิเคชั่นกับกล่องรับสัญญาณฯ</h1>
                        <p style="color:  #999;">วิธีการเชื่อมต่อระหว่างแอพพลิเคชั่นกับกล่องรับสัญญาณฯ ทางเราได้นำเทคโนโลยีการเชื่อมต่อผ่านระบบไวไฟ (Wifi 2.4Ghz) มาพร้อมกับความสามารถการสั่งงานแทนรีโมท และมีฟังก์ชั่นเพิ่มเติมอีกมากมาย ไม่ว่าจะเป็น การใช้งานแอพส่งวิดีโอยูทูป (Youtube) หรือ วิดีโอสตรีมมิ่ง (Video Streaming) ผ่านกล่องรับสัญญาณฯ เพื่อขึ้นบนหน้าจอทีวี จึงทำให้การเชื่อมต่อและการใช้งานระหว่างแอพพลิเคชั่นกับกล่องรับสัญญาณฯ นั้น ง่าย สะดวก และรวดเร็ว</p>
                        <!--<ul class="list-inline">
                            <li class="list-inline-item"><i class="flaticon-apple"></i> Available on Apple</li>
                            <li class="list-inline-item"><i class="flaticon-android"></i> Available on Android</li>
                            <li class="list-inline-item"><i class="flaticon-amazon-logo"></i> Available on Amazon</li>
                        </ul><!-- end list -->
                        <!--<a href="#features" data-scroll class="btn btn-light btn-radius btn-brd">Learn More</a>-->
                    </div>
                </div><!-- end col -->
                <div class="col-md-6 iphones hidden-sm">
                    <div class="text-center move image-center hidden-sm hidden-xs">
                        <img src="uploads/diagrams.png" alt="" class="img-fluid wow fadeInUp">
                    </div>
                </div>
            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->


    <div class="how-its-work clearfix">
        <div class="hc colon1">
            <h2>1</h2>
            <h3>เข้าแอพสโตร์</h3>
            <p class="lead">เข้าไปที่แอพสโตร์บนสมาร์ทโฟน สำหรับ iOS และ Androidy</p>
        </div><!-- end col -->

        <div class="hc colon2">
            <h2>2</h2>
            <h3>ค้นหาแอพฯ</h3>
            <p class="lead">ทำการค้นหาแอพพลิเคชั่น โดยพิมพ์คำว่า S Plus Hybrid ในช่องค้นหา (Search)</p>
        </div><!-- end col -->

        <div class="hc colon3">
            <h2>3</h2>
            <h3>ดาวน์โหลดแอพฯ</h3>
            <p class="lead">สำหรับ iOS (iPhone, iPad) ให้กดปุ่ม Get / ส่วน Android ให้กดปุ่ม Install</p>
        </div><!-- end col -->  

        <div class="hc colon4">
            <h2>4</h2>
            <h3>เพลิดเพลิน</h3>
            <p class="lead">รับชมเนื้อหาที่ใหญ่เต็มจอได้อย่างสนุกเพลิดเพลินไปกับการใช้งานที่สะดวกสบาย</p>
        </div><!-- end col -->  
    </div><!-- end how-its-work -->

    <div id="screenshots" class="section wb">
        <div class="container">
            <div class="section-title text-center">
                <h3>ภาพหน้าจอแอพพลิเคชั่น</h3>  
            </div><!-- end title -->

            <div class="owl-screenshots owl-carousel owl-theme">
                <div class="owl-screen">
                    <div class="service-widget">
                        <div class="post-media entry wow fadeIn">
                            <a href="uploads/screenshot_01.png" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                            <img src="uploads/screenshot_01.png" alt="" class="img-fluid img-rounded">
                            <div class="magnifier"></div>
                        </div>
                    </div><!-- end service -->
                </div><!-- end col -->

                <div class="owl-screen">
                    <div class="service-widget">
                        <div class="post-media entry wow fadeIn">
                            <a href="uploads/screenshot_02.png" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                            <img src="uploads/screenshot_02.png" alt="" class="img-fluid img-rounded">
                            <div class="magnifier"></div>
                        </div>
                    </div><!-- end service -->
                </div><!-- end col -->

                <div class="owl-screen">
                    <div class="service-widget">
                        <div class="post-media entry wow fadeIn">
                            <a href="uploads/screenshot_03.png" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                            <img src="uploads/screenshot_03.png" alt="" class="img-fluid img-rounded">
                            <div class="magnifier"></div>
                        </div>
                    </div><!-- end service -->
                </div><!-- end col -->

                <div class="owl-screen">
                    <div class="service-widget">
                        <div class="post-media entry wow fadeIn">
                            <a href="uploads/screenshot_04.png" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                            <img src="uploads/screenshot_04.png" alt="" class="img-fluid img-rounded">
                            <div class="magnifier"></div>
                        </div>
                    </div><!-- end service -->
                </div><!-- end col -->

                <div class="owl-screen">
                    <div class="service-widget">
                        <div class="post-media entry wow fadeIn">
                            <a href="uploads/screenshot_05.png" data-rel="prettyPhoto[gal]" class="hoverbutton global-radius"><i class="flaticon-unlink"></i></a>
                            <img src="uploads/screenshot_05.png" alt="" class="img-fluid img-rounded">
                            <div class="magnifier"></div>
                        </div>
                    </div><!-- end service -->
                </div><!-- end col -->

            </div><!-- end row -->
        </div><!-- end container -->
    </div><!-- end section -->


	<div id="purchase" class="parallax section db" style="background-image:url('uploads/pr_bg.jpg');">
        <div class="container">
            <div class="section-title text-center">
                <h3>ดาวน์โหลดได้แล้ว!</h3>
                <p class="lead">สามารถดาวน์โหลดแอพพลิเคชั่น S Plus Hybrid บนระบบ AppStore (iPhone/iPad) , GooglePlay (Android)</p>
            </div><!-- end title -->

            <div class="row">
				<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
          
                </div><!-- end col --> 
			
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="btn-buy apple-button">
                        <a href="https://play.google.com/store/apps/details?id=com.softtechnetwork.psis4"><i class="flaticon-android alignleft"></i> Download on the <strong>Google Play</strong></a>
                    </div>
            </div><!-- end col --> 
                
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="btn-buy apple-button">
                        <a href="https://apps.apple.com/us/app/s-hybrid/id1502370631"><i class="flaticon-apple alignleft"></i> Download on the <strong>App Store</strong></a>
                    </div>
                </div><!-- end col -->
                
            </div><!-- end row -->
       </div><!-- end container -->
    </div><!-- end section --> 


<!--   <div id="purchase" class="parallax section db" style="background-image:url('uploads/pr_bg.jpg');">
        <div class="container">
            <div class="section-title text-center">
                <h3>ดาวน์โหลดได้แล้ว!</h3>
                <p class="lead">สามารถดาวน์โหลดแอพพลิเคชั่น S Plus Hybrid บนระบบ AppStore (iPhone/iPad) , GooglePlay (Android) และ Huawei Appstore</p>
            </div><!-- end title -->
<!--
            <div class="row">
				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="btn-buy apple-button">
                        <a href="#"><i class="flaticon-android alignleft"></i> Download on the <strong>Google Play</strong></a>
                    </div>
                </div><!-- end col --> 
<!--			
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="btn-buy apple-button">
                        <a href="#"><i class="flaticon-apple alignleft"></i> Download on the <strong>App Store</strong></a>
                    </div>
                </div><!-- end col -->  
<!--
               <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="btn-buy apple-button">
                        <a href="#"><i class="flaticon-windows alignleft"></i> Download on the <strong>Windows</strong></a>
                    </div>
                </div><!-- end col --> 
<!--            </div><!-- end row -->
<!--        </div><!-- end container -->
<!--    </div><!-- end section --> 

    <div class="copyrights">
        <div class="container">
            <div class="footer-distributed">
                <div class="footer-left">                    
                    <p class="footer-company-name">All Rights Reserved. &copy; 2020 <a href="#">PSI Corporation</a> Design By : 
					<a href="https://www.softtechnw.com/">Softtech Network Company Limited</a></p>
                </div>
            </div>
        </div><!-- end container -->
    </div><!-- end copyrights -->

    <a href="#home" data-scroll class="dmtop global-radius"><i class="fa fa-angle-up"></i></a>

    <!-- ALL JS FILES -->
    <script src="js/all.js"></script>
    <!-- ALL PLUGINS -->
    <script src="js/custom.js"></script>
	<script src="js/zenith.js"></script>
	<script>
		$('#default').zenith({
			layout: 'default' , 
			slideSpeed: 450, 
			autoplaySpeed: 2000
		});
	</script>

</body>
</html>