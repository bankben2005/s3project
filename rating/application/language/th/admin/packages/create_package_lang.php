<?php

$lang['Create Package'] = "Create Package";
$lang['Package List'] = "Package List";
$lang['Package Name'] = "Package Name";
$lang['Package Description'] = "Package Description";
$lang['Price/Month'] = "Price/Month";
$lang['Active'] = "Active";
$lang['Status'] = "Status";
$lang['Create packages success'] = "Create packages success";
$lang['Edit Package'] = "Edit Package";
$lang['Edit packages success'] = "Edit packages success";
