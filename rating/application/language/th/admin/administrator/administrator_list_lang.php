<?php

$lang['Administrator'] = "Administrator";
$lang['Create Administrator'] = "Create Administrator";
$lang['Firstname&amp;Lastname'] = "Firstname&amp;Lastname";
$lang['Email'] = "Email";
$lang['Telephone'] = "Telephone";
$lang['Updated'] = "Updated";
$lang['Status'] = "Status";
