      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Devices')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Devices')?></li>
        </ul>
      </div>

      <div class="row">
        <div class="col-lg-12">
          <div class="tile">
            <div class="tile-body">
              <div class="card">
                        <h5 class="card-header">
                          <a data-toggle="collapse" href="#search-content" aria-expanded="true" aria-controls="search-content" id="heading-search" class="d-block">
                              <i class="fa fa-chevron-down pull-right"></i>
                              <i class="fa fa-search"></i> <?php echo __('SEARCH')?>
                          </a>
                            
                        </h5>

                        <div id="search-content" class="collapse show" aria-labelledby="heading-detail">
                          <div class="card-body">
                            <?php echo form_open('',['name'=>'search-form'])?>
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                      <?php echo form_input([
                                        'name'=>'chip_code',
                                        'class'=>'form-control',
                                        'value'=>@$search_criteria['chip_code'],
                                        'placeholder'=>'Chip Code..'
                                      ])?>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                      <?php echo form_dropdown('country_name',@$select_country_name,@$search_criteria['country_name'],'class="form-control"')?>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                      <?php echo form_dropdown('region_name',@$select_region_name,@$search_criteria['region_name'],'class="form-control"')?>

                                </div>
                                
                                <div class="col-lg-3">
                                      <?php echo form_dropdown('province_region',[
                                        '0'=>__('-- Select Province Region --'),
                                        'Bangkok'=>'Bangkok',
                                        'Northern'=>'Northern',
                                        'NorthEastern'=>'NorthEastern',
                                        'Central'=>'Central',
                                        'Eastern'=>'Eastern',
                                        'Southern'=>'Southern',
                                        'Western'=>'Western',
                                        'Undefine'=>'*Undefine'
                                      ],@$search_criteria['province_region'],'class="form-control"')?>

                                </div>
                            </div>

                            <div class="row">
                              <div class="col-lg-3">

                                <div class="form-group">
                                <?php echo form_input([
                                  'name'=>'search_latitude',
                                  'class'=>'form-control',
                                  'value'=>@$search_criteria['search_latitude'],
                                  'placeholder'=>"Latitude..."
                                ])?>
                                </div>


                              </div>

                              <div class="col-lg-3">
                                <div class="form-group">
                                  <?php echo form_input([
                                    'name'=>'search_longitude',
                                    'class'=>'form-control',
                                    'value'=>@$search_criteria['search_longitude'],
                                    'placeholder'=>"Longitude..."
                                  ])?>
                                </div>
                              </div>

                              <div class="col-lg-3">
                                <div class="form-group">
                                  <label>
                                    <?php echo form_checkbox([
                                      'name'=>'update_from_api',
                                      'value'=>1,
                                      'checked'=>(@$search_criteria['update_from_api'])?TRUE:FALSE
                                    ])?>
                                     <span>Confirm latitude longtitude</span>
                                  </label>
                                </div>
                              </div>
                            </div>

                            <div class="row">
                              <div class="col-lg-12">
                                <?php echo form_button([
                                  'type'=>'button',
                                  'class'=>'btn btn-success float-right',
                                  'content'=>__('Search'),
                                  'onclick'=>'searchDevice()'
                                ])?>
                              </div>
                            </div>
                            <?php echo form_close()?>
                          </div>
                        </div>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <div class="clearfix mb-3"></div>
              <?php if($result_count){?>
              <div class="alert alert-success">
                <strong><?php echo __('Totals')?> : </strong> <?php echo number_format($result_count)?> <?php echo __('devices')?>
              </div>
              <?php }?>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Chip Code')?></th>
                    <th><?php echo __('IP Address')?></th>
                    <th><?php echo __('Country Name')?></th>
                    <th><?php echo __('Region Name')?></th>
                    <th><?php echo __('Address')?></th>
                    <th><?php echo __('Distance from center (KM)')?></th>
                    <th><?php echo __('Created')?></th>
                    <th><?php echo __('Province Region')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($devices as $key => $row){
                    $device_data = $row->devices->get(); 
                    $province_data = new M_provinces();
                    $province_data->where('ipstack_province_name',$row->region_name)
                    ->get();

                    // print_r($row->latitude);
                    // print_r($row->longitude);
                  ?>
                    <tr>
                      <td><?php echo $device_data->ship_code?></td>
                      <td>
                        <a href="http://api.ipstack.com/<?php echo $row->ip_address?>?access_key=<?php echo @$ipstack_key?>" target="_blank">
                        <?php echo $device_data->ip_address?>
                        </a>                          
                      </td>
                      <td><?php echo $row->country_name?></td>
                      <td><?php echo $row->region_name?></td>
                      <td><?php echo $row->city.' '.$row->country_name.' '.$row->zip?></td>
                      <td><?php echo number_format(calculateDistance($row->latitude,$row->longitude,$province_data->latitude,$province_data->longitude,'K'),2);?></td>
                      <td><?php echo datetime_show($row->created)?></td>
                      <td><?php echo $row->province_region?></td>
                      <td>
                        <a href="<?php echo base_url('admin/'.$this->controller.'/view_device/'.$row->devices_id)?>" class="btn btn-secondary btn-sm" target="_blank"><i class="fa fa-pencil"></i></a>
                      </td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
              <div class="row">
                <div class="col-lg-12">
                  <?php echo $pages?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>