      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Rating Packages')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Rating Packages')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/packages/createPackage')?>" class="btn btn-success float-right"><?php echo __('Create Packages')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Package Name')?></th>
                    <th><?php echo __('Package Description')?></th>
                    <th><?php echo __('Package Price')?></th>                    
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($rating_packages as $key => $row){?>
                    <tr>
                        <td><?php echo $row->name?></td>
                        <td><?php echo $row->description?></td>
                        <td><?php echo number_format($row->price_per_month)?></td>
                        <td>
                          <a href="<?php echo base_url('admin/'.$this->controller.'/editPackage/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>