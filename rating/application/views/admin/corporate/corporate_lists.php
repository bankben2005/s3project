      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Corporate')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Corporate')?></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/corporate/createCorporate')?>" class="btn btn-success float-right"><?php echo __('Create Corporate')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Corporate Name')?></th>
                    <th><?php echo __('Corporate Description')?></th>
                    <th><?php echo __('Created')?></th>
                    <th><?php echo __('Updated')?></th>
                    <th><?php echo __('Status')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($corporate as $key => $row){?>
                      <tr>
                        <td><?php echo $row->name?></td>
                        <td><?php echo $row->description?></td>
                        <td><?php echo datetime_show($row->created)?></td>
                        <td><?php echo datetime_show($row->updated)?></td>
                        <td>
                          <?php if($row->active){?>
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td>

                        <td>
                          <a href="<?php echo base_url('admin/corporate/editCorporate/'.$row->id)?>" class="btn btn-secondary btn-sm">
                            <i class="fa fa-pencil"></i>
                          </a>
                        </td>
                      </tr>
                    <?php }?>
                </tbody>
              </table>
          </div>
        </div>
      </div>
    </div>