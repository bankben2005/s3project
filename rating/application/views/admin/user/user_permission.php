<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Edit User Permission')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('User List')?></a></li>
          <li class="breadcrumb-item active"><?php echo __('Edit User Permission')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Edit User Permission').' ('.$user->firstname.' '.$user->lastname.')'?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'user_id',
                    'value'=>$user->id
                  ))?>
                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'email_exist',
                    'value'=>__('Email has been used.Please enter other email address')
                  ))?>
                  <?php echo form_open('',array('name'=>'user-permission-form'))?>
                  <div class="row">

                    <div class="col-lg-12">
                      <table class="table table-hover table-bordered" id="sampleTable">
                          <thead>
                            <tr>
                              <th><?php echo __('Menu Name')?></th>
                              <th><?php echo __('Controller Name')?></th>
                              <th><?php echo __('Method Name')?></th>
                              <th><?php echo __('Enable/Disable')?></th>
                              <th><?php echo __('Event Ability')?></th>
                            </tr>
                          </thead>
                          <tbody>
                            <?php foreach($user_menu as $key => $row){?>
                              <tr>
                                <td><?php echo $row->name?></td>
                                <td><?php echo $row->controller_name?></td>
                                <td><?php echo $row->method_name?></td>
                                <td>
                                  <label class="switch">
                                    <?php echo form_checkbox(array(
                                      'name'=>'user_menu_permission[]',
                                      'value'=>$row->id,
                                      'checked'=>(in_array($row->id, $user_menu_permission))?TRUE:FALSE,
                                      'onclick'=>'checkUserMenuPermission(this)'
                                    ))?>
                                    <span class="slider"></span>
                                  </label>

                                </td>
                                <td></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                      </table>
                    </div>

                  </div>
                  <?php echo form_close()?>
            </div>
        </div>
</div>