      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('User Menu')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('User Menu')?></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/'.$this->controller.'/createUserMenu')?>" class="btn btn-success float-right"><?php echo __('Create User Menu')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Menu Name')?></th>
                    <th><?php echo __('Controller Name')?></th>
                    <th><?php echo __('Method Name')?></th>
                    <th><?php echo __('Updated')?></th>
                    <th><?php echo __('Status')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($user_menu as $key => $row){?>
                    <tr>
                      <td><?php echo $row->name?></td>
                      <td><?php echo $row->controller_name?></td>
                      <td><?php echo $row->method_name?></td>
                      <td><?php echo datetime_show($row->updated)?></td>
                      <td>
                        <?php if($row->active){?>
                          <span class="badge badge-success"><?php echo __('Active','default')?></span>
                        <?php }else{?>
                          <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                        <?php }?>
                      </td>
                      <td>
                        <a href="<?php echo base_url('admin/'.$this->controller.'/editUserMenu/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>
                      </td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>