      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('User')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('User')?></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/'.$this->controller.'/createUser')?>" class="btn btn-success float-right"><?php echo __('Create User')?></a>
              <div class="clearfix mb-3"></div>

              <div class="row">
                <div class="col-lg-12">
                  <?php echo message_warning($this)?>
                </div>
              </div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('AccessType')?></th>
                    <th><?php echo __('Firstname&Lastname')?></th>
                    <th><?php echo __('Email')?></th>
                    <th><?php echo __('Telephone')?></th>
                    <th><?php echo __('Updated')?></th>
                    <th><?php echo __('Status')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($user as $key => $row){?>
                      <tr>
                        <td><?php echo $row->user_accesstype->get()->name?></td>
                        <td><?php echo $row->firstname.' '.$row->lastname?></td>
                        <td><?php echo $row->email?></td>
                        <td><?php echo $row->telephone?></td>
                        <td><?php echo datetime_show($row->updated)?></td>
                        <td>
                          <?php if($row->active){?>
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td>
                        <td>
                            <?php if($row->user_accesstype_id != '1'){?>
                            <a href="<?php echo base_url('admin/'.$this->controller.'/user_permission/'.$row->id)?>" class="btn btn-info btn-sm"><i class="fa fa-key"></i> <?php echo __('User Permission')?></a>
                            <?php }?>

                            <a href="<?php echo base_url('admin/'.$this->controller.'/editUser/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>



                            <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url('admin/'.$this->controller.'/deleteUser/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>

                        </td>
                      </tr>
                    <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>