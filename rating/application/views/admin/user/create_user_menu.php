<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo ($user_menu->id)?__('Edit User Menu'):__('Create User Menu')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller.'/user_menu')?>"><?php echo __('User Menu List')?></a></li>
          <li class="breadcrumb-item active"><?php echo ($user_menu->id)?__('Edit User Menu'):__('Create User Menu')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($user_menu->id)?__('Edit User Menu').' ('.$user_menu->name.')':__('Create User Menu')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                  
                  <?php echo form_open('',array('name'=>'create-usermenu-form'))?>
                  <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label><strong><?php echo __('Menu Name')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'name',
                            'class'=>'form-control',
                            'value'=>@$user_menu->name
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Controller Name')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'controller_name',
                            'class'=>'form-control',
                            'value'=>@$user_menu->controller_name
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Method Name')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'method_name',
                            'class'=>'form-control',
                            'value'=>@$user_menu->method_name
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Status','default')?> : </strong></label>
                          <?php echo form_dropdown('active',array(
                            '1'=>__('Active','default'),
                            '0'=>__('Unactive','default')
                          ),@$user_menu->active,'class="form-control"')?>
                        </div>

                        <div class="form-group">
                          <?php echo form_button(array(
                            'type'=>'submit',
                            'class'=>'btn btn-success pull-right',
                            'content'=>__('Submit','default')
                          ))?>
                        </div>

                      </div>
                  </div>
            </div>
        </div>
</div>