      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Districts')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Districts')?></li>
        </ul>
      </div>
      <div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
    </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/amphur/createDistrict')?>" class="btn btn-success float-right"><?php echo __('Create District')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('District Name')?></th>
                    <th><?php echo __('District Name (EN)')?></th>
                    <th><?php echo __('Amphur')?></th>
                    <th><?php echo __('Latitude/Longitude')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($districts->get() as $key => $row){?>
                    <tr>
                      <td><?php echo $row->district_name?></td>
                      <td><?php echo $row->district_name_eng?></td>
                      <td><?php echo $row->amphurs->get()->amphur_name?></td>
                      <td><?php echo $row->latitude.' / '.$row->longitude?></td>
                      <td>
                        <a href="<?php echo base_url('admin/'.$this->controller.'/editDistrict/'.$row->id)?>" class="btn btn-secondary btn-sm" target="_blank"><i class="fa fa-pencil"></i></a>
                      </td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
