<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo ($advertisement_campaigns->id)?__('Edit Advertisement Campaign'):__('Create Advertisement Campaign')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller.'/advertisement_campaign')?>"><?php echo __('Channel Campaign List')?></a></li>
          <li class="breadcrumb-item active"><?php echo ($advertisement_campaigns->id)?__('Edit Advertisement Campaign'):__('Create Advertisement Campaign')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($advertisement_campaigns->id)?__('Edit Advertisement Campaign').' ('.$advertisement_campaigns->name.')':__('Create Advertisement Campaign')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open_multipart('',array('name'=>'create-advertisement-campaign-form'))?>
                    <div class="row">
                      <div class="col-lg-4">

                        <div class="form-group">
                          <label><strong><?php echo __('Name')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'name',
                            'class'=>'form-control',
                            'value'=>@$advertisement_campaigns->name
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Description')?> : </strong></label>
                          <?php echo form_textarea(array(
                            'name'=>'description',
                            'class'=>'form-control',
                            'rows'=>3,
                            'value'=>@$advertisement_campaigns->description
                          ))?>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <?php 
                          $duration_show = "";

                          if(@$advertisement_campaigns->id){
                            $start_datetime = new DateTime(@$advertisement_campaigns->start_datetime);
                            $end_datetime = new DateTime(@$advertisement_campaigns->end_datetime);
                            
                            $duration_show = $start_datetime->format('d-m-Y H:i').' - '.$end_datetime->format('d-m-Y H:i');
                          }

                        ?>
                        <div class="form-group">
                          <label><strong><?php echo __('StartTime - EndTime')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'duration',
                            'class'=>'form-control',
                            'autocomplete'=>'off',
                            'value'=>@$duration_show
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Active')?> : </strong></label>
                          <?php echo form_dropdown('active',array(
                            '1'=>__('Active','default'),
                            '0'=>__('Unactive','default')
                          ),@$advertisement_campaigns->active,'class="form-control"')?>
                        </div>

                      </div>

                      <div class="col-lg-4">
                          <div class="form-group">
                                <?php //print_r($channel_data)?>
                                  <label><?php echo __('Popup Image')?> : gif|jpg|png</label>
                                  <div class="clearfix"></div>
                                      <?php if($advertisement_campaigns->popup_image && file_exists('uploaded/advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image)){?>
                                                  <?php //echo 'aaaa';?>
                                          <div class="view view-first" style="margin:5px 0px;">
                                                              <img src="<?php echo base_url().'uploaded/advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image;?>" style="width:100%;height: 100%;">
                                                               <div class="mask">
                                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                                   <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url('admin/'.$this->controller.'/deleteAdsPopupImage/'.$advertisement_campaigns->id);?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                               </div>
                                          </div>
                                             
                                                  
                                    <?php }?>
                                              
                                  <div class="clearfix"></div>
                                              
                                  <div class="input-group">
                                                  <span class="input-group-btn">
                                                   <span class="btn btn-primary btn-file">
                                                           <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('popup_image', '', '');?>
                                                       </span>
                                                  </span>
                                                   <input type="text" class="form-control" readonly>
                                  </div>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Popup image redirect url')?> : </strong></label>
                            <?php echo form_input(array(
                              'name'=>'popup_image_url',
                              'class'=>'form-control',
                              'placeholder'=>'ex. http://www.google.com',
                              'value'=>$advertisement_campaigns->popup_image_url
                            ))?>
                          </div>

                      </div>

                    </div>



                    <div class="row">
                      <div class="col-lg-12">
                        <div class="form-group">
                            <label><strong><?php echo __('Set Channels')?> : </strong></label>

                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                  <tr>
                                    <th><?php echo __('Channel Name')?></th>
                                    <th><?php echo __('Status')?></th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <?php foreach($channels as $key => $row){?>
                                    <tr>
                                      <td><a href="<?php echo base_url('admin/channels/editChannel/'.$row->id)?>" target="_blank"><?php echo $row->channel_name?></a></td>
                                      <td>
                                        <label class="switch">
                                          <?php echo form_radio(array(
                                            'name'=>'choose_channel',
                                            'value'=>$row->id,
                                            'checked'=>(@$advertisement_campaigns->channels_id == $row->id)?TRUE:FALSE
                                          ))?>
                                          <span class="slider"></span>
                                        </label>
                                      </td>
                                    </tr>
                                  <?php }?>
                                </tbody>
                            </table>
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12">
                        <div class="form-group">
                          <?php echo form_button(array(
                            'type'=>'submit',
                            'class'=>'btn btn-success float-right',
                            'content'=>__('Submit','default')
                          ))?>
                        </div>
                      </div>
                    </div>
                    
                  <?php echo form_close()?>
            </div>
        </div>
</div>

    <!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo __('Image Preview')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <img src="" id="imagepreview" class="img-fluid" >
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>