      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Advertisment Campaign List')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Advertisment Campaign List')?></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="<?php echo base_url('admin/shopping/createAdvertisementCampaign')?>" class="btn btn-success float-right"><?php echo __('Create Advertisment Campaign')?></a>
              <div class="clearfix mb-3"></div>
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('Ads Name')?></th>
                    <th><?php echo __('Channel Name')?></th>
                    <th><?php echo __('StartTime - EndTime')?></th>
                    <th><?php echo __('Expire Status')?></th>
                    <th><?php echo __('Created')?></th>
                    <th><?php echo __('Updated')?></th>
                    <th><?php echo __('Status','default')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($ads_campaign->get() as $key => $row){?>
                    <tr>
                      <td><?php echo $row->name?></td>
                      <td><?php echo $row->channels->get()->channel_name?></td>
                      <td><?php echo $row->start_datetime.' - '.$row->end_datetime?></td>
                      <td>
                        <?php 
                          if(time() > strtotime($row->end_datetime)){
                            echo '<span class="badge badge-warning">'.__('Expired').'</span>';
                          }else if(time() >= strtotime($row->start_datetime) && time() < strtotime($row->end_datetime)){
                            echo '<span class="badge badge-success">'.__('On active').'</span>';
                          }else if(time() < strtotime($row->start_datetime)){
                            echo '<span class="badge badge-info">'.__('Not Time').'<span>';
                          }
                        ?>
                      </td>
                      <td><?php echo $row->created?></td>
                      <td><?php echo $row->updated?></td>
                      <td>
                        <?php if($row->active){?>
                          <span class="badge badge-success"><?php echo __('Active','default')?></span>
                        <?php }else{?>
                          <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                        <?php }?>
                      </td>
                      <td>

                          <a href="<?php echo base_url('admin/'.$this->controller.'/setAdvertisementImageAndVideo/'.$row->id)?>" class="btn btn-info btn-sm"><i class="fa fa-image"></i> <?php echo __('Setting images and video')?></a>
                          <a href="<?php echo base_url('admin/'.$this->controller.'/editAdvertisementCampaign/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>
                      </td>

                    </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>