<style type="text/css">
  .modal-dialog.modal-800 {
    max-width: 800px;
    margin: 30px auto;
}
</style>
      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Channels')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><?php echo __('Channels')?></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-lg-12">
          <?php echo message_warning($this)?>
        </div>
      </div>


      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <a href="javascript:void(0)" class="btn btn-success float-right" onclick="createChannelProgram(this)" data-channelid="<?php echo $channels_id?>"><?php echo __('Create Channel Program')?></a>

              <a href="javascript:void(0)" class="btn btn-success float-right mr-2" onclick="createChannelProgramMultirows(this)" data-channelid="<?php echo $channels_id?>"><?php echo __('Create with multi rows')?></a>
              <div class="clearfix mb-3"></div>

              <!-- Search Option here -->
              <div class="card">
                  <h5 class="card-header">
                      <a data-toggle="collapse" href="#collapse-example" aria-expanded="true" aria-controls="collapse-example" id="heading-example" class="d-block">
                          <i class="fa fa-chevron-down pull-right"></i>
                         <i class="fa fa-search"></i> <?php echo __('Search')?>
                      </a>
                  </h5>
                  <div id="collapse-example" class="collapse show" aria-labelledby="heading-example">
                      <div class="card-body">
                          <?php echo form_open('',['name'=>'search-program-form','method'=>'get'])?>
                          <div class="row">
                            <div class="col-lg-3">
                              <div class="form-group">
                                <label><?php echo __('Search by name')?></label>
                                <?php echo form_input([
                                  'name'=>'search_name',
                                  'class'=>'form-control',
                                  'placeholder'=>'enter program name..',
                                  'value'=>@$criteria['search_name']
                                ])?>
                              </div>
                            </div>
                           

                            <div class="col-lg-3">
                              <div class="form-group">
                                <label>
                                <?php echo form_checkbox([
                                  'name'=>'search_by_date',
                                  'value'=>1,
                                  'onclick'=>'clickCheckSearchByDate(this)',
                                  'checked'=>(@$criteria['search_by_date'])?TRUE:FALSE
                                ])?> <span><?php echo __('Search by date')?></span>
                              </label>
                                <div class="input-group date" id="search_date" data-target-input="nearest">
                                  <input type="text" name="search_date" class="form-control datetimepicker-input" data-target="#search_date" value="<?php echo (@$criteria['search_date'])?$criteria['search_date']:date('d/m/Y')?>" disabled="disabled"  />
                                  <div class="input-group-append" data-target="#search_date" data-toggle="datetimepicker">
                                      <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                  </div>
                              </div>
                              </div>
                            </div>

                            <div class="col-lg-2">
                              <div class="form-group">
                                <label>&nbsp;</label>
                                <?php echo form_button([
                                  'type'=>'submit',
                                  'class'=>'btn btn-success btn-block',
                                  'content'=>'<i class="fa fa-search"></i> '.__('Search')
                                ])?>
                              </div>  
                            </div>

                            <?php if(@$criteria){?>
                              <div class="col-lg-2">
                                <div class="form-group">
                                  <label>&nbsp;</label>
                                  <a href="<?php echo base_url($this->uri->uri_string())?>" class="btn btn-danger btn-block"><?php echo __('Reset')?></a>
                                </div>
                              </div>
                            <?php }?>
                          </div>

                          <?php echo form_close()?>
                      </div>
                  </div>
              </div>
              <!-- Eof Search Option -->

              <div class="clearfix mb-3"></div>

              <div class="alert alert-success">
                <strong><?php echo __('Result(s)')?> : </strong> <?php echo $program_count?> <?php echo __('record')?>
              </div>

              <div class="row mb-2">
                
                <div class="col-lg-1">
                  <?php echo form_dropdown('sorting',[
                    ''=>__('Sorting'),
                    'time_asc'=>__('Time asc'),
                    'time_desc'=>__('Time desc')
                  ],(@$sorting)?@$sorting:'time_asc','class="form-control" onchange="changeSorting(this.value)"')?>
                </div>

                <div class="col-lg-10">

                </div>
              </div>  

              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th>
                      <label>
                      <?php echo form_checkbox([
                        'name'=>'check_all',
                        'value'=>1,
                        'onclick'=>'checkAllEvent(this)'
                      ])?> <span>All</span> </label>
                    </th>
                    <th><?php echo __('Program Name')?></th>
                    <th><?php echo __('Date')?></th>
                    <th><?php echo __('Duration time')?></th>
                    <th><?php echo __('Status','default')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($channel_programs as $key => $row){?>
                    <tr>
                      <td>
                        <?php echo form_checkbox([
                          'name'=>'check_event[]',
                          'value'=>$row->id,
                          'onclick'=>'clickCheckEvent(this)'
                        ])?>
                      </td>
                      <td><?php echo $row->name?></td>
                      <td><?php echo $row->date?></td>
                      <td><?php echo date('H:i',strtotime($row->start_time)).' - '.date('H:i',strtotime($row->end_time))?></td>
                      <td>
                        <?php if($row->active){?>
                          <span class="badge badge-success"><?php echo __('Active','default')?></span>
                        <?php }else{?>
                          <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                        <?php }?>
                      </td>
                      <td>
                        <a href="javascript:void(0);" class="btn btn-secondary btn-sm" onclick="editChannelProgram(this)" data-rowdata='<?php echo json_encode($row->to_array(),JSON_HEX_APOS)?>'><i class="fa fa-pencil"></i></a>
                        <a href="javascript:void(0);" onclick="deleteChannelProgramBODate(this)" data-rowdata='<?php echo json_encode($row->to_array())?>' class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
                      </td>
                    </tr>
                  <?php }?>
                </tbody>
              </table>


              <div class="row">
                <div class="col-lg-4">
                  <?php echo form_open('',['name'=>'action-event-form'])?>
                    <div class="form-group">
                      <strong><?php echo __('Action Choosed')?> : </strong>
                      <?php echo form_input([
                        'type'=>'hidden',
                        'name'=>'type',
                        'value'=>'action'
                      ])?> 
                      <?php echo form_input([
                        'type'=>'hidden',
                        'name'=>'json_data',
                        'value'=>''
                      ])?>
                      <?php echo form_dropdown('action',[
                        ''=>__('Select Action'),
                        'delete'=>__('Delete')
                      ],'','class="" style="min-height:27px;"')?> 

                      <?php echo form_button([
                        'type'=>'button',
                        'content'=>__('Submit','default'),
                        'onclick'=>'submitActionEventForm(this)'
                      ])?>
                    </div>



                  <?php echo form_close()?>
                </div>
                <div class="col-lg-3">
                  
                </div>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <?php echo $pages?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>



      <!-- Modal For Create One Channel Program-->
      <div class="modal fade" id="createChannelProgramModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Create channel program')?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <?php echo form_open('',['name'=>'create-channel-program'])?>
            <?php echo form_input([
              'type'=>'hidden',
              'name'=>'hide_channels_id'
            ])?>
            <?php echo form_input([
              'type'=>'hidden',
              'name'=>'action',
              'value'=>'create'
            ])?>
            <?php echo form_input([
              'type'=>'hidden',
              'name'=>'hide_channel_programs_id'
            ])?>
            <div class="modal-body">
              
              <div class="form-group">
                <label><strong><?php echo __('Program Name')?> : </strong></label>
                <?php echo form_input([
                  'name'=>'name',
                  'class'=>'form-control'
                ])?>
              </div>

              <div class="form-group">
                <label><strong><?php echo __('Description')?> : </strong></label>
                <?php echo form_textarea([
                  'name'=>'description',
                  'class'=>'form-control',
                  'rows'=>3
                ])?>
              </div>

              <div class="row">
                <div class="col-lg-12">
                  <div class="form-group">
                    <label><strong><?php echo __('Date')?> : </strong></label>
                    <div class="input-group date" id="dateprogram" data-target-input="nearest">
                    <input type="text" name="date" class="form-control datetimepicker-input" data-target="#dateprogram" value="<?php echo date('d/m/Y')?>" />
                    <div class="input-group-append" data-target="#dateprogram" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
                  </div>
                </div>
                <div class="col-lg-6">

                  <div class="form-group">
                    <label><strong><?php echo __('Start Time')?> : </strong></label>
                    <div class="input-group date" id="start_time" data-target-input="nearest">

                      <?php echo form_input(array(
                        'name'=>'start_time',
                        'value'=>'0:00',
                        'type'=>'text',
                        'class'=>'form-control datetimepicker-input',
                        'data-target'=>'#start_time'
                      ));?>

                      <div class="input-group-append" data-target="#start_time" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="col-lg-6">
                  <div class="form-group">
                    <label><strong><?php echo __('End Time')?> : </strong></label>
                    <div class="input-group date" id="end_time" data-target-input="nearest">

                      <?php echo form_input(array(
                        'name'=>'end_time',
                        'value'=>'0:00',
                        'type'=>'text',
                        'class'=>'form-control datetimepicker-input',
                        'data-target'=>'#end_time'
                      ));?>

                      <div class="input-group-append" data-target="#end_time" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-clock-o"></i></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>


              <div class="form-group">
                <label><strong><?php echo __('Status','default')?> : </strong></label>
                <?php echo form_dropdown('active',[
                  '1'=>__('Active','default'),
                  '0'=>__('Unactive','default')
                ],'','class="form-control"')?>
              </div>    
              
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close','default')?></button>
              <button type="submit" class="btn btn-primary"><?php echo __('Save','default')?></button>
            </div>
            <?php echo form_close()?>
          </div>
        </div>
      </div>
      <!-- Eof modal for create one channel program -->

      <!-- Modal For Create Multi Row Channel Program-->
      <div class="modal fade" id="createChannelProgramMultiRowsModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-800" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Create channel program')?></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
              <?php echo form_open('',['name'=>'create-channel-multirows-program','onsubmit'=>'submitChannelProgramMultirows(event)'])?>
              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_multirows_channels_id'
              ])?>

              <div class="form-group">
                <label><strong><?php echo __('Date')?></strong></label>
                <div class="input-group date" id="datemultirows" data-target-input="nearest">
                    <input type="text" name="date" class="form-control datetimepicker-input" data-target="#datemultirows" value="<?php echo date('d/m/Y')?>"  />
                    <div class="input-group-append" data-target="#datemultirows" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>
              </div>

              <a href="javascript:void(0)" class="btn btn-success float-right" onclick="addProgramRows(this)"><i class="fa fa-plus"></i> <?php echo __('Add program rows')?></a>

              <a href="javascript:void(0)" id="btnAddDefaultProgramFromDow" class="btn btn-primary float-right" data-date="<?php echo date('d/m/Y')?>" onclick="addDefaultProgramFromDow(this)" style="margin-right: 10px;"><i class="fa fa-pencil"></i> <?php echo __('Add program from date of week schedule')?></a>


              <div class="clearfix"></div>
              <hr>

              <div id="row-program">

                <div class="row">
                  <div class="col-lg-5">
                    <div class="form-group">
                      <label><strong><?php echo __('Name')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'multirows_name[]',
                        'class'=>'form-control'
                      ])?>
                    </div>

                  </div>
                  <div class="col-lg-5">
                      <div class="row">
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label><strong><?php echo __('Start Time')?></strong></label>
                           <?php echo form_input([
                              'type'=>'time',
                              'name'=>'multirows_starttime[]',
                              'class'=>'form-control',
                              'value'=>'00:00'
                            ])?>
                          </div>
                        </div>
                        <div class="col-lg-6">
                          <div class="form-group">
                            <label><strong><?php echo __('End Time')?></strong></label>
                            <?php echo form_input([
                              'type'=>'time',
                              'name'=>'multirows_endtime[]',
                              'class'=>'form-control',
                              'value'=>'00:00'
                            ])?>
                          </div>
                        </div>
                      </div>

                  </div>

                  <div class="col-lg-2 text-right">
                    <div class="form-group">
                      <label>&nbsp;</label><br>
                      <a href="javascript:void(0)" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                    </div>
                  </div>

                

                </div>



              </div>

              <div class="form-group">
                <?php echo form_button([
                  'type'=>'submit',
                  'class'=>'btn btn-success btn-block',
                  'content'=>__('Submit','default')
                ])?>
              </div>

              <?php echo form_close()?>
            </div>

          </div>
        </div>
      </div>


