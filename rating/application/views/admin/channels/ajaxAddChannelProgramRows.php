                <div class="row">
                  <div class="col-lg-5">
                    <div class="form-group">
                      <!-- <label><strong><?php echo __('Name')?> : </strong></label> -->
                      <?php echo form_input([
                        'name'=>'multirows_name[]',
                        'class'=>'form-control'
                      ])?>
                    </div>

                  </div>
                  <div class="col-lg-5">
                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <!-- <label><strong><?php echo __('Start Time')?></strong></label> -->
                          <?php echo form_input([
                            'type'=>'time',
                            'name'=>'multirows_starttime[]',
                            'class'=>'form-control',
                            'value'=>'00:00'
                          ])?>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <!-- <label><strong><?php echo __('End Time')?></strong></label> -->
                          <?php echo form_input([
                            'type'=>'time',
                            'name'=>'multirows_endtime[]',
                            'class'=>'form-control',
                            'value'=>'00:00'
                          ])?>
                        </div>
                      </div>
                    </div>

                  </div>

                  <div class="col-lg-2 text-right">
                    <div class="form-group">
                      <!-- <label>&nbsp;</label><br> -->
                      <a href="javascript:void(0)" class="btn btn-danger" onclick="removeProgramRows(this)"><i class="fa fa-trash"></i></a>
                    </div>
                  </div>



                </div>