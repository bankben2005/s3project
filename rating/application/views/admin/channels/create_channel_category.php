<div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo ($channel_category->id)?__('Edit Channel Category'):__('Create Channel Category')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller.'/channel_categories')?>"><?php echo __('Channel Category List')?></a></li>
          <li class="breadcrumb-item active"><?php echo ($channel_category->id)?__('Edit Channel Category'):__('Create Channel Category')?></li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo ($channel_category->id)?__('Edit Channel Category Form').' ('.$channel_category->category_name.')':__('Create Channel Category Form')?> </h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">
                
                  <?php echo form_open('',array('name'=>'create-channel-category-form'))?>
                  <div class="row">
                  <div class="col-lg-4">
                      <div class="form-group">
                        <label><strong><?php echo __('Category Name')?> : </strong></label>
                        <?php echo form_input(array(
                          'name'=>'category_name',
                          'class'=>'form-control',
                          'value'=>@$channel_category->category_name
                        ))?>
                      </div>  

                      <div class="form-group">
                        <label><strong><?php echo __('Category Description')?> : </strong></label>
                        <?php echo form_textarea(array(
                          'name'=>'category_description',
                          'class'=>'form-control',
                          'value'=>@$channel_category->category_description,
                          'rows'=>4
                        ))?>
                      </div>

                      <div class="form-group">
                        <label><strong><?php echo __('Status','default')?></strong></label>
                        <?php echo form_dropdown('active',array(
                          '1'=>__('Active','default'),
                          '0'=>__('Unactive','default')                          
                        ),@$channel_category->active,'class="form-control"')?>
                      </div>

                      <div class="form-group">
                        <?php echo form_button(array(
                          'type'=>'submit',
                          'class'=>'btn btn-success float-right',
                          'content'=>__('Submit','default')
                        ))?>
                      </div>
                  </div>
                  </div>
                  <?php echo form_close()?>
            </div>
        </div>
</div>