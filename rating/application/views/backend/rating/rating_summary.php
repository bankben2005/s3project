      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Rating Summary')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('backend')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><a href="#"><?php echo __('Rating Summary')?></a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('No')?></th>
                    <th><?php echo __('Channel Number')?></th>
                    <th><?php echo __('View Seconds')?></th>
                    <th><?php echo __('POL')?></th>
                    <th><?php echo __('Service Id')?></th>
                    <th><?php echo __('VDO PID')?></th>
                    <th><?php echo __('ADO PID')?></th>
                    <th><?php echo __('Start View')?></th>                    
                    <th><?php echo __('Duration (Seconds)')?></th>
                    <th><?php echo __('Created')?></th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>