      <div class="app-title">
        <div>
          <h1><i class="fa fa-th-list"></i> <?php echo __('Devices')?></h1>
          <!-- <p>Table to display analytical data effectively</p> -->
        </div>
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('backend')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item active"><a href="#"><?php echo __('Devices')?></a></li>
        </ul>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="tile">
            <div class="tile-body">
              <table class="table table-hover table-bordered" id="sampleTable">
                <thead>
                  <tr>
                    <th><?php echo __('ChipCode')?></th>
                    <th><?php echo __('Ip Address')?></th>
                    <th><?php echo __('Created')?></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <?php foreach($devices->result() as $key => $row){?>
                      <tr>
                        <td><?php echo $row->ship_code?></td>
                        <td><?php echo $row->ip_address?></td>
                        <td><?php echo yearMonthDayFormat($row->created)?></td>
                        <td>
                          <a href="<?php echo base_url('backend/'.$this->controller.'/view_device/'.$row->devices_id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-eye"></i></a>
                        </td>

                      </tr>
                  <?php }?>
                </tbody>
              </table>
            </div>
          </div>
      </div>
    </div>