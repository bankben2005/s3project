<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Packages extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();

    	$rating_packages = new M_rating_packages();
    	$rating_packages->get();


    	$data = [
    		'rating_packages'=>$rating_packages
    	];


    	$this->template->content->view('admin/packages/packages_list',$data);
    	$this->template->publish();

    }

    public function createPackage(){
    	$this->__createPackage();
    }

    public function editPackage($id){
    	$rating_packages = new M_rating_packages($id);

    	if(!$rating_packages->id){
    		redirect($this->controller);
    	}

    	$this->__createPackage($id);
    }

    private function __createPackage($id = null){


    	$this->template->javascript->add(base_url('assets/admin/js/packages/create_package.js'));

    	$rating_packages = new M_rating_packages($id);


    	if($this->input->post(NULL,FALSE)){
    		// print_r($this->input->post());exit;
    		$rating_packages->name = $this->input->post('name');
    		$rating_packages->description = $this->input->post('description');
    		$rating_packages->show_hide_channels_ranking = (@$this->input->post('show_hide_channels_ranking'))?@$this->input->post('show_hide_channels_ranking'):0;

    		$rating_packages->show_total_device_status = (@$this->input->post('show_total_device_status'))?@$this->input->post('show_total_device_status'):0;

    		$rating_packages->show_tv_program_status = (@$this->input->post('show_tv_program_status'))?@$this->input->post('show_tv_program_status'):0;

    		$rating_packages->show_population_and_devices_status = (@$this->input->post('show_population_and_devices_status'))?@$this->input->post('show_population_and_devices_status'):0;

    		$rating_packages->show_latlon_map_status = (@$this->input->post('show_latlon_map_status'))?@$this->input->post('show_latlon_map_status'):0;

    		$rating_packages->compare_channel_menu_status = (@$this->input->post('compare_channel_menu_status'))?@$this->input->post('compare_channel_menu_status'):0; 

            $rating_packages->compare_channel_export_status = (@$this->input->post('compare_channel_export_status'))?@$this->input->post('compare_channel_export_status'):0;

    		$rating_packages->top_twenty_channels_menu_status = (@$this->input->post('top_twenty_channels_menu_status'))?@$this->input->post('top_twenty_channels_menu_status'):0;

    		$rating_packages->compare_channel_by_region_menu_status = (@$this->input->post('compare_channel_by_region_menu_status'))?@$this->input->post('compare_channel_by_region_menu_status'):0;

    		$rating_packages->realtime_viewers_menu_status = (@$this->input->post('realtime_viewers_menu_status'))?@$this->input->post('realtime_viewers_menu_status'):0;

    		




    		$rating_packages->price_per_month = $this->input->post('price_per_month');
    		$rating_packages->active = $this->input->post('active');

    		if($rating_packages->save()){

    			$txtSuccess = (@$id)?__('Edit packages success','admin/packages/create_package'):__('Create packages success','admin/packages/create_package');

    			$this->msg->add($txtSuccess,'success');
    			redirect($this->uri->uri_string());


    		}




    	}


    	$data = [
    		'rating_packages'=>$rating_packages
    	];


    	$this->template->content->view('admin/packages/create_package',$data);
    	$this->template->publish();
    }
}