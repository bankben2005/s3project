<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Device extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index($page = NULL){

        $this->template->javascript->add(base_url('assets/admin/js/jquery.base64.js'));
        $this->template->javascript->add(base_url('assets/admin/js/device/device_lists.js'));

        $this->load->library([
            'pagination'
        ]);
        $this->page_num = 10;

        
        $criteria = [];
        $devices_addresses = new M_device_addresses();
        if($this->input->get(NULL,FALSE)){
            $decode_query_string = base64_decode($this->input->get('q'));

            /* parse str*/
            parse_str($decode_query_string,$output);

            //print_r($output);exit;
            //print_r($decode_query_string);exit;
            $criteria = $output;

        }

        if(!empty($criteria)){
           // print_r($criteria);
            if($criteria['country_name'] && $criteria['country_name'] != '0' && $criteria['country_name'] != 'Undefine'){
                //echo $criteria['country_name'];exit;
                $devices_addresses->where('country_name',$criteria['country_name']);
                
            }

            if($criteria['country_name'] && $criteria['country_name'] != '0' && $criteria['country_name'] == 'Undefine'){
                //echo $criteria['country_name'];exit;
                $devices_addresses->where('country_name','');
                $devices_addresses->or_where('country_name',NULL);                
            }


            if($criteria['region_name'] && $criteria['region_name'] != '0'){
                $devices_addresses->where('region_name',$criteria['region_name']);
            }

            if($criteria['province_region'] && $criteria['province_region'] != '0' && $criteria['province_region'] != 'Undefine'){
                $devices_addresses->where('province_region',$criteria['province_region']);
            }

            if($criteria['search_latitude']){
                $devices_addresses->where('latitude',$criteria['search_latitude']);
            }

            if($criteria['search_longitude']){
                $devices_addresses->where('longitude',$criteria['search_longitude']);
            }

            if(isset($criteria['update_from_api'])){
                $devices_addresses->where('update_from_api',(isset($criteria['update_from_api']))?$criteria['update_from_api']:0);
            }




            if($criteria['province_region'] && $criteria['province_region'] != '0' && $criteria['province_region'] == 'Undefine'){
                $devices_addresses->where_not_in('province_region',[
                    'Bangkok'=>'Bangkok',
                    'Northern'=>'Northern',
                                'NorthEastern'=>'NorthEastern',
                                'Central'=>'Central',
                                'Eastern'=>'Eastern',
                                'Southern'=>'Southern',
                                'Western'=>'Western'
                ]);
            }

            if($criteria['chip_code']){
                $devices_addresses->where_related('devices','ship_code',$criteria['chip_code']);
            }





            
        }

        $clone = $devices_addresses->get_clone();
        $devicesCount = $clone->count();
        $devices_addresses->order_by('id','desc');

        $devices_addresses->limit($this->page_num,$page);
        $devices_addresses->get();

        //echo $devices_addresses->check_last_query();

        $this->setData('devices',$devices_addresses);
        $this->setData('search_criteria',$criteria);
        $this->config_page($devicesCount,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url('admin/'.$this->controller.'/index/');
        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());
        $this->setData('result_count',$devicesCount);

        $data = $this->getData();
        $data['ipstack_key'] = $this->getIpStackApiKeyAvailable();
        $data['select_country_name'] = $this->getSelectCountryName();
        $data['select_region_name'] = $this->getSelectRegionName();

        $this->template->content->view('admin/device/device_lists',$data);
        $this->template->publish();

        /* older version */

        /*
        $this->loadDataTableStyle();
        $this->loadDataTableScript();

    	$devices = new M_devices();



    	$data = array(
    		'devices'=>$devices->get()
    	);

    	$this->template->content->view('admin/device/device_lists',$data);
    	$this->template->publish();
        */

    }

    public function view_device($id){

        $this->loadDataTableStyle();
        $this->loadDataTableScript();

        $this->loadSweetAlert();
    	$this->template->javascript->add(base_url('assets/admin/js/device/view_device.js'));

    	$query = $this->db->select('*')
    	->from('devices')
    	->join('device_addresses','devices.id = device_addresses.devices_id')
    	->where('devices.id',$id)->get();

    	if($query->num_rows() <= 0){
    		redirect('admin/'.$this->controller);
    	}

    	$data = array(
    		'device_data'=>$query->row(),
            'select_region'=>$this->getSelectRegionName(),
            'all_province'=>$this->allProvinceForTable(),
            'latest_ip_address'=>$this->getCurrentIPAddressByRatingDataTable([
                'devices_id'=>$id
            ])
    	);

    	$this->template->content->view('admin/device/view_device',$data);
    	$this->template->publish();
    }

    public function ajaxUpdateDeviceAddressByIpAddress(){
        $post_data = $this->input->post();
        $api_key = $this->getIpStackApiKeyAvailable();

        $queryData = $this->db->select('*')
        ->from('device_addresses')
        ->where('id',$post_data['device_addresses_id'])
        ->get();
        $rowData = $queryData->row();

        $url = "http://api.ipstack.com/".$post_data['ip_address']."?access_key=".$api_key."";


        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
            CURLOPT_TIMEOUT=>10000
        ));
        $resp = curl_exec($curl);

        curl_close($curl);

        $response = json_decode($resp);

        if(!property_exists($response, 'error')){
            // update device addresses by ipstack data
            $province_region = "";
            $queryProvinceRegion = $this->db->select('ipstack_province_name,province_region')
            ->from('provinces')
            ->where('ipstack_province_name',$response->region_name)
            ->get();

            if($queryProvinceRegion->num_rows() > 0){
                $province_region = $queryProvinceRegion->row()->province_region;
            }

            $this->db->update('device_addresses',[
                'continent_code'=>$response->continent_code,
                'continent_name'=>$response->continent_name,
                'country_code'=>$response->country_code,
                'country_name'=>$response->country_name,
                'region_code'=>$response->region_code,
                'region_name'=>$response->region_name,
                'city'=>$response->city,
                'zip'=>$response->zip,
                'latitude'=>(!$rowData->update_from_api || $rowData->latitude == '0.000000')?$response->latitude:$rowData->latitude,
                'longitude'=>(!$rowData->update_from_api || $rowData->longitude == '0.000000')?$response->longitude:$rowData->longitude,
                'location'=>json_encode($response->location),
                'tmp_latitude'=>($response->latitude != '0.000000')?'':$rowData->tmp_latitude,
                'tmp_longitude'=>($response->longitude != '0.000000')?'':$rowData->tmp_longitude,
                'tmp_latlon_status'=>($response->latitude != '0.000000' && $response->longitude != '0.000000')?0:$rowData->tmp_latlon_status,
                'updated'=>date('Y-m-d H:i:s'),
                'province_region'=>$province_region
            ],['id'=>$post_data['device_addresses_id']]);
        }

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data,
            'url'=>$url,
            'resp'=>$resp
        ]);
    }

    public function ajaxUpdateRegionName(){
        $post_data = $this->input->post();


        $province_region = "";
        $query = $this->db->select('ipstack_province_name,province_region')
        ->from('provinces')
        ->where('ipstack_province_name',$post_data['region_name'])
        ->get();

        if($query->num_rows() > 0){
            $province_region = $query->row()->province_region;
        }


        $this->db->update('device_addresses',[
            'region_name'=>$post_data['region_name'],
            'province_region'=>$province_region,
            'updated'=>date('Y-m-d H:i:s')
        ],['id'=>$post_data['device_addresses_id']]);

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data,
            'province_region'=>$province_region
        ]);
    }

    public function ajaxUpdateProvinceRegion(){
        $post_data = $this->input->post();

        $this->db->update('device_addresses',[
            'province_region'=>$post_data['province_region'],
            'updated'=>date('Y-m-d H:i:s')
        ],['id'=>$post_data['device_addresses_id']]);


        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    public function ajaxUpdateProvinceRegionByZipcdoe(){
        $post_data = $this->input->post();

        /* find province from zipcode and get province_region */
        $query = $this->db->select('*')
        ->from('zipcode')
        ->join('provinces','zipcode.provinces_id = provinces.id')
        ->where('zipcode.zipcode',$post_data['zipcode'])
        ->limit(1)
        ->get();
        /* eof find province from zipcode and get province_region*/

        if($query->num_rows() > 0){
            $this->db->update('device_addresses',[
                'province_region'=>$query->row()->province_region,
                'updated'=>date('Y-m-d H:i:s')
            ],['id'=>$post_data['device_addresses_id']]);
        }

        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data
        ]);
    }

    private function getIpStackApiKeyAvailable(){
        $query = $this->db->select('*')
        ->from('ipstack_api')
        ->where('active',1)
        ->where('over_limit_status',0)
        ->limit(1)
        ->order_by('rand()')
        ->get();

        if($query->num_rows() > 0){
            return $query->row()->api_key;
        }else{
            return '';
        }

    }

    private function getSelectRegionName(){
        $arrSelect = [];
        $arrSelect['0'] = __('-- Select Region Name --','admin/device/view_device');
        $query = $this->db->select('region_name')
        ->from('device_addresses')
        ->where('region_name !=','')
        ->group_by('region_name')
        ->order_by('region_name')
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $arrSelect[$value->region_name] = $value->region_name;
            }
        }
        return $arrSelect;
    }

    private function allProvinceForTable(){
        $query = $this->db->select('*')
        ->from('provinces')
        ->order_by('province_name')
        ->get();
        return $query->result();
    }

    private function getSelectCountryName(){
        $arrReturn = array();
        $arrReturn["0"] = __('-- Select Country Name --','admin/device/device_lists');
        $query = $this->db->select('country_name')
        ->from('device_addresses')
        ->where('country_name !=','')
        ->group_by('country_name')
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $arrReturn[$value->country_name] = $value->country_name;
            }
        }
        $arrReturn['Undefine'] = "*Undefine";
        return $arrReturn;
    }

    private function getCurrentIPAddressByRatingDataTable($data = []){
        $rating_data_table = 'rating_data_'.date('Y').'_'.date('n');

        $query = $this->db->select('devices_id,ip_address')
        ->from($rating_data_table)
        ->where('devices_id',$data['devices_id'])
        ->order_by('id','desc')
        ->limit(1)
        ->get();

        if($query->num_rows() > 0){
            return $query->row()->ip_address;
        }else{
            return "";
        }
    }

    private function calculateDistance($lat1, $lon1, $lat2, $lon2, $unit) {

          $theta = $lon1 - $lon2;
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
          $unit = strtoupper($unit);

          if ($unit == "K") {
              return ($miles * 1.609344);
          } else if ($unit == "N") {
              return ($miles * 0.8684);
          } else {
              return $miles;
          }
    }
}