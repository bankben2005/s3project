<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class User extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

        $this->loadDataTableStyle();
        $this->loadDataTableScript();


    	$user = new M_user();

        if(@$this->admin_data['type'] == 'admin' && @$this->admin_data['email'] != 'pattanawit@psisat.com'){
            /* admin can see only user is not superadmin and can see only user that admin create */
            $user->where('created_by',$this->admin_data['id'])->get();
        }else{
            $user->get();
        }

        //echo $user->check_last_query();exit;

    	//$user->where('id !=',$this->admin_data['id']);

    	// if($this->admin_data['user_accesstype_id'] == '2'){
    	// 	$user->where('created_by',$this->admin_data['id']);
    	// }
    	//$user->get();

    	$data = array(
    		'user'=>$user
    	);

    	$this->template->content->view('admin/user/user_lists',$data);
    	$this->template->publish();

    }

    public function user_menu(){

    	$user_menu = new M_user_menu();


    	$data = array(
    		'user_menu'=>$user_menu->get()
    	);

    	$this->template->content->view('admin/user/user_menu_lists',$data);
    	$this->template->publish();
    }

    public function createUser(){
    	$this->__createUser();
    }

    public function editUser($id){

    	$user = new M_user($id);

    	if(!$user->id){
    		redirect(base_url('admin/'.$this->controller));
    	}

    	$this->__createUser($id);
    }

    public function deleteUser($id){
        $user = new M_user($id);

        if(!$user->id){
            redirect(base_url('admin/'.$this->controller));
        }

        /* write log file for delete user */
         $log_file_path = $this->createLogFilePath('DeleteRatingUser');
        $file_content = date("Y-m-d H:i:s") . ' delete by : '.$this->admin_data['id'].' user value : ' . json_encode($user->to_array()) . "\n";
        file_put_contents($log_file_path, $file_content, FILE_APPEND);
        unset($file_content);

        /* eof write log file for delete user */

        if($user->delete()){
            $this->msg->add(__('Delete user success','admin/user/user_lists'),'success');
            redirect('admin/'.$this->controller);
        }


    }

    public function user_permission($id){
    	$this->template->stylesheet->add(base_url('assets/backend/css/switch/switch.css'));
    	$this->template->javascript->add(base_url('assets/admin/js/user/user_permission.js'));

    	$user = new M_user($id);

    	if(!$user->id){
    		redirect(base_url('admin/'.$this->controller));
    	}

    	$user_menu = new M_user_menu();
    	$user_menu->where('active',1)->get();
    	$data = array(
    		'user'=>$user,
    		'user_menu'=>$user_menu,
    		'user_menu_permission'=>$this->getArrUserMenuPermission(array(
    			'user_id'=>$user->id
    		))
    	);

    	$this->template->content->view('admin/user/user_permission',$data);
    	$this->template->publish();

    }

    public function createUserMenu(){
    	$this->__createUserMenu();
    }
    public function editUserMenu($id){
    	$user_menu = new M_user_menu($id);

    	if(!$user_menu->id){
    		redirect(base_url('admin/'.$this->controller.'/user_menu'));
    	}

    	$this->__createUserMenu($id);
    }

    public function ajaxSetUserMenuPermission(){
    	$post_data = $this->input->post();

    	$user_menu_permission = new M_user_menu_permission();

    	switch ($post_data['check_status']) {
    		case 'checked':
    			$user_menu_permission->user_id =$post_data['user_id'];
    			$user_menu_permission->user_menu_id = $post_data['user_menu_id'];
    			$user_menu_permission->save();
    		break;

    		case 'unchecked':
    			/* check exist first */
    			$check_menu_permission = new M_user_menu_permission();
    			$check_menu_permission->where('user_id',$post_data['user_id'])
    			->where('user_menu_id',$post_data['user_menu_id'])->get();

    			if($check_menu_permission->id){
    				$check_menu_permission->delete();
    			}
    		break;
    		
    		default:
    			# code...
    			break;
    	}

    	echo json_encode(array(
    		'status'=>true,
    		'post_data'=>$post_data
    	));
    }

    public function ajaxSetResetPassword(){
    	$post_data = $this->input->post();

    	$user = new M_user($post_data['user_id']);

    	if($user->id){
    		$user->password = sha1($post_data['new_password']);
    		$user->save();
    	}

    	echo json_encode(array(
    		'status'=>true,
    		'post_data'=>$post_data
    	));
    }
    private function __createUserMenu($id=null){
    	$this->loadValidator();
    	$this->template->javascript->add(base_url('assets/admin/js/user/create_user_menu.js'));

    	$user_menu = new M_user_menu($id);

    	if($this->input->post(NULL,FALSE)){
    		$user_menu->name = $this->input->post('name');
    		$user_menu->controller_name = $this->input->post('controller_name');
    		$user_menu->method_name = $this->input->post('method_name');
    		$user_menu->active = $this->input->post('active');



    		if($user_menu->save()){

                


    			$txt_success = ($id)?__('Edit user menu success','admin/user/create_user_menu'):__('Create user menu success','admin/user/create_user_menu');

    			$this->msg->add($txt_success,'success');
    			redirect($this->uri->uri_string());
    		}
    	}


    	$data = array(
    		'user_menu'=>$user_menu
    	);

    	$this->template->content->view('admin/user/create_user_menu',$data);
    	$this->template->publish();

    }

    public function __createUser($id=null){
        $this->template->stylesheet->add(base_url('assets/backend/css/switch/switch.css'));
        $this->loadDataTableStyle();
        $this->loadDataTableScript();
    	$this->loadValidator();
    	$this->loadSweetAlert();

    	if($id){
    		$this->template->javascript->add(base_url('assets/admin/js/user/edit_user.js'));
    	}else{
    		$this->template->javascript->add(base_url('assets/admin/js/user/create_user.js'));
    	}

    	$user = new M_user($id);


    	if($this->input->post(NULL,FALSE)){
    		$user->user_accesstype_id = $this->input->post('user_accesstype_id');
    		$user->firstname = $this->input->post('firstname');
    		$user->lastname = $this->input->post('lastname');
    		$user->email = $this->input->post('email');
            if(!$id){$user->password = sha1($this->input->post('password'));}
    		
    		$user->telephone = $this->input->post('telephone');
            $user->created_by = $this->admin_data['id'];
            $user->rating_packages_id = $this->input->post('rating_packages_id');

            // $user->admin_status = $this->input->post('admin_status');
            $user->show_hide_channels_ranking = $this->input->post('show_hide_channels_ranking');
            // $user->show_total_device_status =$this->input->post('show_total_device_status');
            // $user->show_tv_program_status = $this->input->post('show_tv_program_status');
            // $user->show_population_and_devices_status = $this->input->post('show_population_and_devices_status');
            // $user->show_latlon_map_status = $this->input->post('show_latlon_map_status');

            
    		$user->active = $this->input->post('active');
            //print_r($this->input->post());exit;

    		if($user->save()){

                /* set user owner channels */
                if($this->input->post('owner_channel_status') == 'yes' && count($this->input->post('user_owner_channels')) > 0){
                    //print_r($this->input->post('user_owner_channels'));exit;
                    $this->setUserOwnerChannel(array(
                        'user_owner_channels'=>$this->input->post('user_owner_channels'),
                        'user_id'=>$user->id
                    ));
                }
                /* eof set user owner channels */



                /* update for parent record owner channel status*/
                if($this->input->post('owner_channel_status') == 'yes' && count($this->input->post('user_owner_channels')) > 0){
                    $update_user = new M_user($user->id);
                    $update_user->owner_channel_status = 1;
                    $update_user->owner_channel_id = $this->input->post('user_owner_channels')[0];
                    $update_user->save();
                }       
                /* eof update for parent record owner channel status*/



    			$txt_success = ($id)?__('Edit user success','admin/user/create_user'):__('Create user success','admin/user/create_user');
    			$this->msg->add($txt_success,'success');

    			redirect($this->uri->uri_string());
    		}
    	}


    	$data = array(
    		'user'=>$user,
    		'user_accesstype'=>$this->getSelectUserAccessType(),
            'channels'=>$this->getAllChannels(),
            'user_owner_channels'=>($id)?$this->getUserOwnerChannels($id):array(),
            'select_rating_packages'=>$this->getSelectRatingPackages()
    	);

        //print_r($data);exit;


    	$this->template->content->view('admin/user/create_user',$data);
    	$this->template->publish();


    }

    public function ajaxCheckEmailExist(){
    	$get_data = $this->input->get();

    	$query = $this->db->select('email,active')
    	->from('user')
    	->where('email',$get_data['email'])
    	->where('active',1)
    	->get();

    	if($query->num_rows() > 0){
    		echo json_encode(array(
    			'valid'=>false
    		));
    	}else{
    		echo json_encode(array(
    			'valid'=>true
    		));
    	}
    }

    private function getSelectUserAccessType(){
    	$arrReturn = array();
    	$arrReturn[""] = __('Select AccessType','admin/user/create_user');
    	$user_accesstype = new M_user_accesstype();
        $user_accesstype->where('id !=',1);

        // if($this->admin_data['type'] == 'admin'){
        //     $user_accesstype->where('id !=',1);
        // }
    	
        $user_accesstype->where('active',1)->get();

    	foreach ($user_accesstype as $key => $value) {
    		# code...
    		$arrReturn[$value->id] = $value->name;
    	}

    	return $arrReturn;

    }

    private function getArrUserMenuPermission($data = array()){
    	$arrData = array();
    	$user_menu_permission = new M_user_menu_permission();

    	$user_menu_permission->where('user_id',$data['user_id'])->get();

    	foreach ($user_menu_permission as $key => $value) {
    		# code...
    		array_push($arrData, $value->user_menu_id);
    	}

    	return $arrData;


    }

    private function getAllChannels(){
        $channels = new M_channels();
        $channels->where('active',1)->order_by('channel_name')->get();

        return $channels;
    }

    private function getUserOwnerChannels($user_id = 0){
        $arrReturn = array();
        $user_owner_channels = new M_user_owner_channels();
        $user_owner_channels->where('user_id',$user_id)->get();

        foreach ($user_owner_channels as $key => $value) {
            # code...
            array_push($arrReturn, $value->channels_id);
        }
        return $arrReturn;
    }

    private function setUserOwnerChannel($arrData = array()){

        /* clear all setting user_owner_channels by user */
        $this->db->delete('user_owner_channels',array(
            'user_id'=>$arrData['user_id']
        ));        
        /* eof clear all setting */

        foreach ($arrData['user_owner_channels'] as $key => $value) {
            # code...
            $user_owner_channels = new M_user_owner_channels();
            $user_owner_channels->user_id =$arrData['user_id'];
            $user_owner_channels->channels_id = $value;
            $user_owner_channels->save();
        }
        return true;

    }

    private function createLogFilePath($filename = '') {
        $log_path = './application/logs/deleteUserLogs';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function getSelectRatingPackages(){
        $arr_return = [];
        $arr_return[''] = __('Select Package','admin/user/create_user');
        $query = $this->db->select('*')
        ->from('rating_packages')
        ->where('active',1)
        ->get();

        if($query->num_rows() > 0){
            foreach ($query->result() as $key => $value) {
                # code...
                $arr_return[$value->id] = $value->name;
            }
        }

        return $arr_return;
    }

}