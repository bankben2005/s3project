<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminLibrary.php';
class Amphur extends AdminLibrary {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();

    }
    public function index(){

    	$this->loadDataTableStyle();
    	$this->loadDataTableScript();

    	$amphur = new M_amphurs();

    	$data = [
    		'amphur'=>$amphur
    	];

    	$this->template->content->view('admin/amphur/amphur_list',$data);
    	$this->template->publish();


    }

    public function createAmphur(){

    }
    public function editAmphur($id){
    	$amphur = new M_amphurs($id);

    	if(!$amphur->id){
    		redirect(base_url('admin/amphur'));
    	}

    	$this->__createAmphur($id);
    }

    public function __createAmphur($id = null){


    	$amphur = new M_amphurs($id);

        if($this->input->post(NULL,FALSE)){
            // print_r($this->input->post());exit;
            $amphur->provinces_id = $this->input->post('provinces_id');
            $amphur->amphur_name = $this->input->post('amphur_name');
            $amphur->latitude = $this->input->post('latitude');
            $amphur->longitude = $this->input->post('longitude');

            if($amphur->save()){
                $this->msg->add(__('Update amphur success','admin/amphur/create_amphur'),'success');
                $this->msg->add('Update amphur success','success');
                redirect($this->uri->uri_string());
            }
        }

    	$data = [
    		'amphur'=>$amphur,
            'select_province'=>$this->getSelectProvince()

    	];

        //print_r($data['select_province']);

    	$this->template->content->view('admin/amphur/create_amphur',$data);
    	$this->template->publish();

    }

    private function getSelectProvince(){
        $province = new M_provinces();
        $province->order_by('province_name','asc')->get();

        $arrReturn[''] = 'Select Province';

        foreach ($province as $key => $value) {
            # code...
            $arrReturn[$value->id] = $value->province_name;
        }

        return $arrReturn;
    }

}