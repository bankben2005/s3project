<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class MediaHubServices extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();
	}



	public function mediaHubGetProgramRatingAndTimeSlot(){
		// request from media hub api 

		$post_data = $this->input->post(); 

		 //print_r($post_data);exit;



		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data,
			'avg_rating'=>$this->getRatingImmediately($post_data),
			'time_slots'=>$this->getChannelTimeSlots($post_data)
		]);	



	} 

	public function mediaHubGetDefaultTimeSlot(){ 

		$post_data = $this->input->post();

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data,
			'time_slot_lists'=>$this->getAllTimeSlotAvailable($post_data)
		]);

	} 

	public function getChannelTimeSlotDataById(){
		$post_data = $this->input->post();
 		
		$arrReturn = [];

		$query = $this->db->select('*')
		->from('channel_timeslots')
		->where('id',$post_data['channel_timeslots_id'])
		->where('active',1)
		->get();

		$row = new StdClass();
		if($query->num_rows() > 0){ 

			$row = $query->row(); 
			$row->{'channel_name'} = $this->getChannelNameByChannelId([
				'channels_id'=>$row->channels_id
			]); 

		}

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data,
			'row'=>$row
		]);
	}

	public function getChannelTimeSlotHasPackageDataById(){
		$post_data = $this->input->post(); 

		$query = $this->db->select('*,channel_timeslot_packages.name as package_name,channel_timeslots.name as program_name')
		->from('channel_timeslot_has_packages')
		->join('channel_timeslot_packages','channel_timeslot_has_packages.channel_timeslot_packages_id = channel_timeslot_packages.id')
		->join('channel_timeslots','channel_timeslots.id = channel_timeslot_has_packages.channel_timeslots_id')
		->where('channel_timeslot_has_packages.id',$post_data['channel_timeslot_has_packages_id'])
		->get();

		$returnData = [];
		if($query->num_rows() > 0){
			$row = $query->row(); 

			$start_datetime = new DateTime($row->start_datetime);
			$end_datetime = new DateTime($row->end_datetime); 



			//print_r($row);exit; 
			$returnData['package_name_txt'] = $row->package_name;
			$returnData['duration_txt'] = $start_datetime->format('H:i').' to '.$end_datetime->format('H:i');
			$returnData['program_name_txt'] = $row->program_name;
			$returnData['package_cost_txt'] = $row->budget;


		}

		echo json_encode([
			'status'=>true,
			'returnData'=>$returnData,
			'post_data'=>$post_data
		]);
	}

	private function getRatingImmediately($post_data = []){ 

		$current_date = date('Y-m-d');

		$query = $this->db->select('AVG(channel_daily_rating_logs.rating) as avg_rating')
		->from('channel_daily_rating_logs')
		->join('channel_daily_rating','channel_daily_rating.id = channel_daily_rating_logs.channel_daily_rating_id')
		->where('channel_daily_rating.channels_id',$post_data['channels_id'])
		->where('channel_daily_rating.date',$current_date)
		->where('channel_daily_rating_logs.created >=',$post_data['start_datetime'])
		->where('channel_daily_rating_logs.created <=',$post_data['end_datetime'])
		->get();

		$row = $query->row();

		return ($row->avg_rating)?$row->avg_rating:0;

	} 

	private function getChannelTimeSlots($post_data = []){ 

		//print_r($post_data);exit;

		$arrChannelTimeSlot = [];

		$query = $this->db->select('*')
		->from('channel_timeslots')
		->where('channels_id',$post_data['channels_id'])
		->where('start_datetime >=',$post_data['start_datetime'])
		->where('end_datetime <=',$post_data['end_datetime'])
		->where('active',1)
		->get();

		//echo $this->db->last_query();exit; 

		//echo $query->num_rows();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				array_push($arrChannelTimeSlot, [
					'channel_timeslots_id'=>$value->id,
					'slot_duration_txt'=>date('H:i',strtotime($value->start_datetime)).' to '.date('H:i',strtotime($value->end_datetime)),
					'slot_duration_start'=>date('H:i',strtotime($value->start_datetime)),
					'slot_duration_end'=>date('H:i',strtotime($value->end_datetime)),
					'slot_program'=>$value->name,
					'slot_rating'=>$this->getSlotRatingImmediatly([
						'start_datetime'=>$value->start_datetime,
						'end_datetime'=>$value->end_datetime,
						'channels_id'=>$value->channels_id
					]),
					'slot_packages'=>$this->getSlotPackages([
						'channel_timeslots_id'=>$value->id
					])
				]);
			}


		}

		return $arrChannelTimeSlot;
	}

	private function getSlotRatingImmediatly($data = []){ 

		$current_date = date('Y-m-d');

		$query = $this->db->select('AVG(channel_daily_rating_logs.rating) as avg_rating')
		->from('channel_daily_rating_logs')
		->join('channel_daily_rating','channel_daily_rating.id = channel_daily_rating_logs.channel_daily_rating_id')
		->where('channel_daily_rating.channels_id',$data['channels_id'])
		->where('channel_daily_rating.date',$current_date)
		->where('channel_daily_rating_logs.created >=',$data['start_datetime'])
		->where('channel_daily_rating_logs.created <=',$data['end_datetime'])
		->get();

		$row = $query->row();

		return ($row->avg_rating)?$row->avg_rating:0;



	}

	private function getSlotPackages($data = []){  

		$arrReturn = [];

		$query = $this->db->select('*,channel_timeslot_has_packages.id as has_package_id')
		->from('channel_timeslot_has_packages')
		->join('channel_timeslot_packages','channel_timeslot_packages.id  = channel_timeslot_has_packages.channel_timeslot_packages_id')
		->where('channel_timeslots_id',$data['channel_timeslots_id'])
		->get(); 

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				array_push($arrReturn, [
					'package_id'=>$value->has_package_id,
					'package_time_txt'=>$value->name,
					'package_cost_txt'=>number_format($value->budget,2).' THB',
					'package_cost'=>$value->budget
				]);
			}
		}

		return $arrReturn;



	}

	private function getChannelNameByChannelId($data = []){ 

		$query = $this->db->select('id,channel_name')
		->from('channels')
		->where('id',$data['channels_id'])
		->get();

		if($query->num_rows() > 0){
			return $query->row()->channel_name;
		}else{
			return '';
		}



	}

	private function getAllTimeSlotAvailable($post_data){ 

		

	}

}