$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('form[name="create-user-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                user_accesstype_id:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                firstname: {
                    validators: {
                        notEmpty: {
                            message:''
                        }
                    }
                },
                lastname:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                },
                email:{
                    validators:{
                        remote: {
                            message: $('input[name="email_exist"]').val(),
                            url: base_url+'admin/user/ajaxCheckEmailExist'
                        },
                        notEmpty:{
                            message:''
                        }
                    }
                },
                password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
                },
                confirm_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'password',
                                message: ''
                        }
                    }
                }
            }
    });


    checkOnClickOwnerChannelStatus();

});


function checkOnClickOwnerChannelStatus(){

    


    $('input[name="owner_channel_status"]').click(function(){
        var status_val = $(this).val();
        if(status_val === 'yes'){
            $('div#panel_owner_status').show();
        }else{
            $('div#panel_owner_status').hide();
        }
    });
}

function checkToggleShowTotalDevice(element){
    // $('select[name=')
    var element = $(element);

    if(element.val() === '1'){
        $('div#show_total_device').css({'display':''});
    }else{
        $('div#show_total_device').css({'display':'none'});
    }

    // console.log(element);
}