$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('form[name="create-corporate-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message:''
                        }
                    }
                }
            }
    });

});