<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome'; 


// dinamic route 
$route['youtube/v1/GetAllCategories'] = "VodYoutubeModule/GetAllCategories"; 
$route['youtube/v1/GetVodByCategoryId'] = "VodYoutubeModule/GetVodByCategoryId";
$route['youtube/v1/GetVodDetail'] = "VodYoutubeModule/GetVodDetail";
$route['youtube/v1/SetVodClick'] = "VodYoutubeModule/SetVodClick"; 
$route['youtube/v1/GetMyPlaylist'] = "VodYoutubeModule/GetMyPlaylist";
$route['youtube/v1/GetUJRequest'] = "VodYoutubeModule/GetUJRequest";
$route['youtube/v1/SetUJRequest'] = "VodYoutubeModule/SetUJRequest";



// ====================== VOD FROM PARTER ==================================
$route['vvod/v1/GetVodMainPage'] = "VodModule/GetVodMainPage";
$route['vvod/v1/GetAllPackages'] = "VodModule/GetAllPackages";
$route['vvod/v1/GetAllVodByCategory'] = "VodModule/GetAllVodByCategory";
$route['vvod/v1/GetVodContent'] = "VodModule/GetVodContent";
$route['vvod/v1/GetVodByPartner'] = "VodModule/GetVodByPartner";
$route['vvod/v1/GetPackagePurchaseHistories'] = "VodModule/GetPackagePurchaseHistories";
$route['vvod/v1/GetVodByPartnerAndCategory'] = "VodModule/GetVodByPartnerAndCategory";

$route['vvod/v1/SetPackagePurchaseTransaction'] = "VodModule/SetPackagePurchaseTransaction"; 

$route['vvod/v1/SetiOSPurchaseFinished'] = "VodModule/SetiOSPurchaseFinished"; 

$route['vvod/v1/GetTransactionData'] = "VodModule/GetTransactionData";

$route['vvod/v1/GetVodLinkByReqUrl'] = "VodModule/GetVodLinkByReqUrl";



// ====================== EOF VOD FROM PARTNER =============================


// ====================== FOR CALLBACK FUNCTION ============================
$route['vvod/v1/callback/transaction/payment'] = "Callback/getTransactionPaymentCallBack";

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
