<?php 

$config['request_api'] = [
	'mv'=>[
		'url'=>'',
		'header'=>[

		],
		'Licence'=>'UFNJdmVyc2lvbjA=',
		'register'=>[
			'request_url'=>'http://159.138.242.213:5001/api/register'
		],
		'login'=>[
			'request_url'=>'http://159.138.242.213:5001/api/login'
		],
		'dummy_content'=>[
			'request_url'=>'http://159.138.242.213:5004/api/dummycontent'
		],
		'dummy_overview'=>[
			'request_url'=>'http://159.138.242.213:5004/api/dummyoverview'
		],
		'ios_payment'=>'http://159.138.242.213:5003/api/ios/payment',
		'content'=>[
			'request_url'=>'http://159.138.242.213:5001/api/content'
		],
		'content_overview'=>[
			'request_url'=>'http://159.138.242.213:5001/api/contentoverview'
		],
		'req_link_url'=>[
			'request_url'=>'http://159.138.242.213:5001/api/requrl'
		]
	]
];
