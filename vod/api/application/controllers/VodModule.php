<?php

defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
require_once(APPPATH. "libraries/api/VodLibrary.php");



/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class VodModule extends REST_Controller {

    private $backend_url;

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key 

        $this->setBackendUrl();
    }

    public function GetVodMainPage_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetVodMainPage');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getVodMainPage($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetVodMainPage_get(){ 

        $vodPartner = [];

        array_push($vodPartner, [
            'partner_id'=>1,
            'partner_contact_name'=>'MV HUB',
            'partner_contact_address'=>'',
            'partner_contact_telephone'=>'',
            'pattner_contact_fax'=>'',
            'menu_image'=>base_url('uploaded/mockup/partner/mv/menu.png'),
            'logo_image'=>base_url('uploaded/mockup/partner/mv/logo.png'),
            'cover_image'=>base_url('uploaded/mockup/partner/mv/cover.png')

        ]);


        $vodList = [];

        array_push($vodList, [
            'category_id'=>1,
            'category_name'=>'สารคดียอดนิยม',
            'vod_by_category'=>[
                [
                    'vod_id'=>1,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/01.jpg')
                ],
                [
                    'vod_id'=>2,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/02.jpg')
                ],
                [
                    'vod_id'=>3,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/03.jpg')
                ],
                [
                    'vod_id'=>4,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/04.jpg')
                ]


            ]

        ]);

        array_push($vodList, [
            'category_id'=>2,
            'category_name'=>'ภาพยนต์ยอดเยี่ยม',
            'vod_by_category'=>[
                [
                    'vod_id'=>5,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/05.jpg')
                ],
                [
                    'vod_id'=>6,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/06.jpg')
                ],
                [
                    'vod_id'=>7,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/07.jpg')
                ],
                [
                    'vod_id'=>8,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/08.jpg')
                ]


            ]

        ]);

        array_push($vodList, [
            'category_id'=>3,
            'category_name'=>'ละคร-ซีรีส์ยอดนิยม',
            'vod_by_category'=>[
                [
                    'vod_id'=>9,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/05.jpg')
                ],
                [
                    'vod_id'=>10,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/06.jpg')
                ],
                [
                    'vod_id'=>11,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/07.jpg')
                ],
                [
                    'vod_id'=>12,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/08.jpg')
                ]


            ]

        ]);


        echo json_encode([
            'status'=>true,
            'PartnerList'=>$vodPartner,
            'VodList'=>$vodList,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ]);





    }

    public function GetAllVodByCategory_get(){

        $vodList = [];

        array_push($vodList,[ 
                    'vod_id'=>13,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/09.jpg')
        ]);

        array_push($vodList,[ 
                    'vod_id'=>14,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/10.jpg')
        ]);

        array_push($vodList,[ 
                    'vod_id'=>15,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/11.jpg')
        ]);
        array_push($vodList,[ 
                    'vod_id'=>16,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/12.jpg')
        ]);
        array_push($vodList,[ 
                    'vod_id'=>17,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/13.jpg')
        ]);
        array_push($vodList,[ 
                    'vod_id'=>18,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/14.jpg')
        ]);
        array_push($vodList,[ 
                    'vod_id'=>19,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/15.jpg')
        ]);
        array_push($vodList,[ 
                    'vod_id'=>15,
                    'vod_name'=>'',
                    'vod_description'=>'',
                    'vod_cover'=>base_url('uploaded/mockup/vod_cover/16.jpg')
        ]); 

        echo json_encode([
            'status'=>true,
            'VodList'=>$vodList,
            'result_code'=>'000',
            'result_desc'=>'Success'
        ]);





    }

    public function GetAllPackages_post(){

        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllPackages');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllPackages($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetAllVodByCategory_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetAllVodByCategory');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getAllVodByCategory($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    } 

    public function GetVodContent_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetVodContent');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getVodContent($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetVodByPartner_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetVodByPartner');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getVodByPartner($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetPackagePurchaseHistories_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetPackagePurchaseHistories');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getPackagePurchaseHistories($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetVodByPartnerAndCategory_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetVodByPartnerAndCategory');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getVodByPartnerAndCategory($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function SetPackagePurchaseTransaction_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetPackagePurchaseTransaction');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setPackagePurchaseTransaction_post($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function SetiOSPurchaseFinished_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('SetiOSPurchaseFinished');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__setiOSPurchaseFinished($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetTransactionData_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetTransactionData');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getTransactionData($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    public function GetVodLinkByReqUrl_post(){
        if($this->post('request') || file_get_contents('php://input')){

                    $request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->post('request');

                    
                    $this->load->library('RequestAuthenAPI');
                    
                    $requestAuthenAPI = RequestAuthenAPI::get_instance();
                    $requestCriteria = new stdClass();
                    $requestCriteria = $requestAuthenAPI->set_requestCriteria($request);

                    //print_r($requestCriteria);exit;

                    $requestAuthenAPI->set_requireMethod('GetVodLinkByReqUrl');
                    if ($requestAuthenAPI = NULL || !$requestAuthenAPI->is_Authen($requestCriteria)) {

                        $error = array();
                        $error['status'] = false;
                        $error['result_code'] = "-001";
                        $error['result_desc'] = "AuthenException : Can't authentication,please try again.";
                        
                        echo json_encode($error);exit;
                    }else{

                        $output = $this->__getVodLinkByReqUrl($requestCriteria);
                        echo json_encode($output);exit;

                    }
        }else{
            $arReturn = array(
                'status' => false,
                'result_code'=>'-005',
                'result_desc'=>'RequestException : Request not found.'
            );
            echo json_encode($arReturn);
        }
    }

    private function __getVodMainPage($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getVodMainPage($requestCriteria);
        return $output;
    }

    private function __getAllPackages($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getAllPackages($requestCriteria);
        return $output;
    }

    private function __getAllVodByCategory($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getAllVodByCategory($requestCriteria);
        return $output;
    }
    private function __getVodByPartner($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getVodByPartner($requestCriteria);
        return $output;
    }

    private function __getPackagePurchaseHistories($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getPackagePurchaseHistories($requestCriteria);
        return $output;
    }


    private function setBackendUrl(){

    }

    private function __getVodContent($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getVodContent($requestCriteria);
        return $output;
    }

    private function __getVodByPartnerAndCategory($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getVodByPartnerAndCategory($requestCriteria);
        return $output;
    }

    private function __setPackagePurchaseTransaction_post($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->setPackagePurchaseTransaction($requestCriteria);
        return $output;
    }

    private function __setiOSPurchaseFinished($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->setiOSPurchaseFinished($requestCriteria);
        return $output;
    }

    private function __getTransactionData($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getTransactionData($requestCriteria);
        return $output;
    }
    private function __getVodLinkByReqUrl($requestCriteria){
        $output = array();
        $VodLibrary = new VodLibrary();
        $output = $VodLibrary->getVodLinkByReqUrl($requestCriteria);
        return $output;
    }
}