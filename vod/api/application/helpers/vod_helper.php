<?php 

if (!function_exists('getPartnerCover')) {

    
    function getPartnerCover($obj_partner) { 
        $partner_url = getPartnerBackendUrl();

        $cover_path = $partner_url.'uploaded/partner/cover/'.$obj_partner->id.'/'.$obj_partner->cover_image;

        if(!does_url_exists($cover_path)){
            return $partner_url.'uploaded/partner/cover/no_picture.png';
        }
        // if(!file_exists($cover_path)){
        //     return $partner_url.'uploaded/partner/cover/no_picture.png';
        // }

        return $cover_path;
       
    }

}

if(!function_exists('getPertnerLogo')){
    function getPertnerLogo($obj_partner){
        $partner_url = getPartnerBackendUrl();


        $logo_path = $partner_url.'uploaded/partner/logo/'.$obj_partner->id.'/'.$obj_partner->logo_image;

        if(!does_url_exists($logo_path)){
            return $partner_url.'uploaded/partner/logo/no_picture.png';
        }

        // if(!file_exists($logo_path)){
        //     return $partner_url.'uploaded/partner/logo/no_picture.png';
        // }
        return $logo_path;
    }
}


if(!function_exists('convertOutputMVResponse')){

    /**
    *
    * @param <type> $array 
    * @param <type> $return
    */
    function convertOutputMVResponse($data = []){

        
    }
} 


if(!function_exists('getPartnerBackendUrl')){
    function getPartnerBackendUrl(){
        switch (ENVIRONMENT) {
            case 'development':
                return 'http://s3remoteservice.development/vod_ci/partner/';
            break;
            case 'testing':

            break;

            case 'production':
                return 'http://s3remoteservice.psi.co.th/vod/partner/';
            break;
            
            default:
                # code...
                break;
        }
    }
}

if(!function_exists('does_url_exists')){
    function does_url_exists($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_NOBODY, true);
        curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if ($code == 200) {
            $status = true;
        } else {
            $status = false;
        }
        curl_close($ch);
        return $status;
    }

}

if(!function_exists('getPackageCoverImage')){
    function getPackageCoverImage($package_data){

         $backend_url = getBackendUrl();

        $cover_txt = $package_data->cover;
        $package_id = $package_data->id;


        if(strpos($cover_txt, 'http') !== false || strpos($cover_txt, 'https') !== false){
            return $cover_txt;
        }else{
            return  $backend_url.'partner/uploaded/partner_packages/'.$package_id.'/'.$cover_txt;
        }
    }
}

if(!function_exists('getBackendUrl')){
    function getBackendUrl(){
        switch (ENVIRONMENT) {
            case 'development':
            return  'http://s3remoteservice.development/vod_ci/';
            break;
            case 'testing':

            break;

            case 'production':
            return  'http://s3remoteservice.psi.co.th/vod/';
            break;
            
            default:
            return  'http://s3remoteservice.psi.co.th/vod/';
            break;
        }
    }
}



?>