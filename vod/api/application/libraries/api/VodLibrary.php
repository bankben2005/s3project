<?php 

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

class VodLibrary{

	private $ci; 
	public $backend_url;
	private $mv_access_token;
	private $mv_id;

	private $requestCriteria;

	function __construct(){
		$this->ci =& get_instance();
		$this->ci->load->helper([
			'vod'
		]);
		$this->setBackendUrl();
	}

	public function getVodMainPage($requestCriteria){  


		// $log_file_path = $this->createLogFilePath('mainpageRequestCriteria-members_id='.$requestCriteria->members_id);
		// $file_content = date("Y-m-d H:i:s") . ' criteria : ' . json_encode($requestCriteria) . "\n"; 
		// $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
		// file_put_contents($log_file_path, $file_content, FILE_APPEND);
		// unset($file_content); 



		//print_r($requestCriteria);exit;

		if(!property_exists($requestCriteria, 'members_id') || $requestCriteria->members_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found members_id parameter'
			];
		}

		if(!property_exists($requestCriteria, 'login_type') || $requestCriteria->login_type == ''){

			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found login_type parameter'
			];	
		}

		// check login type login only
		if($requestCriteria->login_type != 'member'){
			return [
				'status'=>false,
				'result_code'=>'-009',
				'result_desc'=>'You do not have permission,Please signin with member option'
			];
		}


		// check this user already has vvod_users_id
		$checkVVODUserExist = $this->checkVVODUserExist([
			'members_id'=>$requestCriteria->members_id,
			'login_type'=>$requestCriteria->login_type,
			'storage_token'=>$requestCriteria->storage_token
		]);

		if(!$checkVVODUserExist['status']){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>$checkVVODUserExist['message']
			];
		}


		$member_data = $checkVVODUserExist['member_data'];

		$member_data->{'vvod_users_id'} = $member_data->id;

		return [
			'status'=>true,
			'MainPageData'=>$this->getMainPageData([
				'vvod_users_id'=>$member_data->id
			]),
			'VodMember'=>$member_data,
			'AllPartner'=>$this->getAllPartners(),
			'set_storage_token_status'=>$checkVVODUserExist['set_storage_token_status'],
			'storage_token'=>$checkVVODUserExist['storage_token'],
			'vvod_users_id'=>$member_data->id,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];

	}

	public function getAllPackages($requestCriteria){ 




		$arrPartner = [];

		// get all partner and then get all package by partner 

		// if(property_exists($requestCriteria, 'device_type') && $requestCriteria->device_type != '' && in_array($requestCriteria->device_type, ['android','ios'])){

		// 	$queryPartner = $this->ci->db->select('*')
		// 	->from()
		// }else{
			
		// }

		$queryPartner = $this->ci->db->select('*')
			->from('vvod_partners')
			->where('active',1)
			->get();
		

		//echo 'aaa';exit;
		// echo $queryPartner->num_rows();exit;
		if($queryPartner->num_rows() > 0){ 



			foreach ($queryPartner->result() as $key => $row) { 

				//print_r($row);exit;
				# code... 
				// get partner package from partner
				$partner_packages = $this->getPartnerPackages([
					'partner_id'=>$row->id,
					'partner_data'=>$row,
					'requestCriteria'=>$requestCriteria
				]); 

				 //print_r($partner_packages);


				array_push($arrPartner, [
					'id'=>$row->id,
					'contact_name'=>$row->contact_name,
					'contact_address'=>$row->contact_address,
					'contact_telephone'=>$row->contact_telephone,
					'contact_fax'=>$row->contact_fax,
					'cover'=>getPartnerCover($row),
					'logo'=>getPertnerLogo($row),
					'package_list'=>$partner_packages
				]);




			}
			
		} 

		//print_r($arrPartner);exit;
		$getMyPackageList = $this->getMyPackageList([
				'vvod_users_id'=>$requestCriteria->vvod_users_id
		]);

		return [
			'status'=>true,
			'HasPackage'=>(count($getMyPackageList) > 0)?true:false,
			'MyPackage'=>$getMyPackageList,
			'PartnerPackageList'=>$arrPartner,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];

	}

	public function getAllVodByCategory($requestCriteria){ 

		$arrVodList = [];

		// check category id
		if(!property_exists($requestCriteria, 'category_id') || $requestCriteria->category_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found category_id parameter or category_id is empty'
			];
		}
		if(!$this->checkCategoryExist([
			'category_id'=>$requestCriteria->category_id
		])){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found category_id'
			];

		} 

		$queryByPartnerMapCategory = $this->ci->db->select('*')
		->from('vvod_partner_map_category')
		->join('vvod_partners','vvod_partner_map_category.vvod_partners_id = vvod_partners.id')
		->where('vvod_partner_map_category.vvod_categories_id',$requestCriteria->category_id)
		->where('vvod_partner_map_category.active',1)
		->get();


		if($queryByPartnerMapCategory->num_rows() > 0){

			foreach ($queryByPartnerMapCategory->result() as $key => $row) { 

				if($row->slug == 'mv'){
					$is_mv = false;
					$mv_content = '';


					
						$this->checkVVODUserCreateUserWithMVPartner([
							'vvod_partners_id'=>$row->vvod_partners_id,
							'vvod_users_id'=>$requestCriteria->vvod_users_id
						]);
						$is_mv  = true;
						// $mv_content =  $this->getMVDummyContent([

						// ]);
						$mv_content = $this->getMVContent([

						]);
					
						$arrVodList = $this->getMVVodListBySlugToCategory([
							'slug'=>$row->vvod_partner_api_slug,
							'mv_content'=>$mv_content,
							'partner_id'=>$row->vvod_partners_id,
							'partner_contact_name'=>$row->contact_name,
							'partner_contact_address'=>$row->contact_address,
							'partner_contact_telephone'=>$row->contact_telephone,
							'partner_contact_fax'=>$row->contact_fax,
							'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$row->vvod_partners_id.'/'.$row->logo_image,
							'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$row->vvod_partners_id.'/'.$row->cover_image,
							'slice_status'=>false
						]);

						//print_r($arrVodList);exit;

					
				}

				// print_r($row);exit;
				// # code...
				// $vodByCategoryAndPartner = $this->getVodByCategoryAndPartnerId([
				// 	'vvod_partners_id'=>$row->vvod_partners_id,
				// 	'vvod_partner_api_category_id'=>$row->vvod_partner_api_category_id,
				// 	'vvod_partner_api_slug'=>$row->vvod_partner_api_slug,
				// 	'is_map_api_category_status'=>$row->is_map_api_category_status,
				// 	'is_map_api_slug_status'=>$row->is_map_api_slug_status
				// ]); 

				// (count($vodByCategoryAndPartner) > 0)?array_push($arrVodList, $vodByCategoryAndPartner):'';

			}

		}

		return [
			'status'=>true,
			'VodList'=>$arrVodList,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];




	}

	public function getVodContent($requestCriteria){
		// print_r($requestCriteria);exit; 

		if(!property_exists($requestCriteria, 'vod_id') || $requestCriteria->vod_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not foud vod_id parameter'
			];
		} 

		if(!property_exists($requestCriteria, 'partner_id') || $requestCriteria->partner_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not foud partner_id parameter'
			];
		} 


		$queryPartner = $this->ci->db->select('*')
		->from('vvod_partners')
		->where('id',$requestCriteria->partner_id)
		->where('active',1)
		->get();

		if($queryPartner->num_rows() > 0){ 
			$rowPartner = $queryPartner->row();

			switch ($rowPartner->slug) {
				case 'mv': 

					$this->checkVVODUserCreateUserWithMVPartner([
							'vvod_partners_id'=>$rowPartner->id,
							'vvod_users_id'=>$requestCriteria->vvod_users_id
					]);

					// print_r($this->mv_id);
					// print_r($this->mv_access_token);exit;
					$apiVodContent = $this->getMVVODContent([
						'vod_id'=>$requestCriteria->vod_id
					]); 

					$apiVodContent = $apiVodContent->res_data->message;

					//print_r($apiVodContent);exit;
					


					$vodContent = [
						'vod_id'=>$requestCriteria->vod_id,
						'vod_title'=>$apiVodContent->title,
						'vod_description'=>$apiVodContent->description,
						'vod_actors'=>$apiVodContent->TheActors,
						'vod_director'=>$apiVodContent->director,
						'vod_images'=>$apiVodContent->images,
						'vod_link_preview'=>$apiVodContent->link_preview,
						'vod_link'=>$this->getMVVodLink($apiVodContent->link),
						'partner_data'=>[
							'partner_id'=>$rowPartner->id,
							'partner_contact_name'=>$rowPartner->contact_name,
							'partner_contact_address'=>$rowPartner->contact_address,
							'partner_contact_telephone'=>$rowPartner->contact_telephone,
							'partner_contact_fax'=>$rowPartner->contact_fax,
							'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->id.'/'.$rowPartner->logo_image,
							'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->id.'/'.$rowPartner->cover_image

						]
					];



				break;
				
				default:
					# code...
					break;
			} 



		}else{

			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found partner,Please try again'
			];

		}

		return [
			'status'=>true,
			'VodContent'=>$vodContent,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];



	}

	public function getVodByPartnerAndCategory($requestCriteria){
		// print_r($requestCriteria);exit; 

		// check property exist 
		if(!property_exists($requestCriteria, 'vvod_users_id') || $requestCriteria->vvod_users_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found vvod_users_id parameter or vvod_users_id is empty'
			];	
		} 


		if(!property_exists($requestCriteria, 'partner_id') || $requestCriteria->partner_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found partner_id parameter or partner_id is empty'
			];	
		} 


		if(!property_exists($requestCriteria, 'category_id') || $requestCriteria->category_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found category_id parameter or category_id is empty'
			];	
		} 


		// check all property exist in database 
		$arrReturn = [];

		
		$queryPartner = $this->ci->db->select('*')
		->from('vvod_partners')
		->where('id',$requestCriteria->partner_id)
		->where('active',1)
		->get();

		if($queryPartner->num_rows() > 0){ 
			$rowPartner = $queryPartner->row(); 


			// set stdclass for partner //
			$stdReturn = new StdClass();
			$stdReturn->partner_id = $rowPartner->id;
			$stdReturn->partner_contact_name = $rowPartner->contact_name;
			$stdReturn->partner_contact_address = $rowPartner->contact_address;
			$stdReturn->partner_contact_telephone = $rowPartner->contact_telephone;
			$stdReturn->partner_contact_fax = $rowPartner->contact_fax;
			$stdReturn->logo_image = $this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->id.'/'.$rowPartner->logo_image;
			$stdReturn->cover_image = $this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->id.'/'.$rowPartner->cover_image;

			$stdReturn->vod_by_categories = [];
			// eof set stdclass for partner 



			// get partner category and all category map slug or id 
			$partner_map_category = $this->ci->db->select('*,vvod_categories.name as category_name,vvod_categories.description as category_description,vvod_categories.id as category_id')
			->from('vvod_partner_map_category')
			->join('vvod_categories','vvod_partner_map_category.vvod_categories_id = vvod_categories.id')
			->where('vvod_partner_map_category.vvod_partners_id',$rowPartner->id)
			->where('vvod_partner_map_category.vvod_categories_id',$requestCriteria->category_id)
			->where('vvod_partner_map_category.active',1)
			->get();  

			// echo $this->ci->db->last_query();exit;

			switch ($rowPartner->slug) {
				case 'mv': 

					$this->checkVVODUserCreateUserWithMVPartner([
					'vvod_partners_id'=>$rowPartner->id,
					'vvod_users_id'=>$requestCriteria->vvod_users_id
					]);
					// $mv_content =  $this->getMVDummyContent([

					// ]); 
					$mv_content = $this->getMVContent([

					]);

					// print_r($mv_content);exit;

					if($partner_map_category->num_rows() > 0){
						foreach ($partner_map_category->result() as $key => $value) { 

							//print_r($value);exit;

							array_push($arrReturn, [
								'category_id'=>$value->category_id,
								'category_name'=>$value->category_name,
								'category_description'=>$value->category_description,
								'vod_by_category'=>$this->getMVVodListBySlugToCategory([
									'slug'=>$value->vvod_partner_api_slug,
									'mv_content'=>$mv_content,
									'partner_id'=>$rowPartner->id,
									'partner_contact_name'=>$rowPartner->contact_name,
									'partner_contact_address'=>$rowPartner->contact_address,
									'partner_contact_telephone'=>$rowPartner->contact_telephone,
									'partner_contact_fax'=>$rowPartner->contact_fax,
									'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->id.'/'.$rowPartner->logo_image,
									'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->id.'/'.$rowPartner->cover_image,
									'slice_status'=>false
								])
							]);


							
						}
					}
					
					

					//print_r($arrReturn);exit;
					$stdReturn->vod_by_categories = $arrReturn[0];


				break;
				
				default:
					# code...
				break;
			}

			//print_r($arrReturn[0]);exit;

		}

		return [
			'status'=>true,
			'VodList'=>$stdReturn,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];


	}

	public function setPackagePurchaseTransaction($requestCriteria){
		// print_r($requestCriteria);exit;

		// check property exist 
		if(!property_exists($requestCriteria, 'vvod_users_id') || $requestCriteria->vvod_users_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found vvod_users_id parameter or vvod_users_id is empty'
			];	
		}


		if(!property_exists($requestCriteria, 'package_id') || $requestCriteria->package_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found package_id parameter or package_id is empty'
			];
		} 

		// check pacakge exist
		// $this->runTransactionNumber();

		$queryPartnerPackage = $this->ci->db->select('*')
		->from('vvod_partner_packages')
		->where('id',$requestCriteria->package_id)
		->where('active',1)
		->get();

		if($queryPartnerPackage->num_rows() > 0){
			$rowPartnerPackage = $queryPartnerPackage->row();

			//print_r($rowPartnerPackage);exit;

			// run transaction number 
			$transaction_number = $this->runTransactionNumber();


			$dataInsert = [
				'vvod_users_id'=>$requestCriteria->vvod_users_id,
				'vvod_partner_packages_id'=>$requestCriteria->package_id,
				'transaction_number'=>$transaction_number,
				'amount'=>1,
				'sub_total'=>(float)$rowPartnerPackage->price,
				'grand_total'=>(float)$rowPartnerPackage->price,
				'payment_option'=>$requestCriteria->payment_option,
				'created'=>date('Y-m-d H:i:s')
			]; 

			$this->ci->db->insert('vvod_transactions',$dataInsert); 

			

			$insert_id = $this->ci->db->insert_id();

			// create transaction logs 
			$this->setVodTransactionLogs([
				'vvod_transactions_id'=>$insert_id,
				'type'=>'create-transaction',
				'log_message'=>json_encode([
					'grand_total'=>$rowPartnerPackage->price,
					'created'=>date('Y-m-d H:i:s')
				])
			]); 

			$transaction_data = $this->getTransactionDataByTransactionId([
				'vvod_transactions_id'=>$insert_id,
				'vvod_users_id'=>$requestCriteria->vvod_users_id
			]);

			return [
				'status'=>true,
				'AccessToken'=>$this->getAccessTokenByUserId($requestCriteria->vvod_users_id),
				'WebviewPaymentUrl'=>$this->getWebViewPaymentUrl([
					'requestCriteria'=>$requestCriteria,
					'transaction_data'=>$transaction_data,
					'access_token'=>$this->getAccessTokenByUserId($requestCriteria->vvod_users_id)
				]),
				'TransactionData'=>$transaction_data,
				'result_code'=>'000',
				'result_desc'=>'Success'
			];


		}else{
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found package,Please try again'
			];
		}




	}

	public function setiOSPurchaseFinished($requestCriteria){
		// print_r($requestCriteria);exit; 

		// create log file
		// $log_file_path = $this->createLogFilePath('iosSetPurchaseFinished');
		// $file_content = date("Y-m-d H:i:s") . ' criteria : ' . json_encode($requestCriteria) . "\n"; 
		// $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
		// file_put_contents($log_file_path, $file_content, FILE_APPEND);
		// unset($file_content);



		if(!property_exists($requestCriteria, 'payment_status') || $requestCriteria->payment_status == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found payment_status or payment_status is empty'
			];
		} 


		if(!in_array($requestCriteria->payment_status, ['success','unsuccess'])){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : payment_status must be success or unsuccess'
			];
		}

		if(!property_exists($requestCriteria, 'transaction_number') || $requestCriteria->transaction_number == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found transaction_number please check.'
			];
		}


		$queryTransaction = $this->ci->db->select('*')
		->from('vvod_transactions')
		->where('transaction_number',$requestCriteria->transaction_number)
		->where('active',1)
		->get();

		if($queryTransaction->num_rows() > 0){
			$rowTransaction = $queryTransaction->row(); 




			if($requestCriteria->payment_status == 'success'){
				$this->ci->db->update('vvod_transactions',[
					'payment_status'=>1,
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$rowTransaction->id]); 

				$partner_data = $this->getPartnerInformationByPartnerPackageId($rowTransaction->vvod_partner_packages_id); 


				switch ($partner_data->partner_slug) {
					case 'mv':
						// after transaction payment success then send data to mv 
						$this->sendIOSPaymentToMV([
							'vvod_transactions'=>$rowTransaction
						]);
					break;
					
					default:
						# code...
						break;
				}

				

				
			}

			// update transaction logs

			$this->setVodTransactionLogs([
					'vvod_transactions_id'=>$rowTransaction->id,
					'type'=>'transaction-payment',
					'log_message'=>json_encode([
						'payment_status'=>$requestCriteria->payment_status,
						'user_ip'=>$this->getUserIP()
					])
			]);

			// eof update transaction_logs 

			return [
				'status'=>true,
				'result_code'=>'000',
				'result_desc'=>'Success'
			];




		}else{
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found transaction.Please check transaction_number'
			];
		}
	}

	public function getTransactionData($requestCriteria){
		// print_r($requestCriteria);exit;

		if(!property_exists($requestCriteria, 'transaction_number') || $requestCriteria->transaction_number == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found transaction_number'
			];
		}

		$transaction_data = $this->getTransactionDataByTransactionNumber($requestCriteria->transaction_number);

		if(!$transaction_data['status']){
			return [
				'status'=>false,
				'result_code'=>$transaction_data['result_code'],
				'result_desc'=>$transaction_data['result_desc']
			];
		}

		return [
			'status'=>true,
			'TransactionData'=>$transaction_data['TransactionData'],
			'result_code'=>'000',
			'result_desc'=>'Success'
		];



	}

	public function getVodLinkByReqUrl($requestCriteria){

		if(!property_exists($requestCriteria, 'token_req_url') || $requestCriteria->token_req_url == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not foud token_req_url parameter'
			];
		}

		if(!property_exists($requestCriteria, 'vvod_users_id') || $requestCriteria->vvod_users_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not foud vvod_users_id parameter'
			];
		}

		if(!property_exists($requestCriteria, 'partner_id') || $requestCriteria->partner_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not foud partner_id parameter'
			];
		} 


		$queryPartner = $this->ci->db->select('*')
		->from('vvod_partners')
		->where('id',$requestCriteria->partner_id)
		->where('active',1)
		->get();

		if($queryPartner->num_rows() > 0){ 
			$rowPartner = $queryPartner->row();

			switch ($rowPartner->slug) {
				case 'mv': 

					$this->checkVVODUserCreateUserWithMVPartner([
							'vvod_partners_id'=>$rowPartner->id,
							'vvod_users_id'=>$requestCriteria->vvod_users_id
					]);

					$apiVodLinkByTokenRequest = $this->getMVVodLinkByTokenRequest($requestCriteria->token_req_url);


					$vodContent = [
						'vod_name'=>$apiVodLinkByTokenRequest->name,
						'vod_ep'=>$apiVodLinkByTokenRequest->ep,
						'vod_link'=>$this->getMVVodLink($apiVodLinkByTokenRequest->link),
						'partner_data'=>[
							'partner_id'=>$rowPartner->id,
							'partner_contact_name'=>$rowPartner->contact_name,
							'partner_contact_address'=>$rowPartner->contact_address,
							'partner_contact_telephone'=>$rowPartner->contact_telephone,
							'partner_contact_fax'=>$rowPartner->contact_fax,
							'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->id.'/'.$rowPartner->logo_image,
							'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->id.'/'.$rowPartner->cover_image

						]
					];



				break;
				
				default:
					# code...
					break;
			} 



		}else{

			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found partner,Please try again'
			];

		}

		return [
			'status'=>true,
			'VodContent'=>$vodContent,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];
	}

	public function getVodByPartner($requestCriteria){

		//return $this->getVodByPartnerNewVersion($requestCriteria);

		$this->ci->load->config('partners');

		// check property exist 
		if(!property_exists($requestCriteria, 'partner_id') || $requestCriteria->partner_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found partner_id parameter or partner_id is empty'
			];	
		} 

		//$this->
		$arrReturn = [];

		
		$queryPartner = $this->ci->db->select('*')
		->from('vvod_partners')
		->where('id',$requestCriteria->partner_id)
		->where('active',1)
		->get();

		if($queryPartner->num_rows() > 0){ 
			$rowPartner = $queryPartner->row();
			$request_api = $this->ci->config->item('request_api')[(string)$rowPartner->slug]; 


			$stdReturn = new StdClass();
			$stdReturn->partner_id = $rowPartner->id;
			$stdReturn->partner_contact_name = $rowPartner->contact_name;
			$stdReturn->partner_contact_address = $rowPartner->contact_address;
			$stdReturn->partner_contact_telephone = $rowPartner->contact_telephone;
			$stdReturn->partner_contact_fax = $rowPartner->contact_fax;
			$stdReturn->logo_image = $this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->id.'/'.$rowPartner->logo_image;
			$stdReturn->cover_image = $this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->id.'/'.$rowPartner->cover_image;

			$stdReturn->vod_by_categories = []; 

			// print_r($stdReturn);exit;


			// get partner category and all category map slug or id 
			$partner_map_category = $this->ci->db->select('*,vvod_categories.name as category_name,vvod_categories.description as category_description,vvod_categories.id as category_id')
			->from('vvod_partner_map_category')
			->join('vvod_categories','vvod_partner_map_category.vvod_categories_id = vvod_categories.id')
			->where('vvod_partner_map_category.vvod_partners_id',$rowPartner->id)
			->where('vvod_partner_map_category.active',1)
			->get();  

			// echo $this->ci->db->last_query();exit;

			switch ($rowPartner->slug) {
				case 'mv': 

					$this->checkVVODUserCreateUserWithMVPartner([
					'vvod_partners_id'=>$rowPartner->id,
					'vvod_users_id'=>$requestCriteria->vvod_users_id
					]);
					// $mv_content =  $this->getMVDummyContent([

					// ]); 
					$mv_content = $this->getMVContent([

					]);

					// print_r($mv_content);exit;

					if($partner_map_category->num_rows() > 0){
						foreach ($partner_map_category->result() as $key => $value) { 

							//print_r($value);exit;

							$vod_by_category = $this->getMVVodListBySlugToCategory([
									'slug'=>$value->vvod_partner_api_slug,
									'mv_content'=>$mv_content,
									'partner_id'=>$rowPartner->id,
									'partner_contact_name'=>$rowPartner->contact_name,
									'partner_contact_address'=>$rowPartner->contact_address,
									'partner_contact_telephone'=>$rowPartner->contact_telephone,
									'partner_contact_fax'=>$rowPartner->contact_fax,
									'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->id.'/'.$rowPartner->logo_image,
									'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->id.'/'.$rowPartner->cover_image,
									'slice_status'=>true
							]);

							if(count($vod_by_category) > 0){
								array_push($arrReturn, [
									'category_id'=>$value->category_id,
									'category_name'=>$value->category_name,
									'category_description'=>$value->category_description,
									'vod_by_category'=>$vod_by_category
								]);
							}
							


							
						}
					}
					
					

					//print_r($arrReturn);exit;
					$stdReturn->vod_by_categories = $arrReturn;


				break;
				
				default:
					# code...
				break;
			}

			
			

			// if($partner_map_category->num_rows() > 0){
			// 	foreach ($partner_map_category as $key => $row) {
			// 		# code...
			// 		array_push($stdReturn->vod_by_categories, [
			// 			'category_id'=>$row->vvod_categories_id,
			// 			'category_name'=>$row->name,
			// 			'category_description'=>$row->description,
			// 			'vod_list'=>$this->getVodByCategoryAndPartnerId([
			// 				'vvod_partners_id'=>$row->vvod_partners_id,
			// 				'vvod_partner_api_category_id'=>$row->vvod_partner_api_category_id,
			// 				'vvod_partner_api_slug'=>$row->vvod_partner_api_slug,
			// 				'is_map_api_category_status'=>$row->is_map_api_category_status,
			// 				'is_map_api_slug_status'=>$row->is_map_api_slug_status
			// 			])

			// 		]);
					

			// 	}

			// }

			return [
				'status'=>true,
				'VodListByCategories'=>$arrReturn,
				'result_code'=>'000',
				'result_desc'=>'Success'
			];




		}else{
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Partner id does not exist, Please try again'
			];
		}


	}

	private function getVodByPartnerNewVersion($requestCriteria){

		$this->ci->load->config('partners');

		// check property exist 
		if(!property_exists($requestCriteria, 'partner_id') || $requestCriteria->partner_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found partner_id parameter or partner_id is empty'
			];	
		} 

		//$this->
		$arrReturn = [];

		
		$queryPartner = $this->ci->db->select('*')
		->from('vvod_partners')
		->where('id',$requestCriteria->partner_id)
		->where('active',1)
		->get();

		if($queryPartner->num_rows() > 0){ 
			$rowPartner = $queryPartner->row();
			$request_api = $this->ci->config->item('request_api')[(string)$rowPartner->slug]; 


			$stdReturn = new StdClass();
			$stdReturn->partner_id = $rowPartner->id;
			$stdReturn->partner_contact_name = $rowPartner->contact_name;
			$stdReturn->partner_contact_address = $rowPartner->contact_address;
			$stdReturn->partner_contact_telephone = $rowPartner->contact_telephone;
			$stdReturn->partner_contact_fax = $rowPartner->contact_fax;
			$stdReturn->logo_image = $this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->id.'/'.$rowPartner->logo_image;
			$stdReturn->cover_image = $this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->id.'/'.$rowPartner->cover_image;

			$stdReturn->vod_by_partner = $this->getVodListBySlugAndPartner([
				'slug'=>$rowPartner->slug,
				'partner_data'=>$rowPartner,
				'requestCriteria'=>$requestCriteria
			]);

		}


	}

	private function getVodListBySlugAndPartner($data = []){
		$partner_data = $data['partner_data'];
		$requestCriteria = $data['requestCriteria'];
		switch ($data['slug']) {
			case 'mv':
					$this->checkVVODUserCreateUserWithMVPartner([
					'vvod_partners_id'=>$partner_data->id,
					'vvod_users_id'=>$requestCriteria->vvod_users_id
					]);
					// $mv_content =  $this->getMVDummyContent([

					// ]); 
					$mv_content = $this->getMVContent([

					]);

					print_r($mv_content);exit;
			break;
			
			default:
				# code...
				break;
		}
	}

	public function getPackagePurchaseHistories($requestCriteria){

		$arrUserPurchaseHistories = [];
		// check property exist 
		if(!property_exists($requestCriteria, 'vvod_users_id') || $requestCriteria->vvod_users_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found vvod_users_id parameter or vod_user_id is empty'
			];
		}

		$queryVodUser = $this->ci->db->select('*')
		->from('vvod_users')
		->where('id',$requestCriteria->vvod_users_id)
		->where('active',1)
		->get(); 

		 // print_r($requestCriteria);exit;

		//echo $queryVodUser->num_rows();exit;
		if($queryVodUser->num_rows() > 0){

			$rowUser = $queryVodUser->row();

			$queryPurchasePackage = $this->ci->db->select('*')
			->from('vvod_transactions')
			->where('vvod_users_id',$requestCriteria->vvod_users_id)
			->where('payment_status',1)
			->where('active',1)
			->order_by('created','desc')
			->get();

			$arrAllPartner = [];

			//print_r($queryPurchasePackage->result());exit;

			if($queryPurchasePackage->num_rows() > 0){
				foreach ($queryPurchasePackage->result() as $key => $row) {
					# code...
					$partner_data = $this->getPartnerInformationByPartnerPackageId($row->vvod_partner_packages_id); 

					// print_r($partner_data);exit;

					$arrAllPartner[(int)$partner_data->partner_id] = $partner_data; 

					// print_r($arrAllPartner);exit;
					$package_data = $this->getPartnerPackageDataById($row->vvod_partner_packages_id);

					// get package live transaction for get start date and end date 
					$package_live_transaction = $this->getPackageLiveTransactionByTransactionId($row->id);


					array_push($arrUserPurchaseHistories, [
						'partner_data'=>$partner_data,
						'package_name'=>$package_data->package_name,
						'package_description'=>$package_data->package_description,
						'package_price'=>$package_data->package_price,
						'package_currency'=>$package_data->package_currency_symbol,
						'start_datetime'=>(@$package_live_transaction->start_datetime)?date('d-m-Y H:i',strtotime($package_live_transaction->start_datetime)):'-',
						'end_datetime'=>(@$package_live_transaction->end_datetime)?date('d-m-Y H:i',strtotime($package_live_transaction->end_datetime)):'-',
						'created'=>date('Y-m-d H:i:s',@$package_live_transaction->created)
					]);
				}
			}

			// print_r($arrAllPartner);exit;
			$arrHistoryByPartner = []; 

			//print_r($arrAllPartner);exit;

			
			foreach ($arrAllPartner as $key => $value) {
				# code...
				$arrHistoryByPartner[$key]['partner_data'] = $value;
				$arrHistoryByPartner[$key]['history_by_partner'] = [];

				foreach ($arrUserPurchaseHistories as $k => $v) {
					# code... 

					// print_r($v);exit;

					if(trim($key) == $v['partner_data']->partner_id){ 
						array_push($arrHistoryByPartner[$key]['history_by_partner'], $v); 

						// unset($arrHistoryByPartner[$key]['history_by_partner']['partner_data']);

					}
					

				}
			} 

			$arrArrangeArray = [];
			// arrange new array 
			foreach ($arrHistoryByPartner as $key => $value) {
				# code...
				array_push($arrArrangeArray, $value);

			}
			// print_r($arrHistoryByPartner);exit;

			return [
				'status'=>true,
				'HistoryList'=>$arrArrangeArray,
				'result_code'=>'000',
				'result_desc'=>'Success'
			];

			// print_r($arrHistoryByPartner);exit;




		}else{

			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter vod_user_id does not exist,Please try again.'
			];

		}

	}



	private function setBackendUrl(){ 

		switch (ENVIRONMENT) {
			case 'development':
			$this->backend_url =  'http://s3remoteservice.development/vod_ci/';
			break;
			case 'testing':

			break;

			case 'production':
			$this->backend_url =  'http://s3remoteservice.psi.co.th/vod/';
			break;
			
			default:
				# code...
			break;
		}

	}

	private function getAllPartners(){ 

		$arrReturn = [];


		$query = $this->ci->db->select('*')
		->from('vvod_partners')
		->where('active',1)
		->order_by('ordinal','asc')
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $row) {
				# code...
				array_push($arrReturn, [
					'partner_id'=>$row->id,
					'partner_contact_name'=>$row->contact_name,
					'partner_contact_address'=>$row->contact_address,
					'partner_contact_telephone'=>$row->contact_telephone,
					'partner_contact_fax'=>$row->contact_fax,
					'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$row->id.'/'.$row->logo_image,
					'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$row->id.'/'.$row->cover_image
				]);
			}
		}


		return $arrReturn;

	}

	private function getDocumentaryHits(){ 





	}

	private function getMovieHits(){




	}

	private function getSerieHits(){

		
		
	}

	private function getPartnerPackages($data = []){ 

		$requestCriteria = $data['requestCriteria'];
		$arrPartnerPackages = [];
		$partner_data = $data['partner_data'];

		if(property_exists($requestCriteria, 'device_type') && $requestCriteria->device_type != '' && in_array($requestCriteria->device_type, ['android','ios'])){
			$query = $this->ci->db->select('*')
			->from('vvod_partner_packages')
			->where('vvod_partners_id',$data['partner_id'])
			->where('active',1)
			->where('device_type',$requestCriteria->device_type)
			->order_by('price','asc')
			->get();

		}else{
			$query = $this->ci->db->select('*')
			->from('vvod_partner_packages')
			->where('vvod_partners_id',$data['partner_id'])
			->where('active',1)
			->order_by('price','asc')
			->get();

		}

		

		if($query->num_rows() > 0){ 

			// print_r($query->result());exit;

			foreach ($query->result() as $key => $row) {
				# code...

				array_push($arrPartnerPackages, [
					'package_id'=>$row->id,
					'package_purchase_ref_id'=>$row->purchase_ref_id,
					'package_purchase_url'=>@$partner_data->package_purchase_url,
					'package_code'=>$row->code,
					'package_name'=>$row->name,
					'package_description'=>$row->description,
					'package_price'=>$row->price,
					'package_currency_symbol'=>$row->currency_symbol,
					'package_days'=>$row->days,
					'package_cover'=>getPackageCoverImage($row)
				]);

			}


		}

		return $arrPartnerPackages;


	}

	private function checkCategoryExist($data = []){
		$queryCategory = $this->ci->db->select('*')
		->from('vvod_categories')
		->where('id',$data['category_id'])
		->where('active',1)
		->get();

		if($queryCategory->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	private function getVodByCategoryAndPartnerId($data = []){ 

		$arrReturn = [];

		$this->ci->load->config('partners');
		$partner_slug = $this->getPartnerSlubByPartnerId($data['vvod_partners_id']);

		$partner_api_config = ($partner_slug)?$this->ci->config->item('request_api')[(string)$partner_slug]:[];




		if($data['is_map_api_category_status']){
			$api_category_id = $data['vvod_partner_api_category_id'];

			if(count($partner_api_config) > 0){
				$response = $this->curlRequestVodByPartnerConfigAndCategory([
					'config'=>$partner_api_config,
					'category'=>$api_category_id
				]);

				return $this->outputVodFromPartnerResponse([
					'response'=>$response,
					'partner_slug'=>$partner_slug
				]);

			}
				



		}

		if($data['is_map_api_slug_status']){
			$api_slug = $data['vvod_partner_api_slug']; 

			if(count($partner_api_config) > 0){
				$response = $this->curlRequestVodByPartnerConfigAndCategory([
					'config'=>$partner_api_config,
					'category'=>$api_slug
				]);

				return $this->outputVodFromPartnerResponse([
					'response'=>$response,
					'partner_slug'=>$partner_slug
				]);



			}


		}




	}

	private function getPartnerSlubByPartnerId($vvod_partners_id){
		$query = $this->ci->db->select('id,slug')
		->from('vvod_partners')
		->where('id',$vvod_partners_id)
		->get();

		if($query->num_rows() > 0){
			return $query->row()->slug;
		}else{
			return '';
		}
	}

	private function curlRequestVodByPartnerConfigAndCategory($data = []){


				$ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->create_json_file_url);
                //curl_setopt($ch, CURLOPT_HTTPHEADER, $this->curl_authentication_header);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
                curl_setopt($ch, CURLOPT_HEADER, FALSE);
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

                $response = curl_exec($ch);
                curl_close($ch);

                if($response){
                	return json_decode($response);
                }
	}

	private function outputVodFromPartnerResponse($data = []){
		$response = $data['response']; 


		switch ($data['partner_slug']) {
			case 'mv':
				return convertOutputMVResponse([
					'response'=>$response
				]);
			break;
			
			default:
				# code...
			break;
		}

	}

	private function getPartnerInformationByPartnerPackageId($partner_package_id){

		$stdReturn = new StdClass();

		$query = $this->ci->db->select('*')
		->from('vvod_partners')
		->join('vvod_partner_packages','vvod_partners.id = vvod_partner_packages.vvod_partners_id','left')
		->where('vvod_partner_packages.id',$partner_package_id)
		->get();

		if($query->num_rows() > 0){ 
			$rowPartner = $query->row();
			
			
			$stdReturn->partner_id = $rowPartner->vvod_partners_id;
			$stdReturn->partner_contact_name = $rowPartner->contact_name;
			$stdReturn->partner_contact_address = $rowPartner->contact_address;
			$stdReturn->partner_contact_telephone = $rowPartner->contact_telephone;
			$stdReturn->partner_contact_fax = $rowPartner->contact_fax;
			$stdReturn->logo_image = $this->backend_url.'partner/uploaded/partner/logo/'.$rowPartner->vvod_partners_id.'/'.$rowPartner->logo_image;
			$stdReturn->cover_image = $this->backend_url.'partner/uploaded/partner/cover/'.$rowPartner->vvod_partners_id.'/'.$rowPartner->cover_image;
			$stdReturn->partner_slug = $rowPartner->slug;
		}

		return $stdReturn;


	}

	private function checkVVODUserExist($data = []){
		$query = $this->ci->db->select('*')
		->from('vvod_users')
		->where('ref_id',$data['members_id'])
		->where('active',1)
		->get();

		if($query->num_rows() > 0){ 

			$vvod_user_row = $query->row();
			// check storage token if user exist 

			
			// check storage token and check devices exist 
			$getReturn = $this->checkStorageTokenAndDeviceExist([
				'vvod_users_id'=>$vvod_user_row->id,
				'storage_token'=>$data['storage_token']
			]);



			if($getReturn['status']){
				return [
					'status'=>true,
					'set_storage_token_status'=>(isset($getReturn['set_storage']) && $getReturn['set_storage'])?true:false,
					'storage_token'=>(isset($getReturn['storage_token']) && $getReturn['storage_token'] != '')?$getReturn['storage_token']:$data['storage_token'],
					'member_data'=>$query->row()
				];

			}else{
				return [
					'status'=>false,
					'code'=>@$getReturn['code'],
					'message'=>@$getReturn['message']
				];
			}

			
			//return $query->row();
		}else{
			// create new vvod_user from login type and members_id
			$member_data = $this->getUserInformationByMemberAndLoginType([
				'members_id'=>$data['members_id'],
				'login_type'=>'member'
			]);



			if(!$member_data){
				return [
					'status'=>false,
					'code'=>'member-not-found',
					'message'=>'Member not found,Please try again.'
				];
			}else{
				if($data['storage_token'] != ''){
					return [
						'status'=>false,
						'code'=>'token-not-found',
						'message'=>'Storage token not found,Please try again.'
					];
				}
			}

			// insert into vvod_users account 
			$this->ci->db->insert('vvod_users',[
				'ref_id'=>$data['members_id'],
				'user_type'=>'member',
				'firstname'=>$member_data->firstname,
				'lastname'=>$member_data->lastname,
				'email'=>$member_data->email,
				'gender'=>$member_data->gender,
				'language'=>$member_data->language,
				'age'=>$member_data->age,
				'dob'=>$member_data->dob,
				'phone'=>$member_data->phone,
				'picture_profile'=>$member_data->picture_profile,
				'line_id'=>$member_data->line_id,
				'facebook_id'=>$member_data->facebook_id,
				'uuid'=>$member_data->uuid,
				'os'=>$member_data->os,
				'os_version'=>$member_data->os_version,
				'device_type'=>$member_data->device_type,
				'device_token'=>$member_data->device_token,
				'created'=>date('Y-m-d H:i:s'),
				'active'=>1

			]);

			$insert_id = $this->ci->db->insert_id();

			$storage_token = $this->generateStorageToken([
				'vvod_users_id'=>$insert_id
			]);

			// update and generate storage token for vvod_user
			$this->ci->db->insert('vvod_user_authentication_tokens',[
				'vvod_users_id'=>$insert_id,
				'storage_token'=>$storage_token,
				'user_agent'=>$this->getUserAgent(),
				'flag'=>1,
				'created'=>date('Y-m-d H:i:s')
			]);

			$user_data = $this->getVVODUsersRow($insert_id); 

			return [
				'status'=>true,
				'set_storage_token_status'=>true,
				'storage_token'=>$storage_token,
				'member_data'=>$user_data
			];

			

			
		}


	}


	private function getUserInformationByMemberAndLoginType($data = []){

		$members_id = $data['members_id'];

		switch ($data['login_type']) {
			case 'member':

				$queryMember = $this->ci->db->select('*')
				->from('members')
				->where('id',$members_id)
				->get();

				if($queryMember->num_rows() > 0){
					return $queryMember->row();
				}else{
					return false;
				}
				
			break;
			case 'guest':

			break;
			default:
				# code...
			break;
		}
	}


	private function generateStorageToken($data = []){

		$this->ci->load->library([
			'encryption'
		]);
		$this->ci->encryption->initialize(
		        array(
		                'cipher' => 'aes-128',
		                'mode' => 'ctr',
		                'key' => 'XCOokblusWVKbu5e'
		        )
		);

		$myData = [
			'vvod_users_id'=>$data['vvod_users_id'],
			'slug'=>'storage-token'
		];

		$build_to_url = http_build_query($myData);

		// return $this->ci->encryption->encrypt($build_to_url); 

		return md5($data['vvod_users_id'].date('Y-m-d H:i:s'));

	}

	private function getVVODUsersRow($vvod_users_id){
		$query = $this->ci->db->select('*')
		->from('vvod_users')
		->where('id',$vvod_users_id)
		->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	private function getUserAgent(){
		$this->ci->load->library([
			'user_agent'
		]); 

		return $this->ci->agent->platform();


	}

	private function getMyPackageList($data = []){ 

		$arrReturn = [];

		// get all package from vvod_user_package_live 

		$queryMypackage = $this->ci->db->select('*')
		->from('vvod_user_package_live')
		->where('vvod_users_id',$data['vvod_users_id'])
		->where('end_datetime >=',date('Y-m-d H:i:s'))
		->where('active',1)
		->order_by('created','desc')
		->get();

		if($queryMypackage->num_rows() > 0){ 

			foreach ($queryMypackage->result() as $key => $value) {
				# code...

				//print_r($value->vvod_partner_packages_id);exit;
				$package_data = $this->getPartnerPackageDataById($value->vvod_partner_packages_id);


				array_push($arrReturn, [
					'package_id'=>$package_data->package_id,
					'package_code'=>$package_data->package_code,
					'package_name'=>$package_data->package_name,
					'package_description'=>$package_data->package_description,
					'package_price'=>$package_data->package_price,
					'package_currency_symbol'=>$package_data->package_currency_symbol,
					'package_days'=>$package_data->package_days,
					'package_cover'=>$package_data->package_cover,
					'start_datetime'=>$value->start_datetime,
					'end_datetime'=>$value->end_datetime
				]);



			}

		}

		return $arrReturn;


	}

	private function getMainPageData($data = []){ 


		

		$arrReturn = [];

		// get all category is recomend in main page 



		// get all partner by vvod_partners

		$vvod_partners = $this->ci->db->select('*')
		->from('vvod_partners')
		->where('active',1)
		->order_by('id','asc')
		->get();


		if($vvod_partners->num_rows() > 0){

				foreach ($vvod_partners->result() as $key => $value) {
					# code...

					// get content homepage by partner
					switch ($value->slug) {
						case 'mv':
							$this->checkVVODUserCreateUserWithMVPartner([
								'vvod_partners_id'=>$value->id,
								'vvod_users_id'=>$data['vvod_users_id']
							]);
							
							$mv_content  = $this->getMVContentHomePage([

							]);

							// print_r($mv_content);exit;
							array_push($arrReturn, [
								'partner_id'=>$value->id,
								'partner_contact_name'=>$value->contact_name,
								'partner_contact_address'=>$value->contact_address,
								'partner_contact_telephone'=>$value->contact_telephone,
								'partner_contact_fax'=>$value->contact_fax,
								'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$value->id.'/'.$value->logo_image,
								'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$value->id.'/'.$value->cover_image,
								'vod_homepage'=>$this->getMVVodListData([
									'mv_content'=>$mv_content
								])
							]);
						break;
						
						default:
							# code...
						break;
					}




				}

		}


		return $arrReturn;
		//print_r($arrReturn);exit;






		// =================== under this older version ===============================
		//=============================================================================
		$vvod_categories = $this->ci->db->select('*')
		->from('vvod_categories')
		->where('active',1)
		->where('main_page_status',1)
		->order_by('main_page_ordinal','asc')
		->get();

		if($vvod_categories->num_rows() > 0){

			$arrAllPartner = [];

			

			$arrAllPartner = $this->getAllPartnerByMainpageCategoryResult([
				'vvod_categories_result'=>$vvod_categories->result()
			]);

			
			$key = array_search('mv', array_column($arrAllPartner, 'partner_slug'));

			$is_mv = false;
			$mv_content = '';


			if(is_numeric($key)){ 
				$this->checkVVODUserCreateUserWithMVPartner([
					'vvod_partners_id'=>$arrAllPartner[$key]['partner_id'],
					'vvod_users_id'=>$data['vvod_users_id']
				]);
				$is_mv  = true;
				$mv_content = $this->getMVContentHomePage([

				]);

				
			} 

			foreach ($arrAllPartner as $key => $value) {
				# code...
				print_r($value);
			}


			$arrReturn = [
				'category_id'=>'home',
				'category_name'=>'Home',
				'category_name'=>'Home Page',
				'vod_recomended_by_category'=>$this->getMVVodListData([

				])
			];

			

			// foreach ($vvod_categories->result() as $key => $row) {
			// 	# code...
			// 	$vod_recomended_by_category = $this->getMainPageVodRecomendedByCategory([
			// 			'vvod_categories'=>$row,
			// 			'vvod_users_id'=>$data['vvod_users_id'],
			// 			'is_mv'=>$is_mv,
			// 			'mv_content'=>$mv_content
			// 		]);

			// 	if(count($vod_recomended_by_category) > 0){
			// 		array_push($arrReturn, [
			// 			'category_id'=>$row->id,
			// 			'category_name'=>$row->name,
			// 			'category_description'=>$row->description,
			// 			'vod_recomended_by_category'=>$vod_recomended_by_category
			// 		]);
			// 	}
				

			// }
		}

		// if(isset($_GET['test']) && $_GET['test'] == 'test'){
		// 	print_r($arrReturn);exit;
		// }

		return $arrReturn;





	}

	private function getMainPageVodRecomendedByCategory($data = []){ 

			
			$arrVodList = [];


			$getMvMapSlug = $this->ci->db->select('*')
			->from('vvod_partner_map_category')
			->join('vvod_partners','vvod_partner_map_category.vvod_partners_id = vvod_partners.id')
			->where('vvod_partner_map_category.vvod_categories_id',$data['vvod_categories']->id)
			->where('vvod_partner_map_category.active',1)
			->get(); 

			if($getMvMapSlug->num_rows() > 0){ 

				foreach ($getMvMapSlug->result() as $key => $value) {
					# code...
					if($value->slug == 'mv' && $data['is_mv']){ 
						$vvod_partner_api_slug = $value->vvod_partner_api_slug; 

						//print_r($value);exit;

						$arrVodList = $this->getMVVodListBySlugToCategory([
							'slug'=>$vvod_partner_api_slug,
							'mv_content'=>$data['mv_content'],
							'partner_id'=>$value->vvod_partners_id,
							'partner_contact_name'=>$value->contact_name,
							'partner_contact_address'=>$value->contact_address,
							'partner_contact_telephone'=>$value->contact_telephone,
							'partner_contact_fax'=>$value->contact_fax,
							'logo_image'=>$this->backend_url.'partner/uploaded/partner/logo/'.$value->vvod_partners_id.'/'.$value->logo_image,
							'cover_image'=>$this->backend_url.'partner/uploaded/partner/cover/'.$value->vvod_partners_id.'/'.$value->cover_image,
							'slice_status'=>true
						]);


					}
				}

			}

			// print_r($arrVodList);exit;
			return $arrVodList;
		



		// print_r($mv_content);exit;

		


	}

	private function checkStorageTokenAndDeviceExist($data = []){
		$vvod_users_id = $data['vvod_users_id'];
		$storage_token = $data['storage_token']; 


		if($storage_token == "" || $storage_token == "N"){
			// check already record in vvod user by vvod_users_id authentication token row
			$query = $this->ci->db->select('*')
			->from('vvod_user_authentication_tokens')
			->where('vvod_users_id',$vvod_users_id)
			->where('flag',1)
			->get();

			if($query->num_rows() > 0){
				// if exist device login by this user then  set this record to flag equal 0 and generate new storage_token and return and set status to set new token equal to 0

				$row = $query->row();

				$this->ci->db->update('vvod_user_authentication_tokens',[
					'flag'=>0,
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$row->id]);

			}
			// generate new token and also send to response and send status to have to update storage token
				$storage_token = $this->generateStorageToken([
					'vvod_users_id'=>$vvod_users_id
				]);

				// update and generate storage token for vvod_user
				$this->ci->db->insert('vvod_user_authentication_tokens',[
					'vvod_users_id'=>$vvod_users_id,
					'storage_token'=>$storage_token,
					'user_agent'=>$this->getUserAgent(),
					'flag'=>1,
					'created'=>date('Y-m-d H:i:s')
				]); 


				return [
					'status'=>true,
					'set_storage'=>true,
					'storage_token'=>$storage_token
				];


		}else{
			// if this case first check record is flag equal to zero , if equal to zero then return false and message to have to set new storage_token and keep it 



			$query = $this->ci->db->select('*')
			->from('vvod_user_authentication_tokens')
			->where('vvod_users_id',$vvod_users_id)
			->where('CONVERT(VARCHAR(MAX), storage_token) = ',$storage_token)
			->get(); 

			// if(isset($_GET['test']) && $_GET['test'] == 'test'){ 

			// 	echo $this->ci->db->last_query();exit;
			// 	print_r($vvod_users_id);

			// 	print_r($storage_token); 

			// 	print_r($query->num_rows());exit;
			// }

			if($query->num_rows() > 0){

				$row = $query->row();

				if($row->flag == '0'){
					return [
						'status'=>false,
						'code'=>'login-from-other-device',
						'message'=>'This user loged in from other device,Please check.'
					];
				}else{
					return [
						'status'=>true
					];
				}

			}else{
				return [
					'status'=>false,
					'code'=>'storage-token-not-found',
					'message'=>'Storage token not found,Please try again.'
				];
			}






		}

		


		

	}

	private function getAllPartnerByMainpageCategoryResult($data = []){ 

		$arrReturn = [];

		foreach ($data['vvod_categories_result'] as $key => $value) {
			# code...
			$query_check_map = $this->ci->db->select('vvod_partner_map_category.vvod_partners_id,vvod_partner_map_category.vvod_categories_id,vvod_partner_map_category.active,vvod_partners.slug as partner_slug')
			->from('vvod_partner_map_category')
			->join('vvod_partners','vvod_partner_map_category.vvod_partners_id = vvod_partners.id')
			->where('vvod_partner_map_category.vvod_categories_id',$value->id)
			->where('vvod_partner_map_category.active',1)
			->get();

			if($query_check_map->num_rows() > 0){
				foreach ($query_check_map->result() as $key => $value) {
					# code...
					array_push($arrReturn, [
						'partner_id'=>$value->vvod_partners_id,
						'partner_slug'=>$value->partner_slug
					]);

				}
			}


		}

		$temp = array_unique(array_column($arrReturn, 'partner_id'));
		$unique_arr = array_intersect_key($arrReturn, $temp);

		return $unique_arr;

	}

	private function checkVVODUserCreateUserWithMVPartner($data = []){
		$vvod_users_id = $data['vvod_users_id']; 
		$vvod_partners_id = $data['vvod_partners_id']; 



		$queryCheck = $this->ci->db->select('*')
		->from('vvod_user_partner_authorization')
		->where('vvod_users_id',$vvod_users_id)
		->where('vvod_partners_id',$vvod_partners_id)
		->where('active',1)
		->get();

		// if(isset($_GET['test']) && $_GET['test'] == 'test'){
		// 		echo $this->ci->db->last_query();exit;
		// 	}

		if($queryCheck->num_rows() > 0){
			// check access token expire 
			$rowCheck = $queryCheck->row(); 

			

			

			// if access token expire then request login again and get new token then update token and update expire date to vvod_user_partner_authorization

			$currentDateTime = new DateTime();
			$rowExpireDateTime = new DateTime($rowCheck->access_token_expire);

			



			if(($currentDateTime >= $rowExpireDateTime) || !$rowCheck->access_token_expire){ 


				// if(isset($_GET['test']) || $_GET['test'] == 'test'){
				// 	// print_r('aaaa');exit;
				// 	print_r($currentDateTime);
				// 	print_r($rowExpireDateTime);exit;
				$response = $this->getMVLoginToRenewAccessToken([
					'previous_access_token'=>$rowCheck->access_token,
					'partner_api_id'=>$rowCheck->vvod_user_ref_id
				]);  
				$this->mv_access_token = @$response['access_token'];
				$this->mv_id = $rowCheck->partner_api_id;

				if($response){
						$this->ci->db->update('vvod_user_partner_authorization',[
						'access_token'=>($response['access_token'])?$response['access_token']:$rowCheck->access_token,
						'access_token_expire'=>$response['access_token_expire'],
						'updated'=>date('Y-m-d H:i:s')
						],['id'=>$rowCheck->id]);

				}

			}else{
				$this->mv_access_token = $rowCheck->access_token;
				$this->mv_id = $rowCheck->partner_api_id;

			}

				// if(isset($_GET['test']) && $_GET['test'] == 'test'){
				// 	print_r($rowCheck);exit;
				// }

				

				

				

			







		}else{
			// register for get access token and insert row to vvod_user_partner_authorization

			$response  = $this->getMVRegisterVod([
				'vvod_users_id'=>$vvod_users_id
			]); 

		// 	if(isset($_GET['test']) && $_GET['test'] == 'test'){
		// 	print_r($response);exit;
		// }
			$this->mv_access_token = $response['access_token'];
			$this->mv_id = $response['partner_api_id'];

			$this->ci->db->insert('vvod_user_partner_authorization',[
				'vvod_users_id'=>$vvod_users_id,
				'vvod_user_ref_id'=>$response['vvod_user_ref_id'],
				'vvod_partners_id'=>$vvod_partners_id,
				'partner_api_id'=>$response['partner_api_id'],
				'access_token'=>$response['access_token'],
				'access_token_expire'=>$response['access_token_expire'],
				'created'=>date('Y-m-d H:i:s')
			]);




		}






	}

	private function getMVAccessTokenDateTimeExpire($access_token){

		$access_token = base64_decode($access_token);

		preg_match_all('#\{(.*?)\}#', $access_token, $match);  

		//print_r($match);exit;

		$json_text = $match[1][1];

		$exJsonText = explode(',', $json_text);

		$exColon = explode(':', $exJsonText[3]); 

		$expire_timestamp = trim($exColon[1]); 

		$datetime_expire = new DateTime();
		$datetime_expire->setTimestamp($expire_timestamp);

		return $datetime_expire->format('Y-m-d H:i:s');
	}

	private function getMVRegisterVod($data = []){ 

		$this->ci->load->config('partners');

		$mv_config = $this->ci->config->item('request_api')['mv'];

		$licence_token = $mv_config['Licence'];
		$user_agent = $this->getUserAgent();
		$register_url = $mv_config['register']['request_url'];

		// print_r($register_url);exit;
		// $user_id = sha1($data['vvod_users_id']); 

		if(ENVIRONMENT == 'development'){
			$user_id = 'VVODSDEV'.str_pad($data['vvod_users_id'], 8, '0', STR_PAD_LEFT).time();
		}else{
			$user_id = 'VVODS'.str_pad($data['vvod_users_id'], 8, '0', STR_PAD_LEFT).time();
		}
		

		//print_r($user_id);exit;


		$data = [
			'UserId'=>$user_id,
			'Usertoken'=>'',
			'Hosttoken'=>'',
			'Licencetoken'=>$licence_token,
			'userAgent'=>($user_agent)?$user_agent:"Unknown"
		];        


		$data_string = json_encode($data);

		//print_r($data_string);exit;      

		                                                                                                                     
		$ch = curl_init($register_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
		    'Content-Type: application/json',
		    'Content-Length: ' . strlen($data_string)
			)                                                                       
		);                                 

		                                                                      
		                                                                                                                     
		$result = curl_exec($ch); 





		if (curl_errno($ch)) {
		    $error_msg = curl_error($ch);

		    // create log file if curl has error message 
		    // $log_file_path = $this->createLogFilePath('curlErrorLogs');
			// $file_content = date("Y-m-d H:i:s") . ' error : ' . $error_msg . "\n"; 
			// $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
			// file_put_contents($log_file_path, $file_content, FILE_APPEND);
			// unset($file_content); 
		    

		    // print_r($error_msg);exit;
		}

		// echo $result;exit;
		$result_json = json_decode($result); 

		// $log_file_path = $this->createLogFilePath('curlRegisterResponseLogs');
		// 	$file_content = date("Y-m-d H:i:s") . ' response : ' . json_encode($result_json) . "\n"; 
		// 	$file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
		// 	file_put_contents($log_file_path, $file_content, FILE_APPEND);
		// 	unset($file_content); 

		//print_r($result_json);exit;



		

		if(@$result_json->res_code == '200'){


			$res_data = $result_json->res_data; 

			//print_r($res_data);exit;
			$b64_res_data = base64_decode($res_data); 
			$json_res_data = json_decode($b64_res_data); 

			//print_r($json_res_data);exit;


			return [
				'partner_api_id'=>@$json_res_data->MVId,
				'access_token'=>$json_res_data->AccessToken,
				'access_token_expire'=>$this->getMVAccessTokenDateTimeExpire($json_res_data->AccessToken),
				'vvod_user_ref_id'=>$user_id
			];

		}



		//print_r($result);exit;

	}

	private function getMVLoginToRenewAccessToken($data = []){ 

		$this->ci->load->config('partners');

		$mv_config = $this->ci->config->item('request_api')['mv'];

		$licence_token = $mv_config['Licence'];
		$login_url = $mv_config['login']['request_url']; 
		$userid = $data['partner_api_id']; 


		$data_post = [
			'UserId'=>$userid,
			'Licencetoken'=>$mv_config['Licence'],
			'userAgent'=>$this->getUserAgent()
		]; 

		$data_string = json_encode($data_post);

		
		// $log_file_path = $this->createLogFilePath('RenewAccessTokenCriteriaLogs');
		// 	$file_content = date("Y-m-d H:i:s") . ' criteria : ' . $data_string . "\n"; 
		// 	$file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
		// 	file_put_contents($log_file_path, $file_content, FILE_APPEND);
		// 	unset($file_content);

		


		//print_r($data_string);exit;                                                                                   
		                                                                                                                     
		$ch = curl_init($login_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
		    'Content-Type: application/json',
		    'Content-Length: ' . strlen($data_string),
		    'Authorization: Bearer '.$data['previous_access_token']
			)                                                                       
		);                                                                                                                   
		                                                                                                                     
		$result = curl_exec($ch); 

		// if(isset($_GET['test']) && $_GET['test'] == 'test'){
		// 			print_r($result);exit;
		// }








		if (curl_errno($ch)) {
		    $error_msg = curl_error($ch); 

		    

		    // create log file if curl has error message 
		    // $log_file_path = $this->createLogFilePath('curlErrorLogs');
			// $file_content = date("Y-m-d H:i:s") . ' error : ' . $error_msg . "\n"; 
			// $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
			// file_put_contents($log_file_path, $file_content, FILE_APPEND);
			// unset($file_content); 


		    // print_r($error_msg);exit;
		}

		$result_json = json_decode($result);

		

		

		if(@$result_json->res_code == '300'){ 

			// $log_file_path = $this->createLogFilePath('RenewAccessTokenSuccessLogs');
			// $file_content = date("Y-m-d H:i:s") . ' criteria : ' . json_encode($result_json) . "\n"; 
			// $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
			// file_put_contents($log_file_path, $file_content, FILE_APPEND);
			// unset($file_content);


			$res_data = $result_json->res_data;
			$b64_res_data = base64_decode($res_data);
			$json_res_data = json_decode($b64_res_data);


			return [
				'partner_api_id'=>@$json_res_data->MVId,
				'access_token'=>$json_res_data->AccessToken,
				'access_token_expire'=>$this->getMVAccessTokenDateTimeExpire($json_res_data->AccessToken)
			];

		}else{
			// write log file to exception

			// $log_file_path = $this->createLogFilePath('RenewAccessTokenErrorLogs');
			// $file_content = date("Y-m-d H:i:s") . ' criteria : ' . json_encode($result_json) . "\n"; 
			// $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
			// file_put_contents($log_file_path, $file_content, FILE_APPEND);
			// unset($file_content);

			return false;


		}







	}

	private function getMVDummyContent($data = []){ 
		$this->ci->load->config('partners');

		$mv_config = $this->ci->config->item('request_api')['mv'];

		$dummy_content_url = $mv_config['dummy_content']['request_url']; 

		$data = [
			'mvid'=>$this->mv_id
		];

		$data_string = json_encode($data);

		//print_r($data_string);exit;    
		//print_r($this->mv_access_token);exit;                                                                               
		                                                                                                                     
		$ch = curl_init($dummy_content_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
		    'Content-Type: application/json',
		    'token:'.$this->mv_access_token
			)                                                                       
		);                                                                                                                   
		if(curl_exec($ch) === false)
		{
		    // echo 'Curl error: ' . curl_error($ch);
		}
		                                                                                                                     
		$result = curl_exec($ch); 

		$response = json_decode($result);

		//print_r($response);exit;
		return $response;

	}

	private function getMVContent($data = []){
		$this->ci->load->config('partners');

		$mv_config = $this->ci->config->item('request_api')['mv'];

		$content_url = $mv_config['content']['request_url']; 

		$data = [
			'req_page'=>'content'
		];

		$data_string = json_encode($data);

		//print_r($data_string);exit;    
		//print_r($this->mv_access_token);exit;      
		// $this->mv_access_token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQ4LCJuYW1lIjoiVUZOSmRtVnljMmx2YmpBPSIsImlhdCI6MTU4Njc2MjI5MSwiZXhwIjoxNTg3MzY3MDkxLCJhdWQiOiJwc2ktY2xpZW50IiwiaXNzIjoicHNpLW12aHViIiwic3ViIjoibXZodWItdXNlciJ9.LAglkgH2Aa1ONWqKA8c2krDN5QToyR9AlZRZcAZAJa-uOJpj9MiY4oZXoLo9guMouLnxkFg2huDS4am15myPQV9KJd0pEJPa5TVBCA9JtU1gkV2gGYMhIFekQNMKiKCu7P5CDyQQ8aqUvK9EcvbYzQyCXxsrRBrjmAkZzg-qEvk';
		// $this->mv_access_token = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MjQ4LCJuYW1lIjoiVUZOSmRtVnljMmx2YmpBPSIsImlhdCI6MTU4Njc2MjI5MSwiZXhwIjoxNTg3MzY3MDkxLCJhdWQiOiJwc2ktY2xpZW50IiwiaXNzIjoicHNpLW12aHViIiwic3ViIjoibXZodWItdXNlciJ9.LAglkgH2Aa1ONWqKA8c2krDN5QToyR9AlZRZcAZAJa-uOJpj9MiY4oZXoLo9guMouLnxkFg2huDS4am15myPQV9KJd0pEJPa5TVBCA9JtU1gkV2gGYMhIFekQNMKiKCu7P5CDyQQ8aqUvK9EcvbYzQyCXxsrRBrjmAkZzg-qEvk';

		// if(isset($_GET['test']) && $_GET['test'] == 'test'){
		// 	print_r($this->mv_access_token);exit;
		// } 
		                                                                        
		                                                                                                                     
		$ch = curl_init($content_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
		    'Content-Type: application/json',
		    'Authorization:Bearer '.$this->mv_access_token
			)                                                                       
		);                                                                                                                   
		if(curl_exec($ch) === false)
		{
		    // echo 'Curl error: ' . curl_error($ch);
		}
		                                                                                                                     
		$result = curl_exec($ch); 

		// echo $result;exit;

		// if(isset($_GET['test']) && $_GET['test'] == 'test'){ 
		// 	print_r($result);exit;
		// }



		$response = json_decode($result);

		//print_r($response);exit;
		return $response;
	}

	private function getMVContentHomePage($data = []){
		$this->ci->load->config('partners');

		$mv_config = $this->ci->config->item('request_api')['mv'];

		$content_url = $mv_config['content']['request_url']; 

		$data = [
			'req_page'=>'home'
		];

		$data_string = json_encode($data);


		//echo $this->mv_access_token;exit;
		// echo $content_url;
		// print_r($data_string);exit;
           
		                                                                                                                     
		$ch = curl_init($content_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
		    'Content-Type: application/json',
		    'Authorization:Bearer '.$this->mv_access_token
			)                                                                       
		);                                                                                                                   
		if(curl_exec($ch) === false)
		{
		    // echo 'Curl error: ' . curl_error($ch);
		}
		                                                                                                                     
		$result = curl_exec($ch); 

		// echo $result;exit;
		$response = json_decode($result);


		if(isset($_GET['test']) && $_GET['test'] == 'test'){
			print_r($response);exit;
		}


		return $response;
	}

	private function getMVVODContent($data = []){
		$contentid = $data['vod_id']; 

		$this->ci->load->config('partners');

		$mv_config = $this->ci->config->item('request_api')['mv'];

		// $dummy_overview_url = $mv_config['dummy_overview']['request_url'];
		$overview_url = $mv_config['content_overview']['request_url'];


		$data = [
			'id'=>(string)$contentid
		];

		$data_string = json_encode($data);

		//print_r($data_string);exit;    
		//print_r($this->mv_access_token);exit;   

		                                                                        
		                                                                                                                     
		$ch = curl_init($overview_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
		    'Content-Type: application/json',
		    'Authorization: Bearer '.$this->mv_access_token
			)                                                                       
		);                                                                                                                   
		if(curl_exec($ch) === false)
		{
			// if(isset($_GET['test']) && $_GET['test'] == 'test'){
		 //    	echo 'Curl error: ' . curl_error($ch);
			// }
		}
		                                                                                                                     
		$result = curl_exec($ch); 
		 

		$response = json_decode($result); 

		if(isset($_GET['test']) && $_GET['test'] == 'test'){

			print_r($this->mv_access_token);
			print_r($response);exit;
			// print_r($result);exit;
		}   

		return $response;


	}

	private function getRunPsiUserId($data = []){ 

		$this->ci->load->library([
			'encryption'
		]);
		$this->ci->encryption->initialize(
		        array(
		                'cipher' => 'aes-128',
		                'mode' => 'ctr',
		                'key' => 'XCOokblusWVKbu5e'
		        )
		);

		$data_to_build = [
			'usr'=>$data['vvod_users_id']
		];
		$build_to_url = http_build_query($data_to_build); 

		// print_r($build_to_url);exit;



		$psi_user_id = $this->ci->encryption->encrypt($build_to_url); 

		// print_r(md5($build_to_url));exit;

		print_r($psi_user_id);exit;
	}

	private function createLogFilePath($filename = '') {
        $log_path = './application/logs/partner_logs';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }

    private function getMVVodListBySlugToCategory($data = []){ 

    	$arrListAllBySlug = []; 

    	// print_r($data['mv_content']);exit;
    	// if(property_exists($data['mv_content'], 'res_code')){
    	// 	return [];
    	// }


    	// if(isset($_GET['test']) && $_GET['test'] == 'test'){
    	// 	print_r($data['mv_content']);exit;
    	// }

    	foreach ($data['mv_content'] as $key => $value) {
    		# code...
    		//print_r($value->Tab); 
    		$key = array_search($data['slug'], array_column($value->Tab, 'nameTab'));
    		 //print_r($value->Tab);exit;
    		if(is_numeric($key)){
    			// print_r($value->Tab[$key]);exit;
    			foreach ($value->Tab[$key]->listcontent as $k => $v) {
    				# code...
    				array_push($arrListAllBySlug, $v);
    			}
    		}
    	} 

    	// slic array and keep just only 4 content 
    	if($data['slice_status']){
    		$arrListAllBySlug = array_slice($arrListAllBySlug, 0,4);
    	}

    	$arrReturnVodList = [];

    	foreach ($arrListAllBySlug as $key => $value) {
    		# code...
    		array_push($arrReturnVodList, [
    			'vod_id'=>$value->id,
    			'vod_name'=>$value->name,
    			'vod_image'=>$value->img,
    			'partner_id'=>$data['partner_id'],
    			'partner_data'=>[
    				'partner_id'=>$data['partner_id'],
	    			'partner_contact_name'=>$data['partner_contact_name'],
					'partner_contact_address'=>$data['partner_contact_address'],
					'partner_contact_telephone'=>$data['partner_contact_telephone'],
					'partner_contact_fax'=>$data['partner_contact_fax'],
					'logo_image'=>$data['logo_image'],
					'cover_image'=>$data['cover_image']
    			]
    		]);

    	}

    	return $arrReturnVodList;

    	// print_r($arrReturnVodList);exit;
    }

    private function getMVVodListData($data = []){
    	$arrReturn = [];
    	$mv_content = $data['mv_content'];

    	if(is_array($mv_content) && count($mv_content) > 0){
    		foreach ($mv_content as $key => $value) {
    			# code...
    			array_push($arrReturn, [
    				'vod_id'=>$value->id,
    				'vod_name'=>$value->name,
    				'vod_image'=>$value->img,
    				'vod_free_status'=>($value->prm == '0')?true:false
    			]);

    		}
    	}

    	return $arrReturn;

    }



    private function getMVVodLink($vod_link){
    	$arrReturn = [];

    	if(is_array($vod_link) && count($vod_link) > 0){

    		foreach ($vod_link as $key => $value) {
    			# code...

    			if(strpos($value, '/') !== false){

    				/* older version */
	    			$exLink = explode('/', $value); 	    			
	    			$arrChannelName = [];
	    			$arrChannelName[0] = $exLink[3];
	    			$arrChannelName[1] = $exLink[4];
	    			$arrChannelName[2] = $exLink[5];
	    			$channel_name = implode('/', $arrChannelName);
	    			

	    			/* holding version 
	    			$exLink = explode('.mp4', $value);

	    			$subStrLink = substr($exLink[0], -24);
	    			if(strpos($subStrLink, '/') !== false){ 

	    				$exSlash = explode('/', $subStrLink);

	    				$subStrLink = $exSlash[1];
	    			} 

	    			*/

	    			array_push($arrReturn, [
	    				'channel_name'=>$channel_name,
	    				'link_url'=>$value
	    			]);
	    		}else{
	    			array_push($arrReturn, $value);
	    		}

    		}

    		// check if run number array 
    		$checkJustRunNumber = false;
    		foreach ($arrReturn as $key => $value) {
    			# code...
    			if(is_numeric($value)){
    				$checkJustRunNumber = true;
    			}
    		}

    		return ($checkJustRunNumber)?[]:$arrReturn;


    	}else{
    		if(!is_array($vod_link) && strpos($vod_link, '/') !== false){
    			/* older version */
	    			$exLink = explode('/', $vod_link); 	    			
	    			$arrChannelName = [];
	    			$arrChannelName[0] = $exLink[3];
	    			$arrChannelName[1] = $exLink[4];
	    			$arrChannelName[2] = $exLink[5];
	    			$channel_name = implode('/', $arrChannelName);

	    			return [
	    				'channel_name'=>$channel_name,
	    				'link_url'=>$vod_link
	    			];

    		}else{
    				return [];
    		}
    	}

    	// print_r($arrReturn);exit; 

    	// return $arrReturn;
    }

    private function get_string_between($string, $start, $end){
	    $string = ' ' . $string;
	    $ini = strpos($string, $start);
	    if ($ini == 0) return '';
	    $ini += strlen($start);
	    $len = strpos($string, $end, $ini) - $ini;
	    return substr($string, $ini, $len);
	}

    private function runTransactionNumber(){ 

    	$queryIdentCurrent = $this->ci->db->query("select IDENT_CURRENT('vvod_transactions') as identity_current"); 

    	$rowIdentCurrent = ($queryIdentCurrent->row()->identity_current + 1); 

    	$runTransactionNumber = 'S3V'.str_pad($rowIdentCurrent, 6, '0', STR_PAD_LEFT); 

    	return $runTransactionNumber;

    }

    private function setVodTransactionLogs($data = []){

    	$this->ci->db->insert('vvod_transaction_logs',[
    		'vvod_transactions_id'=>$data['vvod_transactions_id'],
    		'type'=>$data['type'],
    		'log_message'=>$data['log_message'],
    		'created'=>date('Y-m-d H:i:s')
    	]);

    }

    private function getTransactionDataByTransactionId($data = []){ 

    	$vvod_users_id = $data['vvod_users_id'];


    	$queryTransaction = $this->ci->db->select('*')
    	->from('vvod_transactions')
    	->where('id',$data['vvod_transactions_id'])
    	->get();

    	//print_r($data);exit;

    	$stdReturn = new StdClass();
    	if($queryTransaction->num_rows() > 0){
    		$rowTransaction = $queryTransaction->row();  

    		//print_r($rowTransaction);exit;



    		

    		$stdReturn->transaction_id = $rowTransaction->id;
    		$stdReturn->transaction_number = $rowTransaction->transaction_number;
    		$stdReturn->sub_total = number_format($rowTransaction->sub_total,2);
    		$stdReturn->grand_total = number_format($rowTransaction->grand_total,2);
    		$stdReturn->payment_option = $rowTransaction->payment_option;
    		$stdReturn->payment_status = ($rowTransaction->payment_status)?'Paid':'Unpaid';
    		$stdReturn->created = $rowTransaction->created;
    		$stdReturn->updated = $rowTransaction->updated;
    		$stdReturn->transaction_package = $this->getPartnerPackageDataById($rowTransaction->vvod_partner_packages_id);

    	}
    	return $stdReturn;

    } 

    private function getPartnerPackageDataById($vvod_partner_packages_id){
    	$query = $this->ci->db->select('*')
    	->from('vvod_partner_packages')
    	->where('id',$vvod_partner_packages_id)
    	->where('active',1)
    	->get(); 

    	$stdReturn = new StdClass();

    	if($query->num_rows() > 0){ 


    		$row = $query->row(); 

    		//print_r($row);exit;

    		$partner_data = $this->getPartnerDataByPartnerId($row->vvod_partners_id);

    		$stdReturn->package_id = $row->id;
    		$stdReturn->package_purchase_ref_id = $row->purchase_ref_id;
    		$stdReturn->package_purchase_url = $partner_data->purchase_url;
    		$stdReturn->package_code = $row->code;
    		$stdReturn->package_name = $row->name;
    		$stdReturn->package_description = $row->description;
    		$stdReturn->package_price = number_format($row->price,2);
    		$stdReturn->package_currency_symbol = $row->currency_symbol;
    		$stdReturn->package_days = $row->days;
    		$stdReturn->package_cover =getPackageCoverImage($row);
    		$stdReturn->partner_id = $partner_data->id;
    		$stdReturn->partner_slug = $partner_data->slug;

    	}

    	return $stdReturn;
    }

    private function getPackageLiveTransactionByTransactionId($vvod_transactions_id){
    	$query = $this->ci->db->select('*')
    	->from('vvod_user_package_live_transaction')
    	->where('vvod_transactions_id',$vvod_transactions_id)
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row();
    	}else{
    		return new StdClass();
    	}
    }

    private function getPartnerDataByPartnerId($vvod_partners_id){
    	$query = $this->ci->db->select('*')
    	->from('vvod_partners')
    	->where('id',$vvod_partners_id)
    	->where('active',1)
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row();
    	}else{
    		return new StdClass();
    	}
    }

    private function getAccessTokenByUserId($vvod_users_id){ 

    	$query = $this->ci->db->select('*')
    	->from('vvod_user_partner_authorization')
    	->where('vvod_users_id',$vvod_users_id)
    	->where('access_token_expire >=',date('Y-m-d H:i:s'))
    	->order_by('id','desc')
    	->limit(1)
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row()->access_token;
    	}else{

    		// check if exist with access token expire 
    		$querySecond = $this->ci->db->select('*')
    		->from('vvod_user_partner_authorization')
    		->where('vvod_users_id',$vvod_users_id)
    		->order_by('id','desc')
    		->limit(1)
    		->get();

    		if($querySecond->num_rows() > 0){
    			return $querySecond->row()->access_token;
    		}
    		return '';
    	}

    }

    private function getWebViewPaymentUrl($data = []){ 

    	$return_url = '';
    	$access_token = $data['access_token'];
    	$transaction_package = $data['transaction_data']->transaction_package;
    	$partner_slug = $transaction_package->partner_slug;
    	$package_purchase_ref_id = $transaction_package->package_purchase_ref_id;
    	$transaction_number = $data['transaction_data']->transaction_number;





    	// print_r($partner_slug);exit;
    	switch ($partner_slug) {
    		case 'mv': 


    			# code...
		    	$data_build_query = [
		    		'AccessToken'=>$access_token,
		    		's3ref'=>$transaction_number,
		    		'pk'=>$package_purchase_ref_id
		    	];

		    	$url = $transaction_package->package_purchase_url;

		    	$url_build_query = http_build_query($data_build_query);

		    	$return_url = $url.'?'.$url_build_query;


    		break;
    		
    		default:
    			# code...
    		break;
    	}

    	return $return_url;

    }

    private function getUserIP(){
                       $client  = @$_SERVER['HTTP_CLIENT_IP'];
                            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                            $remote  = $_SERVER['REMOTE_ADDR'];

                            if(filter_var($client, FILTER_VALIDATE_IP))
                            {
                                $ip = $client;
                            }
                            elseif(filter_var($forward, FILTER_VALIDATE_IP))
                            {
                                $ip = $forward;
                            }
                            else
                            {
                                $ip = $remote;
                            }

                            return $ip;
    }


    private function sendIOSPaymentToMV($data = []){

    	$vvod_transactions = $data['vvod_transactions'];

    	$this->ci->load->config('partners');

    	$mv_config = $this->ci->config->item('request_api')['mv'];

    	$ios_payment_url = $mv_config['ios_payment']; 

    	$partner_packages = $this->getPartnerPackageDataById($vvod_transactions->vvod_partner_packages_id);

    	$curl_data = [
    		's3ref'=>$vvod_transactions->transaction_number,
    		'merchantid'=>'191',
    		'package_id'=>$partner_packages->package_purchase_ref_id,
    		'productdetail'=>$partner_packages->package_name.' - '.$partner_packages->package_description,
    		'total'=>$partner_packages->package_price
    	];


    	// get user access token by users_id token
    	$access_token = $this->getAccessTokenByUserIdAndPartnerId([
    		'vvod_users_id'=>$vvod_transactions->vvod_users_id,
    		'vvod_partners_id'=>$this->getPartnerInformationByPartnerPackageId($vvod_transactions->vvod_partner_packages_id)->partner_id
    	]);

    	// if(isset($_GET['test']) && $_GET['test'] == 'test'){
    	// 	print_r($access_token);exit;
    	// }

    	// print_r($ios_payment_url);exit;
    	// print_r($curl_data);exit;

    	$data_string = json_encode($curl_data);

    	$ch = curl_init($ios_payment_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
			    'Content-Type: application/json',
			    'Content-Length: ' . strlen($data_string),
			    'Authorization: Bearer '.$access_token
			)                                                                       
		);                                                                                                                   
			                                                                                                                     
		$result = curl_exec($ch); 

		// echo $result;exit;
		$result_json = json_decode($result); 

		// create log file after send this to mv
		// $log_file_path = $this->createLogFilePath('iosSetPurchaseMVResponse');
		// $file_content = date("Y-m-d H:i:s") . ' criteria : ' . json_encode($result_json) . "\n"; 
		// $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
		// file_put_contents($log_file_path, $file_content, FILE_APPEND);
		// unset($file_content); 


		return true;
    }

    //private function getPackagePurchase

    private function getTransactionDataByTransactionNumber($transaction_number){
    	// first check transaction number 

    	$query = $this->ci->db->select('*')
    	->from('vvod_transactions')
    	->where('transaction_number',$transaction_number)
    	->where('active',1)
    	->get();

    	if($query->num_rows() > 0){
    		$rowTransaction = $query->row(); 

    		$stdReturn = new StdClass();

    		$stdReturn->transaction_id = $rowTransaction->id;
    		$stdReturn->transaction_number = $rowTransaction->transaction_number;
    		$stdReturn->sub_total = number_format($rowTransaction->sub_total,2);
    		$stdReturn->grand_total = number_format($rowTransaction->grand_total,2);
    		$stdReturn->payment_option = $rowTransaction->payment_option;
    		$stdReturn->payment_status = ($rowTransaction->payment_status)?'Paid':'Unpaid';
    		$stdReturn->created = $rowTransaction->created;
    		$stdReturn->updated = $rowTransaction->updated;
    		$stdReturn->transaction_package = $this->getPartnerPackageDataById($rowTransaction->vvod_partner_packages_id);


    		// print_r($row);exit;
    		//return $stdReturn;
    		return [
    			'status'=>true,
    			'TransactionData'=>$stdReturn
    		];

    	}else{
    		return [
    			'status'=>false,
    			'result_code'=>'-002',
    			'result_desc'=>'Not found transaction_number,Please try other transaction_number'
    		];
    	}



    }

    private function setRequestCriteria($requestCriteria){
    	$this->requestCriteria = $requestCriteria;
    }

    private function getAccessTokenByUserIdAndPartnerId($data = []){
    	$vvod_users_id = $data['vvod_users_id'];
    	$vvod_partners_id = $data['vvod_partners_id'];


    	$query = $this->ci->db->select('*')
    	->from('vvod_user_partner_authorization')
    	->where('vvod_users_id',$vvod_users_id)
    	->where('vvod_partners_id',$vvod_partners_id)
    	->where('active',1)
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row()->access_token;
    	}else{
    		return '';
    	}

    }

    private function getMVVodLinkByTokenRequest($token_req_url){ 

    	$this->ci->load->config('partners');

		$mv_config = $this->ci->config->item('request_api')['mv'];

		// $dummy_overview_url = $mv_config['dummy_overview']['request_url'];
		$vod_link_url = $mv_config['req_link_url']['request_url'];


		$data = [
			'code'=>(string)$token_req_url
		];

		$data_string = json_encode($data);

		//print_r($data_string);exit;    
		//print_r($this->mv_access_token);exit;   

		                                                                        
		                                                                                                                     
		$ch = curl_init($vod_link_url);                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                      
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                 
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                     
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(                         
		    'Content-Type: application/json',
		    'Authorization: Bearer '.$this->mv_access_token
			)                                                                       
		);                                                                                                                   
		if(curl_exec($ch) === false)
		{
			// if(isset($_GET['test']) && $_GET['test'] == 'test'){
		 //    	echo 'Curl error: ' . curl_error($ch);
			// }
		}
		                                                                                                                     
		$result = curl_exec($ch); 
			 

		$response = json_decode($result); 

		if(isset($_GET['test']) && $_GET['test'] == 'test'){

				print_r($response);exit;
				// print_r($result);exit;
		}   
		return $response;


    }




}