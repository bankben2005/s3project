<?php 

class VodYoutubeLibrary{

	private $ci; 
	private $backend_url;

	function __construct(){
		$this->ci =& get_instance();
		$this->setBackendUrl();
	}

	public function getAllCategories($requestCriteria){ 

		$arrCategories = [];




		// check criteria to get my playlist 
		if((property_exists($requestCriteria, 'members_id') && $requestCriteria->members_id != '') && property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type != ''){


			// check this user has private member playlist
			$checkMemberHasPlaylist = $this->checkMemberHasPlaylist($requestCriteria);



			if($checkMemberHasPlaylist['status']){

				// special catgory 

				array_push($arrCategories, [
					'category_id'=>999,
					'category_name'=>'เพลย์ลิสของฉัน',
					'category_cover'=>$this->backend_url.'uploaded/vod_category/playlist/default_playlist.png'
				]);


			}




		}


		$queryCheck = $this->ci->db->select('id,name,active,cover')
		->from('vod_categories')
		->where('active',1)
		->order_by('ordinal','asc')
		->get();

		if($queryCheck->num_rows() > 0){
			foreach ($queryCheck->result() as $key => $value) {
				# code...
				array_push($arrCategories, [
					'category_id'=>$value->id,
					'category_name'=>$value->name,
					'category_cover'=>$this->backend_url.'uploaded/vod_category/'.$value->id.'/'.$value->cover
				]);
			}
		}

		if(isset($_GET['test']) && $_GET['test'] == 'test'){
				print_r($arrCategories);exit;
			}


		return [
			'status'=>true,
			'CategoryList'=>$arrCategories,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];


	}

	public function getVodByCategoryId($requestCriteria){ 

		// check category_id parameter

		if(!property_exists($requestCriteria, 'category_id') || $requestCriteria->category_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found category_id parameter'
			];
		} 


		// check category id  

		if((property_exists($requestCriteria, 'members_id') && $requestCriteria->members_id != '') && (property_exists($requestCriteria, 'login_type') && $requestCriteria->login_type != '') && ($requestCriteria->category_id == 999)){

			return $this->getMyPlaylist($requestCriteria);


		}


		$rowCategory = $this->ci->db->select('*')
		->from('vod_categories')
		->where('id',$requestCriteria->category_id)
		->where('active',1)
		->get(); 

		if($rowCategory->num_rows() <= 0){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Category not found,Please try again'
			];
		}


		$rowData = $rowCategory->row();
		$arrVod = [];


		if($requestCriteria->category_id == '7'){

			$vod_by_cat = $this->ci->db->select('*,vod_owner.company_name as vod_owner_name,
				vod_owner.company_cover,vod.name as vod_name,vod.description as vod_description,vod.cover as vod_cover,vod.id as vod_id')
			->from('vod')
			->join('vod_owner','vod.vod_owner_id = vod_owner.id','left')
			->where('vod.active',1)
			->where('vod.vod_categories_id',$requestCriteria->category_id)
			->or_where('vod.trending_status',1)			
			->order_by('vod.ordinal_by_trending','asc')
			->get();

		}else{

			$vod_by_cat = $this->ci->db->select('*,vod_owner.company_name as vod_owner_name,
				vod_owner.company_cover,vod.name as vod_name,vod.description as vod_description,vod.cover as vod_cover,vod.id as vod_id')
			->from('vod')
			->join('vod_owner','vod.vod_owner_id = vod_owner.id','left')
			->where('vod.vod_categories_id',$requestCriteria->category_id)
			->where('vod.active',1)
			->order_by('vod.ordinal_by_category','asc')
			->order_by('vod.created','desc')
			->get();
		}

		if($vod_by_cat->num_rows() > 0){
			foreach ($vod_by_cat->result() as $key => $value) {
				# code...
				array_push($arrVod, [
					'vod_id'=>$value->vod_id,
					'vod_categories_id'=>$value->vod_categories_id,
					'vod_cover'=>$this->backend_url.'uploaded/vod/'.$value->vod_id.'/'.$value->vod_cover,
					'vod_name'=>$value->vod_name,
					'vod_description'=>$value->vod_description,
					'has_one_episode_status'=>$value->has_one_episode_status,
					'video_url'=>$value->video_url,
					'vod_views'=>$value->views,
					'vod_owner'=>[
						'vod_owner_id'=>$value->vod_owner_id,
						'vod_owner_name'=>$value->vod_owner_name,
						'vod_owner_cover'=>$this->backend_url.'uploaded/vod_owner/'.$value->vod_owner_id.'/'.$value->company_cover
					]
					
				]);
			}

		}


		return [
			'status'=>true,
			'VodList'=>$arrVod,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];
		//return $arrVod;




	} 

	public function getVodDetail($requestCriteria){ 

		// print_r($requestCriteria);exit;
		// echo $requestCriteria->vod_id;exit;

		// check vod_id property

		if(!property_exists($requestCriteria, 'vod_id') || $requestCriteria->vod_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found vod_id parameter'
			];
		} 

		// echo $requestCriteria->vod_id;exit;

		$queryVod = $this->ci->db->select('*')
		->from('vod')
		->where('id',$requestCriteria->vod_id)
		->where('active',1)
		->get();  

		

		if($queryVod->num_rows() <= 0){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found vod_id,Please try again'
			];

		}


		$rowVod = $queryVod->row();

		$stdVod = new StdClass();

			$stdVod->vod_id = $rowVod->id;
			$stdVod->vod_cover = $this->backend_url.'uploaded/vod/'.$rowVod->id.'/'.$rowVod->cover;
			$stdVod->vod_name = $rowVod->name;
			$stdVod->vod_description = $rowVod->description;
			$stdVod->has_one_episode_status = $rowVod->has_one_episode_status;
			$stdVod->video_url = $rowVod->video_url;
			$stdVod->vod_views = $rowVod->views;
			$stdVod->vod_owner = $this->getVodOwnerData([
				'vod_owner_id'=>$rowVod->vod_owner_id
			]);
			$stdVod->vod_season = $this->getAllVodSeasonByVodId([
				'vod_id'=>$rowVod->id
			]);


		

		return [
			'status'=>true,
			'VodData'=>$stdVod,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];


	} 

	public function setVodClick($requestCriteria){
		if(!property_exists($requestCriteria, 'vod_id') || $requestCriteria->vod_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found vod_id parameter'
			];
		} 

		// echo $requestCriteria->vod_id;exit;

		$queryVod = $this->ci->db->select('*')
		->from('vod')
		->where('id',$requestCriteria->vod_id)
		->where('active',1)
		->get();  

		

		if($queryVod->num_rows() <= 0){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found vod_id,Please try again'
			];

		}


		$rowVod = $queryVod->row(); 

		$views = $rowVod->views;
		$views += 1;

		$this->ci->db->update('vod',[
			'views'=>$views
		],['id'=>$rowVod->id]); 

		return [
			'status'=>true,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];


	}

	public function getUJRequest($requestCriteria){ 

		if(!property_exists($requestCriteria, 'members_id') || $requestCriteria->members_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found members_id'
			];
		} 


		if(!property_exists($requestCriteria, 'login_type') || $requestCriteria->login_type == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found login_type'
			];
		}

		// check member or guest exist 
		$checkMemberOrGuestExist = $this->checkMemberOrGuestExist([
			'requestCriteria'=>$requestCriteria
		]);

		if(!$checkMemberOrGuestExist['status']){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found members_id in system'
			];
		} 

		$arrReturn = [];

		$query = $this->ci->db->select('*')
		->from('vod_uj')
		->where('members_id',$requestCriteria->members_id)
		->where('login_type',$requestCriteria->login_type)
		->where('active',1)
		->get();

		if($query->num_rows() > 0){

			foreach ($query->result() as $key => $value) {
				# code...
				array_push($arrReturn, [
					'id'=>$value->id,
					'request_message'=>$value->request_message,
					'read_status'=>$value->read_status,
					'reply_status'=>$value->reply_status,
					'created'=>$value->created,
					'reply_message_list'=>$this->getReplyMessageList([
						'vod_uj_id'=>$value->id
					])
				]);
			}
		}

		return [
			'status'=>true,
			'UJLists'=>$arrReturn,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];



	}

	public function setUJRequest($requestCriteria){
		// print_r($requestCriteria);


		if(!property_exists($requestCriteria, 'members_id') || $requestCriteria->members_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found members_id'
			];
		} 


		if(!property_exists($requestCriteria, 'login_type') || $requestCriteria->login_type == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found login_type'
			];
		}


		if(!property_exists($requestCriteria, 'request_message') || $requestCriteria->request_message == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found request_message or request_message equal null'
			];
		}

		// check member or guest exist 
		$checkMemberOrGuestExist = $this->checkMemberOrGuestExist([
			'requestCriteria'=>$requestCriteria
		]);

		if(!$checkMemberOrGuestExist['status']){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found members_id in system'
			];
		} 


		$this->ci->db->insert('vod_uj',[
			'members_id'=>$requestCriteria->members_id,
			'login_type'=>$requestCriteria->login_type,
			'request_message'=>$requestCriteria->request_message,
			'created'=>date('Y-m-d H:i:s')
		]);


		return [
			'status'=>true,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];



		






	}

	public function getMyPlaylist($requestCriteria){
		if(!property_exists($requestCriteria, 'members_id') || $requestCriteria->members_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found members_id'
			];
		} 


		if(!property_exists($requestCriteria, 'login_type') || $requestCriteria->login_type == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found login_type'
			];
		}


		

		// check member or guest exist 
		$checkMemberOrGuestExist = $this->checkMemberOrGuestExist([
			'requestCriteria'=>$requestCriteria
		]);

		if(!$checkMemberOrGuestExist['status']){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found members_id in system'
			];
		} 

		$arrReturn = [];

		$query = $this->ci->db->select('*')
		->from('vod_member_playlist')
		->where('members_id',$requestCriteria->members_id)
		->where('login_type',$requestCriteria->login_type)
		->where('active',1)
		->get();

		if($query->num_rows() > 0){ 
			$row_playlist = $query->row();

			$queryVod = $this->ci->db->select('*')
			->from('vod')
			->where_in('id',json_decode($row_playlist->playlist))
			->where('active',1)
			->get();

			if($queryVod->num_rows() > 0){
				foreach ($queryVod->result() as $key => $value) {
					# code...

					$vod_owner = $this->getVodOwnerData([
						'vod_owner_id'=>$value->vod_owner_id
					]); 

					array_push($arrReturn, [
						'vod_id'=>$value->id,
						'vod_categories_id'=>$value->vod_categories_id,
						'vod_cover'=>$this->backend_url.'uploaded/vod/'.$value->id.'/'.$value->cover,
						'vod_name'=>$value->name,
						'vod_description'=>$value->description,
						'has_one_episode_status'=>$value->has_one_episode_status,
						'video_url'=>$value->video_url,
						'vod_views'=>$value->views,
						'vod_owner'=>[
							'vod_owner_id'=>$vod_owner['vod_owner_id'],
							'vod_owner_name'=>$vod_owner['vod_owner_name'],
							'vod_owner_cover'=>$vod_owner['vod_owner_cover']
						]
					]);
				}
			}

		}

		return [
			'status'=>true,
			'VodList'=>$arrReturn,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];
	}

	private function setBackendUrl(){ 

		switch (ENVIRONMENT) {
			case 'development':
				$this->backend_url =  'http://s3remoteservice.development/vod_ci/';
			break;
			case 'testing':

			break;

			case 'production':
				$this->backend_url =  'http://s3remoteservice.psi.co.th/vod/';
			break;
			
			default:
				# code...
				break;
		}

	}

	private function getVodOwnerData($data = []){
		$queryVodOwner = $this->ci->db->select('*')
		->from('vod_owner')
		->where('id',$data['vod_owner_id'])
		->get();


		if($queryVodOwner->num_rows() > 0){ 
			$rowOwner = $queryVodOwner->row();
			return [
				'vod_owner_id'=>$rowOwner->id,
				'vod_owner_name'=>$rowOwner->company_name,
				'vod_owner_cover'=>$this->backend_url.'uploaded/vod_owner/'.$rowOwner->id.'/'.$rowOwner->company_cover
			];
		}
	}

	private function getAllVodSeasonByVodId($data = []){
		$arrReturn = [];

		$queryVodSeason = $this->ci->db->select('*')
		->from('vod_season')
		->where('vod_id',$data['vod_id'])
		->where('active',1)
		->get();


		if($queryVodSeason->num_rows() > 0){ 

			foreach ($queryVodSeason->result() as $key => $value) {
				# code...
				array_push($arrReturn, [
					'vod_season_id'=>$value->id,
					'vod_season_name'=>$value->name,
					'vod_season_description'=>$value->description,
					'vod_season_episode'=>$this->getVodEpisodeByVodSeasonId([
						'vod_season_id'=>$value->id
					])
				]);
			}

		} 

		return $arrReturn;

	}

	private function getVodEpisodeByVodSeasonId($data = []){ 
		$arrReturn = [];

		$queryEpisode = $this->ci->db->select('*')
		->from('vod_episode')
		->where('vod_season_id',$data['vod_season_id'])
		->where('active',1)
		->order_by('ordinal','asc')
		->get();

		if($queryEpisode->num_rows() > 0){ 
			foreach ($queryEpisode->result() as $key => $value) {
				# code...
				array_push($arrReturn, [
					'vod_episode_id'=>$value->id,
					'vod_episode_name'=>$value->name,
					'vod_episode_description'=>$value->description,
					'vod_video_url'=>$value->video_url
				]);
			}

		}

		return $arrReturn;
	}

	private function checkMemberOrGuestExist($data = []){ 
		$requestCriteria = $data['requestCriteria'];

		$found_user_status = false;


		if($requestCriteria->login_type == 'member'){

				$queryMember = $this->ci->db->select('*')
				->from('members')
				->where('id',$requestCriteria->members_id)
				->where('active',1)
				->get();
				if($queryMember->num_rows() > 0){
					$found_user_status = true;
				}

		}else if($requestCriteria->login_type == 'guest'){

				$queryGuest = $this->ci->db->select('id,active')
				->from('guests')
				->where('id',$requestCriteria->members_id)
				->where('active',1)
				->get();

				if($queryGuest->num_rows() > 0){
					$found_user_status = true;
				}
		}




		

		// print_r($found_user_status);exit;


		return [
			'status'=>$found_user_status
		];


	}

	private function getReplyMessageList($data = []){

		$arrReturn = [];

		$query = $this->ci->db->select('*')
		->from('vod_uj_reply')
		->where('vod_uj_id',$data['vod_uj_id'])
		->order_by('created','asc')
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				array_push($arrReturn, [
					'reply_id'=>$value->id,
					'reply_message'=>$value->reply_message,
					'read_status'=>$value->read_status,
					'created'=>$value->created
				]);
			}
		}

		return $arrReturn;
	}

	private function checkMemberHasPlaylist($requestCriteria){
		$query = $this->ci->db->select('*')
		->from('vod_member_playlist')
		->where('members_id',$requestCriteria->members_id)
		->where('login_type',$requestCriteria->login_type)
		->where('active',1)
		->get();

		if($query->num_rows() > 0){

			$row = $query->row();

			$playlist = json_decode($row->playlist);

			if(json_last_error() === JSON_ERROR_NONE){
				if(count($playlist) > 0){
					return [
						'status'=>true,
						'data'=>$row
					];
				}else{
					return [
						'status'=>false
					];
				}
			}else{
				return [
					'status'=>false
				];
			}

		}else{
			return [
				'status'=>false
			];

		}
	}


}