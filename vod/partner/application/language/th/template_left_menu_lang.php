<?php

$lang['Dashboard'] = "Dashboard";
$lang['Custom video on demand'] = "Custom video on demand";
$lang['VOD'] = "VOD";
$lang['VOD SEASONS'] = "VOD SEASONS";
$lang['VOD EPISODE'] = "VOD EPISODE";
$lang['VOD OWNER'] = "VOD OWNER";
$lang['VOD CATEGORIES'] = "VOD CATEGORIES";
$lang['General Setting'] = "General Setting";
$lang['USERS'] = "USERS";
$lang['Custom partner'] = "Custom partner";
$lang['PARTNER'] = "PARTNER";
$lang['PARTNER PACKAGE'] = "PARTNER PACKAGE";
$lang['PARTNER LIST'] = "PARTNER LIST";
$lang['PARTNER PACKAGE LIST'] = "PARTNER PACKAGE LIST";
$lang['CATEGORIES'] = "CATEGORIES";
$lang['PARTNER MAP CATEGORY'] = "PARTNER MAP CATEGORY";
$lang['VOD USERS'] = "VOD USERS";
