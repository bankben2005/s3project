<?php

$lang['PARTNER LIST'] = "PARTNER LIST";
$lang['Create partner'] = "Create partner";
$lang['Create partner form'] = "Create partner form";
$lang['Contact Name'] = "Contact Name";
$lang['Contact Address'] = "Contact Address";
$lang['Contact Telephone'] = "Contact Telephone";
$lang['Contact Fax'] = "Contact Fax";
$lang['Slug'] = "Slug";
$lang['Logo Image'] = "Logo Image";
$lang['Cover Image'] = "Cover Image";
$lang['Menu Image'] = "Menu Image";
$lang['Edit partner'] = "Edit partner";
$lang['Edit partner form'] = "Edit partner form";
$lang['Edit partner success'] = "Edit partner success";
