<?php

$lang['PARTNER PACKAGE LIST'] = "PARTNER PACKAGE LIST";
$lang['PARTNER PACKAGE LIST TABLE'] = "PARTNER PACKAGE LIST TABLE";
$lang['Add partner package'] = "Add partner package";
$lang['Contact Name'] = "Contact Name";
$lang['Contact Telephone'] = "Contact Telephone";
$lang['Menu Image'] = "Menu Image";
$lang['Logo Image'] = "Logo Image";
$lang['Cover Image'] = "Cover Image";
$lang['Updated'] = "Updated";
$lang['Partner'] = "Partner";
$lang['Package Name'] = "Package Name";
$lang['Price'] = "Price";
$lang['Currency'] = "Currency";
