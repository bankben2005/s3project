<?php

$lang['PARTNER LIST'] = "PARTNER LIST";
$lang['Create partner map category'] = "Create partner map category";
$lang['Create partner map category form'] = "Create partner map category form";
$lang['PARTNER MAP CATEGORIES LIST'] = "PARTNER MAP CATEGORIES LIST";
$lang['Select Partner'] = "Select Partner";
$lang['Map by api id'] = "Map by api id";
$lang['By api id'] = "By api id";
$lang['By slug'] = "By slug";
$lang['Api category id'] = "Api category id";
$lang['Api slug'] = "Api slug";
$lang['Select PSI Category'] = "Select PSI Category";
$lang['Select Category'] = "Select Category";
$lang['Create map category success'] = "Create map category success";
$lang['Edit partner map category'] = "Edit partner map category";
$lang['Edit partner map category form'] = "Edit partner map category form";
$lang['Update map category success'] = "Update map category success";
