<?php

$lang['PARTNER PACKAGE LIST'] = "PARTNER PACKAGE LIST";
$lang['Create partner package'] = "Create partner package";
$lang['Create partner package form'] = "Create partner package form";
$lang['Select Partner'] = "Select Partner";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['Price'] = "Price";
$lang['Currency Symbol'] = "Currency Symbol";
$lang['Cover'] = "Cover";
$lang['Cover Image'] = "Cover Image";
$lang['Create partner package success'] = "Create partner package success";
$lang['Edit partner package'] = "Edit partner package";
$lang['Edit partner package form'] = "Edit partner package form";
$lang['Edit partner package success'] = "Edit partner package success";
$lang['Days'] = "Days";
$lang['Code'] = "Code";
$lang['Purchase ref id'] = "Purchase ref id";
$lang['Device Type'] = "Device Type";
$lang['Select Device Type'] = "Select Device Type";
$lang['Android'] = "Android";
$lang['iOS'] = "iOS";
