<?php

$lang['MAP CATEGORY LIST'] = "MAP CATEGORY LIST";
$lang['MAP CATEGORY LIST TABLE'] = "MAP CATEGORY LIST TABLE";
$lang['Add map category'] = "Add map category";
$lang['Partner'] = "Partner";
$lang['Category'] = "Category";
$lang['Map Value'] = "Map Value";
$lang['Map By'] = "Map By";
$lang['Status'] = "Status";
$lang['Updated'] = "Updated";
$lang['Slug'] = "Slug";
