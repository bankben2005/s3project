<?php

$lang['VOD USERS LIST'] = "VOD USERS LIST";
$lang['VOD USERS LIST TABLE'] = "VOD USERS LIST TABLE";
$lang['Ref id'] = "Ref id";
$lang['Firstname &amp; Lastname'] = "Firstname &amp; Lastname";
$lang['Gender'] = "Gender";
$lang['Age'] = "Age";
$lang['Picture Profile'] = "Picture Profile";
$lang['Updated'] = "Updated";
$lang['Search'] = "Search";
$lang['firstname or lastname...'] = "firstname or lastname...";
$lang['Result'] = "Result";
$lang['Record(s)'] = "Record(s)";
$lang['Reset'] = "Reset";
$lang['Package'] = "Package";
$lang['Partner'] = "Partner";
$lang['Package Duration'] = "Package Duration";
