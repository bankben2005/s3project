<?php

$lang['Home'] = "Home";
$lang['Status'] = "Status";
$lang['Active'] = "Active";
$lang['Unactive'] = "Unactive";
$lang['Submit'] = "Submit";
$lang['Success'] = "Success";
$lang['Email or password invalid'] = "Email or password invalid";
$lang['Yes'] = "Yes";
$lang['No'] = "No";
$lang['Search'] = "Search";
