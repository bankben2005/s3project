<?php

$lang['CATEGORIES LIST'] = "CATEGORIES LIST";
$lang['Create category'] = "Create category";
$lang['Create category form'] = "Create category form";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['Trending Status'] = "Trending Status";
$lang['Mainpage Status'] = "Mainpage Status";
$lang['Create category success'] = "Create category success";
$lang['Edit category'] = "Edit category";
$lang['Edit category form'] = "Edit category form";
$lang['Edit category success'] = "Edit category success";
$lang['Live Status'] = "Live Status";
