<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/partner_packages')?>"><?php echo __('PARTNER PACKAGE LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vvod_partner_packages->id)?__('Edit partner package'):__('Create partner package')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vvod_partner_packages->id)?__('Edit partner package form'):__('Create partner package form')?> 

  </h6> 


</div>
<div class="card-body"> 

  <?php  

   $image_cover_path = base_url('uploaded/partner_packages/'.@$vvod_partner_packages->id.'/'.@$vvod_partner_packages->cover); 

   $cover_obj = new StdClass();
   $cover_obj->caption = $vvod_partner_packages->cover;
   $cover_obj->size = $vvod_partner_packages->cover_size;
   $cover_obj->width = '120px';
   $cover_obj->url = base_url($this->controller.'/deletePartnerPackageCoverImage');
   $cover_obj->key = $vvod_partner_packages->id;

  ?>  


  <?php echo form_input([
    'type'=>'hidden',
    'name'=>'hide_partner_package_id',
    'value'=>@$vvod_partner_packages->id
  ])?>

  <!-- menu image  -->
   <?php echo form_input([
    'type'=>'hidden',
    'name'=>'hide_cover',
    'value'=>(@$vvod_partner_packages->cover)?json_encode([$image_cover_path]):'[]'
  ])?>

  <?php echo form_input([
    'type'=>'hidden',
    'name'=>'hide_cover_caption_obj',
    'value'=> json_encode([$cover_obj])
  ])?>   
  <!-- eof menu image -->





  <?php echo form_open_multipart('',['name'=>'create-partner-package-form'])?>
  <div class="row">

    <div class="col-lg-4">
      <div class="form-group">
        <label><strong><?php echo __('Select Partner')?> : </strong></label>
        <?php echo form_dropdown('vvod_partners_id',@$select_partner,@$vvod_partner_packages->vvod_partners_id,'class="form-control"')?>
      </div>


      <div class="form-group">
        <label><strong><?php echo __('Code')?> : </strong></label>
        <?php echo form_input([
          'name'=>'code',
          'class'=>'form-control',
          'value'=>@$vvod_partner_packages->code
        ])?>
      </div>

      <div class="form-group">
        <label><strong><?php echo __('Device Type')?> : </strong></label>
        <?php echo form_dropdown('device_type',[
          ''=>__('Select Device Type'),
          'android'=>__('Android'),
          'ios'=>__('iOS')
        ],@$vvod_partner_packages->device_type,'class="form-control"')?>
      </div>

      <div class="form-group">
        <label><strong><?php echo __('Purchase ref id')?> : </strong></label>
        <?php echo form_input([
          'name'=>'purchase_ref_id',
          'class'=>'form-control',
          'value'=>@$vvod_partner_packages->purchase_ref_id
        ])?>
      </div>
      <div class="form-group">
        <label><strong><?php echo __('Name')?> : </strong></label>
        <?php echo form_input([
          'name'=>'name',
          'class'=>'form-control',
          'value'=>@$vvod_partner_packages->name
        ])?>
      </div>

      <div class="form-group">
        <label><strong><?php echo __('Description')?> : </strong></label>
        <?php echo form_textarea([
          'name'=>'description',
          'class'=>'form-control',
          'value'=>@$vvod_partner_packages->description,
          'rows'=>3
        ])?>
      </div>

      <div class="form-group">
        <label><strong><?php echo __('Price')?> : </strong></label>
        <?php echo form_input([
          'name'=>'price',
          'class'=>'form-control',
          'value'=>@$vvod_partner_packages->price
        ])?>
      </div>

      <div class="form-group">
        <label><strong><?php echo __('Currency Symbol')?> : </strong></label>
        <?php echo form_input([
          'name'=>'currency_symbol',
          'class'=>'form-control',
          'value'=>@$vvod_partner_packages->currency_symbol
        ])?>
      </div>

      <div class="form-group">
        <label><strong><?php echo __('Days')?> : </strong></label> 
        <?php echo form_input([
          'type'=>'number',
          'name'=>'days',
          'class'=>'form-control',
          'value'=>@$vvod_partner_packages->days
        ])?>
      </div>

      <div class="form-group">
        <label><strong><?php echo __('Status','default')?> : </strong></label>
        <?php echo form_dropdown('active',[
          '1'=>__('Active','default'),
          '0'=>__('Unactive','default')
        ],@$vvod_partner_packages->active,'class="form-control"')?>
      </div>

      

    </div>





    <div class="col-lg-8">
      <div class="form-group">
        <label><strong><?php echo __('Cover Image')?> : </strong></label>
        <div class="upload-box mb-3">
          <input type="file" class="cover" name="cover" class="file-loading" />
        </div>
      </div>

    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="form-group">
        <?php echo form_button([
          'type'=>'submit',
          'class'=>'btn btn-success float-right',
          'content'=>__('Submit','defaut')
        ])?>
      </div>
    </div>
  </div>



  <?php echo form_close()?>

</div>

</div>