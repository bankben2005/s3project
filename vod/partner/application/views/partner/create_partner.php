<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller)?>"><?php echo __('PARTNER LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vvod_partner->id)?__('Edit partner'):__('Create partner')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vvod_partner->id)?__('Edit partner form'):__('Create partner form')?> 

  </h6> 


</div>
<div class="card-body"> 
 <?php  

 $image_menu_path = base_url('uploaded/partner/menu/'.@$vvod_partner->id.'/'.@$vvod_partner->menu_image);
 $menu_obj = new StdClass();
 $menu_obj->caption = $vvod_partner->menu_image;
 $menu_obj->size = $vvod_partner->menu_image_size;
 $menu_obj->width = '120px';
 $menu_obj->url = base_url($this->controller.'/deletePartnerMenuImage');
 $menu_obj->key = $vvod_partner->id;


 $image_logo_path = base_url('uploaded/partner/logo/'.@$vvod_partner->id.'/'.@$vvod_partner->logo_image); 
 $logo_obj = new StdClass();
 $logo_obj->caption = $vvod_partner->logo_image;
 $logo_obj->size = $vvod_partner->logo_image_size;
 $logo_obj->width = '120px';
 $logo_obj->url = base_url($this->controller.'/deletePartnerLogoImage');
 $logo_obj->key = $vvod_partner->id; 


 $image_cover_path = base_url('uploaded/partner/cover/'.@$vvod_partner->id.'/'.@$vvod_partner->cover_image); 
 $cover_obj = new StdClass();
 $cover_obj->caption = $vvod_partner->cover_image;
 $cover_obj->size = $vvod_partner->cover_image_size;
 $cover_obj->width = '120px';
 $cover_obj->url = base_url($this->controller.'/deletePartnerCoverImage');
 $cover_obj->key = $vvod_partner->id;



 ?> 

 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_partner_id',
  'value'=>@$vvod_partner->id
])?>

<!-- menu image  -->
 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_menu_image',
  'value'=>(@$vvod_partner->menu_image)?json_encode([$image_menu_path]):'[]'
])?>

 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_menu_image_caption_obj',
  'value'=> json_encode([$menu_obj])
])?>   
<!-- eof menu image -->

<!-- logo image -->

 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_logo_image',
  'value'=>(@$vvod_partner->logo_image)?json_encode([$image_logo_path]):'[]'
])?>

 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_logo_image_caption_obj',
  'value'=> json_encode([$logo_obj])
])?>   
<!-- eof logo image -->

<!-- cover image -->
 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_cover_image',
  'value'=>(@$vvod_partner->cover_image)?json_encode([$image_cover_path]):'[]'
])?>

 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_cover_image_caption_obj',
  'value'=> json_encode([$cover_obj])
])?>   
<!-- eof cover image -->


<?php echo form_open_multipart('',['name'=>'create-partner-form'])?> 
<div class="row">
 <div class="col-lg-4">
  <div class="form-group">
    <label><strong><?php echo __('Contact Name')?> : </strong></label>
    <?php echo form_input([
      'name'=>'contact_name',
      'class'=>'form-control',
      'value'=>@$vvod_partner->contact_name
    ])?>
  </div>

  <div class="form-group">
    <label><strong><?php echo __('Contact Address')?> : </strong></label>
    <?php echo form_textarea([
      'name'=>'contact_address',
      'class'=>'form-control',
      'value'=>@$vvod_partner->contact_address,
      'rows'=>3
    ])?>
  </div>

  <div class="form-group">
    <label><strong><?php echo __('Contact Telephone')?> : </strong></label>
    <?php echo form_input([
      'name'=>'contact_telephone',
      'class'=>'form-control',
      'value'=>@$vvod_partner->contact_telephone
    ])?>
  </div>

  <div class="form-group">
    <label><strong><?php echo __('Contact Fax')?> : </strong></label>
    <?php echo form_input([
      'name'=>'contact_fax',
      'class'=>'form-control',
      'value'=>@$vvod_partner->contact_fax
    ])?>
  </div>

  <div class="form-group">
    <label><strong><?php echo __('Slug')?> : </strong></label>
    <?php echo form_input([
      'name'=>'slug',
      'class'=>'form-control',
      'value'=>@$vvod_partner->slug
    ])?>
  </div>

  <div class="form-group">
    <label><strong><?php echo __('Status','default')?> : </strong></label>
    <?php echo form_dropdown('active',[
      '1'=>__('Active','default'),
      '0'=>__('Unactive','default')
    ],@$vvod_partner->active,'class="form-control"')?>
  </div>

</div>

<div class="col-lg-8">
  <div class="form-group">
    <label><strong><?php echo __('Menu Image')?> : </strong></label>
    <div class="upload-box mb-3">
      <input type="file" class="menu_image" name="menu_image" class="file-loading" />
    </div>
  </div>  
  <div class="form-group">
    <label><strong><?php echo __('Logo Image')?> : </strong></label>
    <div class="upload-box mb-3">
      <input type="file" class="logo_image" name="logo_image" class="file-loading" />
    </div>
  </div>

  <div class="form-group">
    <label><strong><?php echo __('Cover Image')?> : </strong></label>
    <div class="upload-box mb-3">
      <input type="file" class="cover_image" name="cover_image" class="file-loading" />
    </div>
  </div>
</div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="form-group">
      <?php echo form_button([
        'type'=>'submit',
        'class'=>'btn float-right btn-success btn-block',
        'content'=>'<i class="fa fa-save"></i> '.__('Submit','default')
      ])?>
    </div>
  </div>
</div>
<?php echo form_close()?>
</div>
</div>
