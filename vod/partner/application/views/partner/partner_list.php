<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('PARTNER LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('PARTNER LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createPartner')?>"><i class="fa fa-plus"></i> <?php echo __('Add partner')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Contact Name')?></th>
                      <th><?php echo __('Contact Telephone')?></th>
                      <th><?php echo __('Menu Image')?></th>
                      <th><?php echo __('Logo Image')?></th>                      
                      <th><?php echo __('Cover Image')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($vvod_partners as $key => $row){?>
                      <tr>
                        <td></td>
                        <td><?php echo $row->contact_name?></td>
                        <td><?php echo $row->contact_telephone?></td>
                        <td>
                          <img src="<?php echo base_url('uploaded/partner/menu/'.$row->id.'/'.$row->menu_image)?>" style="max-width: 150px;">
                        </td>
                        <td>
                          <img src="<?php echo base_url('uploaded/partner/logo/'.$row->id.'/'.$row->logo_image)?>" style="max-width: 150px;">
                        </td>
                        <td>
                          <img src="<?php echo base_url('uploaded/partner/cover/'.$row->id.'/'.$row->cover_image)?>" style="max-width: 150px;">
                        </td>
                        <td>
                          <?php echo $row->updated?>
                        </td>
                        <td>
                          <a href="<?php echo base_url($this->controller.'/editPartner/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pen"></i></a>
                          <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deletePartner/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>

                    <?php }?>

                  </tbody>
                </table>
              </div>
            </div>
</div>