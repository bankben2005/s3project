<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('PARTNER PACKAGE LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('PARTNER PACKAGE LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createPartnerPackage')?>"><i class="fa fa-plus"></i> <?php echo __('Add partner package')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Partner')?></th>
                      <th><?php echo __('Package Name')?></th>
                      <th><?php echo __('Price')?></th>
                      <th><?php echo __('Currency')?></th>                      
                      <th><?php echo __('Cover Image')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($partner_packages as $key => $row){?> 
                      <tr>
                        <td></td>
                        <td><?php echo $row->vvod_partners->get()->contact_name?></td>
                        <td><?php echo $row->name?></td>
                        <td><?php echo $row->price?></td>
                        <td><?php echo $row->currency_symbol?></td>
                        <td>
                          <img src="<?php echo base_url('uploaded/partner_packages/'.$row->id.'/'.$row->cover)?>" style="max-width: 150px;">
                        </td>
                        <td>
                          <?php if($row->active){?>
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td>
                        <td><?php echo $row->updated?></td>
                        <td>
                          <a href="<?php echo base_url($this->controller.'/editPartnerPackage/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pen"></i></a> 

                          <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deletePartnerPackage/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>
                          
                        </td>
                      </tr>

                    <?php }?>

                  </tbody>
                </table>
              </div>
            </div>
</div>