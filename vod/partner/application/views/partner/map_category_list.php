<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('MAP CATEGORY LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('MAP CATEGORY LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createMapCategory')?>"><i class="fa fa-plus"></i> <?php echo __('Add map category')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Partner')?></th>
                      <th><?php echo __('Category')?></th>
                      <th><?php echo __('Map Value')?></th>
                      <th><?php echo __('Map By')?></th>
                      <th><?php echo __('Status')?></th>     
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($vvod_partner_map_category as $key => $row){?> 
                      <tr>
                        <td></td>
                        <td><?php echo $row->vvod_partners->get()->contact_name?></td>
                        <td><?php echo $row->vvod_categories->get()->name?></td>
                        <td>
                          <?php 
                              if($row->is_map_api_slug_status){ 
                                echo $row->vvod_partner_api_slug;
                              }else{
                                echo $row->vvod_partner_api_category_id;
                              }
                          ?>
                        </td>
                        <td>
                          <?php if($row->is_map_api_slug_status){?> 
                            <span class="badge badge-secondary"><?php echo __('Slug')?></span>
                          <?php }else{?>  
                            <span class="badge badge-secondary"><?php echo __('Api id')?></span>
                          <?php }?>
                        </td>
                        <td>
                          <?php if($row->active){?> 
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?> 
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td>
                        <td>
                          <?php echo $row->updated?>
                        </td>
                        <td>
                          <a href="<?php echo base_url($this->controller.'/editMapCategory/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pen"></i></a>
                        </td>
                      </tr>

                    <?php }?>

                  </tbody>
                </table>
              </div>
            </div>
</div>