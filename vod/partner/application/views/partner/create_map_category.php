<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/map_category')?>"><?php echo __('PARTNER MAP CATEGORIES LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vvod_partner_map_category->id)?__('Edit partner map category'):__('Create partner map category')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vvod_partner_map_category->id)?__('Edit partner map category form'):__('Create partner map category form')?> 

  </h6> 


</div>
<div class="card-body"> 
  <?php echo form_open('',['name'=>'create-partner-map-category-form'])?> 
    <div class="row">
      <div class="col-lg-4">
        <div class="form-group">
          <label><strong><?php echo __('Select Partner')?> : </strong></label>
          <?php echo form_dropdown('vvod_partners_id',@$select_partner,@$vvod_partner_map_category->vvod_partners_id,'class="form-control"')?>
        </div>

        <div class="form-group">
          <label><strong><?php echo __('Select PSI Category')?> : </strong></label>
          <?php echo form_dropdown('vvod_categories_id',@$select_category,@$vvod_partner_map_category->vvod_categories_id,'class="form-control"')?>
        </div>

        <div class="form-group">
            <label>
              <?php echo form_radio([
                'name'=>'check_map_type',
                'value'=>'by_api_id',
                'checked'=>(@$vvod_partner_map_category->is_map_api_category_status || !$vvod_partner_map_category->id)?TRUE:FALSE,
                'onclick'=>'clickCheckMapType(this)'
              ])?> <span><?php echo __('By api id')?></span>
            </label> 
            <label>
              <?php echo form_radio([
                'name'=>'check_map_type',
                'value'=>'by_slug',
                'checked'=>(@$vvod_partner_map_category->is_map_api_slug_status)?TRUE:FALSE,
                'onclick'=>'clickCheckMapType(this)'
              ])?> <span><?php echo __('By slug')?></span>
            </label> 
        </div>

        <div id="show_check_map_option">
          <div class="form-group">
            <label><strong><?php echo __('Api category id')?> : </strong></label>
            <?php echo form_input([
              'name'=>'vvod_partner_api_category_id',
              'class'=>'form-control',
              'style'=>'display:block',
              'value'=>@$vvod_partner_map_category->vvod_partner_api_category_id
            ])?>
          </div>

          <div class="form-group" style="display: none;">
            <label><strong><?php echo __('Api slug')?> : </strong></label> 
            <?php echo form_input([
              'name'=>'vvod_partner_api_slug',
              'class'=>'form-control',
              'value'=>@$vvod_partner_map_category->vvod_partner_api_slug
            ])?>
          </div>
        </div> 

        <div class="form-group">
          <label><strong><?php echo __('Status','default')?> : </strong></label> 
          <?php echo form_dropdown('active',[
            '1'=>__('Active','default'),
            '0'=>__('Unactive','default')
          ],@$vvod_partner_api_category->id,'class="form-control"')?>
        </div>

        <div class="form-group">
          <?php echo form_button([
            'type'=>'submit',
            'class'=>'btn btn-success float-right',
            'content'=>__('Submit','default')
          ])?>
        </div>

      </div>
      <div class="col-lg-4">

      </div>
      <div class="col-lg-4">

      </div>
    </div>

  <?php echo form_close()?>

</div>
</div>