<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('VOD USERS LIST')?></li>
  </ol>
</nav>

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('VOD USERS LIST TABLE')?> 
              
          </h6> 
            </div>
            <div class="card-body">
                            <!-- start of search box -->  
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                 <a href="javascript:void(0)" data-toggle="collapse" data-target="#searchBoxCollapse"> <h6><i class="fa fa-search"></i> <?php echo __('Search')?></h6> </a>
                </div>
                <div class="card-body collapse show" id="searchBoxCollapse">  
                  <?php echo form_open('',['name'=>'search-vvod-user-form','onsubmit'=>'onSubmitSearchForm(event)'])?>
                    <div class="row">
                      
                      <div class="col-lg-4">
                        <div class="form-group">
                          <?php echo form_input([
                            'name'=>'search_name',
                            'class'=>'form-control',
                            'value'=>@$criteria['search_name'],
                            'placeholder'=>__('firstname or lastname...')
                          ])?>
                        </div>

                      </div>


                    </div>

                    <div class="row">
                      <div class="col-lg-12">
                        <div class="form-group">
                          <?php echo form_button([
                            'type'=>'submit',
                            'class'=>"btn btn-success float-right",
                            'content'=>'<i class="fa fa-search"></i> '.__('Search','default')
                          ])?> 

                          <?php if(isset($criteria)){?>

                          <a href="<?php echo base_url($this->controller)?>" class="btn btn-danger float-right mr-2"><i class="fa fa-close-o"></i> <?php echo __('Reset')?></a>
                        <?php }?>
                        </div> 


                      </div>
                    </div>
                    <?php echo form_close()?>
                    
                </div>
              </div>
              <!-- eof of search box --> 




              <div class="row">
                <div class="col-lg-12">
                  <div class="alert alert-success">
                    <?php echo __('Result')?> <?php echo @$result_count?> <?php echo __('Record(s)')?>
                  </div>
                </div>
              </div>

              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Ref id')?></th>
                      <th><?php echo __('Firstname & Lastname')?></th>
                      <th><?php echo __('Gender')?></th>
                      <th><?php echo __('Age')?></th>                      
                      <th><?php echo __('Picture Profile')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($vvod_users as $key => $row){?>
                      <tr>
                        <td></td>
                        <td><?php echo $row->ref_id?></td>
                        <td><?php echo $row->firstname.' '.$row->lastname?></td>
                        <td><?php echo $row->gender?></td>
                        <td><?php echo $row->age?></td>
                        <td>
                          <?php if($row->picture_profile){?>
                            <img src="<?php echo $row->picture_profile?>" style="max-width: 80px;">
                          <?php }?>
                        </td>
                        <td><?php echo $row->updated?></td>
                        <td>
                          <a href="javascript:void(0)" class="btn btn-secondary btn-sm"  data-toggle="modal" data-target="#showPackageModal" onclick="clickShowPackageDetail(this)" data-userid="<?php echo $row->id?>" data-userfnln="<?php echo $row->firstname.' '.$row->lastname?>"><i class="fa fa-eye"></i> Package detail</a>
                          <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deleteVvodUser/'.$row->id)?>'}">
                            <i class="fa fa-trash"></i>
                          </a>
                        </td>
                      </tr>

                    <?php }?>

                  </tbody>
                </table>
              </div>

              <!-- for pagination -->
              <div class="row">
                <div class="col-lg-12">
                  <?php echo $pages?>
                </div>
              </div>
              <!-- eof for pagination -->
            </div>
</div>


<!-- Modal -->
<div class="modal fade" id="showPackageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Package detail for <div class="show-firstname-lastname" style="font-size: 10px;"></div></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-bordered" id="packageTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Partner')?></th>
                      <th><?php echo __('Package')?></th>
                      <th><?php echo __('Package Duration')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>

                  </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
      </div>
    </div>
  </div>
</div>