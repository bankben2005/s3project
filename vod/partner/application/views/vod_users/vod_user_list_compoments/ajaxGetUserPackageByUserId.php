<?php if($data->num_rows() > 0){ 

	foreach ($data->result() as $key => $value) { 
		$partner_data = $this->db->select('id,contact_name')
		->from('vvod_partners')
		->where('id',$value->vvod_partners_id)
		->get();
	?>
	<tr>
		<td></td>
		<td><?php echo @$partner_data->contact_name?></td>
		<td>
			<?php echo $value->name.' '.$value->description.' / '.$value->price.' THB'.' / '.$value->days.' Days'?>
		</td>
		<td><?php echo number_format('d/m/Y H:i',strtotime($value->start_datetime)). ' '.number_format('d/m/Y H:i',strtotime($value->end_datetime))?></td>
		<td></td>
	</tr>

<?php } }else{?>

	<tr>
		<td colspan="5" align="center"><label style="color:red; font-size:9px; text-align: center;	">Not found package</label></td>
	</tr>
<?php }?>