<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('CATEGORIES LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('CATEGORIES LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createCategory')?>"><i class="fa fa-plus"></i> <?php echo __('Add category')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Name')?></th>
                      <th><?php echo __('Description')?></th>
                      <th><?php echo __('Mainpage Status')?></th>
                      <th><?php echo __('Live Status')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($vvod_categories as $key => $row){?>
                      <tr>
                        <td></td>
                        <td><?php echo $row->name?></td>
                        <td><?php echo $row->description?></td>
                        <td>
                          <?php if($row->main_page_status){?> 
                            <span class="badge badge-success"><?php echo __('Yes','default')?></span>
                          <?php }else{?> 
                            <span class="badge badge-danger"><?php echo __('No','default')?></span>
                          <?php }?>
                        </td>
                        <td>
                          <?php if($row->live_status){?>
                            <span class="badge badge-success"><?php echo __('Yes','default')?></span>
                          <?php }else{?>  
                            <span class="badge badge-danger"><?php echo __('No','default')?></span>
                          <?php }?>
                        </td>
                        <td>
                          <?php if($row->active){?>
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>  
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td>
                        <td>
                          <?php echo $row->updated?>
                        </td>
                        <td>
                          <a href="<?php echo base_url($this->controller.'/editCategory/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pen"></i></a> 

                          <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deleteCategory/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>
                          
                        </td>
                      </tr>
                    <?php }?>

                  </tbody>
                </table>
              </div>
            </div>
</div>