<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller)?>"><?php echo __('CATEGORIES LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vvod_categories->id)?__('Edit category'):__('Create category')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



<div class="card shadow mb-4">
  <div class="card-header py-3">
    <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vvod_categories->id)?__('Edit category form'):__('Create category form')?> 

  </h6> 


</div>
<div class="card-body"> 

  <?php echo form_open_multipart('',['name'=>'create-category-form'])?>
    <div class="row">
      <div class="col-lg-4">
        <div class="form-group">
          <label><strong><?php echo __('Name')?> : </strong></label>
          <?php echo form_input([
            'name'=>'name',
            'class'=>'form-control',
            'value'=>@$vvod_categories->name
          ])?>
        </div>

        <div class="form-group">
          <label><strong><?php echo __('Description')?> : </strong></label>
          <?php echo form_textarea([
            'name'=>'description',
            'class'=>'form-control',
            'rows'=>3,
            'value'=>@$vvod_categories->description
          ])?>
        </div>

        <div class="form-group">
          <div class="material-switch">
                            <span><strong><?php echo __('Mainpage Status')?> : </strong></span>
                            <?php echo form_checkbox([
                              'name'=>'main_page_status',
                              'value'=>1,
                              'class'=>'switch',
                              'id'=>'main_page_status',
                              'checked'=>(@$vvod_categories->main_page_status)?TRUE:FALSE
                            ])?> 
                          <label for="main_page_status" class="badge-success"></label>
                          
                          </div>
        </div> 

        <div class="form-group">
          <div class="material-switch">
                            <span><strong><?php echo __('Live Status')?> : </strong></span>
                            <?php echo form_checkbox([
                              'name'=>'live_status',
                              'value'=>1,
                              'class'=>'switch',
                              'id'=>'live_status',
                              'checked'=>(@$vvod_categories->live_status)?TRUE:FALSE
                            ])?> 
                          <label for="live_status" class="badge-success"></label>
                          
                          </div>
        </div>

        <div class="form-group">
          <label><strong><?php echo __('Status','default')?> : </strong></label>
          <?php echo form_dropdown('active',[
            '1'=>__('Active','default'),
            '0'=>__('Unactive','default')
          ],@$vvod_categories->active,'class="form-control"')?>
        </div>

        <div class="form-group">
          <?php echo form_button([
            'type'=>'submit',
            'class'=>'btn btn-success float-right',
            'content'=>__('Submit','default')
          ])?>
        </div>

      </div>
      <div class="col-lg-4">

      </div>
      <div class="col-lg-4">

      </div>

    </div>

  <?php echo form_close()?>

</div>
</div>