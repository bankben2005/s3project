<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Partner extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){
    	//print_r($this->session->userdata('user_data'));
    	$vvod_partner = new M_vvod_partners();
    	$vvod_partner->get();

    	$data = array(
    		'vvod_partners'=>$vvod_partner
    	);
    	$this->template->content->view('partner/partner_list',$data);
    	$this->template->publish();
    }



    public function partner_packages(){ 

        $partner_packages = new M_vvod_partner_packages();
        $partner_packages->get();


    	$data = [
            'partner_packages'=>$partner_packages
    	];

    	$this->template->content->view('partner/partner_package_list',$data);
    	$this->template->publish();
    }

    public function map_category(){ 

        $vvod_partner_map_category = new M_vvod_partner_map_category();
        $vvod_partner_map_category->get();


        $data = [
            'vvod_partner_map_category'=>$vvod_partner_map_category
        ];

        $this->template->content->view('partner/map_category_list',$data);
        $this->template->publish();
    }

    public function createPartner(){
    	$this->__createPartner();
    }
    public function editPartner($id){

    	$vvod_partner = new M_vvod_partners($id);

    	if(!$vvod_partner->id){
    		redirect($this->controller);
    	}

    	$this->__createPartner($id);
    }

    public function deletePartner($id){
    	$vvod_partner = new M_vvod_partners($id);

    	if(!$vvod_partner->id){
    		redirect($this->controller);
    	}

    	// chceck all image file 
    	$menu_image = './uploaded/partner/menu/'.$vvod_partner->id.'/'.$vvod_partner->menu_image;

    	$logo_image = './uploaded/partner/logo/'.$vvod_partner->id.'/'.$vvod_partner->logo_image;

    	$cover_image = './uploaded/partner/cover/'.$vvod_partner->id.'/'.$vvod_partner->cover_image;


    	if(file_exists($menu_image)){
    		unlink($menu_image);
    	} 

    	if(file_exists($logo_image)){
    		unlink($logo_image);
    	}

    	if(file_exists($cover_image)){
    		unlink($cover_image);
    	} 


    	if($vvod_partner->delete()){
    		$this->msg->add(__('Delete parter success','partner/partner_list'),'success');
    		redirect($this->controller);
    	}




    }

    public function createPartnerPackage(){

    	$this->__createPartnerPackage();


    }
    public function editPartnerPackage($id){
    	$partner_package = new M_vvod_partner_packages($id);

    	if(!$id){
    		redirect($this->controller.'/partner_packages');
    	}
    	$this->__createPartnerPackage($id);

    }
    public function deletePartnerPackage($id){
    	$partner_package = new M_vvod_partner_packages($id);

    	if(!$id){
    		redirect($this->controller.'/partner_packages');
    	}


    }

    public function createMapCategory(){
        $this->__createMapCategory();
    }

    public function editMapCategory($id){
        $vvod_partner_map_category = new M_vvod_partner_map_category($id);

        if(!$vvod_partner_map_category->id){
            redirect($this->controller.'/map_category');
        }

        $this->__createMapCategory($id);
    }
    public function deleteMapCategory($id){

        $vvod_partner_map_category = new M_vvod_partner_map_category($id);

        if(!$vvod_partner_map_category->id){
            redirect($this->controller.'/map_category');
        }


    }



    private function __createPartner($id = null){

    	$this->loadValidator(); 
		$this->loadSweetAlert();

		$this->loadBootstrapFileuploadMasterStyle();
		$this->loadBootstrapFileuploadMasterScript();

		if($id){
			$this->template->javascript->add(base_url('assets/js/partner/edit_partner.js'));
		}else{	
			$this->template->javascript->add(base_url('assets/js/partner/create_partner.js'));
		}

    	$vvod_partner = new M_vvod_partners($id); 



    	if($this->input->post(NULL,FALSE)){
    		$vvod_partner->contact_name = $this->input->post('contact_name');
    		$vvod_partner->contact_address = $this->input->post('contact_address');
    		$vvod_partner->contact_telephone = $this->input->post('contact_telephone');
    		$vvod_partner->contact_fax = $this->input->post('contact_fax');
    		$vvod_partner->slug = $this->input->post('slug');
    		$vvod_partner->active = $this->input->post('active');

    		if($vvod_partner->save()){

    			// check upload menu image
				if($_FILES['menu_image']['error'] == 0){
					$this->uploadPartnerMenuImage(array(
						'vvod_partners_id'=>$vvod_partner->id
					));
				}
				/* eof check upload image */ 


    			// check upload logo image
    			if($_FILES['logo_image']['error'] == 0){
					$this->uploadPartnerLogoImage(array(
						'vvod_partners_id'=>$vvod_partner->id
					));
				}



    			// check upload cover image
    			if($_FILES['cover_image']['error'] == 0){
					$this->uploadPartnerCoverImage(array(
						'vvod_partners_id'=>$vvod_partner->id
					));
				}


				$txtSuccess = ($id)?__('Edit partner success','partner/create_partner'):__('Create partner success','partner/create_partner');
				$this->msg->add($txtSuccess,'success');
				redirect($this->uri->uri_string());


    		}

    	}


    	$data = [
    		'vvod_partner'=>$vvod_partner
    	];


    	$this->template->content->view('partner/create_partner',$data);
    	$this->template->publish();



    }

    private function __createPartnerPackage($id = null){

        $this->loadValidator(); 
        $this->loadSweetAlert();

        $this->loadBootstrapFileuploadMasterStyle();
        $this->loadBootstrapFileuploadMasterScript();

        

        if($id){
            $this->template->javascript->add(base_url('assets/js/partner/edit_partner_package.js'));
        }else{
            $this->template->javascript->add(base_url('assets/js/partner/create_partner_package.js'));
        }


    	$vvod_partner_packages = new M_vvod_partner_packages($id);


    	if($this->input->post(NULL,FALSE)){ 

            $vvod_partner_packages->vvod_partners_id = $this->input->post('vvod_partners_id');
            $vvod_partner_packages->device_type = $this->input->post('device_type');
            $vvod_partner_packages->code = $this->input->post('code');
            $vvod_partner_packages->purchase_ref_id = $this->input->post('purchase_ref_id');
            $vvod_partner_packages->name = $this->input->post('name');
            $vvod_partner_packages->description = $this->input->post('description');
            $vvod_partner_packages->price = $this->input->post('price');
            $vvod_partner_packages->currency_symbol = $this->input->post('currency_symbol');
            $vvod_partner_packages->days = $this->input->post('days');
            $vvod_partner_packages->active = $this->input->post('active');

            if($vvod_partner_packages->save()){ 

                // check upload menu image
                if($_FILES['cover']['error'] == 0){
                    $this->uploadPartnerPackageCover(array(
                        'vvod_partner_packages_id'=>$vvod_partner_packages->id
                    ));
                }
                /* eof check upload image */ 


                $txtSuccess = ($id)?__('Edit partner package success','partner/create_partner_package'):__('Create partner package success','partner/create_partner_package'); 

                $this->msg->add($txtSuccess,'success');
                redirect($this->uri->uri_string());


            }



    	}


    	$data = [
    		'select_partner'=>$this->getSelectPartner(),
            'vvod_partner_packages'=>$vvod_partner_packages
    	];


    	$this->template->content->view('partner/create_partner_package',$data);
    	$this->template->publish();





    }

    private function __createMapCategory($id = null){

        $this->loadValidator(); 

        if($id){
            $this->template->javascript->add(base_url('assets/js/partner/edit_map_category.js'));
        }else{
            $this->template->javascript->add(base_url('assets/js/partner/create_map_category.js'));
        }


        $this->template->stylesheet->add(base_url('assets/css/switch/material_switch.css'));
        $vvod_partner_map_category = new M_vvod_partner_map_category($id);

        if($this->input->post(NULL,FALSE)){

            $vvod_partner_map_category->vvod_partners_id = $this->input->post('vvod_partners_id');
            $vvod_partner_map_category->vvod_partner_api_category_id = ($this->input->post('check_map_type') == 'by_api_id')?$this->input->post('vvod_partner_api_category_id'):'';
            $vvod_partner_map_category->vvod_partner_api_slug = ($this->input->post('check_map_type') == 'by_slug')?$this->input->post('vvod_partner_api_slug'):'';
            $vvod_partner_map_category->vvod_categories_id = $this->input->post('vvod_categories_id');
            $vvod_partner_map_category->is_map_api_category_status = ($this->input->post('check_map_type') == 'by_api_id')?1:0;
            $vvod_partner_map_category->is_map_api_slug_status = ($this->input->post('check_map_type') == 'by_slug')?1:0;
            $vvod_partner_map_category->active = $this->input->post('active');

            if($vvod_partner_map_category->save()){


                $txtSuccess = ($id)?__('Update map category success','partner/create_map_category'):__('Create map category success','partner/create_map_category');

                $this->msg->add($txtSuccess,'success');
            }

        }



        $data = [
            'vvod_partner_map_category'=>$vvod_partner_map_category,
            'select_partner'=>$this->getSelectPartner(),
            'select_category'=>$this->getSelectVVODCategories()
        ];

        $this->template->content->view('partner/create_map_category',$data);
        $this->template->publish();

    }

    private function uploadPartnerMenuImage($data = []){
    	$vvod_partners_id = $data['vvod_partners_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('uploaded/partner/menu/'.$vvod_partners_id.'')){
			mkdir('uploaded/partner/menu/'.$vvod_partners_id.'',0777,true);
		}
		clearDirectory('uploaded/partner/menu/'.$vvod_partners_id.'/');

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/partner/menu/'.$vvod_partners_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('menu_image')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			if(file_exists('uploaded/partner/menu/'.$vvod_partners_id)){
				rmdir('uploaded/partner/menu/'.$vvod_partners_id);
			}

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

            //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name']; 

			$update_partner = new M_vvod_partners($vvod_partners_id);
			$update_partner->menu_image = $upload_file_name;
			$update_partner->menu_image_size = $_FILES['menu_image']['size']; 
			$update_partner->save();
			return true;
		}


    }

    private function uploadPartnerLogoImage($data = []){

    	$vvod_partners_id = $data['vvod_partners_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('uploaded/partner/logo/'.$vvod_partners_id.'')){
			mkdir('uploaded/partner/logo/'.$vvod_partners_id.'',0777,true);
		}
		clearDirectory('uploaded/partner/logo/'.$vvod_partners_id.'/');

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/partner/logo/'.$vvod_partners_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('logo_image')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			if(file_exists('uploaded/partner/logo/'.$vvod_partners_id)){
				rmdir('uploaded/partner/logo/'.$vvod_partners_id);
			}

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

            //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name']; 

			$update_partner = new M_vvod_partners($vvod_partners_id);
			$update_partner->logo_image = $upload_file_name;
			$update_partner->logo_image_size = $_FILES['logo_image']['size']; 
			$update_partner->save();
			return true;
		}
    	
    }

    private function uploadPartnerCoverImage($data = []){

    	$vvod_partners_id = $data['vvod_partners_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('uploaded/partner/cover/'.$vvod_partners_id.'')){
			mkdir('uploaded/partner/cover/'.$vvod_partners_id.'',0777,true);
		}
		clearDirectory('uploaded/partner/cover/'.$vvod_partners_id.'/');

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/partner/cover/'.$vvod_partners_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('cover_image')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			if(file_exists('uploaded/partner/cover/'.$vvod_partners_id)){
				rmdir('uploaded/partner/cover/'.$vvod_partners_id);
			}

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

            //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name']; 

			$update_partner = new M_vvod_partners($vvod_partners_id);
			$update_partner->cover_image = $upload_file_name;
			$update_partner->cover_image_size = $_FILES['cover_image']['size']; 
			$update_partner->save();
			return true;
		}

    	
    }

    private function uploadPartnerPackageCover($data = []){
       // $vvod_partner_packages = $data['vvod_partner_packages'];
        $vvod_partner_packages_id = $data['vvod_partner_packages_id'];


        $this->load->library(array('upload'));
            //print_r($_FILES);
        if(!is_dir('uploaded/partner_packages/'.$vvod_partner_packages_id.'')){
            mkdir('uploaded/partner_packages/'.$vvod_partner_packages_id.'',0777,true);
        }
        clearDirectory('uploaded/partner_packages/'.$vvod_partner_packages_id.'/');

        $config = array();                                         
        $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
        $config['upload_path'] = 'uploaded/partner_packages/'.$vvod_partner_packages_id.'/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2000';
        $this->upload->initialize($config);
        if(!$this->upload->do_upload('cover')){
            $error = array('error' => $this->upload->display_errors());
            $this->msg->add($error['error'], 'error');
            if(file_exists('uploaded/partner_packages/'.$vvod_partner_packages_id)){
                rmdir('uploaded/partner_packages/'.$vvod_partner_packages_id);
            }

        }else{
            $data_upload = array('upload_data' => $this->upload->data());

            //$promotion_update = new M_promotion($promotion_id);
            $upload_file_name = $data_upload['upload_data']['file_name']; 

            $update_partner_package = new M_vvod_partner_packages($vvod_partner_packages_id);
            $update_partner_package->cover = $upload_file_name;
            $update_partner_package->cover_size = $_FILES['cover']['size']; 
            $update_partner_package->save();
            return true;
        }
    }

    private function getSelectPartner(){
    	$arrReturn = [];

    	$arrReturn[''] = __('Select Partner','partner/create_partner_package');
    	$vvod_partners = new M_vvod_partners();
    	$vvod_partners->where('active',1)
    	->get();

    	foreach ($vvod_partners as $key => $value) {
    		# code...
    		$arrReturn[$value->id] = $value->contact_name;
    	}

    	return $arrReturn;
    }

    // private function getSelectPartner(){
    //     $arrReturn = [];
    //     $arrReturn[''] = __('Select Partner','partner/create_map_category');

        
    // }

    private function getSelectVVODCategories(){
        $arrReturn = [];
        $arrReturn[''] = __('Select Category','partner/create_map_category');

        $vvod_categories = new M_vvod_categories();
        $vvod_categories->where('active',1)
        ->get();

        foreach ($vvod_categories as $key => $value) {
            # code... 
            $arrReturn[$value->id] = $value->name;
        }

        return $arrReturn;

    }

}