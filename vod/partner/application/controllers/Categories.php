<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Categories extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){

    	$vvod_categories = new M_vvod_categories();
    	$vvod_categories->get();


    	$data = [
    		'vvod_categories'=>$vvod_categories
    	];

    	$this->template->content->view('categories/categories_list',$data);
    	$this->template->publish();

    }

    public function createCategory(){
    	$this->__createCategory();
    }

    public function editCategory($id){

    	$vvod_categories = new M_vvod_categories($id);

    	if(!$vvod_categories->id){
    		redirect($this->controller);
    	}

    	$this->__createCategory($id);
    }

    public function deleteCategory($id){
    	$vvod_categories = new M_vvod_categories($id);

    	if(!$vvod_categories->id){
    		redirect($this->controller);
    	}
    }

    private function __createCategory($id = null){ 

    	$this->loadValidator(); 
		$this->loadSweetAlert();

		$this->loadBootstrapFileuploadMasterStyle();
		$this->loadBootstrapFileuploadMasterScript();

    	$this->template->stylesheet->add(base_url('assets/css/switch/material_switch.css'));

    	if($id){
    		$this->template->javascript->add(base_url('assets/js/category/edit_category.js'));
    	}else{
    		$this->template->javascript->add(base_url('assets/js/category/create_category.js'));
    	}

    	$vvod_categories = new M_vvod_categories($id);


    	if($this->input->post(NULL,FALSE)){ 

    		// print_r($this->input->post());exit;

    		$vvod_categories->name = $this->input->post('name');
    		$vvod_categories->description = $this->input->post('description');
    		$vvod_categories->main_page_status = @$this->input->post('main_page_status');
    		$vvod_categories->live_status = @$this->input->post('live_status');

    		if($vvod_categories->save()){


    			$txtSuccess = ($id)?__('Edit category success','categories/create_category'):__('Create category success','categories/create_category');

    			$this->msg->add($txtSuccess,'success');

    			redirect($this->uri->uri_string());

    		}

    	}

    	$data = [
    		'vvod_categories'=>$vvod_categories
    	];

    	$this->template->content->view('categories/create_category',$data);
    	$this->template->publish();

    }

}