<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Vod_users extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index($page = null){

        $this->template->javascript->add(base_url('assets/js/partner/vvod_users_list.js')); 

        $this->load->library([
            'pagination'
        ]);

        $vvod_users = new M_vvod_users();

        if(!empty($_GET)){
            $criteria = $this->getDecryptQueryString([
                'encrypt_txt'=>$_GET['q']
            ]); 

            // print_r($criteria);exit;


            if(isset($criteria['search_name']) && $criteria['search_name'] != ''){
                $vvod_users->like('firstname',$criteria['search_name'],'both');
                $vvod_users->or_like('lastname',$criteria['search_name'],'both');
            } 

            $this->setData('criteria',$criteria);

        }

        $clone = $vvod_users->get_clone();
        $vodUserCount = $clone->count();
        $vvod_users->order_by('id','desc');
        $vvod_users->order_by('active','desc');

        $vvod_users->limit($this->page_num,$page);
        $vvod_users->get();

        //echo $channels->check_last_query();

        $this->setData('vvod_users',$vvod_users);
        $this->config_page($vodUserCount,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url($this->controller.'/index/');
        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());
        $this->setData('result_count',$vodUserCount);

        $data = $this->getData();

    	// $vvod_users = new M_vvod_users();
    	// $vvod_users->order_by('id','desc')
    	// ->get();

    	// $data = [
    	// 	'vvod_users'=>$vvod_users
    	// ];


    	$this->template->content->view('vod_users/vod_user_list',$data);
    	$this->template->publish();

    }

    public function deleteVvodUser($vvod_users_id){
    	$vvod_users = new M_vvod_users($vvod_users_id);

    	if(!$vvod_users->id){
    		redirect(base_url($this->controller));
    	}

    	// delete from vvod_user_authentication_tokens

    	$this->db->delete('vvod_user_authentication_tokens',[
    		'vvod_users_id'=>$vvod_users->id
    	]);


    	// delete from vvod_user_partner_authorization
    	$this->db->delete('vvod_user_partner_authorization',[
    		'vvod_users_id'=>$vvod_users->id
    	]);


    	// delete from vvod_users
    	$this->db->delete('vvod_users',[
    		'id'=>$vvod_users->id
    	]);




    	// delete from vvod_user_package_live_transaction


    	// delete from vvod_user_package_live

    	$this->msg->add(__('Delete user success','vvod_users/vod_user_list'),'success');
    	redirect($this->controller);









    }

    public function ajaxGetQueryString(){
       $post_data = $this->input->post(); 

        unset($post_data['csrf_test_name']); 

        $http_build = http_build_query($post_data);

        //echo $http_build;
        $encryption = base64_encode($http_build);

        // echo $encryption;
        $redirect_url = base_url($this->controller.'/index?q='.$encryption);



        echo json_encode([
            'status'=>true,
            'post_data'=>$post_data,
            'redirect_url'=>$redirect_url
        ]);
    }

    public function ajaxGetUserPackageByUserId(){
        $post_data = $this->input->post();

        $query = $this->db->select('*')
        ->from('vvod_user_package_live')
        ->join('vvod_partner_packages', 'vvod_user_package_live.vvod_partner_packages_id = vvod_partner_packages.id')
        ->where('vvod_user_package_live.vvod_users_id',$post_data['user_id'])
        ->get();


        $view = $this->load->view('vod_users/vod_user_list_compoments/ajaxGetUserPackageByUserId',[
            'data'=>$query
        ],true);


        echo json_encode([
            'status'=> true,
            'view'=>$view,
            'post_data'=>$post_data
        ]);
    }

    private function getDecryptQueryString($data = []){ 

        $decrypt_txt = base64_decode($data['encrypt_txt']); 

        parse_str($decrypt_txt,$output);
        return $output;
    }

}