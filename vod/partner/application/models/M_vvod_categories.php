<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_vvod_categories extends DataMapper {

    //put your code here
    var $table = 'vvod_categories';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   // var $has_one = array(
   //         'vod_season' => array(
   //           'class' => 'M_vod_season',
   //             'other_field' => 'vod_episode',
   //             'join_other_as' => 'vod_season',
   //             'join_table' => 'vod_season'
   //         )
   // );
    
    var $has_many = array(
       'vvod_partner_map_category'=>array(
           'class' => 'M_vvod_partner_map_category',
           'other_field' => 'vvod_categories',
           'join_self_as' => 'vvod_categories',
           'join_other_as' => 'vvod_categories',
           'join_table' => 'vvod_partner_map_category'
         )
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}