<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_vvod_partner_map_category extends DataMapper {

    //put your code here
    var $table = 'vvod_partner_map_category';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'vvod_partners' => array(
             'class' => 'M_vvod_partners',
               'other_field' => 'vvod_partner_map_category',
               'join_other_as' => 'vvod_partners',
               'join_table' => 'vvod_partners'
           ),
           'vvod_categories'=>array(
            'class'=>'M_vvod_categories',
            'other_field'=>'vvod_partner_map_category',
            'join_other_as'=>'vvod_categories',
            'join_table'=>'vvod_categories'
           )
   );
    
   //  var $has_many = array(
   //     'vvod_partner_packages' => array(
   //         'class' => 'M_vvod_partner_packages',
   //         'other_field' => 'vvod_partners',
   //         'join_self_as' => 'vvod_partners',
   //         'join_other_as' => 'vvod_partners',
   //         'join_table' => 'vvod_partner_packages'
   //      )
   // );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}