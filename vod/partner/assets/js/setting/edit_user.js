$(document).ready(function(){

	validateForm(); 
    validateResetPasswordForm();

    $('input[name="email"]').attr('readonly','readonly');
}); 


function validateForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-user-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			type:{
               validators:{
                  notEmpty:{
                     message:''
                 }
             }
         },
         firstname:{
           validators:{
              notEmpty:{
                 message:''
             }
         }
     },
     lastname: {
        validators:{
            notEmpty: {
                message:''
            }
        }

    },
    telephone:{
      validators:{
         notEmpty:{
            message:''
        }
    }
}
},
onSuccess: function(e, data) { 

}
});
} 

function validateResetPasswordForm(){
    var base_url = $('input[name="base_url"]').val();

    $('form[name="reset-password-form"]').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {
            new_password:{
                validators:{
                    notEmpty:{
                        message:''
                    },
                    stringLength: {
                        min: 6,
                        max: 30,
                        message: ''
                    }
                }
            },
            confirm_new_password:{
                validators:{
                    notEmpty:{
                        message:''
                    },
                    identical: {
                        field: 'new_password',
                        message: ''
                    }
                }
            }
        },
        onSuccess: function(e, data) { 
            e.preventDefault(); 

            var form_reset_password = $('form[name="reset-password-form"]');

            var form_data = form_reset_password.serialize();

            submitResetPassword(form_data);


            
        }
    });
}


function submitResetPassword(serialize_data){ 

    var base_url = $('input[name="base_url"]').val();

    $.ajax({
        type: "POST",
        url: base_url+"user/ajaxSetResetPassword",
        async:true,
        dataType:'json',
        data:serialize_data,
        beforeSend: function( xhr ) { 



        },
        success: function(response){
            console.log('========= response =========');
            console.log(response);
            if(response.status){ 
                //location.reload();

                $('#resetPasswordModal').modal('toggle');
                swal({
                        title: "Success!",
                        text: "Password has been reset",
                        icon: "success",
                }).then(function(){

                });

            }else{

            }


        },
        error: function (request, status, error) {
            console.log(request.responseText);
        }
    });

}