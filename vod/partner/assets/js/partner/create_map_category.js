$(document).ready(function(){
	validatorForm();
});


function validatorForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-partner-map-category-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			vvod_partners_id: {
				validators:{
					notEmpty: {
						message:''
					}
				}

			},
			vvod_categories_id:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			}
		},
		onSuccess: function(e, data) { 

		}
	});
}
function clickCheckMapType(element){
	var element = $(element); 

	if(element.val() === 'by_slug'){
		$('input[name="vvod_partner_api_slug"]').closest('div.form-group').css({
			'display':''
		}); 

		$('input[name="vvod_partner_api_category_id"]').closest('div.form-group').css({
			'display':'none'
		});


	}else{

		$('input[name="vvod_partner_api_slug"]').closest('div.form-group').css({
			'display':'none'
		});

		$('input[name="vvod_partner_api_category_id"]').closest('div.form-group').css({
			'display':''
		});
	} 

}