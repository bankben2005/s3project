$(document).ready(function(){
	validatorForm();
	initPartnerPackageCoverImage();
});

function validatorForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-partner-package-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			vvod_partners_id: {
				validators:{
					notEmpty: {
						message:''
					}
				}

			},
			code:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			device_type:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			name:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			price:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			currency_symbol:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			days:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			}
		},
		onSuccess: function(e, data) { 
			
		}
	});
}

function initPartnerPackageCoverImage(){
	$('.cover').fileinput({
		theme: 'fa',
		language: $('input[name="current_lang"]').val(),
		uploadUrl: '',
		dropZoneEnabled: true,
		showUpload: false,
		showRemove: true,
		allowedFileExtensions: ['jpg', 'png', 'gif','jpeg']
	}).on('filebatchuploaderror', function(event, data, msg) {
		console.log('event = '+event+' data = '+data+' msg = '+msg);
	}).on('filebatchuploadsuccess',function(event,data,msg){
		location.reload();
	});
}