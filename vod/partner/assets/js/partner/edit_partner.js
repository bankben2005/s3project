$(document).ready(function(){
	initPartnerMenuImage();
	initPartnerLogoImage(); 
	initPartnerCoverImage();
	validatorForm();
});
	function initPartnerMenuImage(){
		var base_url = $('input[name="base_url"]').val();
		$('.menu_image').fileinput({
			theme: 'fa',
			language: $('input[name="current_lang"]').val(),
			uploadUrl: '',
			dropZoneEnabled: true,
			showUpload: false,
			allowedFileExtensions: ['jpg', 'png', 'gif','jpeg'],
			initialPreview: $.parseJSON($('input[name="hide_menu_image"]').val()),
	            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
	            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
	            initialPreviewDownloadUrl: base_url+'uploaded/partner/menu/'+$('input[name="hide_partner_id"]').val()+'/{filename}', // includes the dynamic `filename` tag to be replaced for each config
	            initialPreviewConfig: $.parseJSON($('input[name="hide_menu_image_caption_obj"]').val()),
	            purifyHtml: true, // this by default purifies HTML data for preview
	            overwriteInitial: true

	        }).on('filebatchuploaderror', function(event, data, msg) {
	        	console.log('event = '+event+' data = '+data+' msg = '+msg);
	        }).on('filebatchuploadsuccess',function(event,data,msg){
	        	location.reload();
	        });

    } 
    function initPartnerLogoImage(){
    	var base_url = $('input[name="base_url"]').val();
		$('.logo_image').fileinput({
			theme: 'fa',
			language: $('input[name="current_lang"]').val(),
			uploadUrl: '',
			dropZoneEnabled: true,
			showUpload: false,
			allowedFileExtensions: ['jpg', 'png', 'gif','jpeg'],
			initialPreview: $.parseJSON($('input[name="hide_logo_image"]').val()),
	            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
	            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
	            initialPreviewDownloadUrl: base_url+'uploaded/partner/logo/'+$('input[name="hide_partner_id"]').val()+'/{filename}', // includes the dynamic `filename` tag to be replaced for each config
	            initialPreviewConfig: $.parseJSON($('input[name="hide_logo_image_caption_obj"]').val()),
	            purifyHtml: true, // this by default purifies HTML data for preview
	            overwriteInitial: true

	        }).on('filebatchuploaderror', function(event, data, msg) {
	        	console.log('event = '+event+' data = '+data+' msg = '+msg);
	        }).on('filebatchuploadsuccess',function(event,data,msg){
	        	location.reload();
	        });
    } 
    function initPartnerCoverImage(){
    	var base_url = $('input[name="base_url"]').val();
		$('.cover_image').fileinput({
			theme: 'fa',
			language: $('input[name="current_lang"]').val(),
			uploadUrl: '',
			dropZoneEnabled: true,
			showUpload: false,
			allowedFileExtensions: ['jpg', 'png', 'gif','jpeg'],
			initialPreview: $.parseJSON($('input[name="hide_cover_image"]').val()),
	            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
	            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
	            initialPreviewDownloadUrl: base_url+'uploaded/partner/cover/'+$('input[name="hide_partner_id"]').val()+'/{filename}', // includes the dynamic `filename` tag to be replaced for each config
	            initialPreviewConfig: $.parseJSON($('input[name="hide_cover_image_caption_obj"]').val()),
	            purifyHtml: true, // this by default purifies HTML data for preview
	            overwriteInitial: true

	        }).on('filebatchuploaderror', function(event, data, msg) {
	        	console.log('event = '+event+' data = '+data+' msg = '+msg);
	        }).on('filebatchuploadsuccess',function(event,data,msg){
	        	location.reload();
	        });
    } 

    function validatorForm(){
    	var base_url = $('input[name="base_url"]').val();

    	$('form[name="create-partner-form"]').bootstrapValidator({
    		message: 'This value is not valid',
    		feedbackIcons: {
    			valid: 'glyphicon glyphicon-ok',
    			invalid: 'glyphicon glyphicon-remove',
    			validating: 'glyphicon glyphicon-refresh'
    		},
    		fields: {
    			contact_name: {
    				validators:{
    					notEmpty: {
    						message:''
    					}
    				}

    			},
    			slug:{
    				validators:{
    					notEmpty:{
    						message:''
    					}
    				}
    			}
    		},
    		onSuccess: function(e, data) { 

    		}
    	});
    }