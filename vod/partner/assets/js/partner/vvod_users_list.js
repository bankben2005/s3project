const base_url = $('input[name="base_url"]').val();

function onSubmitSearchForm(e){
	e.preventDefault(); 

	var form_data = $('form[name="search-vvod-user-form"]').serialize();

    $.ajax({
        type: "POST",
        url: base_url+"Vod_users/ajaxGetQueryString",
        async:true,
        dataType:'json',
        data:form_data,
        beforeSend: function( xhr ) { 



        },
        success: function(response){
           console.log('========= response =========');
           console.log(response);
           if(response.status){ 
               window.location.href=response.redirect_url;
           }else{

           }


       },
       error: function (request, status, error) {
        console.log(request.responseText);
    }
});


}

const clickShowPackageDetail = (e) => {
    let element = $(e);
    let fnln = element.data('userfnln');
    let user_id = element.data('userid');

    $('div.show-firstname-lastname').html('').html(fnln);


    var post_data = { user_id };

    $.ajax({
        type: "POST",
        url: base_url+"Vod_users/ajaxGetUserPackageByUserId",
        async:true,
        dataType:'json',
        data:post_data,
        beforeSend: function( xhr ) { 



        },
        success: function(response){
            console.log('========= response =========');
            console.log(response);
            if(response.status){ 
                // window.location.href=response.redirect_url;
                $('table#packageTable > tbody').html('').append(response.view);
            }else{

            }


        },
        error: function (request, status, error) {
            console.log(request.responseText);
        }
    });
};

// function clickShowPackageDetail(){
//     console.log('abcd');
// }