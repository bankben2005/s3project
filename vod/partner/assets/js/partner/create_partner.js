$(document).ready(function(){
	initPartnerMenuImage();
	initPartnerLogoImage(); 
	initPartnerCoverImage();
	validatorForm();
});

function initPartnerMenuImage(){
	$('.menu_image').fileinput({
		theme: 'fa',
		language: $('input[name="current_lang"]').val(),
		uploadUrl: '',
		dropZoneEnabled: true,
		showUpload: false,
		showRemove: true,
		allowedFileExtensions: ['jpg', 'png', 'gif','jpeg']
	}).on('filebatchuploaderror', function(event, data, msg) {
		console.log('event = '+event+' data = '+data+' msg = '+msg);
	}).on('filebatchuploadsuccess',function(event,data,msg){
		location.reload();
	});
}
function initPartnerLogoImage(){
	$('.logo_image').fileinput({
		theme: 'fa',
		language: $('input[name="current_lang"]').val(),
		uploadUrl: '',
		dropZoneEnabled: true,
		showUpload: false,
		showRemove: true,
		allowedFileExtensions: ['jpg', 'png', 'gif','jpeg']
	}).on('filebatchuploaderror', function(event, data, msg) {
		console.log('event = '+event+' data = '+data+' msg = '+msg);
	}).on('filebatchuploadsuccess',function(event,data,msg){
		location.reload();
	});
} 

function initPartnerCoverImage(){
	$('.cover_image').fileinput({
		theme: 'fa',
		language: $('input[name="current_lang"]').val(),
		uploadUrl: '',
		dropZoneEnabled: true,
		showUpload: false,
		showRemove: true,
		allowedFileExtensions: ['jpg', 'png', 'gif','jpeg']
	}).on('filebatchuploaderror', function(event, data, msg) {
		console.log('event = '+event+' data = '+data+' msg = '+msg);
	}).on('filebatchuploadsuccess',function(event,data,msg){
		location.reload();
	});
}

function validatorForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-partner-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			contact_name: {
				validators:{
					notEmpty: {
						message:''
					}
				}

			},
			contact_telephone:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			slug:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			logo_image:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			},
			cover_image:{
				validators:{
					notEmpty:{
						message:''
					}
				}
			}
		},
		onSuccess: function(e, data) { 
			
		}
	});
}