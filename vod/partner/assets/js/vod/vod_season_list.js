function setVodEpisode(element){
	var element = $(element); 
	var season_id = element.data('seasonid'); 
	var base_url = $('input[name="base_url"]').val();
	$('input[name="hide_season_id"]').val(season_id); 


	// get all episode by season id and then render into modal
	$.ajax({
            type: "POST",
            url: base_url+"vod/ajaxGetRenderEpisodeBySeasonId",
            async:true,
            dataType:'json',
            data:{
            	'season_id':season_id
            },
            beforeSend: function( xhr ) { 

            	$('#tableListAddEpisode > tbody > tr').find('input').val('');

            	$('#tableListAddEpisode > tbody > tr').each(function(index,value){
            		if(index > 0){
            			$(value).remove();
            		}
            	});
            	
            },
            success: function(response){
            	console.log('========= response =========');
            	console.log(response);
                if(response.status){ 

                	if(response.return_data.length > 0){ 
                		$.each(response.return_data,function(index,value){

                			var nth_child_number = index+1; 
                			if(index > 0){ 
                				clickAddEpisode($('a[onclick="clickAddEpisode(this)"]'));                				
                			}
                			var current_tr = $('#tableListAddEpisode > tbody > tr:nth-child('+nth_child_number+')');
                			current_tr.find('input[name="name[]"').val(value.name); 
                			current_tr.find('input[name="description[]"').val(value.description);
                			current_tr.find('input[name="video_url[]"').val(value.video_url);
                			current_tr.find('input[name="hide_episode_id[]"]').val(value.id);

                		});

                	}

                	// console.log(response.return_data.length);
                	
                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });



	$('#setEpisodeModal').modal('toggle');

	console.log(season_id);
}

function clickAddEpisode(element){
	var element = $(element); 

	var html_episode = '<tr>';
	html_episode += '<td></td>';
	html_episode += '<td><input name="name[]" class="form-control"></td>';
	html_episode += '<td><input name="description[]" class="form-control"></td>';
	html_episode += '<td><input name="video_url[]" class="form-control"><input type="hidden" name="hide_episode_id[]"></td>';
	html_episode += '<td><a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="removeEpisodeRow(this)"><i class="fa fa-window-close"></i></a></td>';
	html_episode += '</tr>';  


	$('#tableListAddEpisode > tbody').append(html_episode);




}

function removeEpisodeRow(element){
	var element = $(element);
	element.closest('tr').remove();



}

function saveEpisodeRow(element){
	var element = $(element); 
	var submit_status = true;
	var row_data = []; 
	var base_url = $('input[name="base_url"]').val();


	$('input[name="name[]"]').each(function(i,v){
		var value = $(v);

		if(i === 0){
			if(value.val() === ''){
				value.focus();
				submit_status = false;
				return false;
			}

			if($('input[name="description[]"]').eq(i).val() === ''){
				$('input[name="description[]"]').eq(i).focus();
				submit_status = false;
				return false;
			}

			if($('input[name="video_url[]"]').eq(i).val() === ''){
				$('input[name="video_url[]"]').eq(i).focus();
				submit_status = false;
				return false;
			}
		}

		if(value.val() !== ''){
			row_data.push({
				'name':value.val(),
				'description':$('input[name="description[]"]').eq(i).val(),
				'video_url':$('input[name="video_url[]"]').eq(i).val(),
				'hide_episode_id':$('input[name="hide_episode_id[]"]').eq(i).val()
			});
		}

	}); 

	//console.log(row_data);  return false;

	$.ajax({
            type: "POST",
            url: base_url+"vod/ajaxSaveEpisode",
            async:true,
            dataType:'json',
            data:{
            	'row_data':row_data,
            	'season_id':$('input[name="hide_season_id"]').val()
            },
            beforeSend: function( xhr ) {
            	
            },
            success: function(response){
            	console.log('========= response =========');
            	console.log(response);
                if(response.status){ 

                	// toggle modal and show swal
                	$('#setEpisodeModal').modal('toggle'); 
                	swal({
						title: "สำเร็จ!",
						text: "ทำการอัพเดทข้อมูล Episode แล้ว",
						icon: "success",
					}).then(function(){
						location.reload();
						
					});
                	
                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });








}