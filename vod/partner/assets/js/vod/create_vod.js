$(document).ready(function(){
	initVodCoverImage();
	validateForm();

});

function clickCheckHasOne(element){
	var element = $(element);

	// console.log(element.val()); 
	if(element.val() === '0'){
		$('#seasonContent > div.form-group:nth-child(1)').css({
			'display':'none'
		});
		$('#seasonContent > div.form-group:nth-child(2)').css({
			'display':''
		});
		$('#seasonContent > div#season_row').css({
			'display':''
		});
		// console.log($('#seasonContent >'))
	}else{
		$('#seasonContent > div.form-group:nth-child(1)').css({
			'display':''
		});
		$('#seasonContent > div.form-group:nth-child(2)').css({
			'display':'none'
		});
		$('#seasonContent > div#season_row').css({
			'display':'none'
		});
	}
}

function initVodCoverImage(){
        $('.cover').fileinput({
            theme: 'fa',
            language: $('input[name="current_lang"]').val(),
            uploadUrl: '',
            dropZoneEnabled: true,
            showUpload: false,
            showRemove: true,
            allowedFileExtensions: ['jpg', 'png', 'gif']
        }).on('filebatchuploaderror', function(event, data, msg) {
            console.log('event = '+event+' data = '+data+' msg = '+msg);
        }).on('filebatchuploadsuccess',function(event,data,msg){
            location.reload();
        });
} 

function validateForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-vod-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			vod_owner_id:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			},
			vod_categories_id:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			},
			name: {
                    validators:{
                        notEmpty: {
                            message:''
                        }
                    }
                    
            },
            description:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            },
            cover:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            }
		},
		onSuccess: function(e, data) { 

			// check add at least 1 record into  
			//e.preventDefault();

			var check_has_one = $('input[name="check_has_one"]:checked'); 

			if(parseInt(check_has_one.val()) === 1){ 
				var video_url = $('input[name="video_url"]').val();

				if(video_url === ''){
					// alert something and prevent default 
					e.preventDefault();
					swal({
						title: "คุณกรอกข้อมูลไม่ครบถ้วน!",
						text: "กรุณากรอก Video URL",
						icon: "warning",
					}).then(function(){

						// location.reload();
						$('input[name="video_url"]').focus();
					});
				}

			}else if(parseInt(check_has_one.val()) === 0){
				// check atleast season record
				var count_season_name = $('input[name="hide_season_name[]"]').length; 

				if(count_season_name === 0){
					// alert something and prevent default
					e.preventDefault();
					swal({
						title: "คุณกรอกข้อมูลไม่ครบถ้วน!",
						text: "กรุณาเพิ่มข้อมูลซีซั่น ในกรณีที่มีมากกว่า 1 Video",
						icon: "warning",
					}).then(function(){

						
					});
				}

				// console.log(count_season_name);
			}
			
        }
    });
} 


function clickAddSeason(element){
	var element = $(element); 

	$('#addSeasonModal').modal('toggle');


}

function addMoreSeasonRow(element){
	var element = $(element); 

	var html_row = '<tr>'; 
	html_row += '<td><input name="season_name[]" class="form-control"></td>';
	html_row += '<td><input name="season_description[]" class="form-control"></td>';
	html_row += '<td><a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="deleteCurrentRow(this)"><i class="fa fa-trash"></i></a></td>';
	html_row += '</tr>'; 


	$('#addmoreseason-table > tbody').append(html_row);

}

function deleteCurrentRow(element){
	var element = $(element); 

	element.closest('tr').remove();
} 

function saveAddmoreSeasonRow(element){
	var element = $(element);  

	var row_data = [];
	var base_url = $('input[name="base_url"]').val();
	var submit_status = true;

	$('input[name="season_name[]"]').each(function(i,v){
		var value = $(v);

		if(i === 0){
			if(value.val() === ''){
				value.focus();
				submit_status = false;
				return false;
			}

			if($('input[name="season_description[]"]').eq(i).val() === ''){
				$('input[name="season_description[]"]').eq(i).focus();
				submit_status = false;
				return false;
			}

		}

		if(value.val() !== ''){
			row_data.push({
				'season_name':value.val(),
				'season_description':$('input[name="season_description[]"]').eq(i).val(),
				'season_amount_episode':$('input[name="season_amount_episode[]"]').eq(i).val()
			});
		}

	}); 

	if(!submit_status){
		return false;
	}

	// 

	var list_group_html = '<hr><label class="mt-3 text-center">รายการ Season ที่เพิ่มเข้ามา</label>';
	list_group_html += '<ul class="list-group mb-3" style="font-size:10px;">'; 

	//console.log(row_data); 

	list_group_html += '<li class="list-group-item">'; 
		list_group_html += '<div class="row">';
			list_group_html += '<div class="col-lg-6">';
				list_group_html += 'ชื่อ';
			list_group_html += '</div>';
			list_group_html += '<div class="col-lg-6">'; 

			list_group_html += '</div>';

		list_group_html += '</div>';
	list_group_html += '</li>';

	$.each(row_data,function(index,value){
		list_group_html += '<li class="list-group-item">'; 
			list_group_html += '<div class="row">';
				list_group_html += '<div class="col-lg-6">';
					list_group_html += value.season_name;
				list_group_html += '</div>'; 
				list_group_html += '<div class="col-lg-6">'; 
					list_group_html += '<a href="javascript:void(0);" onclick="removeListSeason(this)"><i class="fa fa-trash"></i></a>'; 
					list_group_html += '<input type="hidden" name="hide_season_name[]" value="'+value.season_name+'">';
					list_group_html += '<input type="hidden" name="hide_season_description[]" value="'+value.season_description+'">';
				list_group_html += '</div>';
			list_group_html += '</div>';
		list_group_html += '</li>';
	});

	list_group_html += '</ul>'; 

	console.log(list_group_html);

	$('#seasonContent > div#season_row').html('').html(list_group_html); 
	$('#addSeasonModal').modal('toggle');

	


}

function validateNumber(evt) {
  var theEvent = evt || window.event;
  var key = theEvent.keyCode || theEvent.which;
  key = String.fromCharCode( key );
  var regex = /[0-9]|\./;
  if( !regex.test(key) ) {
    theEvent.returnValue = false;
    if(theEvent.preventDefault) theEvent.preventDefault();
  }
} 

function removeListSeason(element){
	var element = $(element);

	element.closest('li').remove();
}