function removeEpisodeRow(element){
	var element = $(element); 

	console.log(element);


} 

function changeEpisodeStatus(element){
	var element = $(element); 
	var base_url = $('input[name="base_url"]').val();

	var post_data = {
		'status':element.data('status'),
		'vod_episode_id':element.data('epid')
	}; 

	$.ajax({
            type: "POST",
            url: base_url+"vod/ajaxChangeEpisodeStatus",
            async:true,
            dataType:'json',
            data:post_data,
            beforeSend: function( xhr ) { 

            	
            	
            },
            success: function(response){
            	console.log('========= response =========');
            	console.log(response);
                if(response.status){ 
                	location.reload();
                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });

	
} 

$('tbody').sortable({
    stop: function( event, ui ) {
        $('input[name="ordinal[]"]', ui.item.parent()).each(function (i) {
                $(this).val(i+1);
        });
    }
});

function removeEpisodeRow(element){
	var element = $(element); 

	if(element.data('flag') === 'new'){
		element.closest('tr').remove();
		return false;
	}

	var vod_episode_id = element.data('epid');
	var base_url = $('input[name="base_url"]').val();

	//  if remove from edit step this case also delete from record in database 
	if(confirm('ต้องการลบข้อมูล Episode ใช่หรือไม่') == true){

		$.ajax({
            type: "POST",
            url: base_url+"vod/ajaxDeleteEpisodeRow",
            async:true,
            dataType:'json',
            data:{'vod_episode_id':vod_episode_id},
            beforeSend: function( xhr ) { 

            	
            	
            },
            success: function(response){
            	console.log('========= response =========');
            	console.log(response);
                if(response.status){ 
                	//location.reload(); 
                	element.closest('tr').remove();
                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    	});
	}


} 


function clickToAddMoreEpisode(element){
	var element = $(element); 

    var cal_ordinal_val = $('input[name="ordinal[]"]').length;
    //console.log(cal_ordinal_val);

	var html_episode = '<tr>';
	html_episode += '<td><input name="ep_name[]" class="form-control"></td>';
	html_episode += '<td><input name="ep_description[]" class="form-control"></td>';
	html_episode += '<td><input name="ep_video_url[]" class="form-control"><input type="hidden" name="hide_episode_id[]"></td>'; 
    html_episode += '<td><input name="ordinal[]" class="form-control" value="'+(cal_ordinal_val+1)+'"></td>';
	html_episode += '<td><select name="ep_active[]" class="form-control"><option value="1">Active</option><option value="0">Unactive</option></select></td>';
	html_episode += '<td><a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="removeEpisodeRow(this)" data-flag="new"><i class="fa fa-window-close"></i></a></td>';
	html_episode += '</tr>';  


	$('#episode-table > tbody').append(html_episode);


}