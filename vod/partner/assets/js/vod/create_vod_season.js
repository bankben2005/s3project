$(document).ready(function(){
	validatorForm();
}); 

function validatorForm(){
		var base_url = $('input[name="base_url"]').val();

	$('form[name="create-vod-season"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			vod_id:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			},
			name: {
                    validators:{
                        notEmpty: {
                            message:''
                        }
                    }
                    
            }
		},
		onSuccess: function(e, data) { 
			
        }
    });
}

function clickToAddMoreEpisode(element){
	var element = $(element); 

	// console.log(element); 
	// $('#setEpisodeModal').modal('toggle');
	clickAddEpisode(element);



} 

$('tbody').sortable({
    stop: function( event, ui ) {
        $('input[name="ordinal[]"]', ui.item.parent()).each(function (i) {
                $(this).val(i+1);
        });
    }
});

function clickAddEpisode(element){
	var element = $(element); 

	var cal_ordinal_val = $('input[name="ordinal[]"]').length;
    //console.log(cal_ordinal_val);

	var html_episode = '<tr>';
	html_episode += '<td><input name="ep_name[]" class="form-control"></td>';
	html_episode += '<td><input name="ep_description[]" class="form-control"></td>';
	html_episode += '<td><input name="ep_video_url[]" class="form-control"><input type="hidden" name="hide_episode_id[]"></td>'; 
    html_episode += '<td><input name="ordinal[]" class="form-control" value="'+(cal_ordinal_val+1)+'"></td>';
	html_episode += '<td><select name="ep_active[]" class="form-control"><option value="1">Active</option><option value="0">Unactive</option></select></td>';
	html_episode += '<td><a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="removeEpisodeRow(this)" data-flag="new"><i class="fa fa-window-close"></i></a></td>';
	html_episode += '</tr>';  

	$('#episode-table > tbody').append(html_episode);




}
function removeEpisodeRow(element){
	var element = $(element);
	element.closest('tr').remove();



} 

function clickRemoveTRRow(element){
	var element = $(element);

	element.closest('tr').remove();
}

function saveEpisodeRow(element){
	var element = $(element); 
	var submit_status = true;
	var row_data = []; 
	var base_url = $('input[name="base_url"]').val();


	$('input[name="name[]"]').each(function(i,v){
		var value = $(v);

		if(i === 0){
			if(value.val() === ''){
				value.focus();
				submit_status = false;
				return false;
			}

			if($('input[name="description[]"]').eq(i).val() === ''){
				$('input[name="description[]"]').eq(i).focus();
				submit_status = false;
				return false;
			}

			if($('input[name="video_url[]"]').eq(i).val() === ''){
				$('input[name="video_url[]"]').eq(i).focus();
				submit_status = false;
				return false;
			}
		}

		if(value.val() !== ''){
			row_data.push({
				'name':value.val(),
				'description':$('input[name="description[]"]').eq(i).val(),
				'video_url':$('input[name="video_url[]"]').eq(i).val(),
				'hide_episode_id':$('input[name="hide_episode_id[]"]').eq(i).val()
			});
		}

	});  

	var tr_html = '';
	// render into 
	$.each(row_data,function(i,v){ 
		//console.log(v); 
		tr_html += '<tr>'; 
			tr_html += '<td>'+v.name+'</td>';
			tr_html += '<td>'+v.description+'</td>'; 
			tr_html += '<td>';
			tr_html += v.video_url; 
			tr_html += '<input type="hidden" name="hide_episode_name[]" value="'+v.name+'">';
			tr_html += '<input type="hidden" name="hide_description_name[]" value="'+v.description+'">'; 
			tr_html += '<input type="hidden" name="hide_video_url[]" value="'+v.video_url+'">';

			tr_html += '</td>';
			tr_html += '<td>';
			tr_html += '<button type="button" class="btn btn-danger btn-sm" onclick="clickRemoveTRRow(this)"><i class="fa fa-window-close"></i></button>';
			tr_html += '</td>';
		tr_html += '</tr>';

	}); 

	$('#episode-table > tbody').append(tr_html); 

	$('#setEpisodeModal').modal('toggle');

	// $.ajax({
 //            type: "POST",
 //            url: base_url+"vod/ajaxSaveEpisode",
 //            async:true,
 //            dataType:'json',
 //            data:{
 //            	'row_data':row_data,
 //            	'season_id':$('input[name="hide_season_id"]').val()
 //            },
 //            beforeSend: function( xhr ) {
            	
 //            },
 //            success: function(response){
 //            	console.log('========= response =========');
 //            	console.log(response);
 //                if(response.status){ 

 //                	// toggle modal and show swal
 //                	$('#setEpisodeModal').modal('toggle'); 
 //                	swal({
	// 					title: "สำเร็จ!",
	// 					text: "ทำการอัพเดทข้อมูล Episode แล้ว",
	// 					icon: "success",
	// 				}).then(function(){
	// 					location.reload();
						
	// 				});
                	
 //                }else{
                	
 //                }

                
 //            },
 //            error: function (request, status, error) {
 //                console.log(request.responseText);
 //            }
 //    });








}