$(document).ready(function(){
	validatorForm(); 
	initVodCategoryCoverImage();
});
function validatorForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-vod-categories"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			}
		},
		onSuccess: function(e, data) { 
			
        }
    });
} 

function initVodCategoryCoverImage(){
        $('.cover').fileinput({
            theme: 'fa',
            language: $('input[name="current_lang"]').val(),
            uploadUrl: '',
            dropZoneEnabled: true,
            showUpload: false,
            showRemove: true,
            allowedFileExtensions: ['jpg', 'png', 'gif']
        }).on('filebatchuploaderror', function(event, data, msg) {
            console.log('event = '+event+' data = '+data+' msg = '+msg);
        }).on('filebatchuploadsuccess',function(event,data,msg){
            location.reload();
        });
} 