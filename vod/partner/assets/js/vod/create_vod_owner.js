$(document).ready(function(){
	initVodCoverImage(); 

	validatorForm();
});
function initVodCoverImage(){
	$('.cover').fileinput({
		theme: 'fa',
		language: $('input[name="current_lang"]').val(),
		uploadUrl: '',
		dropZoneEnabled: true,
		showUpload: false,
		showRemove: true,
		allowedFileExtensions: ['jpg', 'png', 'gif']
	}).on('filebatchuploaderror', function(event, data, msg) {
		console.log('event = '+event+' data = '+data+' msg = '+msg);
	}).on('filebatchuploadsuccess',function(event,data,msg){
		location.reload();
	});
} 

function validatorForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-vod-owner-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			// channel_name: {
			// 	validators: {
			// 		callback: {
			// 			message: '',
			// 			callback: function (value, validator, $field) {
			// 				if((band_type === 'C' || band_type === 'ALL') && value === ''){
			// 					return false;
			// 				}else{
			// 					return true;
			// 				}
			// 			}
			// 		}
			// 	}
			// },
			company_name: {
                    validators:{
                        notEmpty: {
                            message:''
                        }
                    }
                    
            },
            contact_firstname:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            },
            contact_lastname:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            },
            contact_telephone:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            },
            cover:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            }
		},
		onSuccess: function(e, data) { 
			
        }
    });
}