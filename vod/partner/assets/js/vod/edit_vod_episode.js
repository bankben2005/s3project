$(document).ready(function(){
	validatorForm();
    checkInitialEditForm();
});
function validatorForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-vod-episode"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			select_vod:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			},
			select_vod_season: {
                    validators:{
                        notEmpty: {
                            message:''
                        }
                    }
                    
            },
            name:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            },
            description:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            }
		},
		onSuccess: function(e, data) { 
			
        }
    });
}  

function checkInitialEditForm(){
    var vod_id = $('select[name="select_vod"]').val();

    if(vod_id){
        changeSelectVod(vod_id); 

        //$('select[name="select_vod_season"]').val($('input[name="hide_vod_season_id"]').val()).change();


    }
}


function changeSelectVod(vod_id){
	var element = $(element); 


 
 	var vod_id = vod_id;
 	var base_url = $('input[name="base_url"]').val(); 

 	if(!vod_id){
 		$('select[name="select_vod_season"]').html('').attr('readonly','readonly');
 		return false;
 	}

 	$.ajax({
            type: "POST",
            url: base_url+"vod/ajaxGetSelectVodSeasonByVodId",
            async:true,
            dataType:'json',
            data:{
            	'vod_id':vod_id
            },
            beforeSend: function( xhr ) { 

            	
            	
            },
            success: function(response){
            	console.log('========= response =========');
            	console.log(response);
                if(response.status){ 
                	//location.reload(); 
                	$('select[name="select_vod_season"]').append(
                			$('<option></option>').attr('value','').text('-- Select vod season--')
                	);

                	$.each(response.select_vod_season,function(index,value){
                		$('select[name="select_vod_season"]').append(
                			$('<option></option>').attr('value',value.id).text(value.name)
                		).removeAttr('readonly');

                	}); 


                    var hide_vod_season_id = $('input[name="hide_vod_season_id"]').val();

                    if(hide_vod_season_id){$('select[name="select_vod_season"]').val(hide_vod_season_id);}




                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });





	console.log(vod_id);

}