<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_vod_categories extends DataMapper {

    //put your code here
    var $table = 'vod_categories';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   // var $has_one = array(
   //         'vod_season' => array(
   //           'class' => 'M_vod_season',
   //             'other_field' => 'vod_episode',
   //             'join_other_as' => 'vod_season',
   //             'join_table' => 'vod_season'
   //         )
   // );
    
    var $has_many = array(
       'vod' => array(
           'class' => 'M_vod',
           'other_field' => 'vod_categories',
           'join_self_as' => 'vod_categories',
           'join_other_as' => 'vod_categories',
           'join_table' => 'vod'
        )
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}