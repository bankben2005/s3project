<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_vod_owner extends DataMapper {

    //put your code here
    var $table = 'vod_owner';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   // var $has_one = array(
   //         'provinces' => array(
   //           'class' => 'M_provinces',
   //             'other_field' => 'amphurs',
   //             'join_other_as' => 'provinces',
   //             'join_table' => 'provinces'
   //         )
   // );
    
    var $has_many = array(
       'vod' => array(
           'class' => 'M_vod',
           'other_field' => 'vod_owner',
           'join_self_as' => 'vod_owner',
           'join_other_as' => 'vod_owner',
           'join_table' => 'vod')
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}