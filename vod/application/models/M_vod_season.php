<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of m_currency
 *
 * @author DoOoO lnw
 */
class M_vod_season extends DataMapper {

    //put your code here
    var $table = 'vod_season';
    /**
     *
     * @property M_hotel_setting $hotel_setting
     */
   var $has_one = array(
           'vod' => array(
             'class' => 'M_vod',
               'other_field' => 'vod_season',
               'join_other_as' => 'vod',
               'join_table' => 'vod'
           )
   );
    
    var $has_many = array(
       'vod_episode' => array(
           'class' => 'M_vod_episode',
           'other_field' => 'vod_season',
           'join_self_as' => 'vod_season',
           'join_other_as' => 'vod_season',
           'join_table' => 'vod_episode')
   );

    function __construct($id = NULL) {
        parent::__construct($id);
    }

    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }
    


}