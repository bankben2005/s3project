<?php 
	if($HasVod){ 

		foreach ($VodList as $key => $value) {?>

		<li class="list-group-item">
				<div class="row">
					<div class="col-lg-3">
						<img src="<?php echo base_url('uploaded/vod/'.$value->id.'/'.$value->cover)?>" class="img-fluid">
					</div>
					<div class="col-lg-8">
						<?php echo $value->name;?>

					</div>
					<div class="col-lg-1">
						<a href="javascript:void(0)" data-vodid="<?php echo $value->id?>" onclick="deleteMemberPlaylistVod(this)"><i class="fa fa-window-close"></i></a>
						<?php echo form_input([
							'type'=>'hidden',
							'name'=>'arrVodListId[]',
							'value'=>$value->id
						])?>
					</div>
				</div>

		</li>
<?php 
		}
	}
?>