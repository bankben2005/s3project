<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('VOD UJ LIST')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>


          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('VOD UJ LIST TABLE')?> 


             <!--  <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createVodSeason')?>"><i class="fa fa-plus"></i> <?php echo __('Add vod season')?></a>  -->

              <!-- <div class="clearfix"></div> -->
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Contact Name')?></th>
                      <th><?php echo __('Request Message')?></th>
                      <th><?php echo __('Email')?></th> 
                      <th><?php echo __('Gender')?></th>
                      <th><?php echo __('Age')?></th>
                      <th><?php echo __('Picture Profile')?></th>
                      <th><?php echo __('Device Type')?></th>
                      <th><?php echo __('Created')?></th>
                      <!-- <th><?php echo __('Read Status','default')?></th>
                      <th><?php echo __('Reply Status','default')?></th> -->
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($vod_uj as $key => $row){ 
                      $user_data = getUserdataByMemberAndLoginType([
                        'members_id'=>$row->members_id,
                        'login_type'=>$row->login_type
                      ]);

                      // print_r($user_data);
                    ?>
                    <tr>
                      <td></td>
                      <td>
                        <a href="javascript:void(0)" onclick="assignVodToMember(this)" data-membersid="<?php echo $row->members_id?>" data-logintype="<?php echo $row->login_type?>">
                        <?php echo $user_data->firstname.' '.$user_data->lastname?>
                        </a>
                      </td>
                      <td><?php echo $row->request_message?></td>
                      <td><?php echo @$user_data->email?></td>
                      <td><?php echo @$user_data->gender?></td>
                      <td><?php echo @$user_data->age?></td>
                      <td>
                        <?php if(@$user_data->picture_profile){?>
                          <img src="<?php echo $user_data->picture_profile?>" style="max-width: 80px;">
                        <?php }?>
                      </td>
                      <td><?php echo @$user_data->device_type?></td>
                      <td><?php echo date('d-m-Y H:i',strtotime(@$row->created))?></td>
                      <!-- <td>
                        
                        <?php if($row->read_status){?>
                          <span class="badge badge-success"><?php echo __('Read')?></span>
                        <?php }else{?>
                          <span class="badge badge-warning"><?php echo __('Unread')?></span>
                        <?php }?>
                      </td>
                      <td>
                        <?php if($row->reply_status){?>
                          <span class="badge badge-success"><?php echo __('Replied')?></span>
                        <?php }else{?>
                          <span class="badge badge-warning"><?php echo __('Pending')?></span>
                        <?php }?>
                      </td> -->

                      <td>
                        <!-- <a href="javascript:void(0)"  class="btn btn-secondary btn-sm" data-requestmessage="<?php echo $row->request_message?>" data-ujid="<?php echo $row->id?>" onclick="clickReplyModal(this)" data-replyhistories='<?php echo getUJReplyHistories($row->id)?>'>
                          <i class="fa fa-reply"></i> <?php echo __('Reply','default')?>
                        </a> -->

                        <?php if(in_array($this->admin_data['email'], [
                          'admin@psisat.com',
                          'kridsada@psisat.com',
                          'jquery4me@psisat.com'
                        ])){?>
                          <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete') == true){window.location.href='<?php echo base_url($this->controller.'/deleteVodUJMessage/'.$row->id)?>'}"><i class="fa fa-window-close"></i></a>
                        <?php }?>
                      </td>
                    </tr>

                    <?php }?>

                  </tbody>
                </table>
              </div>

              <!-- for pagination -->
              <div class="row">
                <div class="col-lg-12">
                  <?php echo $pages?>
                </div>
              </div>
              <!-- eof for pagination -->
            </div>
          </div>


<!-- Modal -->
<div class="modal fade" id="replyMessageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Reply Message')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',['name'=>'reply-message-form','onsubmit'=>"submitReplyMessage(event)"])?> 
      <?php echo form_input([
        'type'=>'hidden',
        'name'=>'hide_vod_uj_id',
        'value'=>''
      ])?>
      <div class="modal-body">
        
          <div id="modal-request-message">

          </div>

          <div class="form-group">
            <label><strong><?php echo __('Reply Message')?> : </strong></label>
            <?php echo form_textarea([
              'name'=>'reply_message',
              'class'=>'form-control',
              'rows'=>3,
              'required'=>'required'
            ])?>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="assignMemberPlaylist" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-1000" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Update Member Playlist')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',['name'=>'member-playlist-form','onsubmit'=>"submitMemberPlaylist(event)"])?> 
      <?php echo form_input([
        'type'=>'hidden',
        'name'=>'hide_member_playlist_data',
        'value'=>''
      ])?>
      <div class="modal-body">

          <div class="row">
            <div class="col-lg-6">
              <div class="card" id="card-user-playlist">
                <div class="card-header text-center"><?php echo __('User Playlist')?></div>
                <div class="card-body">
                  <ul class="list-group" id="member_playlist_listgroup">
                    
                  </ul>
                </div>
              </div>
            </div>
            <div class="col-lg-6">
              <div class="card">
                <div class="card-header text-center"><?php echo __('Choose Video')?></div>
                <div class="card-body">
                  <div class="form-group">
                    <?php echo form_dropdown('select_category',@$select_category,'','class="form-control" onchange="changeSelectCategory(this)"')?>
                  </div>

                  <ul class="list-group" id="vod_listgroup">
                    
                  </ul>

                </div>
              </div>
            </div>

          </div>
        
         
        
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button> -->
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>