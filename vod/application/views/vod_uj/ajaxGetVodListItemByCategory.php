<?php 
	if(count($vod) > 0){ 

		foreach ($vod as $key => $value) {?>

		<li class="list-group-item">
			<div class="row">

					<div class="col-lg-3">
						<img src="<?php echo base_url('uploaded/vod/'.$value->id.'/'.$value->cover)?>" class="img-fluid">
					</div>
					<div class="col-lg-8">
						<?php echo $value->name;?>

					</div>
					<div class="col-lg-1">
						<a href="javascript:void(0)" class="btn btn-success btn-sm float-right" onclick="addVodToMemberPlaylist(this)" data-vodid="<?php echo $value->id?>" data-cover="<?php echo base_url('uploaded/vod/'.$value->id.'/'.$value->cover)?>" data-vodname="<?php echo $value->name?>"><i class="fa fa-arrow-left"></i></a>
					</div>
					
			</div>
			

		</li>


<?php } }?>