<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('Change Password')?></li>
  </ol>
</nav>


<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('Change password form')?>

          		</h6> 


            </div>
            <div class="card-body"> 
              <?php echo form_open('',['name'=>'change-password-form'])?> 
                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label><strong><?php echo __('Old Password')?> : </strong></label>
                      <?php echo form_input([
                        'type'=>'password',
                        'name'=>'old_password',
                        'class'=>'form-control'
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('New Password')?> : </strong></label>
                      <?php echo form_input([
                        'type'=>'password',
                        'name'=>'new_password',
                        'class'=>'form-control'
                      ])?>
                    </div> 


                    <div class="form-group">
                      <label><strong><?php echo __('Confirm new password')?> : </strong></label>
                      <?php echo form_input([
                        'type'=>'password',
                        'name'=>'confirm_new_password',
                        'class'=>'form-control'
                      ])?>
                    </div>

                    <div class="form-group">
                      <?php echo form_button([
                        'type'=>'submit',
                        'class'=>'btn btn-success float-right',
                        'content'=>__('Submit','default')
                      ])?>
                    </div>



                  </div>
                </div>

              <?php echo form_close()?>
            </div>

          </div>

