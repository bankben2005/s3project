<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('Edit Profile')?></li>
  </ol>
</nav>


<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>

          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('Edit profile form')?>

          		</h6> 


            </div>
            <div class="card-body"> 
              <?php echo form_open('',['name'=>'edit-profile-form'])?> 
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label><strong><?php echo __('Email')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'email',
                        'class'=>'form-control',
                        'value'=>@$admin_data->email,
                        'readonly'=>'readonly'
                      ])?>
                    </div>  


                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label><strong><?php echo __('Firstname')?> : </strong></label>
                          <?php echo form_input([
                            'name'=>'firstname',
                            'class'=>'form-control',
                            'value'=>@$admin_data->firstname
                          ])?>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label><strong><?php echo __('Lastname')?> : </strong></label>
                          <?php echo form_input([
                            'name'=>'lastname',
                            'class'=>'form-control',
                            'value'=>@$admin_data->lastname
                          ])?>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Telephone')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'telephone',
                        'class'=>'form-control',
                        'value'=>@$admin_data->telephone
                      ])?>
                    </div>

                    <div class="form-group">
                      <?php echo form_button([
                        'type'=>'submit',
                        'class'=>'btn btn-success float-right',
                        'content'=>__('Submit','default')
                      ])?>
                    </div>

                  </div>
                  <div class="col-lg-6">

                  </div>
                </div>
              <?php echo form_close()?>

            </div>
          </div>
