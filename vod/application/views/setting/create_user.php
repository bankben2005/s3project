<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller)?>"><?php echo __('USER LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vod_administrator->id)?__('Edit user'):__('Create user')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vod_administrator->id)?__('Edit user form'):__('Create user form')?> 

          		</h6> 



            </div>
            <div class="card-body">   
              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'email_exist',
                'value'=>'อีเมล์นี้ถูกใช้แล้ว, กรุณาใช้อีเมล์อื่น.'
              ])?>
              <?php echo form_open_multipart('',['name'=>'create-user-form'])?> 

                <div class="row">
                  <div class="col-lg-4"> 

                    <div class="form-group"> 

                      <?php 
                        $arrAccessType = [];

                        if(@$vod_administrator->type == 'superadmin'){
                          $arrAccessType = [
                            ''=>__('-- Select accesstype --'),
                            'superadmin'=>'Superadmin',
                            'administrator'=>'Administrator'
                          ];
                        }else{
                          $arrAccessType = [
                            ''=>__('-- Select accesstype --'),
                            'administrator'=>'Administrator'
                          ];
                        }
                      ?>
                      <label><strong><?php echo __('Access Type')?> : </strong></label>
                      <?php echo form_dropdown('type',@$arrAccessType,@$vod_administrator->type,'class="form-control"')?>
                    </div>

                    <div class="row">
                      <div class="col-lg-6">
                        <div class="form-group">
                          <label><strong><?php echo __('Firstname')?> : </strong></label>
                          <?php echo form_input([
                            'name'=>'firstname',
                            'class'=>'form-control',
                            'value'=>@$vod_administrator->firstname
                          ])?>
                        </div>

                      </div>
                      <div class="col-lg-6">

                        <div class="form-group">
                          <label><strong><?php echo __('Lastname')?> : </strong></label>
                          <?php echo form_input([
                            'name'=>'lastname',
                            'class'=>'form-control',
                            'value'=>@$vod_administrator->lastname
                          ])?>
                        </div>

                      </div>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Email')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'email',
                        'class'=>'form-control',
                        'type'=>'email',
                        'value'=>@$vod_administrator->email
                      ])?>
                    </div>

                    <div class="form-group">
                          <label><strong><?php echo __('Telephone')?> : </strong></label>
                          <?php echo form_input([
                            'name'=>'telephone',
                            'class'=>'form-control',
                            'value'=>@$vod_administrator->telephone
                          ])?>
                        </div>

                    <?php if(!@$vod_administrator->id){?>
                    <div class="form-group">
                      <label><strong><?php echo ('Password')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'password',
                        'type'=>'password',
                        'class'=>'form-control',

                      ])?>
                    </div>  

                    <div class="form-group">
                      <label><strong><?php echo ('Confirm Password')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'confirm_password',
                        'type'=>'password',
                        'class'=>'form-control',
                        
                      ])?>
                    </div>  
                    <?php }else{?>
                    <a href="javascript:void(0);" data-toggle="modal" data-target="#resetPasswordModal" class="btn btn-primary btn-block"><i class="fa fa-key"></i> <?php echo __('Reset password')?></a>
                    <?php }?> 

                    <div class="form-group">
                      <label><strong><?php echo __('Status','default')?> : </strong></label> 
                      <?php echo form_dropdown('active',[
                        '1'=>__('Active','default'),
                        '0'=>__('Unactive','default')
                      ],@$vod_administrator->active,'class="form-control"')?>
                    </div> 

                    <div class="form-group">
                      <?php echo form_button([
                        'type'=>'submit',
                        'class'=>'btn btn-success float-right',
                        'content'=>__('Submit','default')
                      ])?>
                    </div>  
                  </div>
                  <div class="col-lg-4">

                  </div>
                </div>

              <?php echo form_close()?>

            </div>
          </div>


<!-- Modal -->
<div class="modal fade" id="resetPasswordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Reset password')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',['name'=>'reset-password-form'])?> 
      <?php echo form_input([
        'type'=>'hidden',
        'name'=>'hide_id',
        'value'=>@$vod_administrator->id
      ])?>
      <div class="modal-body">
        
          <div class="form-group">
            <label><strong><?php echo __('New Password')?> : </strong></label>
            <?php echo form_input([
              'type'=>'password',
              'name'=>'new_password',
              'class'=>'form-control'
            ])?>
          </div> 

          <div class="form-group">
            <label><strong><?php echo __('Confirm new password')?> : </strong></label>
            <?php echo form_input([
              'type'=>'password',
              'name'=>'confirm_new_password',
              'class'=>'form-control'
            ])?>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>