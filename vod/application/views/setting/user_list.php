<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('USER LIST')?></li>
  </ol>
</nav>


          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('USER LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createUser')?>"><i class="fa fa-plus"></i> <?php echo __('Add user')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Firstname&Lastname')?></th>
                      <th><?php echo __('Email')?></th>
                      <th><?php echo __('Telephone')?></th>  
                      <th><?php echo __('Created')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($administrator as $key => $row){?>
                      <tr>
                        <td></td>
                        <td><?php echo $row->firstname.' '.$row->lastname?></td>
                        <td><?php echo $row->email?></td>
                        <td><?php echo $row->telephone?></td>
                        <td><?php echo $row->created?></td>
                        <td><?php echo $row->updated?></td>
                        <td>
                          <?php if($row->active){?>
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td>
                        <td>
                          <!-- <a href="<?php echo base_url($this->controller.'/userAccessMenu/'.$row->id)?>" class="btn btn-primary btn-sm"><i class="fa fa-cog"></i> <?php echo __('UAC')?></a>  -->

                          <a href="<?php echo base_url($this->controller.'/editUser/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pen"></i> </a> 

                          <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deleteUser/'.$row->id)?>'}"><i class="fa fa-trash"></i> </a>
                        </td>
                      </tr>
                    <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>