<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller)?>"><?php echo __('VOD LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vod->id)?__('Edit vod'):__('Create vod')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vod->id)?__('Edit vod form'):__('Create vod form')?> 

          		</h6> 


            </div>
            <div class="card-body"> 
            	<?php 
	                $image_path = base_url('uploaded/vod/'.@$vod->id.'/'.@$vod->cover); 

	                $cover_obj = new StdClass();
	                $cover_obj->caption = $vod->cover;
	                $cover_obj->size = $vod->cover_size;
	                $cover_obj->width = '120px';
	                $cover_obj->url = base_url($this->controller.'/deleteVODCoverImage');
	                $cover_obj->key = $vod->id; 
              	?> 

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_vod_id',
                'value'=>@$vod->id
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover',
                'value'=>(@$vod->cover)?json_encode([$image_path]):'[]'
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover_caption_obj',
                'value'=> json_encode([$cover_obj])
              ])?>   


            	<?php echo form_open_multipart('',['name'=>'create-vod-form'])?> 
            		<div class="row">
            			<div class="col-lg-4">
            				<div class="form-group">
            					<label><strong><?php echo __('Owner')?> : </strong></label>
            					<?php echo form_dropdown('vod_owner_id',@$select_owner,@$vod->vod_owner_id,'class="form-control"')?>
            				</div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Category')?> : </strong></label> 
                      <?php echo form_dropdown('vod_categories_id',@$select_vod_categories,@$vod->vod_categories_id,'class="form-control"')?>
                    </div>

            				<div class="form-group">
            					<label><strong><?php echo __('Name')?> : </strong></label> 
            					<?php echo form_input([
            						'name'=>'name',
            						'class'=>'form-control',
            						'value'=>@$vod->name
            					])?>
            				</div> 

            				<div class="form-group">
            					<label><strong><?php echo __('Description')?> : </strong></label> 

            					<?php echo form_textarea([
            						'name'=>'description',
            						'class'=>'form-control',
            						'rows'=>4,
            						'value'=>@$vod->description
            					])?>
            				</div> 
            				<?php //print_r($vod->has_one_episode_status);exit;?>
            				<div class="form-group">
            					<label>
            						<?php echo form_radio([
            							'name'=>'check_has_one',
            							'value'=>1,
            							'checked'=>(@$vod->has_one_episode_status || !@$vod->id)?TRUE:FALSE,
            							'onclick'=>'clickCheckHasOne(this)'
            						])?> <span><?php echo __('Has one video')?></span>
            					</label> 

            					<label>
            						<?php echo form_radio([
            							'name'=>'check_has_one',
            							'value'=>0,
            							'checked'=>(@$vod->id && !@$vod->has_one_episode_status)?TRUE:FALSE,
            							'onclick'=>'clickCheckHasOne(this)'
            						])?> <span><?php echo __('Has many video')?></span>
            					</label>
            				</div> 


            				<div id="seasonContent">
	            				<div class="form-group">
	            					<label><strong><?php echo __('Video URL')?> : </strong></label>
	            					<?php echo form_input([
	            						'name'=>'video_url',
	            						'class'=>'form-control',
	            						'value'=>@$vod->video_url
	            					])?>
	            				</div> 



	            				<div class="form-group" style="display: none;"> 

	            					<a href="javascript:void(0);" class="btn btn-success btn-block" onclick="clickAddSeason(this)"><i class="fa fa-plus"></i> <?php echo __('Add season')?></a>
	            					
	            				</div> 
	            				<div id="season_row" style="display: none;"> 
	            					<?php if($vod->vod_season->get()->result_count() > 0){ ?> 
	            						<hr><label class="mt-3 text-center">รายการ Season</label>
	            						<ul class="list-group mb-3" style="font-size:10px;"> 
	            							<li class="list-group-item">
	            							<div class="row">
	            								<div class="col-lg-5">
	            									ชื่อ
	            								</div>
	            								<div class="col-lg-5">
	            									จำนวน Episode
	            								</div>
                              
	            								<div class="col-lg-2">

	            								</div>
	            							</div>
	            							</li>
	            					<?php 
	            						foreach($vod->vod_season->get() as $key => $row){
	            					?>
	            						<li class="list-group-item">
	            							<div class="row">
	            								<div class="col-lg-5">
	            									<a href="<?php echo base_url($this->controller.'/editVodSeason/'.$row->id)?>">
	            									<?php echo $row->name?>
	            									</a>
	            								</div>
	            								<div class="col-lg-5">
	            									<?php echo $row->vod_episode->get()->result_count()?>
	            								</div>
	            								<div class="col-lg-2">
	            									<a href="javascript:void(0);" onclick="removeListSeason(this)" data-seasonid="<?php echo $row->id?>"><i class="fa fa-trash"></i></a> 
	            									<input type="hidden" name="hide_season_id[]" value="<?php echo $row->id?>">
	            									<input type="hidden" name="hide_season_name[]" value="<?php echo $row->name?>"> 
	            									<input type="hidden" name="hide_season_description[]" value="<?php echo $row->description?>">
	            								</div>
	            							</div>
	            						</li>

	            					<?php } ?>
	            						</ul>
	            					<?php }else{ ?>
                          <hr><label class="mt-3 text-center">รายการ Season</label>
                          <ul class="list-group mb-3" style="font-size:10px;"> 
                            <li class="list-group-item">
                            <div class="row">
                              <div class="col-lg-5">
                                ชื่อ
                              </div>
                              <div class="col-lg-5">
                                จำนวน Episode
                              </div>
                              
                              <div class="col-lg-2">

                              </div>
                            </div>
                            </li>

                        <?php }?>

	            				</div>
            				</div> 



            				<div class="form-group">
            					<label><strong><?php echo __('Status')?></strong></label> 
            					<?php echo form_dropdown('active',[
            						'1'=>__('Active','default'),
            						'0'=>__('Unactive','default')
            					],@$vod->active,'class="form-control"')?>
            				</div>

                    <div class="form-group">
                      
                          <div class="material-switch">
                            <span><strong><?php echo __('Trending Status')?> : </strong></span>
                            <?php echo form_checkbox([
                              'name'=>'trending_status',
                              'value'=>1,
                              'class'=>'switch',
                              'id'=>'trending_status',
                              'checked'=>(@$vod->trending_status)?TRUE:FALSE
                            ])?> 
                          <label for="trending_status" class="badge-success"></label>
                          
                          </div>
                    </div>

            			</div>
            			<div class="col-lg-8">
            				<div class="form-group">
            					<label><strong><?php echo __('Cover')?> : </strong></label>
            					<div class="upload-box mb-3">
			                        <input type="file" class="cover" name="cover" class="file-loading" />
			                    </div>
            				</div>

                    

            			</div>
            			
            		</div>

            		<div class="row">
            			<div class="col-lg-12">
            				<?php echo form_button([
            					'type'=>'submit',
            					'class'=>'btn btn-success float-right',
            					'content'=>__('Submit','default')
            				])?>
            			</div>
            		</div>
            	<?php echo form_close()?>

            </div>
          </div>


<!-- Modal -->
<div class="modal fade" id="addSeasonModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-1000" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Add season')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<a href="javascript:void(0);" class="btn btn-success float-right mb-3" onclick="addMoreSeasonRow(this)"><i class="fa fa-plus"></i> <?php echo __('Add more season')?></a> 
      	<div class="clearfix"></div>

      	<table class="table" id="addmoreseason-table">
      		<thead>
      			<tr>
      				<th><?php echo __('Season Name')?></th>
      				<th><?php echo __('Season Description')?></th>
      				<th></th>
      			</tr>
      		</thead>
      		<tbody>
      			<tr>
      				<td>
      					<?php echo form_input([
      						'name'=>'season_name[]',
      						'class'=>'form-control'
      					])?>
      						
      				</td>
      				<td>
      					<?php echo form_input([
      						'name'=>'season_description[]',
      						'class'=>'form-control'
      					])?>
      				</td>
      			</tr>
      		</tbody>
      	</table>


      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close')?></button>
        <button type="button" class="btn btn-primary" onclick="saveAddmoreSeasonRow(this)"><?php echo __('Save')?></button>
      </div>
    </div>
  </div>
</div>