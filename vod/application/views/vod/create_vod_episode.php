<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/episode')?>"><?php echo __('VOD EPISODE LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vod_episode->id)?__('Edit vod episode'):__('Create vod episode')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vod_episode->id)?__('Edit vod episode form'):__('Create vod episode form')?> 

          		</h6> 


            </div>
            <div class="card-body">   
              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_vod_id',
                'value'=>@$vod_episode->vod_id
              ])?> 

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_vod_season_id',
                'value'=>@$vod_episode->vod_season_id
              ])?> 


              <?php echo form_open_multipart('',['name'=>'create-vod-episode'])?> 
                <div class="row">
                  <div class="col-lg-4"> 
                    <div class="form-group">
                      <label><strong><?php echo __('Vod')?> : </strong></label> 
                      <?php echo form_dropdown('select_vod',@$select_vod,@$vod_episode->vod_id,'class="form-control" onchange="changeSelectVod(this.value)"')?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Vod Season')?> : </strong></label> 
                      <?php echo form_dropdown('select_vod_season',[],'','class="form-control" readonly="readonly"')?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Name')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'name',
                        'class'=>'form-control',
                        'value'=>@$vod_episode->name
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Description')?> : </strong></label> 
                      <?php echo form_textarea([
                        'name'=>'description',
                        'class'=>'form-control',
                        'value'=>@$vod_episode->description,
                        'rows'=>3
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Video Url')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'video_url',
                        'class'=>'form-control',
                        'value'=>@$vod_episode->video_url
                      ])?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Status','default')?> : </strong></label> 
                      <?php echo form_dropdown('active',[
                        '1'=>__('Active','default'),
                        '0'=>__('Unactive','default')
                      ],@$vod_episode->active,'class="form-control"')?>
                    </div> 

                    <div class="form-group">
                      <?php echo form_button([
                        'type'=>'submit',
                        'class'=>'btn btn-success float-right',
                        'content'=>__('Submit','default')
                      ])?>
                    </div>

                  </div>
                  <div class="col-lg-4">

                  </div>
                  <div class="col-lg-4">

                  </div>
                </div>

              <?php echo form_close()?>
            </div>
          </div>
