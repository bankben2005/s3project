<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/categories')?>"><?php echo __('VOD CATEGORIES LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vod_categories->id)?__('Edit vod category'):__('Create vod category')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vod_categories->id)?__('Edit vod category form'):__('Create vod category form')?> 

          		</h6> 


            </div>
            <div class="card-body">   
              <?php 
                  $image_path = base_url('uploaded/vod_category/'.@$vod_categories->id.'/'.@$vod_categories->cover); 

                  $cover_obj = new StdClass();
                  $cover_obj->caption = $vod_categories->cover;
                  $cover_obj->size = $vod_categories->cover_size;
                  $cover_obj->width = '120px';
                  $cover_obj->url = base_url($this->controller.'/deleteVODCategoryCoverImage');
                  $cover_obj->key = $vod_categories->id; 
                ?> 

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_vod_category_id',
                'value'=>@$vod_categories->id
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover',
                'value'=>(@$vod_categories->cover)?json_encode([$image_path]):'[]'
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover_caption_obj',
                'value'=> json_encode([$cover_obj])
              ])?>   


              <?php echo form_open_multipart('',['name'=>'create-vod-categories'])?> 

                <div class="row">
                  <div class="col-lg-4">
                    <div class="form-group">
                      <label><strong><?php echo __('Name')?></strong></label>
                      <?php echo form_input([
                        'name'=>'name',
                        'class'=>'form-control',
                        'value'=>@$vod_categories->name
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Description')?> : </strong></label>
                      <?php echo form_textarea([
                        'name'=>"description",
                        'class'=>'form-control',
                        'value'=>@$vod_categories->description
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Status')?></strong></label> 
                      <?php echo form_dropdown('active',[
                        '1'=>__('Active','default'),
                        '0'=>__('Unactsive','default')
                      ],@$vod_categories->active,'class="form-control"')?>
                    </div>

                    


                  </div>
                  <div class="col-lg-8"> 
                    <div class="form-group">
                      <label><strong><?php echo __('Cover')?> : </strong></label>
                      <div class="upload-box mb-3">
                              <input type="file" class="cover" name="cover" class="file-loading" />
                          </div>
                    </div>

                  </div>


                  
                </div>

                <div class="row">
                    <div class="col-lg-12">
                      <div class="form-group">
                      <?php echo form_button([
                      'type'=>'submit',
                      'class'=>'btn btn-success float-right',
                      'content'=>__('Submit','default')
                      ])?>
                    </div>
                    </div>
                  </div>

              <?php echo form_close()?>
            </div>
          </div>