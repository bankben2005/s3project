<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('VOD SEASON LIST')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>


          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('VOD SEASON LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createVodSeason')?>"><i class="fa fa-plus"></i> <?php echo __('Add vod season')?></a> 

              <!-- <div class="clearfix"></div> -->
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Owner')?></th>
                      <th><?php echo __('Vod Name')?></th> 
                      <th><?php echo __('Season Name')?></th>
                      <th><?php echo __('Season Description')?></th>
                      <th><?php echo __('Amount Episode')?></th>
                      <th><?php echo __('Created')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($vod_season as $key => $row){?>
                      <tr>
                        <td></td>
                        <th><?php echo $row->vod->get()->vod_owner->select('vod_owner.name as owner_name')->owner_name;?></th>
                        <td><?php echo $row->vod->select('vod.name as vod_name')->get()->vod_name?></td> 
                        <td><?php echo $row->name?></td>
                        <td><?php echo $row->description?></td>
                        <td><?php echo $row->vod_episode->get()->result_count()?></td>
                        <td><?php echo $row->created?></td>
                        <td><?php echo $row->updated?></td>
                        <td>
                          <?php if($row->active){?> 
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>

                          <?php }?>
                        </td>
                        <td> 

                          <!-- <a href="javascript:void(0);" class="btn btn-primary btn-sm" onclick="setVodEpisode(this)" data-seasonid="<?php echo $row->id?>"><i class="fa fa-cog"></i> <?php echo __('Episode')?></a> -->
                          <a href="<?php echo base_url($this->controller.'/editVodSeason/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pen"></i></a> 

                          <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deleteVodSeason/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>

                    <?php }?>
                    
                  </tbody>
                </table>
              </div>
            </div>
          </div> 


<!-- Modal -->
<div class="modal fade" id="setEpisodeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-1000" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Season episode management')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',['name'=>'create-episode-form'])?> 
      <?php echo form_input([
        'type'=>'hidden',
        'name'=>'hide_season_id'
      ])?>
      <div class="modal-body">
        <div class="row mb-3">
          <div class="col-lg-12">
            <a href="javascript:void(0)" class="btn btn-success float-right" onclick="clickAddEpisode(this)"><i class="fa fa-plus"></i> <?php echo __('Add episode')?></a>
          </div>
        </div>
        <table class="table" id="tableListAddEpisode">
          <thead>
            <tr>
              <th></th>
              <th><?php echo __('Name')?></th>
              <th><?php echo __('Description')?></th>
              <th><?php echo __('Video URL')?></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td><?php echo form_input([
                'name'=>'name[]',
                'class'=>'form-control'
                ])?></td>
              <td>
                <?php echo form_input([
                  'name'=>'description[]',
                  'class'=>'form-control'
                ])?>
              </td>
              <td>
                <?php echo form_input([
                  'name'=>'video_url[]',
                  'class'=>'form-control'
                ])?>
                <?php echo form_input([
                  'type'=>'hidden',
                  'name'=>'hide_episode_id[]'
                ])?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close','default')?></button>
        <button type="button" class="btn btn-primary" onclick="saveEpisodeRow()"><?php echo __('Save','default')?></button>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>
