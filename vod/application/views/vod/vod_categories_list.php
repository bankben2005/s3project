<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('VOD CATEGORIES LIST')?></li>
  </ol>
</nav>


        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('VOD CATEGORIES LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createVodCategories')?>"><i class="fa fa-plus"></i> <?php echo __('Add vod categories')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Cover')?></th>
                      <th><?php echo __('Name')?></th>
                      <th><?php echo __('Vod Total')?></th>
                      <th><?php echo __('Ordinal')?></th>
                      <th><?php echo __('Created')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($categories as $key => $row){?>
                      <tr>
                        <td></td>
                        <td><img src="<?php echo base_url('uploaded/vod_category/'.$row->id.'/'.$row->cover)?>" style="max-width: 150px;"></td>
                        <td><?php echo $row->name?></td>
                        <td><?php echo $row->vod->get()->result_count()?></td>
                        <td>
                          <?php echo form_input([
                            'name'=>'ordinal[]',
                            'type'=>'hidden',
                            'value'=>$row->ordinal,
                            'data-categoryid'=>$row->id
                          ])?>
                          <span class="show_txt_ordinal"><?php echo $row->ordinal?></span>
                        </td>
                        <td><?php echo $row->created?></td>
                        <td><?php echo $row->updated?></td>
                        <td>
                            <?php if($row->active){?> 

                              <a href="javascript:void(0);" onclick="changeCategoryStatus(this)" data-status="active" data-catid="<?php echo $row->id?>">
                              <span class="badge badge-success"><?php echo __('Active','default')?></span>
                              </a>
                            <?php }else{?> 
                              <a href="javascript:void(0);" onclick="changeCategoryStatus(this)" data-status="unactive" data-catid="<?php echo $row->id?>">
                              <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                              </a>
                            <?php }?>
                        </td>
                        <td>
                          <a href="<?php echo base_url($this->controller.'/manageVideoOrdinal/'.$row->id)?>" class="btn btn-primary btn-sm">
                            <i class="fa fa-cog"></i> <?php echo __('Vod Ordinal')?>
                          </a>
                          <a href="<?php echo base_url($this->controller.'/editVodCategories/'.$row->id)?>"  class="btn btn-secondary btn-sm">
                            <i class="fa fa-pen"></i>
                          </a> 

                          <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deleteVodCategories/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>
                    <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
        </div>