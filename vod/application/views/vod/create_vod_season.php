<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/season')?>"><?php echo __('VOD SEASON LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$vod_season->id)?__('Edit vod season'):__('Create vod season')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$vod_season->id)?__('Edit vod season form'):__('Create vod season form')?> 

          		</h6> 



            </div>
            <div class="card-body">   
              <?php echo form_open_multipart('',['name'=>'create-vod-season'])?> 
              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_vod_season_id',
                'value'=>@$vod_season->id
              ])?>

              <div class="row">
                <div class="col-lg-4"> 

                  <div class="form-group">
                    <label><strong><?php echo __('VOD')?> : </strong></label>
                    <?php echo form_dropdown('vod_id',@$select_vod,@$vod_season->vod_id,'class="form-control"')?>
                  </div>


                  <div class="form-group">
                    <label><strong><?php echo __('Name')?> : </strong></label> 
                    <?php echo form_input([
                      'name'=>'name',
                      'class'=>'form-control',
                      'value'=>@$vod_season->name
                    ])?>
                  </div> 

                  <div class="form-group">
                    <label><strong><?php echo __('Description')?> : </strong></label> 
                    <?php echo form_textarea([
                      'name'=>'description',
                      'class'=>'form-control',
                      'value'=>@$vod_season->description,
                      'rows'=>3
                    ])?>
                  </div> 

                  <div class="form-group">
                    <label><strong><?php echo __('Status','default')?> : </strong></label> 
                    <?php echo form_dropdown('active',[
                      '1'=>__('Active','default'),
                      '0'=>__('Unactive','default')
                    ],@$vod_season->active,'class="form-control"')?>
                  </div>

                </div>
                <div class="col-lg-8">  

                  

                  <a href="javascript:void(0);" class="btn btn-primary float-right" onclick="clickToAddMoreEpisode(this)"><i class="fa fa-plus"></i> <?php echo __('Add more Episode')?></a> 

                  <div class="clearfix"></div>


                  <hr> 

                  <label><strong>รายการ Episode</strong></label>


                  <table class="table" id="episode-table">
                    <thead>
                      <tr>
                        <th><?php echo __('Episode Name')?></th>
                        <th><?php echo __('Episode Description')?></th> 
                        <th><?php echo __('Video URL')?></th>
                        <th><?php echo __('Ordinal')?></th>
                        <th><?php echo __('Status')?></th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($episode_row as $key => $row){?> 
                        <tr>
                          <td>
                              <?php echo form_input([
                                'name'=>'exist_ep_name[]',
                                'class'=>'form-control',
                                'value'=>@$row->name
                              ])?>
                          </td> 
                          <td>
                              <?php echo form_input([
                                'name'=>'exist_ep_description[]',
                                'class'=>'form-control',
                                'value'=>@$row->description
                              ])?>
                          </td> 
                          <td>
                            <?php echo form_input([
                                'name'=>'exist_ep_video_url[]',
                                'class'=>'form-control',
                                'value'=>@$row->video_url
                              ])?>
                          </td> 
                          <td>
                            <?php echo form_input([
                              'name'=>'ordinal[]',
                              'class'=>'form-control',
                              'value'=>@$row->ordinal
                            ])?>
                          </td>
                          <td>
                            <?php if($row->active){?> 

                              <a href="javascript:void(0);" onclick="changeEpisodeStatus(this)" data-status="active" data-epid="<?php echo $row->id?>">
                              <span class="badge badge-success"><?php echo __('Active','default')?></span>
                              </a>
                            <?php }else{?> 
                              <a href="javascript:void(0);" onclick="changeEpisodeStatus(this)" data-status="unactive" data-epid="<?php echo $row->id?>">
                              <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                              </a>
                            <?php }?>
                          </td>
                          <td> 
                            <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="removeEpisodeRow(this)" data-epid="<?php echo $row->id?>"><i class="fa fa-window-close"></i></a> 

                            <?php echo form_input([
                              'type'=>'hidden',
                              'name'=>'exist_ep_id[]',
                              'value'=>@$row->id
                            ])?> 

                            
                          </td>
                        </tr>

                      <?php }?>
                    </tbody>
                    
                  </table>


                </div>
              </div> 

              <div class="row"> 
                <div class="col-lg-12">
                  <?php echo form_button([
                    'type'=>'submit',
                    'class'=>'btn btn-success float-right',
                    'content'=>__('Submit','default')
                  ])?>
                </div>
              </div>


              <?php echo form_close()?>

            </div>
          </div> 


<!-- Modal -->
<div class="modal fade" id="setEpisodeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-1000" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Season episode management')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',['name'=>'create-episode-form'])?> 
      <?php echo form_input([
        'type'=>'hidden',
        'name'=>'hide_season_id'
      ])?>
      <div class="modal-body">
        <div class="row mb-3">
          <div class="col-lg-12">
            <a href="javascript:void(0)" class="btn btn-success float-right" onclick="clickAddEpisode(this)"><i class="fa fa-plus"></i> <?php echo __('Add episode')?></a>
          </div>
        </div>
        <table class="table" id="tableListAddEpisode">
          <thead>
            <tr>
              <th></th>
              <th><?php echo __('Name')?></th>
              <th><?php echo __('Description')?></th>
              <th><?php echo __('Video URL')?></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td></td>
              <td><?php echo form_input([
                'name'=>'name[]',
                'class'=>'form-control'
                ])?></td>
              <td>
                <?php echo form_input([
                  'name'=>'description[]',
                  'class'=>'form-control'
                ])?>
              </td>
              <td>
                <?php echo form_input([
                  'name'=>'video_url[]',
                  'class'=>'form-control'
                ])?>
                <?php echo form_input([
                  'type'=>'hidden',
                  'name'=>'hide_episode_id[]'
                ])?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close','default')?></button>
        <button type="button" class="btn btn-primary" onclick="saveEpisodeRow()"><?php echo __('Save','default')?></button>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>
