<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('VOD EPISODE LIST')?></li>
  </ol>
</nav> 

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>


          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('VOD EPISODE LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createVodEpisode')?>"><i class="fa fa-plus"></i> <?php echo __('Add vod episode')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Vod')?></th>
                      <th><?php echo __('Vod Season')?></th>
                      <th><?php echo __('Episode Name')?></th> 
                      <th><?php echo __('Video Url')?></th>
                      <th><?php echo __('Created')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($episode as $key => $row){ 
                      $vod_season = new M_vod_season($row->vod_season_id); 
                      $vod = new M_vod($vod_season->vod_id);
                      // echo $vod_season->vod_id.'<br>';
                    ?>
                      <tr>
                        <td></td>
                        <td><?php echo $vod->name?></td>
                        <td><?php echo $vod_season->name?></td>
                        <td><?php echo $row->name?></td>
                        <td><?php echo $row->video_url?></td>
                        <td><?php echo $row->created?></td>
                        <td><?php echo $row->updated?></td>
                        <td>
                          <?php if($row->active){?> 
                            <a href="javascript:void(0);"> 
                              <span class="badge badge-success"><?php echo __('Active','default')?></span>
                            </a>
                          <?php }else{?>
                            <a href="javascript:void(0);"> 
                              <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                            </a>
                          <?php }?>
                        </td>
                        <td>
                          <a href="<?php echo base_url($this->controller.'/editVodEpisode/'.$row->id)?>"  class="btn btn-secondary btn-sm">
                            <i class="fa fa-pen"></i>
                          </a> 

                          <a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deleteVodEpisode/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>
                        </td>
                      </tr>

                    <?php }?>
                    
                  </tbody>
                </table>
              </div>
            </div>
        </div>