<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/categories')?>"><?php echo __('VOD CATEGORIES')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('MANAGE VOD ORDINAL')?> <strong>(<?php echo $category_data->name;?>)</strong></li>
  </ol>
</nav>

<?php echo form_input([
  'name'=>'hide_category_id',
  'type'=>'hidden',
  'value'=>$category_id
])?>


          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('MANAGE VOD ORDINAL')?>  <strong>(<?php echo $category_data->name;?>)</strong>
              
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Vod Cover')?></th>
                      <th><?php echo __('Vod Name')?></th>                      
                      <th><?php echo __('Owner')?></th>
                      <th><?php echo __('Total Season')?></th>
                      <th><?php echo __('Ordinal By Category')?></th>
                      <?php if($category_id == '7'){?>
                      <th><?php echo __('Ordinal By Trending')?></th>
                      <?php }?>
                      <th><?php echo __('Status','default')?></th>
                      <th><?php echo __('Updated')?></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($vod as $key => $row){?>
                      <tr>
                        <td></td>
                        <td>
                          <img src="<?php echo base_url('uploaded/vod/'.$row->id.'/'.$row->cover)?>" style="width: 50px;height: 50px;">
                        </td>
                        <td><a href="<?php echo base_url($this->controller.'/editVod/'.$row->id)?>"><?php echo $row->name?></a></td>
                        <td><?php echo $row->vod_owner->get()->company_name?></td>
                        <td><?php echo $row->vod_season->get()->result_count()?></td>
                        <td>
                          <?php echo form_input([
                            'type'=>'hidden',
                            'name'=>'ordinal_by_category[]',
                            'value'=>$row->ordinal_by_category,
                            'data-vodid'=>$row->id
                          ])?>
                          <span class="show_ordinal_by_category"><?php echo $row->ordinal_by_category?></span>
                        </td>
                        <?php if($category_id == '7'){?>
                        <td>
                          <span class="show_ordinal_by_category"><?php echo $row->ordinal_by_trending?></span>
                        </td>
                        <?php }?>
                        <td>
                          <?php if($row->active){?>
                              <span class="badge badge-success"><?php echo __('Active','default')?></span>
                            <?php }else{?>
                              <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                            <?php }?>  
                        </td>
                        <td><?php echo $row->updated?></td>
                      </tr>
                    <?php }?>
                    
                  </tbody>
                </table>
               </div>
             </div>
           </div>
