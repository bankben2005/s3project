<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('VOD LIST')?></li>
  </ol>
</nav>

          
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('VOD LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createVod')?>"><i class="fa fa-plus"></i> <?php echo __('Add vod')?></a>
          </h6> 


            </div> 
            <div class="card-body"> 

              <!-- start of search box -->  
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                 <a href="javascript:void(0)" data-toggle="collapse" data-target="#searchBoxCollapse"> <h6><i class="fa fa-search"></i> <?php echo __('Search')?></h6> </a>
                </div>
                <div class="card-body collapse show" id="searchBoxCollapse">  
                  <?php echo form_open('',['name'=>'search-vod-form','onsubmit'=>'onSubmitSearchForm(event)'])?>
                    <div class="row">
                      <div class="col-lg-4">
                        <div class="form-group">
                          <?php echo form_dropdown('search_owner',@$select_owner,@$criteria['search_owner'],'class="form-control"')?>
                        </div>

                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <?php echo form_dropdown('search_category',@$select_category,@$criteria['search_category'],'class="form-control"')?>
                        </div>

                      </div>
                      <div class="col-lg-4">
                        <div class="form-group">
                          <?php echo form_input([
                            'name'=>'search_name',
                            'class'=>'form-control',
                            'value'=>@$criteria['search_name'],
                            'placeholder'=>__('Vod name...')
                          ])?>
                        </div>

                      </div>
                    </div>

                    <div class="row">
                      <div class="col-lg-12">
                        <div class="form-group">
                          <?php echo form_button([
                            'type'=>'submit',
                            'class'=>"btn btn-success float-right",
                            'content'=>'<i class="fa fa-search"></i> '.__('Search','default')
                          ])?> 

                          <?php if(isset($criteria)){?>

                          <a href="<?php echo base_url($this->controller)?>" class="btn btn-danger float-right mr-2"><i class="fa fa-close-o"></i> <?php echo __('Reset')?></a>
                        <?php }?>
                        </div> 


                      </div>
                    </div>
                    <?php echo form_close()?>
                    
                </div>
              </div>
              <!-- eof of search box --> 




              <div class="row">
                <div class="col-lg-12">
                  <div class="alert alert-success">
                    <?php echo __('Result')?> <?php echo @$result_count?> <?php echo __('Record(s)')?>
                  </div>
                </div>
              </div>
              
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Vod Cover')?></th>
                      <th><?php echo __('Vod Name')?></th>
                      <th><?php echo __('Category')?></th>                      
                      <th><?php echo __('Owner')?></th>
                      <th><?php echo __('Trending Status')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th><?php echo __('Views','default')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                  	<?php foreach($vod as $key => $row){?>
                  		<tr>
                  			<td></td>
                  			<td>
                  				<img src="<?php echo base_url('uploaded/vod/'.$row->id.'/'.$row->cover)?>" style="width: 50px;height: 50px;">
                  			</td>
                  			<td>
                          <a href="<?php echo base_url($this->controller.'/editVod/'.$row->id)?>">
                          <?php echo $row->name?>
                          </a>
                            
                        </td>
                        <td><?php echo $row->vod_categories->select('vod_categories.name as category_name')->get()->category_name?></td>
                  			<td><?php echo $row->vod_owner->get()->company_name?></td>

                        <td>

                          <div class="material-switch">
                          
                            <?php echo form_checkbox([
                              'name'=>'check_trending_status[]',
                              'value'=>$row->id,
                              'class'=>'switch',
                              'id'=>'status_'.$row->id,
                              'onclick'=>'clickCheckTrendingStatus(this)',
                              'checked'=>($row->trending_status)?TRUE:FALSE
                            ])?> 
                          <label for="status_<?php echo $row->id?>" class="badge-success"></label>
                          
                          </div>

                          <!-- <div class="material-switch pull-right">
                            <input id="someSwitchOptionSuccess" name="someSwitchOption001" type="checkbox"/>
                            <label for="someSwitchOptionSuccess" class="label-success"></label>
                          </div> -->
                          
                        </td>
                  			<td>
                            <?php if($row->active){?>
                              <span class="badge badge-success"><?php echo __('Active','default')?></span>
                            <?php }else{?>
                              <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                            <?php }?>   
                        </td>
                        <td>
                          <?php echo $row->views?>
                        </td>
                  			<td><?php echo $row->updated?></td>
                  			<td> 
                          <?php if($row->vod_season->get()->result_count() > 0){?>
                          <!-- <a href="<?php echo base_url($this->controller.'/setVodSeasonOrdinal/'.$row->id)?>" class="btn btn-primary btn-sm"><i class="fa fa-cog"></i> <?php echo __('Season ordinal')?></a> -->
                          <?php }?>
                  				<a href="<?php echo base_url($this->controller.'/editVod/'.$row->id)?>"  class="btn btn-secondary btn-sm">
                  					<i class="fa fa-pen"></i>
                  				</a> 

                  				<a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="if(confirm('Are you sure to delete?') == true){window.location.href='<?php echo base_url($this->controller.'/deleteVod/'.$row->id)?>'}"><i class="fa fa-trash"></i></a>
                  			</td>
                  		</tr>
                  	<?php }?>
                  	
                  </tbody>
                </table>
                


               </div> 

               <!-- for pagination -->
              <div class="row">
                <div class="col-lg-12">
                  <?php echo $pages?>
                </div>
              </div>
              <!-- eof for pagination -->
             </div>
           </div>
