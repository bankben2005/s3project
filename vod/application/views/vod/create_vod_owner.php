<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/owner')?>"><?php echo __('VOD OWNER LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$owner->id)?__('Edit vod owner'):__('Create vod owner')?></li>
  </ol>
</nav>

<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>
<div class="clearfix"></div>



          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$owner->id)?__('Edit vod owner form'):__('Create vod owner form')?> 

          		</h6> 


            </div>
            <div class="card-body"> 

              <?php 
                $image_path = base_url('uploaded/vod_owner/'.@$owner->id.'/'.@$owner->company_cover); 

                $cover_obj = new StdClass();
                $cover_obj->caption = $owner->company_cover;
                $cover_obj->size = $owner->company_cover_size;
                $cover_obj->width = '120px';
                $cover_obj->url = base_url($this->controller.'/deleteOwnerImage');
                $cover_obj->key = $owner->id; 
              ?> 

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_owner_id',
                'value'=>@$owner->id
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover',
                'value'=>(@$owner->company_cover)?json_encode([$image_path]):'[]'
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover_caption_obj',
                'value'=> json_encode([$cover_obj])
              ])?>  

              
            	<?php echo form_open_multipart('',['name'=>'create-vod-owner-form'])?> 
                <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <label><strong><?php echo __('Company Name')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'company_name',
                        'class'=>'form-control',
                        'value'=>@$owner->company_name
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Company Address')?> : </strong></label> 
                      <?php echo form_textarea([
                        'name'=>'company_address',
                        'class'=>'form-control',
                        'value'=>@$owner->company_address,
                        'rows'=>3
                      ])?>
                    </div> 

                    <div class="row">
                      <div class="col-lg-6">
                          <div class="form-group">
                            <label><strong><?php echo __('Contact Firstname')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'contact_firstname',
                              'class'=>'form-control',
                              'value'=>@$owner->contact_firstname
                            ])?>
                          </div>
                      </div>
                      <div class="col-lg-6">
                          <div class="form-group">
                            <label><strong><?php echo __('Contact Lastname')?> : </strong></label>
                            <?php echo form_input([
                              'name'=>'contact_lastname',
                              'class'=>'form-control',
                              'value'=>@$owner->contact_lastname
                            ])?>
                          </div>
                      </div>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Contact Telephone')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'contact_telephone',
                        'class'=>'form-control',
                        'value'=>@$owner->contact_telephone
                      ])?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Contact Fax')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'contact_fax',
                        'class'=>'form-control',
                        'value'=>@$owner->contact_fax
                      ])?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Status')?></strong></label> 
                      <?php echo form_dropdown('active',[
                        '1'=>__('Active','default'),
                        '0'=>__('Unactive','default')
                      ],@$owner->active,'class="form-control"')?>
                    </div>

                  </div>
                  <div class="col-lg-6"> 
                    <div class="form-group">
                      <label><strong><?php echo __('Cover')?> : </strong></label>
                      <div class="upload-box mb-3">
                              <input type="file" class="cover" name="cover" class="file-loading" />
                          </div>
                    </div>

                  </div>
                </div>

                <div class="row">
                  <div class="col-lg-12">
                    <?php echo form_button([
                      'type'=>'submit',
                      'class'=>'btn btn-success float-right',
                      'content'=>__('Submit','default')
                    ])?>
                  </div>
                </div>

              <?php echo form_close()?>
            </div>
          </div>
