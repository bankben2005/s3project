<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('VOD OWNER LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('VOD OWNER LIST TABLE')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createOwner')?>"><i class="fa fa-plus"></i> <?php echo __('Add owner')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Company Cover')?></th>
                      <th><?php echo __('Company Name')?></th>
                      <th><?php echo __('Contact')?></th>                      
                      <th><?php echo __('Contact Telephone')?></th>
                      <th><?php echo __('Created')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                  	<?php foreach($owner as $key => $row){?> 
                  		<tr>
                  			<td></td>
                  			<td><img src="<?php echo base_url('uploaded/vod_owner/'.$row->id.'/'.$row->company_cover)?>" style="width: 50px;height: 50px;"></td> 
                  			<td><?php echo $row->company_name?></td> 
                  			<td><?php echo $row->contact_firstname.' '.$row->contact_lastname?></td> 
                  			<td><?php echo $row->contact_telephone?></td> 
                  			<td><?php echo $row->created?></td>
                  			<td><?php echo $row->updated?></td> 
                  			<td>
                  				<a href="<?php echo base_url($this->controller.'/editOwner/'.$row->id)?>"  class="btn btn-secondary btn-sm">
                  					<i class="fa fa-pen"></i>
                  				</a> 

                  				<!-- <a href="<?php echo base_url($this->controller.'/deleteOwner/'.$row->id)?>" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a> -->
                  			</td>
                  		</tr>
                  	<?php }?>
                  </tbody>
                </table>
               </div>
            </div>
</div>