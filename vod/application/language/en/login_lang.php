<?php

$lang['Remember Me'] = "Remember Me";
$lang['Login'] = "Login";
$lang['Login with Google'] = "Login with Google";
$lang['Login with Facebook'] = "Login with Facebook";
$lang['Forgot Password?'] = "Forgot Password?";
$lang['Create an Account!'] = "Create an Account!";
