<?php

$lang['VOD SEASON LIST'] = "VOD SEASON LIST";
$lang['Create vod season'] = "Create vod season";
$lang['Create vod season form'] = "Create vod season form";
$lang['USER LIST'] = "USER LIST";
$lang['Create user'] = "Create user";
$lang['Create user form'] = "Create user form";
$lang['Access Type'] = "Access Type";
$lang['Firstname'] = "Firstname";
$lang['Lastname'] = "Lastname";
$lang['Telephone'] = "Telephone";
$lang['Edit user'] = "Edit user";
$lang['Edit user form'] = "Edit user form";
$lang['-- Select accesstype --'] = "-- Select accesstype --";
$lang['Reset password'] = "Reset password";
$lang['New Password'] = "New Password";
$lang['Confirm new password'] = "Confirm new password";
$lang['Create user success'] = "Create user success";
$lang['Email'] = "Email";
