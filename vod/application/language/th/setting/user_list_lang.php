<?php

$lang['USER LIST'] = "USER LIST";
$lang['USER LIST TABLE'] = "USER LIST TABLE";
$lang['Add user'] = "Add user";
$lang['Firstname&amp;Lastname'] = "Firstname&amp;Lastname";
$lang['Email'] = "Email";
$lang['Telephone'] = "Telephone";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['UAC'] = "UAC";
$lang['Delete user success'] = "Delete user success";
