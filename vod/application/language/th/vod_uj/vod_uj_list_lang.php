<?php

$lang['VOD UJ LIST'] = "VOD UJ LIST";
$lang['VOD UJ LIST TABLE'] = "VOD UJ LIST TABLE";
$lang['Add vod season'] = "Add vod season";
$lang['Contact Name'] = "Contact Name";
$lang['Email'] = "Email";
$lang['Gender'] = "Gender";
$lang['Age'] = "Age";
$lang['Picture Profile'] = "Picture Profile";
$lang['Device Type'] = "Device Type";
$lang['Created'] = "Created";
$lang['Unread'] = "Unread";
$lang['Pending'] = "Pending";
$lang['Reply Message'] = "Reply Message";
$lang['Request Message'] = "Request Message";
$lang['Read'] = "Read";
$lang['Replied'] = "Replied";
$lang['Add Member Playlist'] = "Add Member Playlist";
$lang['User Playlist'] = "User Playlist";
$lang['Choose Video'] = "Choose Video";
$lang['Update Member Playlist'] = "Update Member Playlist";
$lang['Select Category'] = "Select Category";
$lang['Delete vod uj succcess'] = "Delete vod uj succcess";
