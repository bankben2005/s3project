<?php

$lang['VOD LIST'] = "VOD LIST";
$lang['Create vod'] = "Create vod";
$lang['Create vod form'] = "Create vod form";
$lang['Email'] = "Email";
$lang['Edit Profile'] = "Edit Profile";
$lang['Edit profile form'] = "Edit profile form";
$lang['Firstname'] = "Firstname";
$lang['Lastname'] = "Lastname";
$lang['Telephone'] = "Telephone";
$lang['Edit profile success'] = "Edit profile success";
