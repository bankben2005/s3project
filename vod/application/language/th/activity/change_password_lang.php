<?php

$lang['VOD LIST'] = "VOD LIST";
$lang['Change Password'] = "Change Password";
$lang['Change password form'] = "Change password form";
$lang['Old Password'] = "Old Password";
$lang['New Password'] = "New Password";
$lang['Confirm new password'] = "Confirm new password";
$lang['Change password success'] = "Change password success";
