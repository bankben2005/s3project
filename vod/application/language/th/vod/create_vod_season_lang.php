<?php

$lang['VOD LIST'] = "VOD LIST";
$lang['Create vod season'] = "Create vod season";
$lang['Create vod season form'] = "Create vod season form";
$lang['VOD SEASON LIST'] = "VOD SEASON LIST";
$lang['Name'] = "Name";
$lang['VOD'] = "VOD";
$lang['Select Vod'] = "Select Vod";
$lang['Description'] = "Description";
$lang['Total episode'] = "Total episode";
$lang['Add more Episode'] = "Add more Episode";
$lang['Season episode management'] = "Season episode management";
$lang['Add episode'] = "Add episode";
$lang['Video URL'] = "Video URL";
$lang['Episode Name'] = "Episode Name";
$lang['Episode Description'] = "Episode Description";
$lang['Edit vod season'] = "Edit vod season";
$lang['Edit vod season form'] = "Edit vod season form";
$lang['Status'] = "Status";
$lang['Edit vod season success'] = "Edit vod season success";
$lang['Ordinal'] = "Ordinal";
