<?php

$lang['VOD EPISODE LIST'] = "VOD EPISODE LIST";
$lang['Edit vod episode'] = "Edit vod episode";
$lang['Edit vod episode form'] = "Edit vod episode form";
$lang['Create vod episode'] = "Create vod episode";
$lang['Create vod episode form'] = "Create vod episode form";
$lang['VOD'] = "VOD";
$lang['Vod'] = "Vod";
$lang['Vod Season'] = "Vod Season";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['-- Select vod season --'] = "-- Select vod season --";
$lang['Create vod episode success'] = "Create vod episode success";
$lang['Video Url'] = "Video Url";
