<?php

$lang['VOD EPISODE LIST'] = "VOD EPISODE LIST";
$lang['Create vod category'] = "Create vod category";
$lang['Create vod category form'] = "Create vod category form";
$lang['VOD CATEGORIES LIST'] = "VOD CATEGORIES LIST";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['Status'] = "Status";
$lang['Create vod category success'] = "Create vod category success";
$lang['Edit vod category'] = "Edit vod category";
$lang['Edit vod category form'] = "Edit vod category form";
$lang['Edit vod category success'] = "Edit vod category success";
$lang['Cover'] = "Cover";
