<?php

$lang['VOD EPISODE LIST'] = "VOD EPISODE LIST";
$lang['VOD EPISODE LIST TABLE'] = "VOD EPISODE LIST TABLE";
$lang['Add vod episode'] = "Add vod episode";
$lang['Owner'] = "Owner";
$lang['Episode Name'] = "Episode Name";
$lang['Episode Description'] = "Episode Description";
$lang['Video Url'] = "Video Url";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Vod'] = "Vod";
$lang['Vod Season'] = "Vod Season";
$lang['Delete episode success'] = "Delete episode success";
