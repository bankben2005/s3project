<?php

$lang['VOD OWNER LIST'] = "VOD OWNER LIST";
$lang['VOD LIST TABLE'] = "VOD LIST TABLE";
$lang['Add vod'] = "Add vod";
$lang['Company Name'] = "Company Name";
$lang['Contact'] = "Contact";
$lang['Contact Telephone'] = "Contact Telephone";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['VOD OWNER LIST TABLE'] = "VOD OWNER LIST TABLE";
$lang['Add owner'] = "Add owner";
$lang['Company Cover'] = "Company Cover";
