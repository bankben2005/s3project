<?php

$lang['VOD OWNER LIST'] = "VOD OWNER LIST";
$lang['Create vod owner'] = "Create vod owner";
$lang['Create vod owner form'] = "Create vod owner form";
$lang['Company Name'] = "Company Name";
$lang['Company Address'] = "Company Address";
$lang['Contact Firstname'] = "Contact Firstname";
$lang['Contact Lastname'] = "Contact Lastname";
$lang['Contact Telephone'] = "Contact Telephone";
$lang['Contact Fax'] = "Contact Fax";
$lang['Status'] = "Status";
$lang['Cover'] = "Cover";
$lang['Create vod owner success'] = "Create vod owner success";
$lang['Edit vod owner'] = "Edit vod owner";
$lang['Edit vod owner form'] = "Edit vod owner form";
$lang['Edit vod owner success'] = "Edit vod owner success";
