<?php

$lang['VOD SEASON LIST'] = "VOD SEASON LIST";
$lang['VOD SEASON LIST TABLE'] = "VOD SEASON LIST TABLE";
$lang['Add vod season'] = "Add vod season";
$lang['Vod Name'] = "Vod Name";
$lang['Season Name'] = "Season Name";
$lang['Season Description'] = "Season Description";
$lang['Amount Episode'] = "Amount Episode";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Episode'] = "Episode";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['Video URL'] = "Video URL";
$lang['Add episode'] = "Add episode";
$lang['Owner'] = "Owner";
$lang['Season episode management'] = "Season episode management";
$lang['Delete vod season success'] = "Delete vod season success";
