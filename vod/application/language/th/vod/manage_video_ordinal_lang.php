<?php

$lang['MANAGE VOD ORDINAL'] = "MANAGE VOD ORDINAL";
$lang['Vod Cover'] = "Vod Cover";
$lang['Vod Name'] = "Vod Name";
$lang['Owner'] = "Owner";
$lang['Total Season'] = "Total Season";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Season ordinal'] = "Season ordinal";
$lang['Ordinal By Category'] = "Ordinal By Category";
$lang['Ordinal By Trending'] = "Ordinal By Trending";
$lang['VOD CATEGORIES'] = "VOD CATEGORIES";
