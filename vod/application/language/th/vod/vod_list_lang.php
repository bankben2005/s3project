<?php

$lang['VOD LIST'] = "VOD LIST";
$lang['VOD LIST TABLE'] = "VOD LIST TABLE";
$lang['Vod Cover'] = "Vod Cover";
$lang['Vod Name'] = "Vod Name";
$lang['Owner'] = "Owner";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Add vod'] = "Add vod";
$lang['Total Season'] = "Total Season";
$lang['Season ordinal'] = "Season ordinal";
$lang['Category'] = "Category";
$lang['Result'] = "Result";
$lang['Record(s)'] = "Record(s)";
$lang['Delete vod success'] = "Delete vod success";
$lang['Search'] = "Search";
$lang['Vod name...'] = "Vod name...";
$lang['Reset'] = "Reset";
$lang['Trending Status'] = "Trending Status";
