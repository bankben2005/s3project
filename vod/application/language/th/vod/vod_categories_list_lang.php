<?php

$lang['VOD CATEGORIES LIST'] = "VOD CATEGORIES LIST";
$lang['VOD CATEGORIES LIST TABLE'] = "VOD CATEGORIES LIST TABLE";
$lang['Add vod categories'] = "Add vod categories";
$lang['Name'] = "Name";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Vod Total'] = "Vod Total";
$lang['Vod Ordinal'] = "Vod Ordinal";
$lang['Ordinal'] = "Ordinal";
$lang['Cover'] = "Cover";
