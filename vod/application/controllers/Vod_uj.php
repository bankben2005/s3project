<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once APPPATH . 'libraries/Backend_controller.php';
class Vod_uj extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();

		$this->load->library([
			'pagination'
		]);
	}

	public function index($page = null){  
		// $this->loadDataTableCSS();
		$this->loadSweetAlert();
		$this->loadSortable();
		
		// $this->loadDataTableJS();


		$this->template->javascript->add(base_url('assets/js/vod_uj/vod_uj_list.js'));

		$vod_uj = new M_vod_uj();


		$clone = $vod_uj->get_clone();
        $vodUJCount = $clone->count();
        $vod_uj->order_by('id','desc');
        $vod_uj->order_by('active','desc');

        $vod_uj->limit($this->page_num,$page);
        $vod_uj->get();

        //echo $channels->check_last_query();

        $this->setData('vod_uj',$vod_uj);
        $this->config_page($vodUJCount,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url($this->controller.'/index/');
        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());
        $this->setData('result_count',$vodUJCount);

        $data = $this->getData();
        $data['select_category'] = $this->getSelectVodCategories();



		// $vod_uj->order_by('created','desc')
		// ->get();

		// $data = [
		// 	'vod_uj'=>$vod_uj,
		// 	'select_category'=>$this->getSelectVodCategories()
		// ];


		$this->template->content->view('vod_uj/vod_uj_list',$data);
		$this->template->publish();



	}

	public function deleteVodUJMessage($vod_uj_id){

		$vod_uj = new M_vod_uj($vod_uj_id);

		if($vod_uj->delete()){
			$this->msg->add(__('Delete vod uj succcess','vod_uj/vod_uj_list'),'succcess');
			redirect($this->controller);
		}
	}

	public function ajaxVodUJReplyMessage(){
		$post_data = $this->input->post();

		$vod_uj = new M_vod_uj($post_data['vod_uj_id']);


		$vod_uj_reply = new M_vod_uj_reply();
		$vod_uj_reply->vod_uj_id = $vod_uj->id;
		$vod_uj_reply->reply_message = $post_data['reply_message'];
		$vod_uj_reply->vod_administrator_id = $this->admin_data['id'];
		$vod_uj_reply->save();


		// update reply status into vod_uj
		$vod_uj->read_status = 1;
		$vod_uj->reply_status = 1;
		$vod_uj->save();

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);	
	}

	public function ajaxDeleteReplyMessage(){
		$post_data = $this->input->post();

		$vod_uj_reply = new M_vod_uj_reply($post_data['reply_message_id']);

		$vod_uj_reply->delete();

		echo json_encode([
			'status'=>true
		]);

	}

	public function ajaxGetMemberPlaylist(){
		$post_data = $this->input->post();


		$arrReturn = [];



		$query = $this->db->select('*')
		->from('vod_member_playlist')
		->where('members_id',$post_data['members_id'])
		->where('login_type',$post_data['login_type'])
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			$rowMemberPlayList = $query->row();

			if($rowMemberPlayList->playlist || $rowMemberPlayList != '[]'){
				$decode_playlist_id = json_decode($rowMemberPlayList->playlist);

				if(json_last_error() == JSON_ERROR_NONE){
					// print_r($decode_playlist_id);exit;
					// $queryVod = $this->db->select('*')
					// ->from('vod')
					// ->where_in('id',$decode_playlist_id)
					// ->where('active',1)
					// ->get();

					// if($queryVod->num_rows() > 0){

					// 	foreach ($queryVod->result() as $key => $value) {
					// 		# code...
					// 		array_push($arrReturn, $value);
					// 	}

					// }

					// print_r($decode_playlist_id);exit;
					foreach ($decode_playlist_id as $key => $value) {
						# code...
							$queryVod = $this->db->select('*')
							->from('vod')
							->where('id',$value)
							->where('active',1)
							->get();

							array_push($arrReturn, $queryVod->row());

					}

				}

			}


		}else{
			// member not add playlist yet
		}


		$data = [
			'HasVod'=>(count($arrReturn) > 0)?true:false,
			'VodList'=>$arrReturn
		];


		// get view for render initial
		$view = $this->load->view('vod_uj/ajaxRenderInitialMemberPlayList',$data,true);




		echo json_encode([
			'status'=>true,
			'view'=>$view,
			'HasVod'=>(count($arrReturn) > 0)?true:false,
			'VodList'=>$arrReturn,
			'post_data'=>$post_data
		]);
	}

	public function ajaxDeleteMemberPlaylistVod(){
		$post_data = $this->input->post();

		$queryVodMemberPlaylist = $this->db->select('*')
		->from('vod_member_playlist')
		->where('members_id',$post_data['members_id'])
		->where('login_type',$post_data['login_type'])
		->where('active',1)
		->get();

		if($queryVodMemberPlaylist->num_rows() > 0){
			$row = $queryVodMemberPlaylist->row();

			$playlist = json_decode($row->playlist);

			if(json_last_error() === JSON_ERROR_NONE){

				$delete_id = (int)$post_data['delete_id'];

				$search_key = array_search($delete_id, $playlist);

				if(is_numeric($search_key)){
					// print_r($search_key);exit;
					unset($playlist[$search_key]);

					// update playlist into table
					$this->db->update('vod_member_playlist',[
						'playlist'=>json_encode(array_values($playlist)),
						'updated'=>date('Y-m-d H:i:s')
					],[
						'members_id'=>$post_data['members_id'],
						'login_type'=>$post_data['login_type']
					]);

					// print_r($playlist);
				}

			}


		}

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	}

	private function getSelectVodCategories(){
		$arrReturn = []; 

		$arrReturn[''] = __('Select Category','vod_uj/vod_uj_list');

		$query = $this->db->select('*')
		->from('vod_categories')
		->where('active',1)
		->order_by('name','asc')
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $value) {
				# code...
				$arrReturn[$value->id] = $value->name;
			}
		}

		return $arrReturn;




	}

	public function ajaxGetVodListItemByCategory(){
		$post_data = $this->input->post(); 

		$arrNotIn = [];
		$arrNotIn = $this->getArrMemberPlayListByMemberAndLoginType([
			'members_id'=>$post_data['members_id'],
			'login_type'=>$post_data['login_type']
		]);

		// print_r($arrNotIn);exit;
		$vodList = [];


		if(count($arrNotIn) > 0){
			$query = $this->db->select('*')
			->from('vod')
			->where('vod_categories_id',$post_data['vod_categories_id'])
			->where('active',1)
			->where_not_in('id',$arrNotIn)
			->order_by('created','desc')
			->get();

		}else{
			$query = $this->db->select('*')
			->from('vod')
			->where('vod_categories_id',$post_data['vod_categories_id'])
			->where('active',1)
			->order_by('created','desc')
			->get();
		}

		

		// print_r($query->result());exit;

		if($query->num_rows() > 0){$vodList = $query->result();}

		$data = [
			'vod'=>$vodList
		];


		$view = $this->load->view('vod_uj/ajaxGetVodListItemByCategory',$data,true);



		echo json_encode([
			'view'=>$view,
			'status'=>true,
			'post_data'=>$post_data
		]);
	}


	private function getArrMemberPlayListByMemberAndLoginType($data = []){

		$arrReturn = [];
		$query = $this->db->select('*')
		->from('vod_member_playlist')
		->where('members_id',$data['members_id'])
		->where('login_type',$data['login_type'])
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			$row = $query->row();

			$arrReturn = json_decode($row->playlist);
		}else{
			// add new record to mebmer 
			$this->db->insert('vod_member_playlist',[
				'members_id'=>$data['members_id'],
				'login_type'=>$data['login_type'],
				'playlist'=>'[]',
				'created'=>date('Y-m-d H:i:s')
			]);
		}

		//print_r($arrReturn);exit;

		return $arrReturn;
	}

	public function ajaxAddVodToMemberPlaylist(){
		$post_data = $this->input->post(); 


		$playlist = $this->getArrMemberPlayListByMemberAndLoginType([
			'members_id'=>$post_data['members_id'],
			'login_type'=>$post_data['login_type']
		]);

		array_push($playlist, $post_data['vod_id']);

		// print_r($arrPlayList);
		$this->db->update('vod_member_playlist',[
			'playlist'=>json_encode(array_values($playlist)),
			'updated'=>date('Y-m-d H:i:s')
		],[
			'members_id'=>$post_data['members_id'],
			'login_type'=>$post_data['login_type']
		]);










		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	}

}