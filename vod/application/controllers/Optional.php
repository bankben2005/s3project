<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

header('Content-Type: application/json'); 

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

class Optional extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */ 

	function __construct(){
		parent::__construct();

		$this->load->config('partners');
	}
	public function index(){



		//print_r('aaaa');exit;
		//$this->load->view('welcome_message');
	}

	public function checkS3RemoteUser(){
		$json_return = [];
		if($this->input->post('request') || file_get_contents('php://input')){

				$request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->input->post('request');

				$requestCriteria = json_decode($request);

				if(!property_exists($requestCriteria, 'login_type') || $requestCriteria->login_type == ''){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found login_type'
					];
				}

				if(!in_array($requestCriteria->login_type, ['line','facebook'])){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : Login type must be line or facebook '
					];	
				}


				if(!property_exists($requestCriteria, 'login_id') || $requestCriteria->login_id == ''){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found login_id'
					];
				}

				$checkAuthorization  = $this->checkAuthorization($requestCriteria);

				if(!$checkAuthorization['status']){
					$json_return = [
						'status'=>false,
						'result_code'=>$checkAuthorization['result_code'],
						'result_desc'=>$checkAuthorization['result_desc']
					];	

					echo json_encode($json_return);exit;
				}
				


				$partner = $checkAuthorization['partner'];

				//print_r($partner);exit;


				$memberData = $this->checkMemberByLoginTypeAndLoginId([
					'login_type'=>$requestCriteria->login_type,
					'login_id'=>$requestCriteria->login_id
				]);


				// print_r($memberData);exit;

				if(!$memberData['status']){
					$json_return = [
						'status'=>false,
						'result_code'=>$memberData['result_code'],
						'result_desc'=>$memberData['result_desc']
					];

					echo json_encode($json_return);exit;
				}

				// if found member check this member already has vvod_users and partner authorization 

				$checkVVODUsers = $this->checkCreateVVODUsersAndPartnerAuthorization([
					'requestCriteria'=>$requestCriteria,
					'memberData'=>$memberData
				]);

				
				// check last step 
				$checkUserPartnerAuthorization = $this->checkUserPartnerAuthorization([
					'vvod_users_id'=>$checkVVODUsers['vvod_users_id'],
					'partner_slug'=>$partner
				]);

				if(!$checkUserPartnerAuthorization['status']){
					$json_return = [
						'status'=>false,
						'result_code'=>'-009',
						'result_desc'=>'Authorization Exception : User do not verify with S3 Hybrid ,Please verify with S3 Hybrid'
					];

					echo json_encode($json_return);exit;
				}

				$json_return = [
					'status'=>true,
					'user_ref_id'=>$checkVVODUsers['vvod_users_id'],
					// 'mvid'=>$checkUserPartnerAuthorization['authorization_data']->vvod_user_ref_id,
					'UserData'=>$memberData['MemberData'],
					'result_code'=>'000',
					'result_desc'=>'Success'
				];

				if($partner == 'mv'){
					$json_return['mvid'] = $checkUserPartnerAuthorization['authorization_data']->vvod_user_ref_id;
				}



			echo json_encode($json_return);



		}else{
			echo json_encode([
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found request'
			]);
		}
	}

	public function SetVodPackage(){
		$json_return = [];
		if($this->input->post('request') || file_get_contents('php://input')){

				$request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->input->post('request');

				$requestCriteria = json_decode($request);

				if(!property_exists($requestCriteria, 'ref_id') || $requestCriteria->ref_id == ''){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found ref_id'
					]);exit;
				}

				if(!property_exists($requestCriteria, 'package_type') || $requestCriteria->package_type == ''){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found package_type'
					]);exit;
				}

				// check package type ios or android only
				if(!in_array($requestCriteria->package_type, ['ios','android'])){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : package type must be ios or android'
					]);exit;
				}

				if(!property_exists($requestCriteria, 'package_code') || $requestCriteria->package_code == ''){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found package_code'
					]);exit;
				}

				if(!property_exists($requestCriteria, 'package_name') || $requestCriteria->package_name == ''){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found package_name'
					]);exit;
				}

				if(!property_exists($requestCriteria, 'package_price') || $requestCriteria->package_price == ''){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found package_price'
					]);exit;
				}

				if(!is_numeric($requestCriteria->package_price)){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : package_price must be numeric only'
					]);exit;
				}

				if(!property_exists($requestCriteria, 'package_days') || $requestCriteria->package_days == ''){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found package_days'
					]);exit;
				}

				if(!property_exists($requestCriteria, 'package_cover_image') || $requestCriteria->package_cover_image == ''){
					echo json_encode([
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found package_cover_image'
					]);exit;
				}

				$checkAuthorization  = $this->checkAuthorization($requestCriteria);

				if(!$checkAuthorization['status']){
					$json_return = [
						'status'=>false,
						'result_code'=>$checkAuthorization['result_code'],
						'result_desc'=>$checkAuthorization['result_desc']
					];	

					echo json_encode($json_return);exit;
				}
				


				$partner = $checkAuthorization['partner'];

				$partner_data = $this->getPartnerDataBySlug($partner);

				// print_r($partner);exit;


				// check ref id already exist by purchase_ref_id and package_type and package code
				$checkPackageAlreadyExist = $this->checkPackageAlreadyExist([
					'requestCriteria'=>$requestCriteria,
					'partner_data'=>$partner_data
				]);

				if($checkPackageAlreadyExist['status']){
					echo json_encode([
						'status'=>false,
						'result_code'=>$checkPackageAlreadyExist['result_code'],
						'result_desc'=>$checkPackageAlreadyExist['result_desc']
					]);exit;
				}


				$this->db->insert('vvod_partner_packages',[
					'vvod_partners_id'=>$partner_data->id,
					'purchase_ref_id'=>$requestCriteria->ref_id,
					'code'=>$requestCriteria->package_code,
					'name'=>$requestCriteria->package_name,
					'description'=>$requestCriteria->package_description,
					'price'=>$requestCriteria->package_price,
					'currency_symbol'=>'THB',
					'days'=>$requestCriteria->package_days,
					'cover'=>$requestCriteria->package_cover_image,
					'device_type'=>$requestCriteria->package_type,
					'created'=>date('Y-m-d H:i:s')
				]);

				$insert_id = $this->db->insert_id();

				$package_data = $this->getPartnerPackageDataById($insert_id);

				unset($package_data->package_purchase_url);

				echo json_encode([
					'status'=>true,
					'PackageData'=>$package_data,
					'result_code'=>'000',
					'result_desc'=>'Success'
				]);






		}

	}

	private function checkMemberByLoginTypeAndLoginId($data = []){

		$arrReturn = [];
		$rowMember = new StdClass();


		switch ($data['login_type']) {
			case 'line':
				# code...
				$query = $this->db->select('*')
				->from('members')
				->where('line_id',$data['login_id'])
				->get();

				if($query->num_rows() > 0){
					$row = $query->row();

					if(!$row->active){
						$arrReturn = [
							'status'=>false,
							'result_code'=>'-009',
							'result_desc'=>'Authorize exception : User has been blocked,Please contact to administrator.'

						];	

						return $arrReturn;
					}

					$rowMember = $row;
				}
			break;
			case 'facebook':
				$query = $this->db->select('*')
				->from('members')
				->where('facebook_id',$data['login_id'])
				->get();

				if($query->num_rows() > 0){
					$row = $query->row();


					if(!$row->active){
						$arrReturn = [
							'status'=>false,
							'result_code'=>'-009',
							'result_desc'=>'Authorize exception : User has been blocked,Please contact to administrator.'
						];

						return $arrReturn;
					}

					$rowMember = $row;
				}


			break;
			
			default:
				# code...
			break;
		}

		$arrReturn = [
			'status'=>true,
			'MemberData'=>$rowMember
		];

		return $arrReturn;

		


	}

	public function SetTransaction(){

		$json_return = [];
		if($this->input->post('request') || file_get_contents('php://input')){

				$request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->input->post('request');

				$requestCriteria = json_decode($request);

				
				if(!property_exists($requestCriteria, 'user_ref_id') || $requestCriteria->user_ref_id == ''){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : Not found user_ref_id'
					];

					echo json_encode($json_return);exit;
				}

				if(!property_exists($requestCriteria, 'package_ref_id') || $requestCriteria->package_ref_id == ''){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : Not found package_ref_id'
					];

					echo json_encode($json_return);exit;
				}

				$checkAuthorization = $this->checkAuthorization($requestCriteria);

				if(!$checkAuthorization['status']){
					$json_return = [
						'status'=>false,
						'result_code'=>$checkAuthorization['result_code'],
						'result_desc'=>$checkAuthorization['result_desc']
					];	

					echo json_encode($json_return);exit;
				}



				$member_data = $this->getVVODUsersRow($requestCriteria->user_ref_id); 

				if(!$member_data){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : Not found user_ref_id in system'
					];

					echo json_encode($json_return);exit;
				}





				$partner_data = $this->getPartnerDataByAuthorizationHeader();

				// check package ref_id exist 

				$checkPackageRefExist = $this->checkPackageRefExist([
					'partner_data'=>$partner_data,
					'requestCriteria'=>$requestCriteria
				]);

				if(!$checkPackageRefExist['status']){
					echo json_encode([
						'status'=>false,
						'result_code'=>$checkPackageRefExist['result_code'],
						'result_desc'=>$checkPackageRefExist['result_desc']
					]);
				}

				$partner_package = $checkPackageRefExist['package_data'];



				$transaction_number = $this->runTransactionNumber();


				$dataInsert = [
					'vvod_users_id'=>$requestCriteria->user_ref_id,
					'vvod_partner_packages_id'=>$partner_package->id,
					'transaction_number'=>$transaction_number,
					'amount'=>1,
					'sub_total'=>(float)$partner_package->price,
					'grand_total'=>(float)$partner_package->price,
					'payment_option'=>'undefined',
					'created'=>date('Y-m-d H:i:s')
				]; 

				$this->db->insert('vvod_transactions',$dataInsert); 

				

				$insert_id = $this->db->insert_id();

				// create transaction logs 
				$this->setVodTransactionLogs([
					'vvod_transactions_id'=>$insert_id,
					'type'=>'create-transaction',
					'log_message'=>json_encode([
						'grand_total'=>$partner_package->price,
						'created'=>date('Y-m-d H:i:s')
					])
				]); 

				$transaction_data = $this->getTransactionDataByTransactionId([
					'vvod_transactions_id'=>$insert_id,
					'vvod_users_id'=>$requestCriteria->user_ref_id
				]);


				// print_r($partner_data);exit;

				// return [
				// 	'status'=>true,
				// 	'TransactionData'=>$transaction_data
				// ];

				echo json_encode([
					'status'=>true,
					's3ref'=>$transaction_data->transaction_number,
					'TransactionData'=>$transaction_data
				]);





		}



	}

	private function checkCreateVVODUsersAndPartnerAuthorization($data = []){

		$requestCriteria = $data['requestCriteria'];
		$member_data = $data['memberData']['MemberData'];

		// print_r($member_data);exit;
		// check member already create vvod_users

		$queryCheck = $this->db->select('*')
		->from('vvod_users')
		->where('ref_id',@$member_data->id)
		->where('active',1)
		->get();

		if($queryCheck->num_rows() > 0){
			// check user account has been expired by partner authentication token

			return [
				'status'=>true,
				'vvod_users_id'=>$queryCheck->row()->id
			];	



		}else{
			// create new vvod_users 
			$this->db->insert('vvod_users',[
				'ref_id'=>$member_data->id,
				'user_type'=>'member',
				'firstname'=>$member_data->firstname,
				'lastname'=>$member_data->lastname,
				'email'=>$member_data->email,
				'gender'=>$member_data->gender,
				'language'=>$member_data->language,
				'age'=>$member_data->age,
				'dob'=>$member_data->dob,
				'phone'=>$member_data->phone,
				'picture_profile'=>$member_data->picture_profile,
				'line_id'=>$member_data->line_id,
				'facebook_id'=>$member_data->facebook_id,
				'uuid'=>$member_data->uuid,
				'os'=>$member_data->os,
				'os_version'=>$member_data->os_version,
				'device_type'=>$member_data->device_type,
				'device_token'=>$member_data->device_token,
				'created'=>date('Y-m-d H:i:s'),
				'active'=>1

			]);

			$insert_id = $this->db->insert_id();

			$storage_token = $this->generateStorageToken([
				'vvod_users_id'=>$insert_id
			]);


			// update and generate storage token for vvod_user
			$this->ci->db->insert('vvod_user_authentication_tokens',[
				'vvod_users_id'=>$insert_id,
				'storage_token'=>$storage_token,
				'user_agent'=>$this->getUserAgent(),
				'flag'=>1,
				'created'=>date('Y-m-d H:i:s')
			]);

			$user_data = $this->getVVODUsersRow($insert_id); 

			return [
				'status'=>true,
				'vvod_users_id'=>$insert_id,
				'set_storage_token_status'=>true,
				'storage_token'=>$storage_token,
				'member_data'=>$user_data
			];

		}

		// check Header which partner 

		// print_r(getallheaders());exit;

		
		



	}

	private function getPartnerDataBySlug($partner_slug){

		$query = $this->db->select('*')
		->from('vvod_partners')
		->where('slug',$partner_slug)
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			$rowPartner = $query->row();
			return $rowPartner;
		}else{
			return new StdClass();
		}


	}
	private function generateStorageToken($data = []){

		$this->load->library([
			'encryption'
		]);
		$this->encryption->initialize(
		        array(
		                'cipher' => 'aes-128',
		                'mode' => 'ctr',
		                'key' => 'XCOokblusWVKbu5e'
		        )
		);

		$myData = [
			'vvod_users_id'=>$data['vvod_users_id'],
			'slug'=>'storage-token'
		];

		$build_to_url = http_build_query($myData);

		// return $this->ci->encryption->encrypt($build_to_url); 

		return md5($data['vvod_users_id'].date('Y-m-d H:i:s'));

	}

	private function getUserAgent(){
		$this->ci->load->library([
			'user_agent'
		]); 

		return $this->ci->agent->platform();


	}

	private function getVVODUsersRow($vvod_users_id){
		$query = $this->db->select('*')
		->from('vvod_users')
		->where('id',$vvod_users_id)
		->get();

		if($query->num_rows() > 0){
			return $query->row();
		}else{
			return false;
		}
	}

	private function checkAuthorization($requestCriteria){

		$this->load->helper([
			'our'
		]);


		$headers = getallheaders();






		$authorization = @$headers['Authorization'];

		if($authorization){
			$ex_authorization = explode('Basic', $authorization);

			$encode_authrization = trim(@$ex_authorization[1]);

			$decode_authorization = base64_decode(base64_decode($encode_authrization));

			//echo $decode_authorization;exit;
			parse_str($decode_authorization,$str_parse);


			$partner = $str_parse['partner'];


			// check partner in partner list =

			$api_config = $this->config->item('api');

			if(!in_array($partner, $api_config['arr_partner'])){
				return [
					'status'=>false,
					'result_code'=>'-009',
					'result_desc'=>'Authentication exception : Do not authorize,Please contact administrator.'
				];
			}else{
				return [
					'status'=>true,
					'partner'=>$partner
				];
			}



		}else{
			return [
				'status'=>false,
				'result_code'=>'-009',
				'result_desc'=>'Authentication exception : Do not authorize,Please contact administrator.'
			];
		}

	}

	private function getPartnerDataByAuthorizationHeader(){
		$partner_data = new StdClass();

		$headers = getallheaders();

		$authorization = @$headers['Authorization'];

		if($authorization){
			$ex_authorization = explode('Basic', $authorization);

			$encode_authrization = trim(@$ex_authorization[1]);

			$decode_authorization = base64_decode(base64_decode($encode_authrization));

			//echo $decode_authorization;exit;
			parse_str($decode_authorization,$str_parse);


			$partner = $str_parse['partner'];


			$partner_data = $this->getPartnerDataBySlug($partner);
		}

		return $partner_data;

	}

	private function checkPackageRefExist($data = []){ 

		$partner_data = $data['partner_data'];
		$requestCriteria = $data['requestCriteria'];

		$queryCheck = $this->db->select('*')
		->from('vvod_partner_packages')
		->where('vvod_partners_id',$partner_data->id)
		->where('purchase_ref_id',$requestCriteria->package_ref_id)
		->where('active',1)
		->get();

		if($queryCheck->num_rows() > 0){
			$row = $queryCheck->row();

			return [
				'status'=>true,
				'package_data'=>$row
			];



		}else{
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : Not found package_ref_id'
			];
		}


	}

	
	private function runTransactionNumber(){ 

    	$queryIdentCurrent = $this->db->query("select IDENT_CURRENT('vvod_transactions') as identity_current"); 

    	$rowIdentCurrent = ($queryIdentCurrent->row()->identity_current + 1); 

    	$runTransactionNumber = 'S3V'.str_pad($rowIdentCurrent, 6, '0', STR_PAD_LEFT); 

    	return $runTransactionNumber;

    }

    private function setVodTransactionLogs($data = []){

    	$this->db->insert('vvod_transaction_logs',[
    		'vvod_transactions_id'=>$data['vvod_transactions_id'],
    		'type'=>$data['type'],
    		'log_message'=>$data['log_message'],
    		'created'=>date('Y-m-d H:i:s')
    	]);

    }

    private function getTransactionDataByTransactionId($data = []){ 

    	$vvod_users_id = $data['vvod_users_id'];


    	$queryTransaction = $this->db->select('*')
    	->from('vvod_transactions')
    	->where('id',$data['vvod_transactions_id'])
    	->get();

    	//print_r($data);exit;

    	$stdReturn = new StdClass();
    	if($queryTransaction->num_rows() > 0){
    		$rowTransaction = $queryTransaction->row();  

    		$stdReturn->transaction_id = $rowTransaction->id;
    		$stdReturn->transaction_number = $rowTransaction->transaction_number;
    		$stdReturn->sub_total = number_format($rowTransaction->sub_total,2);
    		$stdReturn->grand_total = number_format($rowTransaction->grand_total,2);
    		$stdReturn->payment_option = $rowTransaction->payment_option;
    		$stdReturn->payment_status = ($rowTransaction->payment_status)?'Paid':'Unpaid';
    		$stdReturn->created = $rowTransaction->created;
    		$stdReturn->updated = $rowTransaction->updated;
    		$stdReturn->transaction_package = $this->getPartnerPackageDataById($rowTransaction->vvod_partner_packages_id);

    	}
    	return $stdReturn;

    }
    private function getPartnerPackageDataById($vvod_partner_packages_id){
    	$query = $this->db->select('*')
    	->from('vvod_partner_packages')
    	->where('id',$vvod_partner_packages_id)
    	->where('active',1)
    	->get(); 

    	$stdReturn = new StdClass();

    	if($query->num_rows() > 0){ 


    		$row = $query->row(); 

    		//print_r($row);exit;

    		$partner_data = $this->getPartnerDataByPartnerId($row->vvod_partners_id);

    		$stdReturn->package_id = $row->id;
    		$stdReturn->package_purchase_ref_id = $row->purchase_ref_id;
    		$stdReturn->package_purchase_url = $partner_data->purchase_url;
    		$stdReturn->package_code = $row->code;
    		$stdReturn->package_name = $row->name;
    		$stdReturn->package_description = $row->description;
    		$stdReturn->package_price = number_format($row->price,2);
    		$stdReturn->package_currency_symbol = $row->currency_symbol;
    		$stdReturn->package_days = $row->days;
    		$stdReturn->package_cover = getPackageCoverImage($row);
    		$stdReturn->partner_id = $partner_data->id;
    		$stdReturn->partner_slug = $partner_data->slug;

    	}

    	return $stdReturn;
    }

    private function getPartnerDataByPartnerId($vvod_partners_id){
    	$query = $this->db->select('*')
    	->from('vvod_partners')
    	->where('id',$vvod_partners_id)
    	->where('active',1)
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row();
    	}else{
    		return new StdClass();
    	}
    }

    private function checkUserPartnerAuthorization($data = []){
    	$vvod_users_id = $data['vvod_users_id'];
    	$partner_slug = $data['partner_slug'];

    	$partnerData = $this->getPartnerDataBySlug($partner_slug);

    	$query = $this->db->select('*')
    	->from('vvod_user_partner_authorization')
    	->where('vvod_users_id',$vvod_users_id)
    	->where('vvod_partners_id',$partnerData->id)
    	->where('access_token_expire >=',date('Y-m-d H:i:s'))
    	->get();

    	if($query->num_rows() > 0){
    		return [
    			'status'=>true,
    			'authorization_data'=>$query->row()
    		];
    	}else{
    		return [
    			'status'=>false
    		];
    	}




    }

    private function checkPackageAlreadyExist($data = []){
    	$requestCriteria = $data['requestCriteria'];    	
    	$partner_data = $data['partner_data'];

    	//print_r($partner_data);exit;

    	$query = $this->db->select('*')
    	->from('vvod_partner_packages')
    	->where('vvod_partners_id',$partner_data->id)
    	->where('purchase_ref_id',$requestCriteria->ref_id)
    	->where('active',1)
    	->get();

    	if($query->num_rows() > 0){
    		return [
    			'status'=>true,
    			'result_code'=>'-003',
    			'result_desc'=>'Exist exception : package has been exist,Please check and send again'
    		];
    	}else{
    		return [
    			'status'=>false
    		];
    	}

    }

}