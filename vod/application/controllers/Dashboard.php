<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Dashboard extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){
    	//print_r($this->session->userdata('user_data'));
    	$data = array(

    	);
    	$this->template->content->view('dashboard/dashboard',$data);
    	$this->template->publish();
    }

    public function ajaxSetSidenavToggle(){
        $post_data = $this->input->post();

        if($post_data['toggled_status'] == 'toggled'){
            $this->session->set_userdata('toggle_status','sidenav-toggled');
        }else{
            $this->session->set_userdata('toggle_status','');
        }

        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data
        ));
    }

}