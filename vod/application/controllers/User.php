<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class User extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
	}

	public function index(){ 

		$vod_administrator = new M_vod_administrator();
		$vod_administrator->where('id !=',$this->admin_data['id'])
		->get();

		$data = [
			'administrator'=>$vod_administrator
		];

		$this->template->content->view('setting/user_list',$data);
		$this->template->publish();

	}

	public function createUser(){
		$this->__createUser();
	}

	public function editUser($id){ 
		$vod_administrator = new M_vod_administrator($id);

		if(!$vod_administrator->id){
			redirect($this->controller);
		}

		$this->__createUser($id);

	}

	public function deleteUser($id){ 
		$vod_administrator = new M_vod_administrator($id); 

		if(!$vod_administrator->id){
			redirect($this->controller);
		} 

		if($vod_administrator->delete()){
			$this->msg->add(__('Delete user success','setting/user_list'),'success');
			redirect($this->controller);
		}



	}

	public function userAccessMenu($user_id){  

		$vod_administrator = new M_vod_administrator($user_id);

		if(!$vod_administrator->id){
			redirect($this->controller);
		} 



		$data = [

		];

		$this->template->content->view('setting/user_access_menu',$data);
		$this->template->publish();



	}

	public function ajaxCheckEmailExist(){
		$get_data = $this->input->get(); 

		$vod_administrator = new M_vod_administrator();
		$vod_administrator->where('email',$get_data['email'])
		->where('active',1)
		->get();

		if($vod_administrator->id){
			echo json_encode([
				'valid'=>false
			]);

		}else{
			echo json_encode([
				'valid'=>true
			]);
		}


		
	}

	public function ajaxSetResetPassword(){
		$post_data = $this->input->post(); 

		$vod_administrator = new M_vod_administrator($post_data['hide_id']);

		if($vod_administrator->id){

			$vod_administrator->password = sha1($this->input->post('new_password'));
			$vod_administrator->save();

		}


		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	}
	private function __createUser($id = null){  

		$this->loadValidator();
		$this->loadSweetAlert();

		if($id){
			$this->template->javascript->add(base_url('assets/js/setting/edit_user.js'));
		}else{
			$this->template->javascript->add(base_url('assets/js/setting/create_user.js'));
		}

		$vod_administrator = new M_vod_administrator($id); 

		if($this->input->post(NULL,FALSE)){ 

			$vod_administrator->email = $this->input->post('email');
			$vod_administrator->firstname  = $this->input->post('firstname');
			$vod_administrator->lastname = $this->input->post('lastname');
			$vod_administrator->telephone = $this->input->post('telephone');
			$vod_administrator->type = $this->input->post('type');

			if(!$id){
				$vod_administrator->password = sha1($this->input->post('password'));
			}

			if($vod_administrator->save()){ 

				$txtSuccess = ($id)?__('Edit user success','setting/create_user'):__('Create user success','setting/create_user');
				$this->msg->add($txtSuccess,'success');
				redirect($this->uri->uri_string());

			}

		}


		$data = [
			'vod_administrator'=>$vod_administrator
		];


		$this->template->content->view('setting/create_user',$data);
		$this->template->publish();



	}

}