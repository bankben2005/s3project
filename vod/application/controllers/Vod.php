<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Vod extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
	}

	public function index($page = null){  

		$this->template->stylesheet->add(base_url('assets/css/switch/material_switch.css'));

		$this->template->javascript->add(base_url('assets/js/vod/vod_list.js'));

		$this->load->library([
			'pagination'
		]);

		$vod = new M_vod();  

		if(!empty($_GET)){
			$criteria = $this->getDecryptQueryString([
				'encrypt_txt'=>$_GET['q']
			]); 

			if(isset($criteria['search_owner']) && $criteria['search_owner'] != ''){
				$vod->where('vod_owner_id',$criteria['search_owner']);
			}

			if(isset($criteria['search_category']) && $criteria['search_category'] != ''){
				$vod->where('vod_categories_id',$criteria['search_category']);
			} 

			if(isset($criteria['search_name']) && $criteria['search_name'] != ''){
				$vod->like('name',$criteria['search_name'],'both');
			} 

			$this->setData('criteria',$criteria);

		}
		
		$clone = $vod->get_clone();
        $vodCount = $clone->count();
        $vod->order_by('id','desc');
        $vod->order_by('active','desc');

        $vod->limit($this->page_num,$page);
        $vod->get();

        //echo $channels->check_last_query();

        $this->setData('vod',$vod);
        $this->config_page($vodCount,$page);

        $config_page = [];
        $config_page = $this->getData('config_page');
        $config_page['base_url'] = base_url($this->controller.'/index/');
        $config_page['reuse_query_string'] = TRUE;        
        $config_page['attributes'] = array('class' => 'page-link');
        $this->pagination->initialize($config_page);
        $this->setData('pages',$this->pagination->create_links());
        $this->setData('result_count',$vodCount);

        $data = $this->getData();
        $data['select_category'] = $this->getSelectCategory();
        $data['select_owner'] = $this->getSelectOwner();

		$this->template->content->view('vod/vod_list',$data);
		$this->template->publish();

	} 

	public function season(){ 
		$this->loadSweetAlert();

		$this->template->javascript->add(base_url('assets/js/vod/vod_season_list.js')); 

		
		$vod_season = new M_vod_season();

		$data = [
			'vod_season'=>$vod_season->get()
		];

		$this->template->content->view('vod/vod_season_list',$data);
		$this->template->publish();

	} 

	public function categories(){ 

		$this->template->javascript->add('https://code.jquery.com/ui/1.12.0/jquery-ui.min.js'); 

		$this->template->javascript->add(base_url('assets/js/vod/vod_categories_list.js'));

		$categories = new M_vod_categories();
		$categories->order_by('ordinal','asc')->get();
			# code...

		$data = [
			'categories'=>$categories
		];


		$this->template->content->view('vod/vod_categories_list',$data);
		$this->template->publish();
	} 

	public function setVodSeasonOrdinal($vod_id){
		$check_vod = new M_vod($vod_id);

		if(!$check_vod->id){
			redirect($this->controller);
		} 

		$vod_season = new M_vod_season();
		$vod_season->where('vod_id',$vod_id)->get();


		$data = [

		];

		$this->template->content->view('vod/set_vod_season_ordinal',$data);
		$this->template->publish();

	}

	public function manageVideoOrdinal($category_id){ 

		$this->template->javascript->add('https://code.jquery.com/ui/1.12.0/jquery-ui.min.js');

		$this->template->javascript->add(base_url('assets/js/vod/manage_video_ordinal.js?'.time()));



		if($category_id == 7){
			$vod = new M_vod();
			$vod->where('vod_categories_id',$category_id)
			->where('active',1)
			->or_where('trending_status',1)
			->order_by('ordinal_by_trending','asc')
			->get();


		}else{ 
			$vod = new M_vod();
			$vod->where('vod_categories_id',$category_id)
			->where('active',1)
			->order_by('ordinal_by_category','asc')
			->get();

		}

		
		$category_data = $this->getCategoryDataById($category_id);
		

		$data = [
			'category_data'=>$category_data,
			'category_id'=>$category_id,
			'vod'=>$vod
		];

		$this->template->content->view('vod/manage_video_ordinal',$data);
		$this->template->publish();
	}

	public function createVodCategories(){
		$this->__createVodCategories();
	}

	public function editVodCategories($id){
		$categories = new M_vod_categories($id);

		if(!$id){
			redirect($this->controller.'/categories');
		}

		$this->__createVodCategories($id);
	}

	public function deleteVodCategories($id){
		$categories = new M_vod_categories($id);

		if(!$id){
			redirect($this->controller.'/categories');
		}
	}

	public function createVodSeason(){
		$this->__createVodSeason();
	}

	public function editVodSeason($id){
		$vod_season = new M_vod_season($id);

		if(!$vod_season->id){
			redirect($this->controller.'/season');

		}

		$this->__createVodSeason($id);

	}
	public function deleteVodSeason($id){
		$vod_season = new M_vod_season($id);

		if(!$vod_season->id){
			redirect($this->controller.'/season');

		}  

		// delete all vod episode that related vod season
		$this->db->delete('vod_episode',[
			'vod_season_id'=>$vod_season->id
		]); 

		if($vod_season->delete()){
			$this->msg->add(__('Delete vod season success','vod/vod_season_list'),'success');
			redirect($this->controller.'/season');
		}


	}

	public function deleteVODCoverImage(){
		$post_data = $this->input->post(); 


		$vod = new M_vod($post_data['key']);

		if($vod->id){
			$file_path = './uploaded/vod/'.$vod->id.'/'.$vod->cover;

			if(file_exists($file_path)){
				unlink($file_path);

				$vod->cover = '';
				$vod->cover_size ='';
				$vod->save();
			}
		}

		echo json_encode([
			'valid'=>true
		]);



	}

	public function deleteVODCategoryCoverImage(){
		$post_data = $this->input->post(); 


		$vod_categories = new M_vod_categories($post_data['key']);

		if($vod_categories->id){
			$file_path = './uploaded/vod_category/'.$vod_categories->id.'/'.$vod_categories->cover;

			if(file_exists($file_path)){
				unlink($file_path);

				$vod_categories->cover = '';
				$vod_categories->cover_size ='';
				$vod_categories->save();
			}
		}

		echo json_encode([
			'valid'=>true
		]);
	}

	public function deleteOwnerImage(){
		$post_data = $this->input->post(); 


		$vod_owner = new M_vod_owner($post_data['key']);

		if($vod_owner->id){
			$file_path = './uploaded/vod_owner/'.$vod_owner->id.'/'.$vod_owner->company_cover;

			if(file_exists($file_path)){
				unlink($file_path);

				$vod_owner->company_cover = '';
				$vod_owner->company_cover_size = '';
				$vod_owner->save();
			}
		}

		echo json_encode([
			'valid'=>true
		]);


	}

	public function ajaxSaveEpisode(){
		$post_data = $this->input->post(); 

		//print_r($post_data); 

		// clear all episode and insert new 
		$this->db->delete('vod_episode',[
			'vod_season_id'=>$post_data['season_id']
		]);

		foreach ($post_data['row_data'] as $key => $value) {
			# code... 

			$this->db->insert('vod_episode',[
				'vod_season_id'=>$post_data['season_id'],
				'name'=>$value['name'],
				'description'=>$value['description'],
				'video_url'=>$value['video_url'],
				'created'=>date('Y-m-d H:i:s'),
				'active'=>1
			]);	

		} 





		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	} 

	public function ajaxGetRenderEpisodeBySeasonId(){
		$post_data = $this->input->post(); 

		$arrReturn = [];

		$query = $this->db->select('*')
		->from('vod_episode')
		->where('vod_season_id',$post_data['season_id'])
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result_array() as $key => $value) {
				# code...
				array_push($arrReturn, $value);
			}
		}

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data,
			'return_data'=>$arrReturn
		]);


	} 

	public function ajaxDeleteSeasonById(){
		$post_data = $this->input->post(); 

		// print_r($post_data); 

		// delete all record in vod episode

		$vod_episode = new M_vod_episode();
		$vod_episode->where('vod_season_id',$post_data['vod_season_id'])
		->get(); 

		foreach ($vod_episode as $key => $value) {
			# code...
			$this->db->delete('vod_episode',[
				'id'=>$value->id
			]);
		} 


		// delete vod season
		$vod_season = new M_vod_season($post_data['vod_season_id']);

		if($vod_season->id){ 

			$this->db->delete('vod_season',[
				'id'=>$vod_season->id
			]);

		} 


		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	} 

	public function ajaxChangeEpisodeStatus(){
		$post_data = $this->input->post(); 

		$vod_episode = new M_vod_episode($post_data['vod_episode_id']);

		if($vod_episode->id){
			if($post_data['status'] == 'active'){
				$vod_episode->active = 0;
				$vod_episode->save();
			}else{
				$vod_episode->active = 1;
				$vod_episode->save();
			}
		}
		



		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	} 

	public function ajaxDeleteEpisodeRow(){
		$post_data = $this->input->post();  

		$vod_episode = new M_vod_episode($post_data['vod_episode_id']);

		$vod_episode->delete();


		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	} 

	public function ajaxGetSelectVodSeasonByVodId(){
		$post_data = $this->input->post();

		$vod_season = new M_vod_season();
		$vod_season->where('vod_id',$post_data['vod_id'])
		->where('active',1)
		->get();

		$arrReturn = []; 
		//$arrReturn[''] = __('-- Select vod season --','vod/create_vod_episode');

		foreach ($vod_season as $key => $value) {
			# code...
			array_push($arrReturn, [
				'id'=>$value->id,
				'name'=>$value->name
			]);
		}

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data,
			'select_vod_season'=>$arrReturn
		]);
	}

	public function ajaxUpdateOrdinalByCategory(){
		$post_data = $this->input->post(); 

		if(isset($post_data['arr_post']) && count($post_data['arr_post']) > 0){

			foreach ($post_data['arr_post'] as $key => $value) {
				# code...
				$this->db->update('vod',[
					'ordinal_by_category'=>$value['ordinal'],
					'ordinal_by_trending'=>($post_data['category_id'] == '7')?$value['ordinal']:0,
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$value['vod_id']]);
			}

		}




		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	} 

	public function ajaxUpdateCategoriesOrdinal(){
		$post_data = $this->input->post(); 

		if(isset($post_data['arr_post']) && count($post_data['arr_post']) > 0){

			foreach ($post_data['arr_post'] as $key => $value) {
				# code...
				$this->db->update('vod_categories',[
					'ordinal'=>$value['ordinal'],
					'updated'=>date('Y-m-d H:i:s')
				],['id'=>$value['category_id']]);
			}
		}

		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	} 

	public function ajaxGetQueryString(){ 

		


		$post_data = $this->input->post(); 

		unset($post_data['csrf_test_name']); 

		$http_build = http_build_query($post_data);

		//echo $http_build;
		$encryption = base64_encode($http_build);

		// echo $encryption;
		$redirect_url = base_url($this->controller.'/index?q='.$encryption);



		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data,
			'redirect_url'=>$redirect_url
		]);
	}

	public function ajaxSetVodTrendingStatus(){
		$post_data = $this->input->post(); 

		$vod = new M_vod($post_data['vod_id']);
		$vod->trending_status = ($post_data['checked_status'] == 'checked')?1:0;
		$vod->save();



		echo json_encode([
			'status'=>true,
			'post_data'=>$post_data
		]);
	}

	private function __createVodSeason($id = null){  

		$this->template->javascript->add('https://code.jquery.com/ui/1.12.0/jquery-ui.min.js');

		$this->loadValidator();



		if($id){
			$this->template->javascript->add(base_url('assets/js/vod/edit_vod_season.js'));
		}else{
			$this->template->javascript->add(base_url('assets/js/vod/create_vod_season.js'));
		}

		$vod_season = new M_vod_season($id);  


		if($this->input->post(NULL,FALSE)){ 
			$post_data = $this->input->post(); 

			// print_r($post_data);exit;

			$vod_season->vod_id = $this->input->post('vod_id');
			$vod_season->name = $this->input->post('name');
			$vod_season->description = $this->input->post('description');
			$vod_season->active = $this->input->post('active');

			if($vod_season->save()){

				if(!$id){ 

					if(isset($post_data['ep_name']) && count($post_data['ep_name']) > 0){
						foreach ($post_data['ep_name'] as $key => $value) {
							# code... 
							if($value){
								$insert_episode = new M_vod_episode();
								$insert_episode->vod_id = $vod_season->vod_id;
								$insert_episode->name = $value;
								$insert_episode->description = $post_data['ep_description'][$key];
								$insert_episode->video_url = $post_data['ep_video_url'][$key];
								$insert_episode->ordinal = $post_data['ordinal'][$key];
								$insert_episode->active = $post_data['ep_active'][$key]; 
								$insert_episode->save();
							}
						}
					}
				}else{ 

					// edit case 
					/* exist case */ 

					if(isset($post_data['exist_ep_id']) && count($post_data['exist_ep_id']) > 0){
						foreach ($post_data['exist_ep_id'] as $key => $value) {
							# code...
							$update_ep = new M_vod_episode($value);
							$update_ep->name = $post_data['exist_ep_name'][$key];
							$update_ep->description = $post_data['exist_ep_description'][$key];
							$update_ep->video_url = $post_data['exist_ep_video_url'][$key];
							$update_ep->ordinal = $post_data['ordinal'][$key];
							$update_ep->save();
						}
					}


					$count_already_exist = 0;
					$count_already_exist = count($post_data['exist_ep_id']);


					if(isset($post_data['ep_name']) && count($post_data['ep_name']) > 0){
						foreach ($post_data['ep_name'] as $key => $value) {
							# code... 
							if($value){
								$insert_episode = new M_vod_episode();
								$insert_episode->vod_season_id = $vod_season->id;
								$insert_episode->name = $value;
								$insert_episode->description = $post_data['ep_description'][$key];
								$insert_episode->video_url = $post_data['ep_video_url'][$key]; 
								$insert_episode->ordinal = $count_already_exist+1;
								$insert_episode->active = $post_data['ep_active'][$key];
								$insert_episode->save();  
							}
						}
					}


				} // eof else 


				$txtSuccess = ($id)?__('Edit vod season success','vod/create_vod_season'):__('Create vod season success','vod/vod_id'); 

				$this->msg->add($txtSuccess,'success');
				redirect($this->uri->uri_string());

			}

		}



		$data = [
			'vod_season'=>$vod_season,
			'select_vod'=>$this->getSelectVod(),
			'episode_row'=>$this->getEpisodeRowByVodSeason([
				'vod_season_id'=>@$vod_season->id
			])
		];


		$this->template->content->view('vod/create_vod_season',$data);
		$this->template->publish();




	}



	public function episode(){ 

		$episode = new M_vod_episode();
		$episode->get();

		$data = [
			'episode'=>$episode
		];

		$this->template->content->view('vod/vod_episode_list',$data);
		$this->template->publish();
	} 

	public function createVodEpisode(){
		$this->__createVodEpisode();
	}

	public function editVodEpisode($id){ 
		$episode = new M_vod_episode($id);

		if(!$episode->id){
			redirect($this->controller.'/episode');
		} 

		$this->__createVodEpisode($id);


	}
	public function deleteVodEpisode($id){
		$episode = new M_vod_episode($id);

		if(!$episode->id){
			redirect($this->controller.'/episode');
		} 


		if($episode->delete()){
			$this->msg->add(__('Delete episode success','vod/vod_episode_list'),'success');
			redirect($this->controller.'/episode');
		}
	} 

	private function __createVodEpisode($id = null){ 

		$this->loadValidator();

		if($id){
			$this->template->javascript->add(base_url('assets/js/vod/edit_vod_episode.js'));
		}else{
			$this->template->javascript->add(base_url('assets/js/vod/create_vod_episode.js'));
		}

		$vod_episode = new M_vod_episode($id);


		if($this->input->post(NULL,FALSE)){ 

			$vod_episode->vod_season_id = $this->input->post('select_vod_season');
			$vod_episode->name = $this->input->post('name');
			$vod_episode->description = $this->input->post('description');
			$vod_episode->active = $this->input->post('active'); 

			if($vod_episode->save()){ 

				$txtSuccess = ($id)?__('Edit vod episode success','vod/create_vod_episode'):__('Create vod episode success','vod/create_vod_episode'); 

				$this->msg->add($txtSuccess,'success');

				redirect($this->uri->uri_string());

			}


		} 

		$vod_episode->{'vod_id'} = $this->getVodIdByVodSeasonId([
			'vod_season_id'=>@$vod_episode->vod_season_id
		]); 

		//print_r($vod_episode);

		$data = [
			'vod_episode'=>$vod_episode,
			'select_vod'=>$this->getSelectVod(),

		];

		$this->template->content->view('vod/create_vod_episode',$data);
		$this->template->publish();
	}

	public function owner(){ 

		$owner = new M_vod_owner();
		$owner->get(); 

		$data = [
			'owner'=>$owner
		];

		$this->template->content->view('vod/vod_owner_list',$data);
		$this->template->publish();

	}

	public function createOwner(){
		$this->__createOwner();
	}

	public function editOwner($id){
		$owner = new M_vod_owner($id);

		if(!$owner->id){
			redirect($this->controller.'/owner');
		}

		$this->__createOwner($id);
	}

	public function deleteOwner($id){
		$owner = new M_vod_owner($id);

		if(!$owner->id){
			redirect($this->controller.'/owner');
		} 


	}

	private function __createOwner($id = null){ 

		$this->loadValidator();

		$this->loadBootstrapFileuploadMasterStyle();
		$this->loadBootstrapFileuploadMasterScript();

		if($id){
			$this->template->javascript->add(base_url('assets/js/vod/edit_vod_owner.js'));
		}else{
			$this->template->javascript->add(base_url('assets/js/vod/create_vod_owner.js'));
		} 


		$owner = new M_vod_owner($id); 


		if($this->input->post(NULL,FALSE)){

			$owner->company_name = $this->input->post('company_name');
			$owner->company_address = $this->input->post('company_address');
			$owner->contact_firstname = $this->input->post('contact_firstname');
			$owner->contact_lastname = $this->input->post('contact_lastname');
			$owner->contact_telephone = $this->input->post('contact_telephone');
			$owner->contact_fax = $this->input->post('contact_fax'); 
			$owner->active = $this->input->post('active');

			if($owner->save()){ 

				/* check upload image */
				if($_FILES['cover']['error'] == 0){
					$this->uploadCoverImage(array(
						'vod_owner_id'=>$owner->id
					));
				}
				/* eof check upload image */ 

				$txtSuccess = ($id)?__('Edit vod owner success','vod/create_vod_owner'):__('Create vod owner success','vod/create_vod_owner');

				$this->msg->add($txtSuccess,'success');
				redirect($this->uri->uri_string());


			}


		}



		$data = [
			'owner'=>$owner
		];

		$this->template->content->view('vod/create_vod_owner',$data);
		$this->template->publish();
	}

	public function createVod(){
		$this->__createVod();
	}
	public function editVod($id){

		$vod = new M_vod($id);

		if(!$vod->id){
			redirect($this->controller);
		}

		$this->__createVod($id);
	}
	public function deleteVod($id){	
		$vod = new M_vod($id);

		if(!$vod->id){
			redirect($this->controller);
		} 

		// check all source related this vod

		// -- check has season 

		// -- check and delete all season

		// -- check delete all episode in season 

		if($vod->vod_season->get()->result_count() > 0){
			foreach ($vod->vod_season->select('vod_season.id as vod_season_id')->get() as $key => $value) {
				# code...
				$vod_season = new M_vod_season($value->vod_season_id);

				// delete all apisode
				$this->db->delete('vod_episode',[
					'vod_season_id'=>$vod_season->id
				]);

				$this->db->delete('vod_season',[
					'id'=>$vod_season->id
				]); 


			}
		} 


		// check file path of vod cover 
		if($vod->cover){
			$file_path = base_url('uploaded/vod/'.$vod->id.'/'.$vod->cover);

			if(file_exists($file_path)){
				unlink($file_path);
			}
		}

		if($vod->delete()){

			$this->msg->add(__('Delete vod success','vod/vod_list'),'success');

			redirect($this->controller);
		}



		




	} 

	private function __createVod($id = null){ 

		$this->loadValidator(); 
		$this->loadSweetAlert();

		$this->loadBootstrapFileuploadMasterStyle();
		$this->loadBootstrapFileuploadMasterScript();

		$this->template->stylesheet->add(base_url('assets/css/switch/material_switch.css'));

		if($id){
			$this->template->javascript->add(base_url('assets/js/vod/edit_vod.js'));
		}else{
			$this->template->javascript->add(base_url('assets/js/vod/create_vod.js'));
		}

		$vod = new M_vod($id);


		if($this->input->post(NULL,FALSE)){ 
			$vod->vod_owner_id = $this->input->post('vod_owner_id'); 
			$vod->vod_categories_id = $this->input->post('vod_categories_id');
			$vod->name = $this->input->post('name');
			$vod->description = $this->input->post('description');
			$vod->has_one_episode_status = $this->input->post('check_has_one');
			$vod->video_url = ($this->input->post('check_has_one') === '1')?$this->input->post('video_url'):'';
			$vod->active = $this->input->post('active');
			$vod->trending_status = $this->input->post('trending_status');

			if($vod->save()){  


				// check if has many season
				if($this->input->post('check_has_one') == '0'){ // mean has many season 
					$post_data = $this->input->post();

					if(!$id){ 

						if(isset($post_data['hide_season_name']) && count($post_data['hide_season_name']) > 0){ 

							foreach ($post_data['hide_season_name'] as $key => $value) {
								# code...
								$this->db->insert('vod_season',[
									'vod_id'=>$vod->id,
									'name'=>$value,
									'description'=>$post_data['hide_season_description'][$key],
									'created'=>date('Y-m-d H:i:s')
								]); 

								//echo $this->db->last_query();exit;
							} 




						}

					}else{ // if already exist id 

						if(isset($post_data['more_season_name']) && count($post_data['more_season_name']) > 0){
							foreach ($post_data['more_season_name'] as $key => $value) {
								# code... 
								$insert_vod_season = new M_vod_season(); 
								$insert_vod_season->vod_id = $vod->id;
								$insert_vod_season->name = $value;
								$insert_vod_season->description = $post_data['more_season_description'][$key];
								$insert_vod_season->amount_episode = $post_data['more_season_amount_episode'][$key];
								$insert_vod_season->save();

							}


						}




					}
					

				}



				/* check upload image */
				if($_FILES['cover']['error'] == 0){
					$this->uploadVodCoverImage(array(
						'vod_id'=>$vod->id
					));
				}
				/* eof check upload image */ 

				$txtSuccess = ($id)?__('Edit vod success','vod/create_vod'):__('Create vod success','vod/create_vod');

				$this->msg->add($txtSuccess,'success');
				redirect($this->uri->uri_string());



			}



		}



		$data = [
			'vod'=>$vod,
			'select_owner'=>$this->getSelectOwner(),
			'select_vod_categories'=>$this->getSelectCategory()
		];

		$this->template->content->view('vod/create_vod',$data);
		$this->template->publish();

	}

	private function __createVodCategories($id = null){ 

		$this->loadValidator();

		$this->loadBootstrapFileuploadMasterStyle();
		$this->loadBootstrapFileuploadMasterScript();

		if($id){
			$this->template->javascript->add(base_url('assets/js/vod/edit_vod_categories.js'));
		}else{
			$this->template->javascript->add(base_url('assets/js/vod/create_vod_categories.js'));
		}


		$vod_categories = new M_vod_categories($id);

		if($this->input->post(NULL,FALSE)){ 

			$vod_categories->name = $this->input->post('name');
			$vod_categories->description = $this->input->post('description');
			$vod_categories->active = $this->input->post('active');

			if($vod_categories->save()){ 

				/* check upload image */
				if($_FILES['cover']['error'] == 0){
					$this->uploadVodCategoryCoverImage(array(
						'vod_category_id'=>$vod_categories->id
					));
				}
				/* eof check upload image */ 


				$txtSuccess = ($id)?__('Edit vod category success','vod/create_vod_categories'):__('Create vod category success','vod/create_vod_categories'); 

				$this->msg->add($txtSuccess,'success');
				redirect($this->uri->uri_string());

			}

		}

		$data  = [
			'vod_categories'=>$vod_categories
		];

		$this->template->content->view('vod/create_vod_categories',$data);
		$this->template->publish();
	} 

	private function uploadVodCoverImage($data  = []){
		$vod_id = $data['vod_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('uploaded/vod/'.$vod_id.'')){
			mkdir('uploaded/vod/'.$vod_id.'',0777,true);
		}
		clearDirectory('uploaded/vod/'.$vod_id.'/');

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/vod/'.$vod_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('cover')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			if(file_exists('uploaded/vod/'.$vod_id)){
				rmdir('uploaded/vod/'.$vod_id);
			}

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

                //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name']; 

			$update_vod = new M_vod($vod_id);
			$update_vod->cover = $upload_file_name;
			$update_vod->cover_size = $_FILES['cover']['size']; 
			$update_vod->save();
			return true;
		}
	}

	private function uploadVodCategoryCoverImage($data = []){
		$vod_category_id = $data['vod_category_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('uploaded/vod_category/'.$vod_category_id.'')){
			mkdir('uploaded/vod_category/'.$vod_category_id.'',0777,true);
		}
		clearDirectory('uploaded/vod_category/'.$vod_category_id.'/');

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/vod_category/'.$vod_category_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('cover')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			if(file_exists('uploaded/vod_category/'.$vod_category_id)){
				rmdir('uploaded/vod_category/'.$vod_category_id);
			}

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

                //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name']; 

			$update_vod_category = new M_vod_categories($vod_category_id);
			$update_vod_category->cover = $upload_file_name;
			$update_vod_category->cover_size = $_FILES['cover']['size']; 
			$update_vod_category->save();

			return true;
		}
	}

	private function uploadCoverImage($data = []){
		$vod_owner_id = $data['vod_owner_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('uploaded/vod_owner/'.$vod_owner_id.'')){
			mkdir('uploaded/vod_owner/'.$vod_owner_id.'',0777,true);
		}
		clearDirectory('uploaded/vod_owner/'.$vod_owner_id.'/');

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/vod_owner/'.$vod_owner_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('cover')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			if(file_exists('uploaded/vod_owner/'.$vod_owner_id)){
				rmdir('uploaded/vod_owner/'.$vod_owner_id);
			}

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

                //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name']; 

			$update_vod_owner = new M_vod_owner($vod_owner_id);
			$update_vod_owner->company_cover = $upload_file_name;
			$update_vod_owner->company_cover_size = $_FILES['cover']['size']; 
			$update_vod_owner->save();
			return true;
		}


	}

	private function getSelectOwner(){
		$arrReturn = []; 

		$arrReturn[''] = __('-- Select Owner --','vod/create_vod');

		$owner = new M_vod_owner();
		$owner->where('active',1)->get();

		foreach ($owner as $key => $value) {
			# code...
			$arrReturn[$value->id] = $value->company_name;
		}

		return $arrReturn;
	} 

	private function getSelectVod(){ 
		$arrReturn = []; 
		$arrReturn[''] = __('Select Vod','vod/create_vod_season');
		$vod = new M_vod();
		$vod->where('active',1)
		->where('has_one_episode_status',0)
		->order_by('name','asc')
		->get();

		foreach ($vod as $key => $value) {
			# code...
			$arrReturn[$value->id] = $value->name.' - ['.$value->vod_owner->get()->company_name.']';
		}

		return $arrReturn;

	} 

	private function getEpisodeRowByVodSeason($data = []){ 



		$vod_episode = new M_vod_episode();
		$vod_episode->where('vod_season_id',$data['vod_season_id'])
		->get(); 
		return $vod_episode;

		

	} 

	private function getVodIdByVodSeasonId($data = []){ 
		$vod_season = new M_vod_season($data['vod_season_id']); 

		return $vod_season->vod_id;

	} 

	private function getSelectCategory(){
		$arrReturn = [];
		$arrReturn[''] = __('-- Select Category --','vod/create_vod');

		$vod_categories = new M_vod_categories();
		$vod_categories->where('active',1)->get();

		foreach ($vod_categories as $key => $value) {
			# code...
			$arrReturn[$value->id] = $value->name;
		}

		return $arrReturn;
	}

	private function getDecryptQueryString($data = []){ 

		$decrypt_txt = base64_decode($data['encrypt_txt']); 

		parse_str($decrypt_txt,$output);
		return $output;
	}

	private function getCategoryDataById($category_id){
		$vod_category = new M_vod_categories($category_id);

		if($vod_category->id){
			return $vod_category;
		}else{
			return null;
		}
	}
}