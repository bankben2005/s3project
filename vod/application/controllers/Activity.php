<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/Backend_controller.php';
class Activity extends Backend_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
		parent::__construct();
	}

	public function index(){ 

	}

	public function profile(){ 

		$this->loadValidator();


		$this->template->javascript->add(base_url('assets/js/activity/profile.js'));

		$vod_administrator = new M_vod_administrator($this->admin_data['id']); 


		if($this->input->post(NULL,FALSE)){
			$vod_administrator->firstname = $this->input->post('firstname');
			$vod_administrator->lastname = $this->input->post('lastname');
			$vod_administrator->telephone = $this->input->post('telephone');

			if($vod_administrator->save()){
				$this->msg->add(__('Edit profile success','activity/profile'),'success');
				redirect($this->uri->uri_string());
			}

		}

		$data = [
			'admin_data'=>$vod_administrator
		];


		$this->template->content->view('activity/profile',$data);
		$this->template->publish();

	}

	public function change_password(){ 

		$this->loadValidator();
		$this->template->javascript->add(base_url('assets/js/activity/change_password.js')); 

		if($this->input->post(NULL,FALSE)){
			$vod_administrator = new M_vod_administrator($this->admin_data);

			$vod_administrator->password = sha1($this->input->post('new_password'));

			if($vod_administrator->save()){
				$this->msg->add(__('Change password success','activity/change_password'),'success');

				redirect($this->uri->uri_string());
			}

		}

		$data = [

		];

		$this->template->content->view('activity/change_password',$data);
		$this->template->publish();

	}

	public function ajaxCheckOldPassword(){
		$get = $this->input->get();

		$vod_administrator = new M_vod_administrator();

		$vod_administrator->where('id',$this->admin_data['id'])
		->where('password',sha1($get['old_password']))
		->get();

		if($vod_administrator->id){
			echo json_encode([
				'valid'=>true
			]);
		}else{
			echo json_encode([
				'valid'=>false
			]);
		}



	}
}