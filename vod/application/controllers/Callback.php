<?php
defined('BASEPATH') OR exit('No direct script access allowed'); 

header('Content-Type: application/json'); 

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Callback extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */ 

	function __construct(){
		parent::__construct();

		$this->load->config('partners');
	}
	public function index(){



		//print_r('aaaa');exit;
		//$this->load->view('welcome_message');
	}

	public function getTransactionPaymentCallBack(){ 

		// have to define reference id 

		// print_r($this->input->post());exit;

		// get data from post value
		$json_return = [];
		if($this->input->post('request') || file_get_contents('php://input')){

				$request = (file_get_contents('php://input'))?file_get_contents('php://input'):$this->input->post('request');

				$requestCriteria = json_decode($request);

				$log_file_path = $this->createLogFilePath('callBackRequestCriteria');
		        $file_content = date("Y-m-d H:i:s") . ' criteria : ' . json_encode($requestCriteria) . "\n"; 
		        $file_content .= 'Ip Address : '.$this->getUserIP(). "\n";
		        file_put_contents($log_file_path, $file_content, FILE_APPEND);
		        unset($file_content);


				$checkAuthorization = $this->checkAuthorization($requestCriteria);

				if(!$checkAuthorization['status']){
					$json_return = [
						'status'=>false,
						'result_code'=>$checkAuthorization['status'],
						'result_desc'=>$checkAuthorization['result_desc']
					];

				}

				// call back status
				if(!property_exists($requestCriteria, 'callback_status') || $requestCriteria->callback_status == ''){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found callback_status'
					];
				}

				if(!in_array($requestCriteria->callback_status, ['success','unsuccess'])){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : callback_status must be success or unsuccess only'
					];	
				}

				if(!property_exists($requestCriteria, 's3ref') || $requestCriteria->s3ref == ''){
					$json_return = [
						'status'=>false,
						'result_code'=>'-002',
						'result_desc'=>'Parameter exception : not found s3ref'
					];
				}



				// check refid from vvod_transaction
				$transaction_data = $this->getTransactionByRefId([
					's3ref'=>@$requestCriteria->s3ref,
					'requestCriteria'=>$requestCriteria
				]);


				if(!$transaction_data['status']){
					$json_return = [
						'status'=>false,
						'result_code'=>$transaction_data['result_code'],
						'result_desc'=>$transaction_data['result_desc']
					];
				}else{
					$json_return = [
						'status'=>true,
						'TransactionData'=>$transaction_data['TransactionData'],
						'result_code'=>'000',
						'result_desc'=>'Success'
					];
				}







				echo json_encode($json_return);



		}


		// $data_mv = [
		// 	'partner'=>'mv',
		// 	'function'=>'vod-drm',
		//     'hash'=>sha1('mv')
		// ]; 


		// $build_to_url = http_build_query($data_mv);

		// print_r(base64_encode(base64_encode($build_to_url)));exit;






	}

	private function checkAuthorization($requestCriteria){ 
		$this->load->config('partners');
		$mv_config = $this->config->item('api')['mv'];

		//print_r($mv_config);exit;
		$headers = [];
		foreach($_SERVER as $name => $value) {
		    if($name != 'HTTP_MOD_REWRITE' && (substr($name, 0, 5) == 'HTTP_' || $name == 'CONTENT_LENGTH' || $name == 'CONTENT_TYPE')) {
		        $name = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', str_replace('HTTP_', '', $name)))));
		        if($name == 'Content-Type') $name = 'Content-type';
		        $headers[$name] = $value;
		    }
		}
		// $headers = getallheaders();

		//print_r($headers);exit;
		if(!isset($headers['Authorization']) || $headers['Authorization'] == ''){

			return [
				'status'=>false,
				'result_code'=>'-009',
				'result_desc'=>'Authorization exception : Do not authorization,Please try again'
			];
		}

		// print_r($headers);exit;

		if($headers['Authorization'] != $mv_config['ApiKey']){
			return [
				'status'=>false,
				'result_code'=>'-009',
				'result_desc'=>'Authorization exception : Do not authorization,Please try again'
			];
		}

		return [
			'status'=>true
		];


	}

	private function getTransactionByRefId($data = []){
		$requestCriteria = $data['requestCriteria'];

		$query = $this->db->select('*')
		->from('vvod_transactions')
		->where('transaction_number',$data['s3ref'])
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			$row = $query->row(); 


			if($row->callback_status){
				return [
					'status'=>false,
					'result_code'=>'-003',
					'result_desc'=>'Transaction has been updated,'
				];
			}
			// if($row->pay)
			$this->db->update('vvod_transactions',[
				'payment_status'=>($requestCriteria->callback_status == 'success')?1:0,
				'callback_status'=>1,
				'updated'=>date('Y-m-d H:i:s')
			],['id'=>$row->id]);


			// update data into vvod_user_package_live_transaction and also update into user_package_live

			$this->updateToUserPackageLive([
				'transaction_data'=>$row,
				'requestCriteria'=>$requestCriteria
			]);


			

			$transaction_data = new StdClass();
			$transaction_data->transaction_number = $row->transaction_number;
			$transaction_data->amount = $row->amount;
			$transaction_data->sub_total = $row->sub_total;
			$transaction_data->grand_total = $row->grand_total;
			$transaction_data->payment_option = $row->payment_option;
			$transaction_data->payment_status = ($requestCriteria->callback_status == 'success')?'paid':'unpaid';

			// if(isset($_GET['test']) && $_GET['test'] == 'test'){
			// 	print_r($transaction_data);exit;
			// }




			return [
				'status'=>true,
				'TransactionData'=>$transaction_data,
				'result_code'=>'000',
				'result_desc'=>'Success'
			];


		}else{
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Parameter exception : not found s3ref please try again'
			];
		}
	}

	private function createLogFilePath($filename = '') {
        $log_path = './application/logs/callback_logs';

        $dirs = explode('/', $log_path);

        $checked_log_Path = '';
        $pieces = array();

        foreach ($dirs as $dir) {

            if (trim($dir) != '') {
                $pieces[] = $dir;

                $checked_log_Path = implode('/', $pieces);

                if (!is_dir($checked_log_Path)) {
                    mkdir($checked_log_Path);
                    chmod($checked_log_Path, 0777);
                }
            }
        }

        $log_file_path = $checked_log_Path . '/' . $filename . '-' . date("YmdHis") . '.txt';

        return $log_file_path;
    }
    private function getUserIP(){
                       $client  = @$_SERVER['HTTP_CLIENT_IP'];
                            $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
                            $remote  = $_SERVER['REMOTE_ADDR'];

                            if(filter_var($client, FILTER_VALIDATE_IP))
                            {
                                $ip = $client;
                            }
                            elseif(filter_var($forward, FILTER_VALIDATE_IP))
                            {
                                $ip = $forward;
                            }
                            else
                            {
                                $ip = $remote;
                            }

                            return $ip;
    }

    private function updateToUserPackageLive($data = []){

    	$transaction_data = $data['transaction_data'];
    	$requestCriteria = $data['requestCriteria'];

    	$package_data = $this->getPartnerPackageDataById($transaction_data->vvod_partner_packages_id);

    	// check exist 
    	$queryCheck = $this->db->select('*')
    	->from('vvod_user_package_live')
    	->where('vvod_users_id',$transaction_data->vvod_users_id)
    	->where('vvod_partner_packages_id',$transaction_data->vvod_partner_packages_id)
    	->where('active',1)
    	->get();

    	if($queryCheck->num_rows() > 0){ 
    		$rowUserPackageLive = $queryCheck->row();

    		$currentDateTime = new DateTime();
    		$currentExpireDateTime = new DateTime($rowUserPackageLive->end_datetime);


    		// already exist renew current record and 
    		// 1. first have to check end_datetime expired?
    		// if expired set start_datetime and end_datetime today to count plus number
    		// if not expire just plus end_datetime plus number of package day
    		// and also insert new record into package live transaction 

    		if($currentDateTime > $currentExpireDateTime){
    			// this case expired 

    			$getEndDateTime = new DateTime();
    			$getEndDateTime->add(new DateInterval('P'.$package_data->days.'D'));

    			$this->db->update('vvod_user_package_live',[
    				'start_datetime'=>$currentDateTime->format('Y-m-d H:i:s'),
    				'end_datetime'=>$getEndDateTime->format('Y-m-d H:i:s'),
    				'updated'=>date('Y-m-d H:i:s')
    			],['id'=>$rowUserPackageLive->id]); 

    			// update into vvod_user_package_live_transaction

    			$this->db->insert('vvod_user_package_live_transaction',[
    				'vvod_transactions_id'=>$transaction_data->id,
    				'vvod_user_package_live_id'=>$rowUserPackageLive->id,
    				'start_datetime'=>$currentDateTime->format('Y-m-d H:i:s'),
    				'end_datetime'=>$getEndDateTime->format('Y-m-d H:i:s')
    			]);

    		}else{
    			// this case not expired

    			$nextExpireDateTime = $currentExpireDateTime;
    			$nextExpireDateTime->add(new DateInterval('P'.$package_data->days.'D'));

    			$this->db->update('vvod_user_package_live',[
    				'end_datetime'=>$nextExpireDateTime->format('Y-m-d H:i:s'),
    				'updated'=>date('Y-m-d H:i:s')
    			],['id'=>$rowUserPackageLive->id]); 

    			$this->db->insert('vvod_user_package_live_transaction',[
    				'vvod_transactions_id'=>$transaction_data->id,
    				'vvod_user_package_live_id'=>$rowUserPackageLive->id,
    				'start_datetime'=>$currentDateTime->format('Y-m-d H:i:s'),
    				'end_datetime'=>$nextExpireDateTime->format('Y-m-d H:i:s')
    			]);



    		}







    	}else{
    		// if not exist insert new record to vvod_user_package_live
    		// and alsol insert new record into package live transaction 

    		$currentDateTime = new DateTime();

    		$nextExpireDateTime = new DateTime();
    		$nextExpireDateTime->add(new DateInterval('P'.$package_data->days.'D'));


    		$this->db->insert('vvod_user_package_live',[
    			'vvod_users_id'=>$transaction_data->vvod_users_id,
    			'vvod_partner_packages_id'=>$transaction_data->vvod_partner_packages_id,
    			'start_datetime'=>$currentDateTime->format('Y-m-d H:i:s'),
    			'end_datetime'=>$nextExpireDateTime->format('Y-m-d H:i:s'),
    			'created'=>date('Y-m-d H:i:s'),
    			'active'=>1
    		]);

    		$insert_id = $this->db->insert_id();

    		$this->db->insert('vvod_user_package_live_transaction',[
    				'vvod_transactions_id'=>$transaction_data->id,
    				'vvod_user_package_live_id'=>$insert_id,
    				'start_datetime'=>$currentDateTime->format('Y-m-d H:i:s'),
    				'end_datetime'=>$nextExpireDateTime->format('Y-m-d H:i:s')
    		]);

    		


    	}
    }

    private function getPartnerPackageDataById($vvod_partner_packages_id){
    	$query = $this->db->select('*')
    	->from('vvod_partner_packages')
    	->where('id',$vvod_partner_packages_id)
    	->get();

    	if($query->num_rows() > 0){
    		return $query->row();
    	}else{
    		return '';
    	}


    }
}
