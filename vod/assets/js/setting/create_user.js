$(document).ready(function(){

	validateForm();
}); 


function validateForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-user-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
            email:{
                    validators:{
                        remote: {
                            message: $('input[name="email_exist"]').val(),
                            url: base_url+'user/ajaxCheckEmailExist'
                        },
                        notEmpty:{
                            message:''
                        }
                    }
            },
			type:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			},
			firstname:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			},
			lastname: {
                    validators:{
                        notEmpty: {
                            message:''
                        }
                    }
                    
            },
            telephone:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            },
            password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        stringLength: {
                                min: 6,
                                max: 30,
                                message: ''
                        }
                    }
            },
            confirm_password:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        identical: {
                                field: 'password',
                                message: ''
                        }
                    }
            }
		},
		onSuccess: function(e, data) { 
			
        }
    });
}