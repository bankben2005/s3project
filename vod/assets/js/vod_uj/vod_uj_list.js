$(document).ready(function(){

	// Sortable.create(demo1, {
	//   animation: 100,
	//   group: 'list-1',
	//   draggable: '.list-group-item',
	//   handle: '.list-group-item',
	//   sort: true,
	//   filter: '.sortable-disabled',
	//   chosenClass: 'active'
	// });

	Sortable.create(member_playlist_listgroup, {
	  group: 'list-1',
	  handle: '.list-group-item',
	  onSort: function( ) {
        // do stuff

        var newArrPlayList = getNewPlayListSort();
        // console.log('updated');
        var post_data = $.parseJSON($('input[name="hide_member_playlist_data"]').val());

        post_data.playlist = newArrPlayList;

        console.log(post_data);
      }
	});

});

function clickReplyModal(element){
	var element = $(element);

	// console.log(element);
	var request_message = element.data('requestmessage');
	var reply_message = element.data('replyhistories');

	console.log(reply_message);

	// console.log(request_message); 

	$('form[name="reply-message-form"]').find('input[name="hide_vod_uj_id"]').val(element.data('ujid'));

	$('#modal-request-message').html('').html(
		'<div class="alert alert-secondary"><strong>Request Message : </strong>'+request_message+'</div>'
	);	

	if(reply_message.length > 0){
		$(reply_message).each(function(index,value){

			// console.log(value);
			var txt_reply = '<div class="alert alert-info">';
				txt_reply += '<strong>Reply Message : </strong>'+value.reply_message;

					txt_reply += '<a href="javascript:void(0);" class="float-right" onclick="deleteReplyMessage(this)" data-replymessageid="'+value.id+'"><i class="fa fa-window-close"></i></a>';
					txt_reply += '<br><small style="font-size:8px;">'+getDateStr(value.created)+'</small>';
				txt_reply += '</div>';
			$('#modal-request-message').append(txt_reply);
		});
	}


	$('#replyMessageModal').modal('toggle');
}

function submitReplyMessage(e){
	e.preventDefault(); 


	var reply_message_form = $('form[name="reply-message-form"]');
	// reply_message_form.

	var reply_message = reply_message_form.find('textarea[name="reply_message"]').val();

	// console.log(reply_message);

	var base_url = $('input[name="base_url"]').val();

	$.ajax({
            type: "POST",
            url: base_url+"vod_uj/ajaxVodUJReplyMessage",
            async:true,
            dataType:'json',
            data:{
            	'reply_message':reply_message,
            	'vod_uj_id':reply_message_form.find('input[name="hide_vod_uj_id"]').val()
            },
            beforeSend: function( xhr ) { 
            	$('#replyMessageModal').modal('toggle');
            	
            	
            },
            success: function(response){

                if(response.status){ 
                	// location.reload();
                	swal({
						title: "สำเร็จ!",
						text: "ทำการตอบกลับข้อความสำเร็จ",
						icon: "success",
					}).then(function(){


						location.reload();
						// $('input[name="video_url"]').focus();
					});

                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });
}

function deleteReplyMessage(element){
	var element = $(element);
	var post_data = {
		'reply_message_id':element.data('replymessageid')
	};

	var base_url = $('input[name="base_url"]').val();

	$.ajax({
            type: "POST",
            url: base_url+"vod_uj/ajaxDeleteReplyMessage",
            async:true,
            dataType:'json',
            data:post_data,
            beforeSend: function( xhr ) { 
            },
            success: function(response){

                if(response.status){ 
                	// element.closest('div.alert').remove().fadeOut('slow');

                	element.closest('div.alert').fadeOut(300,function(){
                		$(this).remove();
                	});


                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });
}

function getDateStr(date){
	var date = new Date(date);
	var dateStr = 
	("00" + date.getDate()).slice(-2) + "/" +
	  ("00" + (date.getMonth() + 1)).slice(-2) + "/" +
	  
	  date.getFullYear() + " " +
	  ("00" + date.getHours()).slice(-2) + ":" +
	  ("00" + date.getMinutes()).slice(-2) + ":" +
	  ("00" + date.getSeconds()).slice(-2);

	 return dateStr;

}

function assignVodToMember(element){
	var element = $(element);

	var post_data = {
		'members_id':element.data('membersid'),
		'login_type':element.data('logintype')
	}; 

	$('input[name="hide_member_playlist_data"]').val(JSON.stringify(post_data));


	// get user play list for render current playlist 

	var base_url = $('input[name="base_url"]').val();

	$.ajax({
            type: "POST",
            url: base_url+"vod_uj/ajaxGetMemberPlaylist",
            async:true,
            dataType:'json',
            data:post_data,
            beforeSend: function( xhr ) { 

            	$('ul#member_playlist_listgroup').html(
            		'<li class="list-group-item text-center"><i class="fa fa-spin fa-spinner"></i></li>'
            	);
            },
            success: function(response){

            	console.log('====== response ======');
            	console.log(response);

                if(response.status){ 

                	 $('ul#member_playlist_listgroup').html('').html(response.view);




                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });


	$('#assignMemberPlaylist').modal('show');



	// console.log(post_data);
}


function getNewPlayListSort(){

	var arrReturn = [];

	if($('ul#member_playlist_listgroup li').length >= 1){

			$('ul#member_playlist_listgroup > li').each(function(i,e){
				var element = $(e);

				var vod_id = element.find('input[name="arrVodListId[]"]').val();

				arrReturn.push(parseInt(vod_id));

				// console.log(element);
			});

	}

	return arrReturn;

}


function deleteMemberPlaylistVod(element){
	var element = $(element);

	var post_data = $.parseJSON($('input[name="hide_member_playlist_data"]').val());

	post_data.delete_id = parseInt(element.data('vodid'));

	var assign_anchor = $('table > tbody').find('a[data-membersid="'+post_data.members_id+'"][data-logintype="'+post_data.login_type+'"]');

	// console.log(assign_anchor); return false;

	var base_url = $('input[name="base_url"]').val();

	$.ajax({
            type: "POST",
            url: base_url+"vod_uj/ajaxDeleteMemberPlaylistVod",
            async:true,
            dataType:'json',
            data:post_data,
            beforeSend: function( xhr ) { 

            },
            success: function(response){

            	console.log('====== response ======');
            	console.log(response);

            	//return false;

                if(response.status){ 

                	// re render modal and member playlist
                	element.closest('li').fadeOut(300,function(){
                		$(this).remove();
                	});

                	var select_category = $('select[name="select_category"]');

                	if(select_category.val() != ''){
                		changeSelectCategory(select_category);
                	}
                	
                	// find anchor element by member and login type 
                	//assignVodToMember(assign_anchor);

                	// console.log(as)
                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });
}

function changeSelectCategory(element){
	var element = $(element);

	$('ul#vod_listgroup').html(
            		'<li class="list-group-item text-center"><i class="fa fa-spin fa-spinner"></i></li>'
    );

    var post_data = $.parseJSON($('input[name="hide_member_playlist_data"]').val());
    post_data.vod_categories_id  = element.val();


    var base_url = $('input[name="base_url"]').val();

	$.ajax({
            type: "POST",
            url: base_url+"vod_uj/ajaxGetVodListItemByCategory",
            async:true,
            dataType:'json',
            data:post_data,
            beforeSend: function( xhr ) { 

            },
            success: function(response){

            	console.log('====== response ======');
            	console.log(response);

            	//return false;

                if(response.status){ 

                	// re render modal and member playlist
                	// element.closest('li').remove();
                	// find anchor element by member and login type 
                	//assignVodToMember(assign_anchor);

                	// console.log(as)
                	$('ul#vod_listgroup').html('').html(response.view);

                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });

	console.log(element);
}

function addVodToMemberPlaylist(element){
	var element = $(element);

	var post_data = $.parseJSON($('input[name="hide_member_playlist_data"]').val());
    post_data.vod_id  = parseInt(element.data('vodid'));

	// console.log(post_data);
	var base_url = $('input[name="base_url"]').val();

	$.ajax({
            type: "POST",
            url: base_url+"vod_uj/ajaxAddVodToMemberPlaylist",
            async:true,
            dataType:'json',
            data:post_data,
            beforeSend: function( xhr ) { 



            },
            success: function(response){

            	console.log('====== response ======');
            	console.log(response);

            	//return false;

                if(response.status){ 

                	var add_lg_item = '<li class="list-group-item">';
                			add_lg_item += '<div class="row">';

                				add_lg_item += '<div class="col-lg-3">';
                					add_lg_item += '<img src="'+element.data('cover')+'" class="img-fluid">';
                				add_lg_item += '</div>';

                				add_lg_item += '<div class="col-lg-8">';
                					add_lg_item += element.data('vodname');
                				add_lg_item += '</div>';

                				add_lg_item += '<div class="col-lg-1">';
                					add_lg_item += '<a href="javascript:void(0)" data-vodid="'+element.data('vodid')+'" onclick="deleteMemberPlaylistVod(this)"><i class="fa fa-window-close"></i></a>';
                					add_lg_item += '<input type="hidden" name="arrVodListId[]" value="'+element.data('void')+'">';
                				add_lg_item += '</div>';

                			add_lg_item += '</div>';	
                		add_lg_item += '</li>';

                	$('ul#member_playlist_listgroup').append(add_lg_item);

                	element.closest('li').fadeOut(300,function(){
                		$(this).remove();



                	});

                	// re render modal and member playlist
                	// element.closest('li').remove();
                	// find anchor element by member and login type 
                	//assignVodToMember(assign_anchor);

                	// console.log(as)
                	//$('ul#vod_listgroup').html('').html(response.view);
                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });
}