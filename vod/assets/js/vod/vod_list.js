function onSubmitSearchForm(e){
	e.preventDefault(); 

	var form_data = $('form[name="search-vod-form"]').serialize();

	var base_url = $('input[name="base_url"]').val();


	$.ajax({
            type: "POST",
            url: base_url+"vod/ajaxGetQueryString",
            async:true,
            dataType:'json',
            data:form_data,
            beforeSend: function( xhr ) { 

            	
            	
            },
            success: function(response){
            	console.log('========= response =========');
            	console.log(response);
                if(response.status){ 
                	//location.reload();
                	window.location.href=response.redirect_url;
                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });


}

function clickCheckTrendingStatus(element){
    var element = $(element); 
    var base_url = $('input[name="base_url"]').val(); 

    var post_data = {
        'vod_id':element.val(),
        'checked_status':(element.is(':checked'))?'checked':'unchecked'
    };

    //console.log(post_data);
    $.ajax({
            type: "POST",
            url: base_url+"vod/ajaxSetVodTrendingStatus",
            async:true,
            dataType:'json',
            data:post_data,
            beforeSend: function( xhr ) { 

                
                
            },
            success: function(response){
                console.log('========= response =========');
                console.log(response);
                if(response.status){ 
                    //location.reload();
                    //window.location.href=response.redirect_url;
                }else{
                    
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });


}