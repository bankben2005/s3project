$(document).ready(function(){
	validatorForm();
	initCoverImage();
});
function validatorForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="create-vod-categories"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			name:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			}
		},
		onSuccess: function(e, data) { 
			
        }
    });
}  

function initCoverImage(){
	var base_url = $('input[name="base_url"]').val();
	$('.cover').fileinput({
		theme: 'fa',
		language: $('input[name="current_lang"]').val(),
		uploadUrl: '',
		dropZoneEnabled: true,
		showUpload: false,
		allowedFileExtensions: ['jpg', 'png', 'gif'],
		initialPreview: $.parseJSON($('input[name="hide_cover"]').val()),
            initialPreviewAsData: true, // identify if you are sending preview data only and not the raw markup
            initialPreviewFileType: 'image', // image is the default and can be overridden in config below
            initialPreviewDownloadUrl: base_url+'uploaded/vod_category/'+$('input[name="hide_vod_category_id"]').val()+'/{filename}', // includes the dynamic `filename` tag to be replaced for each config
            initialPreviewConfig: $.parseJSON($('input[name="hide_cover_caption_obj"]').val()),
            purifyHtml: true, // this by default purifies HTML data for preview
            overwriteInitial: true

        }).on('filebatchuploaderror', function(event, data, msg) {
        	console.log('event = '+event+' data = '+data+' msg = '+msg);
        }).on('filebatchuploadsuccess',function(event,data,msg){
        	location.reload();
        });
} 