$('tbody').sortable({
    stop: function( event, ui ) { 
    	var arr_postdata = []; 

        $('input[name="ordinal_by_category[]"]', ui.item.parent()).each(function (i,v) { 
        		// console.log('v element'); 
        		var element = $(v);
        		var ordinal = i+1;

        		element.closest('tr').find('span.show_ordinal_by_category').html('').html(ordinal);
                $(this).val(ordinal); 

                // var vod_id = element.data('vodid');
                

                // var post_data = {
                // 	'vod_id':vod_id,
                // 	'ordinal':ordinal
                // };	

                // ajaxUpdateOrdinalByCategory(post_data); 
                arr_postdata.push({
	    			'ordinal':element.val(),
	    			'vod_id':element.data('vodid')
    			});


        }); 

        ajaxUpdateOrdinalByCategory(arr_postdata);
    }
});


function ajaxUpdateOrdinalByCategory(arr_post_data = []){

	var base_url = $('input[name="base_url"]').val();

	 	$.ajax({
            type: "POST",
            url: base_url+"vod/ajaxUpdateOrdinalByCategory",
            async:true,
            dataType:'json',
            data:{
                'arr_post':arr_post_data,
                'category_id':$('input[name="hide_category_id"]').val()
            },
            beforeSend: function( xhr ) { 

            	
            	
            },
            success: function(response){
            	console.log('========= response =========');
            	console.log(response);
                if(response.status){ 
                	

                }else{
                	
                }

                
            },
            error: function (request, status, error) {
                console.log(request.responseText);
            }
    });


}