$(document).ready(function(){
	validateForm();
});

function validateForm(){
	var base_url = $('input[name="base_url"]').val();

	$('form[name="edit-profile-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			firstname:{
					validators:{
						notEmpty:{
							message:''
						}
					}
			},
			lastname: {
                    validators:{
                        notEmpty: {
                            message:''
                        }
                    }
                    
            },
            telephone:{
            		validators:{
            			notEmpty:{
            				message:''
            			}
            		}
            }
		},
		onSuccess: function(e, data) { 
			
        }
    });
}