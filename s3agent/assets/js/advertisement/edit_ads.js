$(document).ready(function(){
    var base_url = $('input[name="base_url"]').val();
    $('form[name="create-advertisement-campaign-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
                }

            }
    });

    $('input[name="duration"]').daterangepicker({
            autoApply:true,
            timePicker: true,
            timePicker24Hour: true,
            //timePickerIncrement: 30,
            // minDate:moment(new Date()),
            locale: {
                format: 'DD-MM-YYYY HH:mm'
            }
    }, function(start, end, label) {
      console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
});


});


$('.pop').click(function(){
        var imgurl = $(this).parent().parent().find('img').attr('src');
        console.log(imgurl);
        $('#imagepreview').attr('src', imgurl); // here asign the image to the modal when the user click the enlarge link
        $('#imagemodal').modal('show'); // imagemodal is the id attribute assigned to the bootstrap modal, then i use the show function


});
