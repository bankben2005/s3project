$(document).ready(function(){
	initialDuration();
	validateForm();
});

function initialDuration(){
	    $('input[name="duration"]').daterangepicker({
            autoApply:true,
            timePicker: true,
            timePicker24Hour: true,
            //timePickerIncrement: 30,
            // minDate:moment(new Date()),
            locale: {
                format: 'DD-MM-YYYY HH:mm'
            }
	    }, function(start, end, label) {
	      console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
		});
}

function validateForm(){
	var base_url = $('input[name="base_url"]').val(); 

    $('form[name="create-channel-timeslots-form"]').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        integer:{
                        	message:''
                        }
                    }
                },
                lowest_budget:{
                    validators:{
                        notEmpty:{
                            message:''
                        },
                        integer:{
                        	message:''
                        }
                    }
                },
                highest_budget: {
                    validators: {
                        notEmpty:{
                            message:''
                        }
                    }
                }
            }
    });
}