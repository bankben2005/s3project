
$(document).ready(function(){
		$('button.fc-prev-button').click(function(){
	   		alert('prev is clicked, do something');
		});

		$('button.fc-next-button').click(function(){
		   alert('nextis clicked, do something');
		});
});

document.addEventListener('DOMContentLoaded', function() {
	// var calendarEl = document.getElementById('calendar');
	// var initialLocaleCode = 'th';

	// var calendar = new FullCalendar.Calendar(calendarEl, {
	// 	plugins: [ 'interaction', 'dayGrid', 'timeGrid' ],
	// 	header: {
	// 		left: 'prev,next today',
	// 		center: 'title',
	// 		right: 'dayGridMonth,timeGridWeek,timeGridDay'
	// 	},
	// 	defaultDate: $('input[name="hide_current_date"]').val(),
	// 	locale: initialLocaleCode,
 //      navLinks: true, // can click day/week names to navigate views
 //      selectable: true,
 //      selectMirror: true,
 //      select: function(arg) { 

 //      	// console.log(arg);
 //       //  var title = prompt('Event Title:');
 //       //  if (title) {
 //       //    calendar.addEvent({
 //       //      title: title,
 //       //      start: arg.start,
 //       //      end: arg.end,
 //       //      allDay: arg.allDay
 //       //    })
 //       //  }
 //       //  calendar.unselect();
 //       showModalSetTimeSlot(arg);
 //   },
 //   editable: true,
 //      eventLimit: true, // allow "more" link when too many events
 //      events: [
 //      {
 //      	title: 'All Day Event',
 //      	start: '2019-08-01'
 //      },
 //      {
 //      	title: 'Long Event',
 //      	start: '2019-08-07',
 //      	end: '2019-08-10'
 //      },
 //      {
 //      	groupId: 999,
 //      	title: 'Repeating Event',
 //      	start: '2019-08-09T16:00:00'
 //      },
 //      {
 //      	groupId: 999,
 //      	title: 'Repeating Event',
 //      	start: '2019-08-16T16:00:00'
 //      },
 //      {
 //      	title: 'Conference',
 //      	start: '2019-08-11',
 //      	end: '2019-08-13'
 //      },
 //      {
 //      	title: 'Meeting',
 //      	start: '2019-08-12T10:30:00',
 //      	end: '2019-08-12T12:30:00'
 //      },
 //      {
 //      	title: 'Lunch',
 //      	start: '2019-08-12T12:00:00'
 //      },
 //      {
 //      	title: 'Meeting',
 //      	start: '2019-08-12T14:30:00'
 //      },
 //      {
 //      	title: 'Happy Hour',
 //      	start: '2019-08-12T17:30:00'
 //      },
 //      {
 //      	title: 'Dinner',
 //      	start: '2019-08-12T20:00:00'
 //      },
 //      {
 //      	title: 'Birthday Party',
 //      	start: '2019-08-13T07:00:00'
 //      },
 //      {
 //      	title: 'Click for Google',
 //      	url: 'http://google.com/',
 //      	start: '2019-08-28'
 //      }
 //      ]
 //  });

	// calendar.render();

	initialRenderCalendar();
	initialDuration();
	validateForm();





});


function initialDuration(){
	$('input[name="duration"]').daterangepicker({
		autoApply:true,
		timePicker: true,
		timePicker24Hour: true,
            //timePickerIncrement: 30,
            // minDate:moment(new Date()),
            locale: {
            	format: 'DD-MM-YYYY HH:mm'
            }
        }, function(start, end, label) {
        	console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
        });
}


function showModalSetTimeSlot(arg){ 

	console.log(' === arg === ');
	console.log(arg);

	$('#modalSetTimeSlot').modal('toggle'); 

	// $('input[name="duration"]').daterangepicker({
	// 	'startDate':'02-01-2020 00:00',
	// 	'endDate':'03-01-2020 00:00',
	// 	locale: {
 //                format: 'DD-MM-YYYY HH:mm'
 //            }
	// });
	var startdate = moment(arg.start).format('DD-MM-YYYY HH:mm');
	var enddate = moment(arg.end);
	enddate = enddate.subtract(1,'days').format('DD-MM-YYYY HH:mm');
	console.log(enddate);
	$('input[name="duration"]').data('daterangepicker').setStartDate(startdate);
	$('input[name="duration"]').data('daterangepicker').setEndDate(enddate); 

	$('input[name="hide_channel_timeslot_id"]').val('');
	$('input[name="name"]').val('');
	$('input[name="lowest_budget"]').val('');
	$('input[name="highest_budget"]').val('');
	$('#slot-package-div').html('');

}

function validateForm(){
	var base_url = $('input[name="base_url"]').val(); 

	$('form[name="edit-timeslot-form"]').bootstrapValidator({
		message: 'This value is not valid',
		feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
			validating: 'glyphicon glyphicon-refresh'
		},
		excluded: [':disabled'],
		fields: {
			name:{
                    validators:{
                        notEmpty:{
                            message:''
                        }
                    }
            },
		},
		onSuccess: function(e, data) { 

			// check add at least 1 record into  
			e.preventDefault();

			var form_data = $('form[name="edit-timeslot-form"]').serialize(); 

			//console.log(form_data);

			//return false;

			$.ajax({
				url: base_url+"TimeSlot/ajaxSetChannelTimeslot",
				type: "post",
				data: form_data,
				async:true,
				dataType:'json',
				success: function (response) {
					console.log('==response==');
					console.log(response); 

					$('#modalSetTimeSlot').modal('toggle');

					rerenderCalendarFromDB();

				},
				error: function (request, status, error) {
					console.log(request.responseText);
				}


			});

			// $('#calendar').addEvent({
			// 	title: 'Birthday Party',
   //    			start: '2019-08-13T07:00:00'
			// });


			

			
			
		}
	});
}

function initialRenderCalendar(){
	var base_url = $('input[name="base_url"]').val(); 

	$.ajax({
		url: base_url+"TimeSlot/ajaxGetEventForRenderCalendar",
		type: "post",
		data: {},
		async:true,
		dataType:'json',
		beforeSend:function(){
			//$('#calendar').html('').html('<center><i class="fa fa-spin fa-spinner"></i></center>');

		},
		success: function (response) {
			console.log('==response==');
			console.log(response); 
				$('#calendar').fullCalendar({
						      		header: {
						      			left: 'prev,next today',
						      			center: 'title',
						      			right: 'month,agendaWeek'
						      		},
						      		displayEventTime: false,
						      		selectable:true,
						      		editable: true,
						      		droppable: true, // this allows things to be dropped onto the calendar
						      		events:response.events,
						      		select: function(startDate, endDate, jsEvent, view, resource) {

						      			var arg = {
						      				'start':startDate.format(),
						      				'end':endDate.format()
						      			};
						      			console.log(startDate);
						      			console.log(endDate);
						      			showModalSetTimeSlot(arg);
						      		},
						   //    		dayClick: function(date, jsEvent, view) {

									//     console.log(date.format());
									//     showModalSetTimeSlot({
									//     	'date':date.format()
									//     });

									// },
						      		eventClick:function(info){ 

						      			console.log(info);
						      			var start_datetime = moment(info.start);
								      	var end_datetime = moment(info.end); 

								      	console.log('== information ==');
								      	console.log(info);
								      	console.log(start_datetime.format('DD-MM-YYYY HH:mm'));
								      	console.log(end_datetime.format('DD-MM-YYYY HH:mm')); 

								      	var data_info = {
								      		'id':info.groupId,
								      		'title':info.title,
								      		'start_datetime':start_datetime,
								      		'end_datetime':end_datetime,
								      		'lowest_budget':info.extendedProps.lowest_budget,
								      		'highest_budget':info.extendedProps.highest_budget,
								      		'title_for_show':info.extendedProps.title_for_show,
								      		'channel_timeslot_has_packages':info.extendedProps.channel_timeslot_has_packages
								      	};	
								      	$('div#slot-package-div').html('');
								      	showModelSetTimeSlotEdit(data_info);
						      		},

						      		viewRender: function(view, element) {
						        		var b = $('#calendar').fullCalendar('getDate');
						        		var firstdateOfMonth = b.format('L');
						        		var spl_date = firstdateOfMonth.split('/');

						        		console.log(spl_date);
						        		clickNextPrevMonth({
						        			'select_month':spl_date[1],
						        			'select_year':spl_date[2]
						        		});
						    		}
						    });

			




		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}


	});

}
function rerenderCalendarFromDB(){ 
	var base_url = $('input[name="base_url"]').val(); 

	var b = $('#calendar').fullCalendar('getDate');
	var firstdateOfMonth = b.format('L');
	var spl_date = firstdateOfMonth.split('/');

	$.ajax({
		url: base_url+"TimeSlot/ajaxGetEventForRenderCalendar",
		type: "post",
		data: {
			'select_month':spl_date[1],
			'select_year':spl_date[2]
		},
		async:true,
		dataType:'json',
		beforeSend:function(){
		},
		success: function (response) {
			console.log('==response==');
			console.log(response); 

			$('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('addEventSource', response.events);         
            $('#calendar').fullCalendar('rerenderEvents' );
            $('form[name="edit-timeslot-form"]').data('bootstrapValidator').resetForm();

		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}


	});


}

function renderCalendarByEventResponse(response){ 

}

function showModelSetTimeSlotEdit(data_info){

	$('#modalSetTimeSlot').modal('toggle');
	//console.log(data_info);  
	$('input[name="hide_channel_timeslot_id"]').val(data_info.id);
	$('input[name="name"]').val(data_info.title);  

	var startdate = data_info.start_datetime.format('DD-MM-YYYY HH:mm');
	var enddate = data_info.end_datetime.format('DD-MM-YYYY HH:mm');

	$('input[name="duration"]').data('daterangepicker').setStartDate(startdate);
	$('input[name="duration"]').data('daterangepicker').setEndDate(enddate);
	$('input[name="lowest_budget"]').val(data_info.lowest_budget);
	$('input[name="highest_budget"]').val(data_info.highest_budget); 

	if(data_info.channel_timeslot_has_packages.length > 0){
		console.log('this package');
		console.log(data_info.channel_timeslot_has_packages);

		$.each(data_info.channel_timeslot_has_packages,function(index,value){
			
			var additional_data = {};
			additional_data = {
				'index':index,
				'value':value
			};

			console.log('=== additional_data ==');
			console.log(additional_data);


			var anchor_element = $('a[onclick="clickAddTimeSlotPackage(this)"]'); 

			
			setTimeout(function(){
					clickAddTimeSlotPackage(anchor_element,additional_data);  
			}, 50);
			

			
		});
		
	}




}


function clickNextPrevMonth(select_month_year){
	var base_url = $('input[name="base_url"]').val(); 

	$.ajax({
		url: base_url+"TimeSlot/ajaxGetEventForRenderCalendar",
		type: "post",
		data: select_month_year,
		async:true,
		dataType:'json',
		beforeSend:function(){
		},
		success: function (response) {
			console.log('==response==');
			console.log(response); 

			$('#calendar').fullCalendar('removeEvents');
            $('#calendar').fullCalendar('addEventSource', response.events);         
            $('#calendar').fullCalendar('rerenderEvents' );

		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}


	});

	
}


function clickAddTimeSlotPackage(element,additional_data=null){
	var element = $(element); 
	var additional_data = additional_data;


	var base_url = $('input[name="base_url"]').val(); 

	$.ajax({
		url: base_url+"TimeSlot/ajaxGetAddTimeslotPackageRow",
		type: "post",
		data: {},
		async:true,
		dataType:'json',
		beforeSend:function(){ 

		},
		success: function (response) {
			console.log('==response==');
			console.log(response); 

			$('div#slot-package-div').append(response.timeslot_package_row).ready(function(){
				if(additional_data){

						// var select_element = $('select.timeslot_package').eq(additional_data.index);
						// var input_element = $('input.package_price').eq(additional_data.index);
						// select_element.val(additional_data.value.channel_timeslot_packages_id);
						// input_element.val(additional_data.value.budget);
					setTimeout(function(){
						var select_element = $('select.timeslot_package').eq(additional_data.index);
						var input_element = $('input.package_price').eq(additional_data.index);
						select_element.val(additional_data.value.channel_timeslot_packages_id);
						input_element.val(additional_data.value.budget);
					}, 100);
				}
			});

			
			


		},
		error: function (request, status, error) {
			console.log(request.responseText);
		}


	});




}

function removePackageRow(element){
	var element = $(element);

	element.closest('div.row').remove();
}