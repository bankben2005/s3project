<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url('admin/'.$this->controller)?>"><?php echo __('PROGRAM LIST')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo (@$program->id)?__('Edit program'):__('Create program')?></li>
  </ol>
</nav>


<div class="row">
  <div class="col-lg-12">
    <?php echo message_warning($this)?>
  </div>
</div>


          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo (@$program->id)?__('Edit program form'):__('Create program form')?> 

          		</h6> 


            </div>
            <div class="card-body"> 

              <?php 
                $image_path = base_url('uploaded/program/'.@$program->id.'/'.@$program->cover); 

                $cover_obj = new StdClass();
                $cover_obj->caption = $program->cover;
                $cover_obj->size = $program->cover_size;
                $cover_obj->width = '120px';
                $cover_obj->url = base_url('admin/'.$this->controller.'/deleteProgramCover');
                $cover_obj->key = $program->id; 
              ?> 

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_program_id',
                'value'=>@$program->id
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover',
                'value'=>(@$program->cover)?json_encode([$image_path]):'[]'
              ])?>

              <?php echo form_input([
                'type'=>'hidden',
                'name'=>'hide_cover_caption_obj',
                'value'=> json_encode([$cover_obj])
              ])?>  

              
            	<?php echo form_open_multipart('',['name'=>'create-program-form'])?> 

                <div class="row">
                  <div class="col-lg-4">
                    <?php //echo $program->api_channels_id?>

                    <div class="form-group">
                      <label><strong><?php echo __('Select Channel')?> : </strong></label>
                      <?php echo form_dropdown('select_channel',@$select_channel,@$program->api_channels_id,'class="form-control"')?>
                    </div>


                    <div class="form-group">
                      <label><strong><?php echo __('Name')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'name',
                        'class'=>'form-control',
                        'value'=>@$program->program_name
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('Description')?> : </strong></label>
                      <?php echo form_textarea([
                        'name'=>'description',
                        'class'=>'form-control',
                        'value'=>@$program->program_description,
                        'rows'=>4
                      ])?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('On air day')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'on_air_day',
                        'class'=>'form-control',
                        'value'=>@$program->on_air_day
                      ])?>
                    </div> 

                    <div class="form-group">
                      <label><strong><?php echo __('On air time')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'on_air_time',
                        'class'=>'form-control',
                        'value'=>@$program->on_air_time
                      ])?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Duration')?> : </strong></label>
                      <?php echo form_input([
                        'name'=>'duration',
                        'class'=>'form-control',
                        'value'=>@$duration
                      ])?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Is Hightlight program')?> : </strong></label>
                      <?php echo form_dropdown('is_highlight_program',[
                        '0'=>__('No Highlight'),
                        '1'=>__('Highlight')
                      ],@$program->is_highlight_program,'class="form-control"')?>
                    </div>

                    <div class="form-group">
                      <label><strong><?php echo __('Status')?> : </strong></label>
                      <?php echo form_dropdown('active',[
                        '1'=>__('Active','default'),
                        '0'=>__('Unactive','default')
                      ],@$program->active,'class="form-control"')?>
                    </div>


                  </div>

                  <div class="col-lg-8">
                    <div class="form-group">
                      <label><strong><?php echo __('Cover')?> : </strong></label>
                      <div class="upload-box mb-3">
                              <input type="file" class="cover" name="cover" class="file-loading" />
                          </div>
                    </div>



                  </div>

                </div> 

                <div class="row">
                  <div class="col-lg-12">
                    <div class="form-group">
                      <?php echo form_button([
                        'type'=>'submit',
                        'class'=>'btn btn-success float-right',
                        'content'=>__('Submit','default')
                      ])?>
                    </div>
                  </div>
                </div>

              <?php echo form_close()?>

          </div>
        </div>