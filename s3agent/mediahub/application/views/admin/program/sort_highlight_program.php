<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('HIGHLIGHT PROGRAM LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('HIGHLIGHT PROGRAM LIST')?> 
              
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Cover')?></th>
                      <th><?php echo __('Name')?></th>
                      <th><?php echo __('Duration')?></th>
                      <th><?php echo __('Ordinal')?></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($program as $key => $row){?>
                    <tr>
                      <td>
                        <a href="javascript:void(0)">
                          <i class="fa fa-arrows-alt"></i>
                        </a>
                      </td>
                      <td>
                          <img src="<?php echo base_url('uploaded/program/'.$row->id.'/'.$row->cover)?>" style="max-width: 80px;" />
                      </td>
                      <td><?php echo $row->program_name?></td>
                      <td><?php echo date('d-m-Y H:i',strtotime($row->program_start_datetime)).' - '.date('d-m-Y H:i',strtotime($row->program_end_datetime))?></td>
                      
                      <td>
                          <?php echo form_input([
                            'type'=>'hidden',
                            'name'=>'ordinal_highlight_program[]',
                            'value'=>$row->ordinal,
                            'data-programid'=>$row->id
                          ])?>
                          <span class="show_ordinal_program"><?php echo $row->ordinal?></span>
                      </td>
                    </tr>
                    <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
</div>