<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('PROGRAM LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('PROGRAM LIST')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url('admin/'.$this->controller.'/createProgram')?>"><i class="fa fa-plus"></i> <?php echo __('Add program')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Cover')?></th>
                      <th><?php echo __('Name')?></th>
                      <th><?php echo __('Duration')?></th>
                      <th><?php echo __('Highlight Program')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th><?php echo __('Status','default')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>
                    <?php foreach($program as $key => $row){?>
                      <tr>
                        <td><?php echo ++$key?></td>
                        <td>
                          <img src="<?php echo base_url('uploaded/program/'.$row->id.'/'.$row->cover)?>" style="max-width: 80px;" />
                        </td>
                        <td><?php echo $row->program_name?></td>
                        <td><?php echo date('d-m-Y H:i',strtotime($row->program_start_datetime)).' - '.date('d-m-Y H:i',strtotime($row->program_end_datetime))?></td>
                        <td>
                          <?php if($row->is_highlight_program){?>
                            <span class="badge badge-success"><?php echo __('Highlight')?></span>
                          <?php }else{?>
                            <span class="badge badge-secondary"><?php echo __('No Highlight')?></span>
                          <?php }?>
                        </td>
                        <td><?php echo $row->updated?></td>
                        <td>
                          <?php if($row->active){?>
                            <span class="badge badge-success"><?php echo __('Active','default')?></span>
                          <?php }else{?>
                            <span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
                          <?php }?>
                        </td>
                        <td> 


                          <a href="<?php echo base_url('admin/'.$this->controller.'/editProgram/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pen"></i></a> 

                          <a href="javascript:void(0);" class="btn btn-danger btm-sm"><i class="fa fa-trash"></i></a>


                        </td>
                      </tr>

                    <?php }?>
                  </tbody>
                </table>
              </div>
            </div>
</div>