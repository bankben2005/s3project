<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()?>"><?php echo __('Home','default')?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><?php echo __('PROGRAM LIST')?></li>
  </ol>
</nav>

<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo __('HIGHLIGHT PROGRAM')?> 
              <a class="btn btn-success float-right text-white" href="<?php echo base_url($this->controller.'/createProgram')?>"><i class="fa fa-plus"></i> <?php echo __('Add highlight program')?></a>
          </h6> 


            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th><?php echo __('Name')?></th>
                      <th><?php echo __('Duration')?></th>
                      <th><?php echo __('Created')?></th>
                      <th><?php echo __('Updated')?></th>
                      <th></th>
                    </tr>
                  </thead>

                  <tbody>

                  </tbody>
                </table>
              </div>
            </div>
</div>