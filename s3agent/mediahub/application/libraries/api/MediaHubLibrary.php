<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class MediaHubLibrary{


	private $ci;
	function __construct() {
		$CI =& get_instance();
		$this->ci = $CI;

	}

	public function getAccountLoginWithFacebook($requestCriteria){
		if(!property_exists($requestCriteria, 'facebook_id') || property_exists($requestCriteria, 'facebook_id') == ''){
			return array(
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'ไม่พบ Facebook ID'
			);
		}


		$query = $this->ci->db->select('*')
		->from('account')
		->where('facebook_id',$requestCriteria->facebook_id)
		->get();

		if($query->num_rows() > 0){
			/* exist */
			$row = $query->row();

			if(!$row->active){
				return array(
					'status'=>false,
					'result_code'=>'-009',
					'result_desc'=>'คุณไม่มีสิทธิ์เข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
				);

			}


			/* if found return all member data */
			$data_return  = $this->getLoginAccountDataByRow($row);
			return array(
				'status'=>true,
				'AccountData'=>$data_return,
				'result_code'=>'000',
				'result_desc'=>'Success'
			);


		}else{
			/* not exist */
			/* insert to new record in member table */
			$birthday_datetime = new DateTime($requestCriteria->birthday);

			$query = $this->ci->db->insert('account',array(
				'facebook_id'=>$requestCriteria->facebook_id,
				'firstname'=>$requestCriteria->firstname,
				'lastname'=>$requestCriteria->lastname,
				'email'=>$requestCriteria->email,
				'gender'=>ucfirst($requestCriteria->gender),
				'dob'=> $birthday_datetime->format('Y-m-d'),
				'phone'=>$requestCriteria->phone,
				'picture_profile'=>$requestCriteria->picture_profile,
				'created'=>date('Y-m-d H:i:s'),
				'uuid'=>@$requestCriteria->uuid,
				'os'=>@$requestCriteria->os,
				'os_version'=>@$requestCriteria->os_version,
				'device_type'=>@$requestCriteria->device_type

			));

			$insert_id = $this->ci->db->insert_id();

			$member_data = $this->ci->db->select('*')
			->from('account')
			->where('id',$insert_id)
			->get();

			return array(
				'status'=>true,
				'AccountData'=>$this->getLoginAccountDataByRow($member_data->row()),
				'result_code'=>'000',
				'result_desc'=>'Success'
			);

		}
	}

	public function getAccountLoginWithLine($requestCriteria){
		if(!property_exists($requestCriteria, 'line_id') || property_exists($requestCriteria, 'line_id') == ''){
			return array(
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'ไม่พบ Line ID'
			);
		}


		$query = $this->ci->db->select('*')
		->from('account')
		->where('line_id',$requestCriteria->line_id)
		->get();

		if($query->num_rows() > 0){
			/* exist */
			$row = $query->row();

			if(!$row->active){
				return array(
					'status'=>false,
					'result_code'=>'-009',
					'result_desc'=>'คุณไม่มีสิทธิ์เข้าใช้งาน กรุณาติดต่อผู้ดูแลระบบ'
				);
			}

			/* if found return all member data */
			$data_return  = $this->getLoginAccountDataByRow($row);
			return array(
				'status'=>true,
				'AccountData'=>$data_return,
				'result_code'=>'000',
				'result_desc'=>'Success'
			);


		}else{
			/* not exist */
			/* insert to new record in member table */

			$birthday_datetime = new DateTime(@$requestCriteria->birthday);


			$query = $this->ci->db->insert('account',array(
				'line_id'=>$requestCriteria->line_id,
				'firstname'=>$requestCriteria->firstname,
				'lastname'=>$requestCriteria->lastname,
				'email'=>$requestCriteria->email,
				'gender'=>ucfirst($requestCriteria->gender),
				'dob'=> $birthday_datetime->format('Y-m-d'),
				'phone'=>$requestCriteria->phone,
				'picture_profile'=>$requestCriteria->picture_profile,
				'created'=>date('Y-m-d H:i:s'),
				'uuid'=>@$requestCriteria->uuid,
				'os'=>@$requestCriteria->os,
				'os_version'=>@$requestCriteria->os_version,
				'device_type'=>@$requestCriteria->device_type

			));

			$insert_id = $this->ci->db->insert_id();
			$member_data = $this->ci->db->select('*')
			->from('account')
			->where('id',$insert_id)
			->get();

			return array(
				'status'=>true,
				'AccountData'=>$this->getLoginAccountDataByRow($member_data->row()),
				'result_code'=>'000',
				'result_desc'=>'Success'
			);


		}
	} 

	public function getAllHilightPrograms($requestCriteria){  

		$this->ci->load->library([
			'datamapper'
		]);

		$arrReturn = [];  



		$query = $this->ci->db->select('*')
		->from('program')
		->where('is_highlight_program',1)
		->where('active',1)
		->where('CONVERT(char(10), program_start_datetime,126) <= ',date('Y-m-d'))
		->where('CONVERT(char(10), program_end_datetime,126) >= ',date('Y-m-d'))
		->order_by('ordinal','asc')
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $row) {
				# code...
				$programRatingAndTimeSlot = $this->getProgramRatingAndTimeSlot([
					'program'=>$row
				]); 

				array_push($arrReturn, [
					'id'=>$row->id,
					'program_name'=>$row->program_name,
					'program_cover'=>base_url('uploaded/program/'.$row->id.'/'.$row->cover),
					'program_on_air_txt'=>$row->on_air_day,
					'program_channel'=>$row->api_channel_name,
					'program_duration_txt'=>$row->on_air_time,
					'program_rating'=>@$programRatingAndTimeSlot->avg_rating,
					'time_slots'=>@$programRatingAndTimeSlot->time_slots

				]);



			}
		}



		return [
			'status'=>true,
			'AllHighlightPrograms'=>$arrReturn,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];
		//return $arrReturn;



	}

	public function getDefaultTimeSlots($requestCriteria){
		

		if(!property_exists($requestCriteria, 'date') || $requestCriteria->date == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found date'
			];
		} 


		// get all from channel program  
		$arrReturn = [];


		$dateFilter = new DateTime($requestCriteria->date);

		$query = $this->ci->db->select('*')
		->from('program')
		->where('active',1)
		->where('CONVERT(char(10), program_start_datetime,126) <= ',$dateFilter->format('Y-m-d'))
		->where('CONVERT(char(10), program_end_datetime,126) >= ',$dateFilter->format('Y-m-d'))
		->order_by('is_highlight_program','desc')
		->order_by('ordinal','asc')
		->get(); 



		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $row) { 
				# code... 

				$programRatingAndTimeSlot = $this->getProgramRatingAndTimeSlot([
					'program'=>$row
				]); 

				if(isset($_GET['test']) && $_GET['test']=='test'){
					print_r($programRatingAndTimeSlot);
				}


				//print_r($programRatingAndTimeSlot);
				if($programRatingAndTimeSlot){
					array_push($arrReturn, $programRatingAndTimeSlot->time_slots);
				}
				
			}
		} 

		return [
			'status'=>true,
			'TimeSlots'=>$arrReturn,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];





	}

	public function getTop10Channels($requestCriteria){
		$content= file_get_contents('http://s3remoteservice.psi.co.th/rating/api/GetChannelHits?band_type=C');
		print_r($content);
	} 

	public function setTimeSlotReservation($requestCriteria){ 

		if(!property_exists($requestCriteria, 'account_id') || $requestCriteria->account_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id parameter'
			];	
		}  

		if(!property_exists($requestCriteria, 'date') || $requestCriteria->date == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found date parameter'
			];
		} 

		if(!property_exists($requestCriteria, 'channel_timeslots_id') || $requestCriteria->channel_timeslots_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found channel_timeslots_id parameter'
			];
		} 

		if(!property_exists($requestCriteria, 'slot_packages') || count($requestCriteria->slot_packages) == 0){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found slot_packages parameter,Or slot_packages equal to zero'
			];
		} 

		// check account id exist 
		if(!$this->checkAccountIdExist(['account_id'=>$requestCriteria->account_id])){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id,Plese try again.'
			];
		} 


		// check already exist by channel timeslots id and date

		$queryCheckExist = $this->ci->db->select('*')
		->from('reservations')
		->where('date',date('Y-m-d',strtotime($requestCriteria->date)))
		->where('api_channel_timeslots_id',$requestCriteria->channel_timeslots_id)
		->get(); 

		//echo $queryCheckExist->num_rows();exit;

		if($queryCheckExist->num_rows() > 0){
			// check again find in reservation_items 

			$row_reservation = $queryCheckExist->row(); 


			foreach ($requestCriteria->slot_packages as $key => $value) {
				# code...
				$queryCheckItemExist = $this->ci->db->select('*')
				->from('reservation_items')
				->where('reservations_id',$row_reservation->id)
				->where('api_has_packages_id',$value)
				->get();

				if($queryCheckItemExist->num_rows() <= 0){
					// insert new record 

					//$rowItem = $queryCheckItemExist->row();

					// getChannelTimeSlotHasPackages data
					$timeslot_has_package_data = $this->getChannelTimeSlotHasPackageData([
						'api_has_packages_id'=>$value
					]);  

					$this->ci->db->insert('reservation_items',[
						'reservations_id'=>$row_reservation->id,
						'api_has_packages_id'=>$value,
						'package_name_txt'=>$timeslot_has_package_data->returnData->package_name_txt,
						'duration_txt'=>$timeslot_has_package_data->returnData->duration_txt,
						'program_name_txt'=>$timeslot_has_package_data->returnData->program_name_txt,
						'package_cost_txt'=>$timeslot_has_package_data->returnData->package_cost_txt,
						'created'=>date('Y-m-d H:i:s')
					]);

				}
			}
		}else{
			// insert normal step 

			// insert into reservations table
			$this->ci->db->insert('reservations',[
				'account_id'=>$requestCriteria->account_id,
				'api_channel_timeslots_id'=>$requestCriteria->channel_timeslots_id,
				'date'=>date('Y-m-d',strtotime($requestCriteria->date)),
				'created'=>date('Y-m-d H:i:s')
			]);

			$insert_id = $this->ci->db->insert_id(); 


			foreach ($requestCriteria->slot_packages as $key => $value) { 

				$timeslot_has_package_data = $this->getChannelTimeSlotHasPackageData([
						'api_has_packages_id'=>$value
				]);  

					$this->ci->db->insert('reservation_items',[
						'reservations_id'=>$insert_id,
						'api_has_packages_id'=>$value,
						'package_name_txt'=>$timeslot_has_package_data->returnData->package_name_txt,
						'duration_txt'=>$timeslot_has_package_data->returnData->duration_txt,
						'program_name_txt'=>$timeslot_has_package_data->returnData->program_name_txt,
						'package_cost_txt'=>$timeslot_has_package_data->returnData->package_cost_txt,
						'created'=>date('Y-m-d H:i:s')
					]);
			}

		}


		return [
			'status'=>true,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];




	}

	public function getHistoriesReservation($requestCriteria){ 

		if(!property_exists($requestCriteria, 'account_id') || $requestCriteria->account_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id parameter'
			];	
		} 

		// check account id exist 
		if(!$this->checkAccountIdExist(['account_id'=>$requestCriteria->account_id])){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id,Plese try again.'
			];
		} 

		$arrReturn = [];

		// get data from reservations table
		$query = $this->ci->db->select('*')
		->from('reservations')
		->where('account_id',$requestCriteria->account_id)
		->where('active',1)
		->get(); 

		$arrChannels = [];

		if($query->num_rows() > 0){ 

			foreach ($query->result() as $key => $row) {
				// get channel time slots data by id 
				$channelTimeSlotData = $this->getChannelTimeSlotDataByApiId([
					'api_channel_timeslots_id'=>$row->api_channel_timeslots_id
				]); 

				if(isset($_GET['test']) && $_GET['test'] == 'test'){ 

					print_r($row);exit;
					//print_r($row->api_channel_timeslots_id);exit;
					print_r($channelTimeSlotData);exit;
				}

				array_push($arrChannels, [
					'date'=>$row->date,
					'channels_id'=>$channelTimeSlotData->row->id,
					'channel'=>$channelTimeSlotData->row->channel_name,
					'date_txt'=>date('d M Y',strtotime($row->date)),
					'reservation_lists'=>$this->getReservationListByReservationId([
						'reservations_id'=>$row->id
					])
				]);

				array_push($arrReturn, $channelTimeSlotData->row);
			}

			

		} 
 
 		return [
 			'status'=>true,
 			'ReservationHitories'=>$arrChannels,
 			'result_code'=>'000',
 			'result_desc'=>'Success'
 		];

	}

	public function getViewAdvertisement($requestCriteria){ 

		if(!property_exists($requestCriteria, 'account_id') || $requestCriteria->account_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id parameter'
			];	
		}  

		if(!property_exists($requestCriteria, 'month') || $requestCriteria->month == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found month parameter'
			];
		} 

		if(!property_exists($requestCriteria, 'year') || $requestCriteria->year == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found year parameter'
			];
		} 

		// check account id exist 
		if(!$this->checkAccountIdExist(['account_id'=>$requestCriteria->account_id])){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id,Plese try again.'
			];
		}  



		$arrReturn = [];

		// get from reservation table

		$query = $this->ci->db->select('*')
		->from('reservations')
		->where('MONTH(date)',$requestCriteria->month)
		->where('YEAR(date)',$requestCriteria->year)
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $row) {
				# code...
				$channelTimeSlotData = $this->getChannelTimeSlotDataByApiId([
					'api_channel_timeslots_id'=>$row->api_channel_timeslots_id
				]); 

				if($channelTimeSlotData){
					array_push($arrReturn, [
						'date'=>$row->date,
						'channels_id'=>$channelTimeSlotData->row->id,
						'channel'=>$channelTimeSlotData->row->channel_name,
						'date_txt'=>date('d M Y',strtotime($row->date)),
						'reservation_lists'=>$this->getReservationListByReservationId([
							'reservations_id'=>$row->id
						])
					]);
				}

				//array_push($arrReturn, $channelTimeSlotData->row);
			}
		}

		// print_r($arrReturn); 

		return [
			'status'=>true,
			'month_year_txt'=>date('M Y',strtotime('01-'.$requestCriteria->month.'-'.$requestCriteria->year)),
			'Advertisements'=>$arrReturn,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];








		


	}

	public function setAccountInformation($requestCriteria){
		// print_r($requestCriteria); 

		if(!property_exists($requestCriteria, 'account_id') || $requestCriteria->account_id == ''){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id parameter'
			];	
		}  

		// check account id exist 
		if(!$this->checkAccountIdExist(['account_id'=>$requestCriteria->account_id])){
			return [
				'status'=>false,
				'result_code'=>'-002',
				'result_desc'=>'Not found account_id,Plese try again.'
			];
		}  

		$this->ci->db->update('account',[
			'firstname'=>$requestCriteria->firstname,
			'lastname'=>$requestCriteria->lastname,
			'phone'=>$requestCriteria->telephone,
			'address'=>$requestCriteria->address,
		],['id'=>$requestCriteria->account_id]);

		return [
			'status'=>true,
			'result_code'=>'000',
			'result_desc'=>'Success'
		];







	}

	private function getLoginAccountDataByRow($row){
		$firstname = $row->firstname;
		$lastname = $row->lastname;

		$firstname = (strpos($firstname, '?') !== false)?str_replace('?', '', $firstname):$firstname;

		$lastname = (strpos($lastname, '?') !== false)?str_replace('?', '', $lastname):$lastname;


		return array(
			'id'=>$row->id,
			'firstname'=>$firstname,
			'lastname'=>$lastname,
			'email'=>@$row->email,
			'gender'=>@$row->gender,
			'language'=>@$row->language,
			'age'=>@$row->age,
			'dob'=>@date('d/m/Y',strtotime($row->dob)),
			'phone'=>@$row->phone,
			'picture_profile'=>@$row->picture_profile
		);
	} 

	private function checkAccountIdExist($data = []){

		$query = $this->ci->db->select('id,active')
		->from('account')
		->where('id',$data['account_id'])
		->where('active',1)
		->get();

		if($query->num_rows() > 0){
			return true;
		}else{
			return false;
		}
	}

	private function getProgramRatingAndTimeSlot($data = []){ 

		$response = $this->curlGetProgramRatingAndTimeSlot($data);
		return $response;



	}

	private function getReservationListByReservationId($data = []){ 

		$arrReturn = [];

		$query = $this->ci->db->select('*')
		->from('reservation_items')
		->where('reservations_id',$data['reservations_id'])
		->get();

		if($query->num_rows() > 0){
			foreach ($query->result() as $key => $row) {
				# code... 
				array_push($arrReturn, [
					'duration_txt'=>$row->duration_txt,
					'program_name_txt'=>$row->program_name_txt,
					'package_time_txt'=>$row->package_name_txt,
					'package_cost_txt'=>$row->package_cost_txt
				]);
			}
			
		} 

		return $arrReturn;
	}

	private function curlGetProgramRatingAndTimeSlot($data = []){ 

		$program = $data['program'];

		$url = $this->getCurlGetProgramRatingAndTimeSlotUrl();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, [
			'start_datetime'=>$program->program_start_datetime,
			'end_datetime'=>$program->program_end_datetime,
			'channels_id'=>$program->api_channels_id
		]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);  


		$response = json_decode($response); 

		return $response;

	} 

	private function getChannelTimeSlotDataByApiId($data = []){


		$url = $this->getCurlGetChannelTimeSlotDataByIdUrl();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, [
			'channel_timeslots_id'=>$data['api_channel_timeslots_id']
		]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);  

		//echo $response;exit;


		$response = json_decode($response); 

		return $response;
	} 

	private function getChannelTimeSlotHasPackageData($data = []){
		$url = $this->getCurlGetChannelTimeSlotHasPackageDataByIdUrl();

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, [
			'channel_timeslot_has_packages_id'=>$data['api_has_packages_id']
		]);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
		curl_close($ch);  

		//echo $response;exit;


		$response = json_decode($response); 

		return $response;
	}


	private function getCurlGetProgramRatingAndTimeSlotUrl(){
		switch (ENVIRONMENT) {
			case 'development':
			return 'http://s3remoterating.development/MediaHubServices/mediaHubGetProgramRatingAndTimeSlot';
			break;

			case 'testing':

			break; 

			case 'production':
			return 'http://s3remoteservice.psi.co.th/rating/MediaHubServices/mediaHubGetProgramRatingAndTimeSlot';
			break;
			
			default:
				# code...
			break;
		}
	} 

	private function getCurlGetChannelTimeSlotDataByIdUrl(){
		switch (ENVIRONMENT) {
			case 'development':
				return 'http://s3remoterating.development/MediaHubServices/getChannelTimeSlotDataById';
			break; 

			case 'testing':

			break;

			case 'production':
				return 'http://s3remoteservice.psi.co.th/rating/MediaHubServices/getChannelTimeSlotDataById';
			break;
			
			default:
				# code...
			break;
		}
	}

	private function getCurlGetChannelTimeSlotHasPackageDataByIdUrl(){
		switch (ENVIRONMENT) {
			case 'development':
				return 'http://s3remoterating.development/MediaHubServices/getChannelTimeSlotHasPackageDataById';
			break; 

			case 'testing':

			break;

			case 'production':
				return 'http://s3remoteservice.psi.co.th/rating/MediaHubServices/getChannelTimeSlotHasPackageDataById';
			break;
			
			default:
				# code...
			break;
		}
	}


}