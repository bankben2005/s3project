<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AdminController.php';
class Program extends AdminController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){ 

    	$program = new M_program();
    	$program->order_by('id','desc')
    	->get();

    	$data = [
    		'program'=>$program
    	];


    	$this->template->content->view('admin/program/program_list',$data);
    	$this->template->publish();

    }

    public function highlight(){

    	$data = [

    	];


    	$this->template->content->view('admin/program/highlight_program_list',$data);
    	$this->template->publish();
    } 

    public function sort_highlight(){  

    	$this->template->javascript->add('https://code.jquery.com/ui/1.12.0/jquery-ui.min.js');

		$this->template->javascript->add(base_url('assets/admin/js/program/sort_highlight.js'));

    	$highlight_program = new M_program();
    	$highlight_program->where('active',1)
    	->where('is_highlight_program',1)
    	->order_by('ordinal','asc')
    	->get();


    	$data = [
    		'program'=>$highlight_program
    	]; 

    	$this->template->content->view('admin/program/sort_highlight_program',$data);
    	$this->template->publish();
    }

    public function ajaxUpdateOrdinalHighlightProgram(){
    	$post_data = $this->input->post(); 

    	if(count($post_data['arr_post']) > 0){
    		foreach ($post_data['arr_post'] as $key => $value) {
    			# code...
    			$this->db->update('program',[
    				'ordinal'=>$value['ordinal'],
    				'updated'=>date('Y-m-d H:i:s')
    			],['id'=>$value['program_id']]);

    		}
    	}


    	echo json_encode([
    		'status'=>true,
    		'post_data'=>$post_data
    	]);
    }

    public function createProgram(){
    	$this->__createProgram();
    }
    public function editProgram($id){

    	$query = $this->db->select('id,active')
    	->from('program')
    	->where('id',$id)
    	->where('active',1)
    	->get();

    	if($query->num_rows() <= 0){
    		redirect('admin/'.$this->controller);
    	}
    	$this->__createProgram($id);
    }

    public function deleteProgram($id){

    	$query = $this->db->select('id,active')
    	->from('program')
    	->where('id',$id)
    	->where('active',1)
    	->get();

    	if($query->num_rows() <= 0){
    		redirect('admin/'.$this->controller);
    	}

    }

    public function deleteProgramCover(){
    	$key = $this->input->post('key');


    	$program = new M_program($key);

    	if($program->id){

    	}

    	echo json_encode([
    		'valid'=>true
    	]);
    }

    private function __createProgram($id = null){ 

    	$this->loadValidator();

    	$this->loadDateRangePickerStyle();
    	$this->loadBootstrapFileuploadMasterStyle();

    	$this->loadDateRangePickerScript();
    	$this->loadBootstrapFileuploadMasterScript();


    	if($id){
    		$this->template->javascript->add(base_url('assets/admin/js/program/edit_program.js'));
    	}else{
    		$this->template->javascript->add(base_url('assets/admin/js/program/create_program.js'));
    	}


    	$program = new M_program($id);

    	if($this->input->post(NULL,FALSE)){ 

    		$exDuration = explode(' - ', $this->input->post('duration'));

    		$start_datetime = new DateTime($exDuration[0]);
    		$end_datetime = new DateTime($exDuration[1]);



    		$program->program_name = $this->input->post('name');
    		$program->program_description = $this->input->post('description');
    		$program->program_start_datetime = $start_datetime->format('Y-m-d H:i:s');
    		$program->program_end_datetime = $end_datetime->format('Y-m-d H:i:s');
    		$program->on_air_day = $this->input->post('on_air_day');
    		$program->on_air_time = $this->input->post('on_air_time');
    		$program->is_highlight_program = $this->input->post('is_highlight_program');
    		$program->api_channels_id = $this->input->post('select_channel');
    		$program->api_channel_name = $this->getApiChannelNameByChannelId([
    			'api_channels_id'=>$this->input->post('select_channel')
    		]);
    		$program->active = $this->input->post('active');

    		if($program->save()){
    			/* check upload image */
				if($_FILES['cover']['error'] == 0){
					$this->uploadCoverImage(array(
						'program_id'=>$program->id
					));
				}
				/* eof check upload image */ 

				$txtMessage = ($id)?__('Edit program success','admin/program/create_program'):__('Create program success','admin/program/create_program'); 

				$this->msg->add($txtMessage,'success');

				redirect($this->uri->uri_string());
    		}






    	}

    	// $this->getSelectAllChannelRating();
    	// exit;


    	$data = [
    		'program'=>$program,
    		'select_channel'=>$this->getSelectAllChannelRating(),
    		'duration'=>$this->getDurationForInputText([
    			'program'=>$program
    		])
    	]; 

    	// print_r($data);


    	$this->template->content->view('admin/program/create_program',$data);
    	$this->template->publish();




    }

    private function getSelectAllChannelRating(){ 

    	$arrSelect = [];
    	$arrSelect[''] = __('Select Channel','admin/program/create_program');
    	$url = 'http://s3remoteservice.psi.co.th/rating/welcome/getAllChannelRating';
    	// create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch); 

        $output = json_decode($output);

        foreach ($output->response as $key => $value) {
        	# code...
        	$arrSelect[$value->id] = $value->name;
        }

        return $arrSelect;

    } 

    private function uploadCoverImage($data = []){
    	$program_id = $data['program_id'];


		$this->load->library(array('upload'));
            //print_r($_FILES);
		if(!is_dir('uploaded/program/'.$program_id.'')){
			mkdir('uploaded/program/'.$program_id.'',0777,true);
		}
		clearDirectory('uploaded/program/'.$program_id.'/');

		$config = array();                                         
		$config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
		$config['upload_path'] = 'uploaded/program/'.$program_id.'/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '2000';
		$this->upload->initialize($config);
		if(!$this->upload->do_upload('cover')){
			$error = array('error' => $this->upload->display_errors());
			$this->msg->add($error['error'], 'error');
			if(file_exists('uploaded/program/'.$program_id)){
				rmdir('uploaded/program/'.$program_id);
			}

		}else{
			$data_upload = array('upload_data' => $this->upload->data());

                //$promotion_update = new M_promotion($promotion_id);
			$upload_file_name = $data_upload['upload_data']['file_name']; 

			$update_program = new M_program($program_id);
			$update_program->cover = $upload_file_name;
			$update_program->cover_size = $_FILES['cover']['size']; 
			$update_program->save();
			return true;
		}
    }

    private function getDurationForInputText($data = []){ 


    	$program = $data['program']; 
    	$duration_txt = '';

    	if($program->id){
    		$start_datetime = new DateTime($program->program_start_datetime);
    		$end_datetime = new DateTime($program->program_end_datetime); 
    		$duration_txt = $start_datetime->format('d-m-Y H:i').' - '.$end_datetime->format('d-m-Y H:i');

    	}else{
    		$duration_txt = date('d-m-Y H:i').' - '.date('d-m-Y H:i');
    	}

    	return $duration_txt;



    }

    private function getApiChannelNameByChannelId($data = []){ 
    	$api_channels_id = $data['api_channels_id'];

    	$arrSelect = [];
    	$arrSelect[''] = __('Select Channel','admin/program/create_program');
    	$url = 'http://s3remoteservice.psi.co.th/rating/welcome/getAllChannelRating';
    	// create curl resource
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch); 

        $output = json_decode($output); 

        $key = array_search($api_channels_id, array_column($output->response, 'id'));

        if(is_numeric($key)){
        	return $output->response[$key]->name;
        }else{
        	return '';
        }


    }

}