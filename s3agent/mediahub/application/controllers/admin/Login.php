<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();

		$this->load->library(array('Lang_controller','Msg'));
		$this->load->helper(array('our','lang','cookie'));
	}
	public function index(){  

		if($this->session->userdata('admin_data')){
			redirect(base_url('admin/dashboard'));
		}

		if($this->input->post(NULL,FALSE)){

			$queryCheck = $this->db->select('*')
			->from('administrator')
			->where('email',$this->input->post('email'))
			->where('password',sha1($this->input->post('password')))
			->get();

			if($queryCheck->num_rows() > 0){
				
				$row = $queryCheck->row();
				if($this->input->post('remember_me')){
						$cookie_value = array(
							'email'=>base64_encode(base64_encode($row->email)),
							'password'=>base64_encode(base64_encode($this->input->post('password')))
						);
						set_cookie(array(
							'name'=>'remember_me_email',
							'value'=>$cookie_value['email'],
							'expire'=>1296000,
							'prefix'=>'_mediahubadmin_'
						));
						set_cookie(array(
							'name'=>'remember_me_password',
							'value'=>$cookie_value['password'],
							'expire'=>1296000,
							'prefix'=>'_mediahubadmin_'
						));

				}else{
						delete_cookie('_mediahubadmin_remember_me_email');
						delete_cookie('_mediahubadmin_remember_me_password');
				}

				if(!$row->active){
					$this->msg->add(__('Access has been disable,Please try again.','admin/login'),'error');
					redirect($this->uri->uri_string());
				}


				$this->session->set_userdata('admin_data',$queryCheck->row_array());

				
				redirect(base_url('admin/dashboard'));
				


			}else{
				$this->msg->add(__('Email or password invalid','admin/login'),'error');
				redirect($this->uri->uri_string());
			}
		}

		$data = [
			'remember_me'=>$this->getRememberMe()
		];

		$this->load->view('admin/login',$data);
	}

	public function logout(){
		$this->session->set_userdata('admin_data','');
		redirect(base_url());
	}

	private function getRememberMe(){
		$arReturn = array(
    		'email'=>'',
    		'password'=>''
    	);

    	if(get_cookie('_mediahubadmin_remember_me_email') && get_cookie('_mediahubadmin_remember_me_password')){

    		$username = base64_decode(base64_decode(get_cookie('_mediahubadmin_remember_me_email')));
    		$admin_password = base64_decode(base64_decode(get_cookie('_mediahubadmin_remember_me_password')));
    		
    		$arReturn['email'] = $username;
    		$arReturn['password'] = $admin_password;
    	}
    	return $arReturn;

	}
}
