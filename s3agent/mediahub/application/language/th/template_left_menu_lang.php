<?php

$lang['Dashboard'] = "Dashboard";
$lang['Custom video on demand'] = "Custom video on demand";
$lang['VOD'] = "VOD";
$lang['VOD SEASONS'] = "VOD SEASONS";
$lang['VOD EPISODE'] = "VOD EPISODE";
$lang['VOD OWNER'] = "VOD OWNER";
$lang['VOD CATEGORIES'] = "VOD CATEGORIES";
$lang['General Setting'] = "General Setting";
$lang['USERS'] = "USERS";
$lang['PROGRAM'] = "PROGRAM";
$lang['Timeslot Program'] = "Timeslot Program";
$lang['HILIGH PROGRAM'] = "HILIGH PROGRAM";
$lang['HIGHLIGHT PROGRAM'] = "HIGHLIGHT PROGRAM";
$lang['PROGRAM LIST'] = "PROGRAM LIST";
$lang['ARRANG HIGHLIGHT PROGRAM'] = "ARRANG HIGHLIGHT PROGRAM";
$lang['SORT HIGHLIGHT'] = "SORT HIGHLIGHT";
