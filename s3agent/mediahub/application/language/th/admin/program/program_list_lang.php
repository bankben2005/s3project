<?php

$lang['PROGRAM LIST'] = "PROGRAM LIST";
$lang['Add program'] = "Add program";
$lang['Name'] = "Name";
$lang['Duration'] = "Duration";
$lang['Highlight Program'] = "Highlight Program";
$lang['Created'] = "Created";
$lang['Updated'] = "Updated";
$lang['Highlight'] = "Highlight";
$lang['Cover'] = "Cover";
$lang['No Highlight'] = "No Highlight";
