<?php

$lang['VOD OWNER LIST'] = "VOD OWNER LIST";
$lang['Create vod owner'] = "Create vod owner";
$lang['Create vod owner form'] = "Create vod owner form";
$lang['Create program'] = "Create program";
$lang['Create program form'] = "Create program form";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['Duration'] = "Duration";
$lang['Is Hightlight program'] = "Is Hightlight program";
$lang['No Highlight'] = "No Highlight";
$lang['Highlight'] = "Highlight";
$lang['Status'] = "Status";
$lang['PROGRAM LIST'] = "PROGRAM LIST";
$lang['Cover'] = "Cover";
$lang['Select Channel'] = "Select Channel";
$lang['Create program success'] = "Create program success";
$lang['Edit program'] = "Edit program";
$lang['Edit program form'] = "Edit program form";
$lang['Edit program success'] = "Edit program success";
$lang['On air day'] = "On air day";
$lang['On air time'] = "On air time";
