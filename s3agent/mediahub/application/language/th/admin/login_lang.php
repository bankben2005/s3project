<?php

$lang['Media Hub Administrator'] = "Media Hub Administrator";
$lang['Remember Me'] = "จดจำการเข้าใช้ระบบ";
$lang['Login'] = "เข้าสู่ระบบ";
$lang['Email or password invalid'] = "อีเมล์หรือรหัสผ่านไม่ถูกต้อง";
$lang['Enter Email Address...'] = "กรอกอีเมล์...";
$lang['Password'] = "รหัสผ่าน";
