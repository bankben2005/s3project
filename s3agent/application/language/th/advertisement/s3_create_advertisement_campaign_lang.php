<?php

$lang['S3 Campaign lists'] = "รายการแคมเปญ";
$lang['Create advertisement campaign'] = "สร้างแคมเปญโฆษณา";
$lang['Name'] = "ชื่อ";
$lang['Description'] = "รายละเอียด";
$lang['StartTime - EndTime'] = "เริ่มต้น - สิ้นสุด";
$lang['Active'] = "สถานะเปิดใช้งาน";
$lang['Popup Image'] = "รูปภาพ Popup บนมือถือ";
$lang['pls_select_file'] = "กรุณาเลือกไฟล์";
$lang['Popup image redirect url'] = "URL สำหรับรูปภาพ Popup";
$lang['Edit advertisement campaign'] = "แก้ไขแคมเปญโฆษณา";
$lang['confirm_delete_image'] = "ต้องการลบรูปภาพใช่หรือไม่";
$lang['Create advertisement campaign success'] = "สร้างแคมเปญโฆษณาสำเร็จ";
$lang['Image Preview'] = "ดูรูปภาพขนาดใหญ่";
$lang['Edit advertisement campaign success'] = "แก้ไขแคมเปญโฆษณาสำเร็จ";
$lang['Go to set advertisement image and video'] = "<strong> > > ไปยังหน้าตั้งค่ารูปโฆษณาและวิดีโอ</strong> <a href='%s' target='_blank'>คลิก</a>";
$lang['Delete popup image success'] = "Delete popup image success";
