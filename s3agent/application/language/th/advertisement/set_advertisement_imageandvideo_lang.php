<?php

$lang['Set advertisment images and video'] = "ตั้งค่ารูปภาพและวิดีโอ";
$lang['Channel Campaign List'] = "รายการแคมเปญโฆษณา";
$lang['Click for upload image'] = "คลิกเพื่ออัพโหลดรูปภาพ";
$lang['Click for add youtube url'] = "คลิกเพื่อเพิ่ม Youtube video";
$lang['Video Url'] = "URL ของวิดีโอ";
$lang['Start Time'] = "เริ่มเมื่อ";
$lang['Upload image and set duration'] = "อัพโหลดรูปภาพและตั้งเวลา เริ่มต้น -สิ้นสุด";
$lang['Duration'] = "เริ่มต้น-สิ้นสุด";
$lang['Image'] = "รูปภาพ";
$lang['pls_select_file'] = "กรุณาเลือกไฟล์";
$lang['Edit image and set duration'] = "แก้ไขรูปภาพและตั้งเวลา เริ่มต้น-สิ้นสุด";
$lang['Add/Edit youtube url and duration'] = "เพิ่ม / แก้ไข URLของ Youtube และกำหนดเวลา เริ่มต้น-สิ้นสุด";
$lang['Start'] = "เริ่มเมื่อ";
$lang['Youtube URL'] = "URL ของ Youtube";
$lang['Image Preview'] = "ดูรูปภาพขนาดใหญ่";
$lang['confirm_delete_image'] = "ต้องการลบรูปภาพใช่หรือไม่";
$lang['Create advertisement image success'] = "เพิ่มรูปภาพไปยังแคมเปญโฆษณาสำเร็จ";
$lang['Delete advertisement image success'] = "ลบรูปภาพจากแคมเปญโฆษณาสำเร็จ";
$lang['Edit advertisement image success'] = "แก้ไขรูปภาพจากแคมเปญโฆษณาสำเร็จ";
