<?php

$lang['S3 Campaign lists'] = "รายการแคมเปญ";
$lang['S3 campaign list table'] = "ตารางแสดงรายการแคมเปญ";
$lang['Advertisement Name'] = "ชื่อ";
$lang['Period'] = "เริ่มต้น-สิ้นสุด";
$lang['Expire Status'] = "สถานะดำเนินการ";
$lang['Created'] = "สร้างเมื่อ";
$lang['Updated'] = "แก้ไขล่าสุด";
$lang['Status'] = "สถานะ";
$lang['Expired'] = "สิ้นสุด";
$lang['Setting image and video'] = "ตั้งค่ารูปภาพและวิดีโอ";
$lang['Create advertisement campaign'] = "สร้างแคมเปญโฆษณา";
$lang['Delete advertisement campaign success'] = "Delete advertisement campaign success";
