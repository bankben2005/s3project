<?php

$lang['Email'] = "อีเมล์ / ชื่อเข้าใช้ระบบ";
$lang['Enter email'] = "Enter email";
$lang['Password'] = "รหัสผ่าน";
$lang['Remember Password'] = "จดจำการเข้าใช้ระบบ";
$lang['Login'] = "เข้าสู่ระบบ";
$lang['Forgot Password?'] = "ลืมรหัสผ่าน?";
$lang['Email or password incorrect'] = "อีเมล์หรือรหัสผ่านไม่ถูกต้อง";
$lang['User has been disabled,Please contact administrator'] = "User has been disabled,Please contact administrator";
