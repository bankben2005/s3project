<?php

$lang['Email or password incorrect'] = "อีเมล์หรือรหัสผ่านไม่ถูกต้อง";
$lang['อีเมล์หรือรหัสผ่านไม่ถูกต้อง'] = "อีเมล์หรือรหัสผ่านไม่ถูกต้อง";
$lang['User has been disabled,Please contact administrator'] = "บัญชีของคุณถูกระงับการใช้งาน กรุณาติดต่อผู้ดูแลระบบ";
$lang['Dashboard'] = "หน้าหลัก";
$lang['Advertisement'] = "โฆษณา";
$lang['Expired'] = "สิ้นสุดแล้ว";
$lang['Active'] = "เปิดใช้งาน";
$lang['Unactive'] = "ไม่เปิดใช้งาน";
$lang['Submit'] = "ตกลง";
$lang['Success'] = "สำเร็จ";
$lang['Save'] = "บันทึกข้อมูล";
$lang['No Expired'] = "กำลังดำเนินการ";
$lang['Status'] = "Status";
$lang['Close'] = "Close";
