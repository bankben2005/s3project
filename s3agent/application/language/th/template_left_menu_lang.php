<?php

$lang['Channel Agent System'] = "Channel Agent System";
$lang['CHANNEL AGENT SYSTEM'] = "CHANNEL AGENT SYSTEM";
$lang['Dashboard'] = "หน้าหลัก";
$lang['Advertisement'] = "โฆษณา";
$lang['Signout'] = "ออกจากระบบ";
$lang['ADVERTISEMENT SYSTEM'] = "ADVERTISEMENT SYSTEM";
$lang['S3 ADVERTISEMENT'] = "S3 ADVERTISEMENT";
$lang['System Manual'] = "คู่มือการใช้ระบบ";
$lang['Change Password'] = "เปลี่ยนรหัสผ่าน";
$lang['Channel time slots'] = "Channel time slots";
