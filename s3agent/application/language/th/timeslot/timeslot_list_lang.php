<?php

$lang['Channel time slot lists'] = "Channel time slot lists";
$lang['Channel time slot table'] = "Channel time slot table";
$lang['Create time slot'] = "Create time slot";
$lang['Advertisement Name'] = "Advertisement Name";
$lang['Period'] = "Period";
$lang['Expire Status'] = "Expire Status";
$lang['Updated'] = "Updated";
$lang['Status'] = "Status";
$lang['Budget Range'] = "Budget Range";
