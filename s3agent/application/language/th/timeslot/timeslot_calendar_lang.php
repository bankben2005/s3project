<?php

$lang['Channel time slot'] = "Channel time slot";
$lang['Edit time slot'] = "Edit time slot";
$lang['Start time'] = "Start time";
$lang['End time'] = "End time";
$lang['Lowest Budget'] = "Lowest Budget";
$lang['Highest Budget'] = "Highest Budget";
$lang['Duration'] = "Duration";
$lang['Name'] = "Name";
$lang['Description'] = "Description";
$lang['Add slot package'] = "Add slot package";
