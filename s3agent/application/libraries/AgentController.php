<?php
     class AgentController extends CI_Controller{
            
             public $meta_title = '';
             public $meta_keywords = '';
             public $meta_description = '';
             public $data = array();
             public $page_num = 10;
             public $controller = '';
             public $method = '';
             public $language = 'th';
             public $default_language;
             public $site_setting = '';
             public $agent_data = array();
             private $conn_mysql;
             public $onesignal_config = array();
             public $extrameta = "";
             public $encryption_key = 'k^R&<cKQ(44f{mb,';
             public $event_ability = array();
             public $channels_data = array();
            
         public function __construct() {
                    parent::__construct();
                    $CI = & get_instance();
                    $this->setController($CI->router->class);
                    $this->setMethod($CI->router->method);

                    $this->load->library(array('Msg', 'Lang_controller'));
                    $this->load->helper(array('lang', 'our', 'inflector','html','cookie','url'));
                    
                    $this->template->set_template('template');


                    $this->_checkAlready_signin();
                    $this->_setAgentDataAndChannelData();

                    $this->_load_js();
                    $this->_load_css(); 

                    //print_r($this->agent_data);

                    //print_r($this->channels_data);exit;

                    //print_r($this->session->userdata());


            }
     
    /**
     * load javascript
     */
    public function _load_js() {

        $this->template->javascript->add(base_url('assets/vendor/jquery/jquery.min.js'));
        $this->template->javascript->add(base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'));
        $this->template->javascript->add(base_url('assets/vendor/jquery-easing/jquery.easing.min.js'));
        $this->template->javascript->add(base_url('assets/js/sb-admin.min.js'));


    }
    /**
     * load style sheet
     */
    public function _load_css() {

        $this->template->stylesheet->add(base_url('assets/vendor/bootstrap/css/bootstrap.min.css'));
        $this->template->stylesheet->add(base_url('assets/vendor/font-awesome/css/font-awesome.min.css'));
        $this->template->stylesheet->add(base_url('assets/css/sb-admin.css'));
        $this->template->stylesheet->add(base_url('assets/css/additional_style.css'));


    }

    public function loadSweetAlert(){
        $this->template->javascript->add('https://unpkg.com/sweetalert/dist/sweetalert.min.js');
    }
    
    public function loadValidator(){
        $this->template->stylesheet->add(base_url('assets/css/bootstrapvalidator/bootstrapValidator.min.css'));
        $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/bootstrapValidator.min.js'));

        if($this->config->item('language') == 'th'){
            $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/language/th_TH.js'));
        }else{
            $this->template->javascript->add(base_url('assets/js/bootstrapvalidator/language/en_US.js'));
        }
    }
    public function loadImageUploadStyle(){
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_common.css'));
        $this->template->stylesheet->add(base_url('assets/css/imghover/img_hover.css'));

    }
    public function loadImageUploadScript(){
        $this->template->javascript->add(base_url('assets/js/fileupload_script.js'));
    }
    public function loadDataTableStyle(){
        $this->template->stylesheet->add(base_url('assets/vendor/datatables/dataTables.bootstrap4.css'));
    }
    public function loadDataTableScript(){
        $this->template->javascript->add(base_url('assets/js/plugins/jquery.dataTables.min.js'));
        $this->template->javascript->add(base_url('assets/js/plugins/dataTables.bootstrap.min.js'));
        $this->template->javascript->add(base_url('assets/js/enable_datatable.js'));
    }

    public function loadDateRangePickerStyle(){
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-daterangepicker/daterangepicker.css'));
    }
    public function loadDateRangePickerScript(){
        $this->template->javascript->add(base_url('assets/js/bootstrap-daterangepicker/moment.min.js'));
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');
        $this->template->javascript->add(base_url('assets/js/bootstrap-daterangepicker/daterangepicker.js'));
    }
    public function loadTempusDominusStyle(){
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-tempusdominus/tempusdominus-bootstrap-4.min.css'));
    }
    public function loadTempusDominusScript(){
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/moment.min.js');
        $this->template->javascript->add('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.0/locale/th.js');

        $this->template->javascript->add(base_url('assets/js/bootstrap-tempusdominus/tempusdominus-bootstrap-4.min.js'));
    }

    public function loadTypeAheadScript(){
        $this->template->javascript->add(base_url('assets/js/typeahead/bootstrap3-typeahead.min.js'));
    }

    public function loadTagsInputStyle(){
        $this->template->stylesheet->add(base_url('assets/css/bootstrap-tagsinput/bootstrap-tagsinput.css'));
    }
    public function loadTagsInputScript(){
        $this->template->javascript->add(base_url('assets/js/bootstrap-tagsinput/bootstrap-tagsinput.min.js'));

    }

    public function loadChartJsScript(){
        $this->template->javascript->add(base_url('assets/vendors/chart.js/Chart.min.js'));
    }

    public function loadBootstrapNotify(){
        $this->template->javascript->add(base_url('assets/js/plugins/bootstrap-notify.min.js'));
    }

    // public function loadFullCalendarStyle(){
    //     $this->template->stylesheet->add(base_url('assets/plugins/fullcalendar/packages/core/main.css'));
    //     $this->template->stylesheet->add(base_url('assets/plugins/fullcalendar/packages/daygrid/main.css'));
    //     $this->template->stylesheet->add(base_url('assets/plugins/fullcalendar/packages/timegrid/main.css'));
    //     $this->template->stylesheet->add(base_url('assets/plugins/fullcalendar/packages/list/main.css'));
    // }

    // public function loadFullCalendarScript(){

    //     $this->template->javascript->add(base_url('assets/plugins/fullcalendar/packages/core/main.js'));
    //     $this->template->javascript->add(base_url('assets/plugins/fullcalendar/packages/core/locales-all.js'));
    //     $this->template->javascript->add(base_url('assets/plugins/fullcalendar/packages/interaction/main.js'));
    //     $this->template->javascript->add(base_url('assets/plugins/fullcalendar/packages/daygrid/main.js'));
    //     $this->template->javascript->add(base_url('assets/plugins/fullcalendar/packages/timegrid/main.js'));
    //     $this->template->javascript->add(base_url('assets/plugins/fullcalendar/packages/list/main.js'));

    // }

    public function loadFullCalendarStyle(){
         $this->template->stylesheet->add(base_url('assets/plugins/fullcalendar/css/fullcalendar.min.css'));
        // $this->template->stylesheet->add(base_url('assets/plugins/fullcalendar/css/fullcalendar.print.min.css'));
    }
    public function loadFullCalendarScript(){
        $this->template->javascript->add(base_url('assets/plugins/fullcalendar/js/lib/moment.min.js'));
        $this->template->javascript->add(base_url('assets/plugins/fullcalendar/js/fullcalendar.min.js'));
        // $this->template->javascript->add(base_url('assets/js/plugins/moment.min.js'));
        $this->template->javascript->add(base_url('assets/plugins/jquery-ui.custom.min.js'));
        // $this->template->javascript->add(base_url('assets/js/plugins/fullcalendar.min.js'));
        $this->template->javascript->add(base_url('assets/plugins/fullcalendar/js/locale/th.js'));

    }



    public function getEventAbility(){
        $query = $this->db->select('*')
        ->from('AccountantMenuPermission')
        ->join('AccountantMenu','AccountantMenuPermission.AccountantMenu_id = AccountantMenu.id')
        ->where('AccountantMenu.Controller_name',$this->controller)
        ->where('AccountantMenu.Method_name',$this->method)
        ->where('AccountantMenuPermission.Accountant_id',$this->administrator_data['id'])
        ->get();

        //echo $this->db->last_query();exit;
        if($query->num_rows() > 0){
            $this->event_ability = json_decode($query->row()->EventAbility,true);
        }
        //print_r($this->event_ability);
    }

    public function _checkAlready_signin(){
        if(!$this->session->userdata('agent_data')){
            redirect(base_url('signin'));
        }else{
            $this->agent_data = $this->session->userdata('agent_data');
           // print_r($this->administrator_data);exit;
        }
    }

    public function _getAccountData(){
        if($this->session->userdata('administrator_id') && $this->session->userdata('administrator_data')){
            $this->administrator_data = $this->session->userdata('administrator_data');
            //$this->account_data = $this->session->userdata('member_data');
        }

    }

    /**
     * get default language of hotel
     */
    public function _default_langauge() {
        $shop_language = new M_gshop_shop_language();
        $shop_language->where('gshop_shop_id', $this->shop_id)->where('default_language', 1)->get();
        $this->default_language = $shop_language->language->get()->getForeign_language_name();
       // echo "default lang =".$this->default_language;
    }

    /**
     * change lang system
     */
    public function change_lang() {
       
        if ($this->input->post(NULL, FALSE)) {
            $this->session->set_userdata('language', $this->input->post('language'));
        }
    }

    /**
     *
     * @param <type> $file_view  ชื่อ file view
     * @param <type> $return true = return view �?ลับมา�?สดงเรย / false = ไม่ return view �?ลับ
     */
    public function setView($file_view = null, $return = false) {
        if ($return)
            $this->load->view($file_view, $this->getData());
        else
            $this->setData('the_content', $this->load->view($file_view, $this->getData(), true));
    }

    /**
     * �?สดง view
     */
    public function getView() {
        $this->load->view($this->getTheme(), $this->getData());
    }

    /**
     *
     * @return them template
     */
    public function getTheme() {
        return $this->theme;
    }

    public function setTheme($theme) {
        $this->theme = $theme;
    }

    public function getMeta_title() {
        return $this->meta_title;
    }

    public function setMeta_title($meta_title) {
        $this->setData('meta_title', $meta_title);
        $this->meta_title = $meta_title;
    }

    public function getMeta_keywords() {
        return $this->meta_keywords;
    }

    public function setMeta_keywords($meta_keywords) {
        $this->setData('meta_keywords', $meta_keywords);
        $this->meta_keywords = $meta_keywords;
    }

    public function getMeta_description() {
        return $this->meta_description;
    }

    public function setMeta_description($meta_description) {
        $this->setData('meta_description', $meta_description);
        $this->meta_description = $meta_description;
    }

    public function getData($key = null) {
        if (is_null($key) || empty($key))
            return $this->data;
        else
            return $this->data[$key];
    }

    public function setData($key, $data) {
        $this->data[$key] = $data;
    }

    public function getPage_num() {
        return $this->page_num;
    }

    public function setPage_num($page_num) {
        $this->page_num = $page_num;
    }

    public function getController() {
        return $this->controller;
    }

    public function setController($controller) {
        $this->setData('controller', $controller);
        $this->controller = $controller;
    }

    public function getMethod() {
        return $this->method;
    }

    public function setMethod($method) {
        $this->setData('method', $method);
        $this->method = $method;
    }

    /**
     * จัด�?ารเรื่อง pagination
     * @param <type> $total จำนวน�?ถวทั้งหมด
     * @param <type> $cur_page หน้าปัจจุบัน
     */
    public function config_page($total, $cur_page) {

        //print_r($cur_page);exit;
                $config['base_url'] = base_url();
                $config['total_rows'] = $total;
                $config['per_page'] = $this->page_num;

                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';
                $config['num_tag_open'] = '<li class="page-item">';
                $config['num_tag_close'] = '</li>';

                $config['prev_tag_open'] = '<li class="paging_btn">';
                $config['prev_tag_close'] = '</li>';
                $config['prev_link'] = '« ' . __('Previous', 'b2c_default');

                $config['cur_tag_open'] = '<li class="active"><a class="page-link">';
                $config['cur_tag_close'] = '</a></li>';
                $config['next_link'] = __('Next', 'b2c_default') . ' »';
                $config['next_tag_open'] = '<li class="paging_btn">';
                $config['next_tag_close'] = '</li>';
                $config['num_links'] = 5;

                $config['first_tag_open'] = '<li>';
                $config['first_tag_close'] = '</li>';
                $config['first_link'] = __('First', 'b2c_default');
                $config['last_tag_open'] = '<li>';
                $config['last_tag_close'] = '</li>';
                $config['last_link'] = __('Last', 'b2c_default');
                $config['cur_page'] = $cur_page;
                $this->data['config_page'] = $config;
                $this->setData('config_page', $config);
            }

        private function _setAgentDataAndChannelData(){
            $this->agent_data = $this->session->userdata('agent_data');

            $CI =& get_instance();
            $query_channel = $CI->db->select('*')
            ->from('channels')
            ->where('id',$this->agent_data['owner_channel_id'])
            ->get();

            $this->channels_data = $query_channel->row();

            // print_r($this->agent_data);
            // print_r($this->channels_data);exit;

            //$agent_data = $this->session->userdata('agent_data');
            //print_r($agent_data);
        }

}