<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="#"><?php echo __('Dashboard','default')?></a>
        </li>
      </ol>
      <!-- Icon Cards-->
      <div class="row">
        <div class="col-xl-6 col-sm-6 mb-3">
          <div class="card text-white bg-success o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-comments"></i>
              </div>
              <div class="mr-5 advertisement-active"><span><?php echo $advertisement_count['advertisement_active']?></span> <?php echo __('ADVERTISEMENTS ACTIVE')?></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('advertisement/s3_campaign_list')?>">
              <span class="float-left"><?php echo __('View Details')?></span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>
        <div class="col-xl-6 col-sm-6 mb-3">
          <div class="card text-white bg-primary o-hidden h-100">
            <div class="card-body">
              <div class="card-body-icon">
                <i class="fa fa-fw fa-list"></i>
              </div>
              <div class="mr-5 advertisement-all"><span><?php echo $advertisement_count['advertisement_all']?></span> <?php echo __('ALL ADVERTISEMENT')?></div>
            </div>
            <a class="card-footer text-white clearfix small z-1" href="<?php echo base_url('advertisement/s3_campaign_list')?>">
              <span class="float-left"><?php echo __('View Details')?></span>
              <span class="float-right">
                <i class="fa fa-angle-right"></i>
              </span>
            </a>
          </div>
        </div>

    </div>