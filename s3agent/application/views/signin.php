<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>PSI AGENT - AUTHENTICATION</title>
  <!-- Bootstrap core CSS-->
  <link href="<?php echo base_url('assets/vendor/bootstrap/css/bootstrap.min.css')?>" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="<?php echo base_url('assets/vendor/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="<?php echo base_url('assets/css/sb-admin.css')?>" rel="stylesheet">

  <style type="text/css">
    h1{
      font-size: 1.5rem;
    }
  </style>
</head>

<body class="bg-dark">
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header text-center">
      <img src="<?php echo base_url('assets/images/psi-icon.png')?>"><br> 
        <h1>ADVERTISEMENT SYSTEM</h1>
      </div>
      <div class="card-body">
        <?php echo message_warning($this)?>
        <?php echo form_open('',array('name'=>'signin-form'))?>

          <div class="form-group">
            <strong><label><?php echo __('Email')?></label></strong>
            <?php echo form_input(array(
              'name'=>'email',
              'class'=>'form-control',
              'value'=>@$remember_me['email']
            ))?>
          </div>

          <div class="form-group">
            <strong><label><?php echo __('Password')?> : </label></strong>
            <?php echo form_input(array(
              'type'=>'password',
              'class'=>'form-control',
              'name'=>'password',
              'value'=>@$remember_me['password']
            ))?>
          </div>


          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <?php echo form_checkbox(array(
                  'name'=>'stay_signin',
                  'value'=>1,
                  'class'=>'form-check-input',
                  'checked'=>($remember_me['email'] || $remember_me['password'])?TRUE:FALSE
                ))?> <?php echo __('Remember Password')?>
            </div>
          </div>

          <div class="form-group">
            <?php echo form_button(array(
              'type'=>'submit',
              'class'=>"btn btn-primary btn-block",
              'content'=>__('Login')
            ))?>
          </div>

        <?php echo form_close()?>


        <div class="text-center" style="display: none;">
          <a class="d-block small" href="<?php echo base_url($this->router->fetch_class().'/forgot_password')?>"><?php echo __('Forgot Password?')?></a>
        </div>
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="<?php echo base_url('assets/vendor/jquery/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
  <!-- Core plugin JavaScript-->
  <script src="<?php echo base_url('assets/vendor/jquery-easing/jquery.easing.min.js')?>"></script>
</body>

</html>
