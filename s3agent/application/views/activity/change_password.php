<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('dashboard')?>"><?php echo __('Dashboard','default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Change Password')?></li>
      </ol>

      <div class="row">
        <div class="col-lg-12">
          <?php echo message_warning($this)?>
        </div>
      </div>

      <div class="card">
      	<div class="card-header">
      		<?php echo __('Change password form')?>
      	</div>
      	<div class="card-body">

          <?php echo form_open('',array('name'=>'change-password-form'))?>
            <div class="col-lg-4">
                <div class="form-group">
                  <label><strong><?php echo __('Old Password')?> : </strong></label>
                  <?php echo form_input(array(
                    'type'=>'password',
                    'name'=>'old_password',
                    'class'=>'form-control'
                  ))?>
                </div>


                <div class="form-group">
                  <label><strong><?php echo __('New Password')?> : </strong></label>
                  <?php echo form_input(array(
                    'type'=>'password',
                    'class'=>'form-control',
                    'name'=>'new_password'
                  ))?>
                </div>

                <div class="form-group">
                  <label><strong><?php echo __('Confirm New Password')?> : </strong></label>
                  <?php echo form_input(array(
                    'type'=>'password',
                    'class'=>'form-control',
                    'name'=>'confirm_new_password'
                  ))?>
                </div>

                <div class="form-group">
                  <?php echo form_button(array(
                    'type'=>'submit',
                    'class'=>'btn btn-success btn-block',
                    'content'=>'<i class="fa fa-key"></i> '.__('Confirm change password')
                  ))?>
                </div>


            </div>
          <?php echo form_close()?>

        </div>
      </div>
