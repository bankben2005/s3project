<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>PSI - CHANNEL AGENT SYSTEM</title>
  
  <?php echo $this->template->stylesheet?>
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <?php echo form_input(array(
    'type'=>'hidden',
    'name'=>'base_url',
    'value'=>base_url()
  ))?>



  <!-- Navigation Start template left menu -->
  <?php $this->load->view('template_left_menu')?>
  <!-- Eof template left menu -->

  <div class="content-wrapper">
    <div class="container-fluid">
      <?php echo $this->template->content?>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Softtech Network Co.,Ltd 2019 v 1.0</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="<?php echo base_url('signin/signout')?>">Logout</a>
          </div>
        </div>
      </div>
    </div>


    <?php echo $this->template->javascript;?>



  </div>
</body>

</html>
