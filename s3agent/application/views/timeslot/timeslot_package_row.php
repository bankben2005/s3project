<div class="row">
	<div class="col-lg-4">
		<div class="form-group">
			<?php echo form_dropdown('timeslot_package[]',$select_package,'','class="form-control timeslot_package"')?>
		</div>
	</div>

	<div class="col-lg-7">
		<div class="input-group mb-3">
		 <!--  <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username" aria-describedby="basic-addon2"> -->
		  <?php echo form_input([
		  	'name'=>'package_price[]',
		  	'class'=>'form-control package_price'
		  ])?>
		  <div class="input-group-append">
		    <span class="input-group-text" id="basic-addon2">Baht</span>
		  </div>
		</div>
	</div>

	<div class="col-lg-1">
		<a href="javascript:void(0)" onclick="removePackageRow(this)"><i class="fa fa-window-close"></i></a>

	</div>
</div>	