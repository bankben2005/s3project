<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('dashboard')?>"><?php echo __('Dashboard','default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url($this->controller)?>">
        	<?php echo __('Timeslots list')?>
        	</a>
        		
        </li>
        <li class="breadcrumb-item active">
        	<?php echo ($channel_timeslots->id)?__('Edit channel timeslot'):__('Create channel timeslot')?>
        </li>
      </ol>
      <div class="row">
      	<div class="col-lg-12">
      		<?php echo message_warning($this)?>
      	</div>
      </div>

      <div class="card">
      	<div class="card-header">
      		<?php echo ($channel_timeslots->id)?__('Edit channel timeslot'):__('Create channel timeslot')?>
      	</div>
      	<div class="card-body">
      		<?php echo form_open_multipart('',array('name'=>'create-channel-timeslots-form'))?>
            <div class="row">
              <div class="col-lg-6">
                <div class="form-group">
                  <label><strong><?php echo __('Duration')?> : </strong></label> 
                  <?php echo form_input([
                    'name'=>'duration',
                    'class'=>'form-control',
                    'value'=>@$channel_timeslots->duration_txt
                  ])?>
                </div>

                <div class="row">
                  <div class="col-lg-6">
                    <label><strong><?php echo __('Lowest Budget')?> : </strong></label> 
                    <?php echo form_input([
                      'name'=>'lowest_budget',
                      'class'=>'form-control',
                      'value'=>@$channel_timeslots->lowest_budget
                    ])?>
                  </div>
                  <div class="col-lg-6">
                    
                      <label><strong><?php echo __('Highest Budget')?> : </strong></label> 
                      <?php echo form_input([
                        'name'=>'highest_budget',
                        'class'=>'form-control',
                        'value'=>@$channel_timeslots->highest_budget
                      ])?>
                    
                  </div>
                </div> 

                <div class="form-group">
                  <label><strong><?php echo __('Status','default')?></strong></label>
                  <?php echo form_dropdown('active',[
                    '1'=>__('Active','default'),
                    '0'=>__('Unactive','default')
                  ],@$channel_timeslots->active,'class="form-control"')?>
                </div>


                <div class="form-group">
                  <?php echo form_button([
                    'type'=>'submit',
                    'class'=>'btn btn-primary float-right',
                    'content'=>__('Submit','default')
                  ])?>
                </div>


              </div>
              <div class="col-lg-6">

              </div>
            </div>

          <?php echo form_close()?>
        </div>
      </div>
