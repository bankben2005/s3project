<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('dashboard')?>"><?php echo __('Dashboard','default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Channel time slot lists')?></li>
      </ol>

      <div class="row">
      	<div class="col-lg-12">
      		<?php echo message_warning($this)?>
      	</div>
      </div>
      <div class="card">
      	<div class="card-header">
      		<?php echo __('Channel time slot table')?>
      		
      		<a href="<?php echo base_url($this->controller.'/createTimeSlot')?>" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i> <?php echo __('Create time slot')?></a>		
      	</div>
      	<div class="card-body">
      		<div class="table-responsive">
	            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	             <thead>
	                <tr>	                  
	                  <th><?php echo __('Period')?></th>
	                  <th><?php echo __('Budget Range')?></th>
	                  <th><?php echo __('Updated')?></th>
	                  <th><?php echo __('Status')?></th>
	                  <th width="30%"></th>
	                </tr>
	             </thead>

	            <tbody>
                
              </tbody>
            </table>
          </div>
        </div>
      </div>