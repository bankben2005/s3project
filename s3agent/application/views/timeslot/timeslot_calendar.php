 <?php echo form_input([
  'type'=>'hidden',
  'name'=>'hide_current_date',
  'value'=>date('Y-m-d')
 ])?>

 <?php echo form_input([
 	'type'=>'hidden',
 	'name'=>'hide_channel_id',
 	'value'=>@$this->channels_data->id
 ])?>

  <!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('dashboard')?>"><?php echo __('Dashboard','default')?></a>
        </li>
        <li class="breadcrumb-item active"><?php echo __('Channel time slot')?></li>
      </ol>

      <div class="row">
        <div class="col-lg-12">
          <div id='calendar'></div>
        </div>
      </div>
      


  <!-- Modal -->
<div class="modal fade" id="modalSetTimeSlot" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-large" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Edit time slot')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="row">
        <div class="col-lg-12" id="show_btn_delete">

        </div>
      </div>
      <?php echo form_open('',['name'=>'edit-timeslot-form'])?>
      <?php echo form_input([
      	'type'=>'hidden',
      	'name'=>'hide_channel_id',
      	'value'=>@$this->channels_data->id
      ])?>
      <?php echo form_input([
        'type'=>'hidden',
        'name'=>'hide_channel_timeslot_id',
        'value'=>''
      ])?>
      <div class="modal-body">

        <div class="row">
          <div class="col-lg-5">
            <div class="form-group">
              <label><strong><?php echo __('Name')?> : </strong></label> 
              <?php echo form_input([
                'name'=>'name',
                'class'=>'form-control'
              ])?>
            </div> 

            <div class="form-group">
              <label><strong><?php echo __('Description')?> : </strong></label>
              <?php echo form_textarea([
                'name'=>'description',
                'class'=>'form-control',
                'rows'=>2
              ])?>
            </div>

            <div class="form-group">
              <label><strong><?php echo __('Duration')?> : </strong></label>
              <?php echo form_input([
                'name'=>'duration',
                'class'=>'form-control'
              ])?>
            </div>

            <div class="row">
          <div class="col-lg-6">
            <div class="form-group">
              <label><strong><?php echo __('Lowest Budget')?> : </strong></label>
              <?php echo form_input([
                'name'=>'lowest_budget',
                'class'=>'form-control'
              ])?>
            </div> 

            

          </div>
          <div class="col-lg-6"> 
            <div class="form-group">
              <label><strong><?php echo __('Highest Budget')?> : </strong></label>
              <?php echo form_input([
                'name'=>'highest_budget',
                'class'=>'form-control'
              ])?>
            </div>

          </div>
        </div>  

          </div>
          <div class="col-lg-7">

            <a href="javascript:void(0)" class="btn btn-primary float-right mb-3" onclick="clickAddTimeSlotPackage(this)"><i class="fa fa-plus"></i> <?php echo __('Add slot package')?></a> 

            <div class="clearfix"></div>
            <div id="slot-package-div">

            </div>

          </div>
        </div>

      	

      	



      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo __('Close','default')?></button>
        <?php echo form_button([
        	'type'=>'submit',
        	'class'=>'btn btn-primary float-right',
        	'content'=>__('Submit','default')
        ])?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>

