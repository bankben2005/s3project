<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('dashboard')?>"><?php echo __('Dashboard','default')?></a>
        </li>
        <li class="breadcrumb-item"><?php echo __('Advertisement','default')?></li>
        <li class="breadcrumb-item active"><?php echo __('S3 Campaign lists')?></li>
      </ol>

      <div class="row">
      	<div class="col-lg-12">
      		<?php echo message_warning($this)?>
      	</div>
      </div>
      <div class="card">
      	<div class="card-header">
      		<?php echo __('S3 campaign list table')?>
      		
      		<a href="<?php echo base_url($this->controller.'/s3_create_advertisement_campaign')?>" class="btn btn-primary btn-sm float-right"><i class="fa fa-plus"></i> <?php echo __('Create advertisement campaign')?></a>		
      	</div>
      	<div class="card-body">
      		<div class="table-responsive">
	            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
	             <thead>
	                <tr>
	                  <th><?php echo __('Advertisement Name')?></th>
	                  <th><?php echo __('Period')?></th>
	                  <th><?php echo __('Expire Status')?></th>
	                  <th><?php echo __('Updated')?></th>
	                  <th><?php echo __('Status')?></th>
	                  <th width="30%"></th>
	                </tr>
	             </thead>

	             <tbody>
	             	<?php foreach($s3_campaign as $key => $row){?>
	             		<tr>
	             			<td><?php echo $row->name?></td>
	             			<td>
	             				<?php echo date('d/m/Y H:i',strtotime($row->start_datetime))?> - 
	             				<?php echo date('d/m/Y H:i',strtotime($row->end_datetime))?>
	             			</td>
	             			<td>

	             				<?php if(strtotime($row->end_datetime) < strtotime(date('Y-m-d H:i:s'))){?>
	             					<span class="badge badge-warning"><?php echo __('Expired','default')?></span>
	             				<?php }else{?>
	             					<span class="badge badge-success"><?php echo __('No Expired','default')?></span>
	             				<?php }?>
	             			</td>
	             			<td>
	             				<?php echo s3AgentDateTimeFormat($row->updated)?>
	             			</td>

	             			<td>
	             				<?php if($row->active){?>
	             					<span class="badge badge-success"><?php echo __('Active','default')?></span>
	             				<?php }else{?>
	             					<span class="badge badge-danger"><?php echo __('Unactive','default')?></span>
	             				<?php }?>
	             			</td>

	             			<td>
	             				<a href="<?php echo base_url('advertisement/s3_setting_image_video/'.$row->id)?>" class="btn btn-info btn-sm"><i class="fa fa-image"></i> <?php echo __('Setting image and video')?></a>

	             				<a href="<?php echo base_url('advertisement/s3_edit_advertisement_campaign/'.$row->id)?>" class="btn btn-secondary btn-sm"><i class="fa fa-pencil"></i></a>

	             				<a href="javascript:void(0)" onclick="if(confirm('ต้องการลบแคมเปญนี้ใช่หรือไม่') == true){window.location.href='<?php echo base_url($this->controller.'/delete_campaign/'.$row->id)?>'}" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i></a>
	             			</td>
	             		</tr>

	             	<?php }?>
	             </tbody>
	            </table>
        	</div>

      	</div>
      	<div class="card-footer">

      	</div>

      </div>