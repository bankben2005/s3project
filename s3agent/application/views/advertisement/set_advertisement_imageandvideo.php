<style type="text/css">
  /*.tile{
    overflow: auto;
  }*/
  table#table-images > tbody > tr > td{
    vertical-align: middle;
    width: 200px;
  }
  table#table-images > tbody > tr > td > img{
    /*vertical-align: middle;*/
    /*width: 200px;
    height: 138px;*/

  }
  #upload-ads-content{
    background-image: url('<?php echo base_url('assets/images/tv-background_2.png')?>');
    background-size: contain;
    height:770px;
    padding-top:20px;
    background-repeat: no-repeat;
    /*width: 1200px;*/
    width: 800px;
    /*overflow: auto;*/

  }
  #show_screen{
        width: 100%;
    margin: 0 auto;
    margin-left: 10px;
    /*margin-top: 40px;*/
  }
  #show_screen img{
    max-height: 102px;
    max-width: 152px;
    opacity: 1;
  }
  #show_screen .bx-upload{
    display: table-cell;
    text-align: center;
    border:1px dashed #ddd;
    padding: 0;
    /*min-height: 139px;*/
    border-width: thin;
    vertical-align: middle;
    width: 151px;
    height: 105px;
  }
  #show_screen .bx-upload a{
    position: relative;
    top: 45px;
    font-size: 10px;
  }
  #show_screen .bx-upload:hover{
    opacity: 0.7;
  }

  .view {
    border: 0;
    width: 100%;
    height: 100%;
    overflow: hidden;
    position: relative;
    text-align: center;
    -webkit-box-shadow: 1px 1px 2px #e6e6e6;
    -moz-box-shadow: 1px 1px 2px #e6e6e6;
    box-shadow: 1px 1px 2px #e6e6e6;
    cursor: default;
    background: #fff url(../images/bgimg.jpg) no-repeat center center;
  }
  .view img{
    display:initial;
  }
  .view .mask, .view .content {
    width: 100%;
    height: 100%;
  }

  /*@media(min-width: 1920px){
    #upload-ads-content{
          padding-top: 34px;
          padding-left: 25px;
          height: 1151px;
    }
    #show_screen .bx-upload {
      min-width: 313px;
      min-height: 218px;
    }
    #show_screen img {
      min-height: 104px;
      min-width: 310px;
      opacity: 1;
    }
  }

  @media(min-width: 1440px) and (max-width: 1535px){
    #upload-ads-content{
      height: 1140px;
      padding-top: 33px;
      padding-left: 25px

    }
    #show_screen .bx-upload {
     
              width: 221px;
    height: 151px;

    }
    @media(min-width: 1536px) and (max-width: 1919px){
      #show_screen .bx-upload{
        width: 237px;
        height: 162px;
      }
    }

    #show_screen .bx-upload a {
      position: relative;
      top: 62px;
    }
    #show_screen img{
      min-width: 210px;
    }

    #show_screen{
          margin-left: 2px;
    }*/

  }

/*  @media(min-width: 1366px) and (max-width: 1439px){

    #show_screen .bx-upload {
        width: 204px;
        height: 142px;
    }


    #show_screen img{
      max-width: 200px;
    }
    #show_screen .bx-upload a{
          font-size: 12px;
    }

  }

  @media (min-width: 1280px) and (max-width: 1365px){
    #show_screen .bx-upload{
          width: 186px;
          height: 129px
    }
  }

  @media (min-width: 1024px) and (max-width: 1279px){
    #show_screen .bx-upload{
        width: 140px;
        height: 97px;
    }
    #show_screen .bx-upload a{
          top: 27px;
          font-size: 14px;
    }
    #show_screen img{
      max-height: 94px;
    }
    
  }
*/
  

  /*@media(min-width: 768px) and (max-width: 1023px){
    #show_screen .bx-upload{
      width: 139px;
      height: 96px;
    }
    #show_screen .bx-upload a {
      position: relative;
      top: 25px;
      font-size: 12px;
    }
    #show_screen img{
      max-height: 94px;
    }
  }*/
  @media(min-width: 992px) and (max-width: 1064px){
    #show_screen .bx-upload{
      width: 148px !important;
      height: 103px !important;
    }
  }
</style>
<div class="app-title">
        
        <ul class="app-breadcrumb breadcrumb side">
          <li class="breadcrumb-item"><a href="<?php echo base_url('admin')?>"><i class="fa fa-home fa-lg"></i></a></li>
          <li class="breadcrumb-item"><a href="<?php echo base_url($this->controller.'/s3_campaign_list')?>"><?php echo __('Channel Campaign List')?></a></li>
          <li class="breadcrumb-item active"><?php echo __('Set advertisment images and video')?> (<?php echo $ads_campaign->name;?>)</li>
        </ul>
</div>
<div class="row">
          <div class="col-lg-12">
            <?php echo message_warning($this)?>
          </div>
</div>
<div class="tile mb-4">

        
        <div class="page-header">
          <div class="row">
            <div class="col-lg-12">
              <h2 class="mb-3 line-head" id="buttons"><?php echo __('Set advertisment images and video')?> (<?php echo $ads_campaign->name;?>)</h2>
            </div>
          </div>
        </div>
        <div class="row">
            

            <div class="col-lg-12">

                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'hide_ads_campaign_id',
                    'value'=>$ads_campaign->id
                  ))?>

                  <?php 
                  $hide_start_date = new DateTime();
                  $hide_start_date->add(new DateInterval('P1D'));
                  echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'hide_start_date',
                    'value'=>$hide_start_date->format('d-m-Y')
                  ))?>

                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'hide_end_date',
                    'value'=>date('d-m-Y',strtotime($ads_campaign->end_datetime))
                  ))?>


                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'ads_uploaded_path',
                    'value'=>$this->ads_uploaded_path
                  ))?>

                  <?php echo form_input(array(
                    'type'=>'hidden',
                    'name'=>'ads_image_url',
                    'value'=>$this->ads_image_url
                  ))?>
                
                  <?php echo form_open('',array('name'=>'set-advertisement-imagevideo-form'))?>

                    


                      <div class="col-lg-12" id="upload-ads-content">


                        <div class="col-lg-12" id="show_screen">
                        <?php 
                          $position = 1;
                          for($i=1;$i<=4;$i++){

                        ?>

                          <div class="row">
                             <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="bx-upload text-center">

                                <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo $this->ads_image_url.'advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image']?>" class="img-responsive">
                                    <div class="mask">
                                                        <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                        <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>

                                                        <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url().$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>
                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="bx-upload">
                                <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo $this->ads_image_url.'advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image']?>" class="img-responsive">
                                    <div class="mask">
                                            <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>
                                             
                                             <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                              
                                              <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url().$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>

                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="bx-upload">
                                <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo $this->ads_image_url.'advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image']?>" class="img-responsive">
                                    <div class="mask">
                                                <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                           
                                                <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url().$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>

                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="bx-upload">
                                 <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo $this->ads_image_url.'advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image']?>" class="img-responsive">
                                    <div class="mask">
                                                <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                           
                                                <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url().$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>

                            <?php 
                                  $arr_image = getImageByPosition($ads_images,$position);
                              ?>
                            <div class="bx-upload">
                                 <?php if(@$arr_image['id']){?>
                                  <div class="view view-first">
                                    <img src="<?php echo $this->ads_image_url.'advertisement/campaign_images/'.$arr_image['id'].'/'.$arr_image['image']?>" class="img-responsive">
                                    <div class="mask">
                                                <a href="javascript:void(0)" class="btn btn-info" data-imageid="<?php echo @$arr_image['id']?>" data-imagename="<?php echo @$arr_image['image']?>" data-duration="<?php echo @$arr_image['duration']?>" data-toggle="modal" data-target="#editAdsCampaignImageModal" onclick="editAdsCampaignImage(this)">
                                                          <i class="fa fa-pencil"></i>
                                                        </a>

                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                           
                                                <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url().$this->controller.'/delete_image/'.$arr_image['id'];?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                  </div>
                                <?php $position++; }else{?>
                                  <a href="javascript:void(0)" class="click_upload_image" data-position="<?php echo $position++;?>" data-toggle="modal" data-target="#uploadImageModal"><?php echo __('Click for upload image')?></a>

                                <?php }?>
                            </div>




                          </div>

                        <?php }?>
                        </div>



                      </div>
                    <?php echo form_close()?>
                    <hr>
                    <div class="row mb-3" style="display: none;">
                      <div class="col-lg-12 mb-3 text-right">

                          <a href="javascript:void(0)" class="btn btn-info btn-sm" data-toggle="modal" data-target="#setYoutubeUrl"><i class="fa fa-plus"></i> <?php echo __('Click for add youtube url')?></a>
                          
                      </div>

                      <div class="col-lg-12">
                        <table class="table table-bordered">
                          <thead>
                            <tr>
                              <th><?php echo __('Video Url')?></th>
                              <th><?php echo __('Start Time')?></th>
                              <th></th>
                            </tr>
                          </thead>
                          <tbody>
                              <?php foreach($ads_campaign->advertisement_campaign_videos->get() as $key => $row){?>
                                <tr>
                                  <td><?php echo $row->video_url?></td>
                                  <td><?php echo $row->start_datetime?></td>
                                  <td>
                                    <a href="javascript:void(0)" class="btn btn-secondary btn-sm" data-startdatetime="<?php echo date('d-m-Y H:i',strtotime($row->start_datetime))?>" data-youtubeurl="<?php echo $row->video_url?>" data-adcvideoid="<?php echo $row->id?>" onclick="editAdsCampaignVideos(this)"><i class="fa fa-pencil"></i></a>

                                    <a href="javascript:void(0)" class="btn btn-danger btn-sm" onclick="deleteAdsCampaignVideos(this)" data-adcvideoid="<?php echo $row->id?>"><i class="fa fa-trash"></i></a>

                                  </td>
                                </tr>
                              <?php }?>
                          </tbody>
                        </table>
                      </div>
                    </div>

                    


            </div>
        </div>
</div>

<!-- Modal -->
<div class="modal fade" id="uploadImageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Upload image and set duration')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <?php echo form_open_multipart('',array('name'=>'upload-image-form'))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'hide_position'
      ))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'advertisement_campaigns_id'
      ))?>
      <div class="modal-body">
          <div class="form-group">
            <label><strong><?php echo __('Duration')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'duration',
              'class'=>'form-control',
              'autocomplete'=>'off',
              'value'=>$hide_start_date->format('d-m-Y').' 00:00'.' - '.$hide_start_date->format('d-m-Y').' 00:00'
            ))?>
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Image')?> (jpg)</strong></label>

            <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('ads_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                        </div>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
         <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Save','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>



<!-- Modal -->
<div class="modal fade" id="editAdsCampaignImageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Edit image and set duration')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open_multipart(base_url($this->controller.'/postEditAdsImage'),array('name'=>'edit-adsimage-form'))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'hide_ads_image_id',
        'value'=>''
      ))?>
      <div class="modal-body">
        

          <div class="form-group">
            <label><strong><?php echo __('Duration')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'edit_duration',
              'class'=>'form-control',
              'autocomplete'=>'off',
              'value'=>''
            ))?>
          </div>

          <div class="form-group">
            <img src="" class="img-fluid">
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Image')?> (jpg)</strong></label>

            <div class="input-group">
                                        <span class="input-group-btn">
                                         <span class="btn btn-primary btn-file">
                                                 <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('ads_image', '', '');?>
                                             </span>
                                        </span>
                                         <input type="text" class="form-control" readonly>
                        </div>
          </div>


        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Save','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="setYoutubeUrl" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?php echo __('Add/Edit youtube url and duration')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <?php echo form_open('',array('name'=>'add-youtube-url'))?>
      <?php echo form_input(array(
        'type'=>'hidden',
        'name'=>'ads_campaign_videos_id',
        'value'=>'0'
      ))?>
      <div class="modal-body">
        
          <div class="form-group">
            <label><strong><?php echo __('Start')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'start_datetime',
              'class'=>'form-control',
              'autocomplete'=>'off'
            ))?>
          </div>

          <div class="form-group">
            <label><strong><?php echo __('Youtube URL')?> : </strong></label>
            <?php echo form_input(array(
              'name'=>'youtube_url',
              'class'=>'form-control',
            ))?>
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        <?php echo form_button(array(
          'type'=>'submit',
          'class'=>'btn btn-primary',
          'content'=>__('Save','default')
        ))?>
      </div>
      <?php echo form_close()?>
    </div>
  </div>
</div>




<!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo __('Image Preview')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <img src="" id="imagepreview" class="img-fluid" >
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>