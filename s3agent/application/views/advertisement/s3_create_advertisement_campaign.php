<!-- Breadcrumbs-->
      <ol class="breadcrumb">
        <li class="breadcrumb-item">
          <a href="<?php echo base_url('dashboard')?>"><?php echo __('Dashboard','default')?></a>
        </li>
        <li class="breadcrumb-item">
        	<a href="<?php echo base_url($this->controller.'/s3_campaign_list')?>">
        	<?php echo __('S3 Campaign lists')?>
        	</a>
        		
        </li>
        <li class="breadcrumb-item active">
        	<?php echo ($advertisement_campaigns->id)?__('Edit advertisement campaign'):__('Create advertisement campaign')?>
        </li>
      </ol>
      <div class="row">
      	<div class="col-lg-12">
      		<?php echo message_warning($this)?>
      	</div>
      </div>

      <div class="card">
      	<div class="card-header">
      		<?php echo ($advertisement_campaigns->id)?__('Edit advertisement campaign'):__('Create advertisement campaign')?>
      	</div>
      	<div class="card-body">
      		<?php echo form_open_multipart('',array('name'=>'create-advertisement-campaign-form'))?>
      		<div class="row">
                      <div class="col-lg-4">

                        <div class="form-group">
                          <label><strong><?php echo __('Name')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'name',
                            'class'=>'form-control',
                            'value'=>@$advertisement_campaigns->name
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Description')?> : </strong></label>
                          <?php echo form_textarea(array(
                            'name'=>'description',
                            'class'=>'form-control',
                            'rows'=>3,
                            'value'=>@$advertisement_campaigns->description
                          ))?>
                        </div>
                      </div>
                      <div class="col-lg-4">
                        <?php 
                          $duration_show = "";

                          if(@$advertisement_campaigns->id){
                            $start_datetime = new DateTime(@$advertisement_campaigns->start_datetime);
                            $end_datetime = new DateTime(@$advertisement_campaigns->end_datetime);
                            
                            $duration_show = $start_datetime->format('d-m-Y H:i').' - '.$end_datetime->format('d-m-Y H:i');
                          }

                        ?>
                        <div class="form-group">
                          <label><strong><?php echo __('StartTime - EndTime')?> : </strong></label>
                          <?php echo form_input(array(
                            'name'=>'duration',
                            'class'=>'form-control',
                            'autocomplete'=>'off',
                            'value'=>@$duration_show
                          ))?>
                        </div>

                        <div class="form-group">
                          <label><strong><?php echo __('Active')?> : </strong></label>
                          <?php echo form_dropdown('active',array(
                            '1'=>__('Active','default'),
                            '0'=>__('Unactive','default')
                          ),@$advertisement_campaigns->active,'class="form-control"')?>
                        </div>

                      </div>

                      <div class="col-lg-4">
                          <div class="form-group">
                                <?php //print_r($channel_data)?>
                                  <label><?php echo __('Popup Image')?> : (jpg)</label>
                                  <div class="clearfix"></div>
                                      <?php if($advertisement_campaigns->popup_image && file_exists($ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image)){?>
                                                  <?php //echo 'aaaa';?>
                                          <div class="view view-first" style="margin:5px 0px;">
                                                              <img src="<?php echo $ads_image_url.'advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image;?>" style="width:100%;height: 100%;">
                                                               <div class="mask">
                                                                <a href="javascript:void(0);" class="pop btn btn-secondary"><i class="fa fa-eye"></i></a>
                                                                   <a href="#" onclick="if(confirm('<?php echo __('confirm_delete_image');?>')==true){window.location='<?php echo base_url($this->controller.'/deleteAdsPopupImage/'.$advertisement_campaigns->id);?>'}else{return false;}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                                               </div>
                                          </div>
                                             
                                                  
                                    <?php }?>
                                              
                                  <div class="clearfix"></div>
                                              
                                  <div class="input-group">
                                                  <span class="input-group-btn">
                                                   <span class="btn btn-primary btn-file">
                                                           <i class="fa fa-upload"></i> <?php echo __('pls_select_file');?><?php echo  form_upload('popup_image', '', '');?>
                                                       </span>
                                                  </span>
                                                   <input type="text" class="form-control" readonly>
                                  </div>
                          </div>

                          <div class="form-group">
                            <label><strong><?php echo __('Popup image redirect url')?> : </strong></label>
                            <?php echo form_input(array(
                              'name'=>'popup_image_url',
                              'class'=>'form-control',
                              'placeholder'=>'ex. http://www.google.com',
                              'value'=>$advertisement_campaigns->popup_image_url
                            ))?>
                          </div>

                      </div>

            </div>
            <div class="row">
            	<div class="col-lg-12">
            		<div class="form-group">
            			<?php echo form_button(array(
            				'type'=>'submit',
            				'class'=>'btn btn-primary float-right',
            				'content'=>__('Submit','default')
            			))?>
            		</div>
            	</div>
            </div>
      		<?php echo form_close()?>

      	</div>
      </div>


    <!-- Creates the bootstrap modal where the image will appear -->
<div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"><?php echo __('Image Preview')?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
          <img src="" id="imagepreview" class="img-fluid" >
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo __('Close','backend/default')?></button>
      </div>
    </div>
  </div>
</div>
