<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AgentController.php';
class TimeSlot extends AgentController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
        parent::__construct();
    }

    public function index(){  

    	// $this->loadTempusDominusStyle();
    	$this->loadDateRangePickerStyle();
    	$this->loadFullCalendarStyle();

    	// $this->loadTempusDominusScript();
    	$this->loadDateRangePickerScript();
    	$this->loadFullCalendarScript();

    	$this->loadValidator();


    	$this->template->stylesheet->add(base_url('assets/css/timeslot/timeslot_calendar.css'));
    	$this->template->javascript->add(base_url('assets/js/timeslot/timeslot_calendar.js'));

    	$channel_timeslots = new M_channel_timeslots();
    	$channel_timeslots->order_by('id','desc')->get();



    	$data = [
    		'channel_timeslots'=>$channel_timeslots
    	];

    	$this->template->content->view('timeslot/timeslot_calendar',$data);
    	$this->template->publish();

    }

    public function ajaxSetChannelTimeslot(){
    	$post_data = $this->input->post();  

      // print_r($post_data);exit;

    	$exDuration = explode(' - ', $post_data['duration']); 

    	$startDateTime = new DateTime($exDuration[0]);
    	$endDateTime = new DateTime($exDuration[1]); 

        $endDT = new DateTime($exDuration[1]);

    	$endDateTime = $endDateTime->modify('+1 day'); 

    	$startTime = $startDateTime->format('H:i');
    	$endTime = $endDateTime->format('H:i');

        // print_r($endDT);exit;

    	
      $interval = new DateInterval('P1D');
      $daterange = new DatePeriod($startDateTime, $interval ,$endDateTime); 

      if($this->input->post('hide_channel_timeslot_id') && $this->input->post('hide_channel_timeslot_id') != ''){

            $update_timeslot = new M_channel_timeslots($this->input->post('hide_channel_timeslot_id'));

            $update_timeslot->name = $post_data['name'];
            $update_timeslot->description = $post_data['description'];
            $update_timeslot->start_datetime = $startDateTime->format('Y-m-d H:i:s');
            $update_timeslot->end_datetime = $endDT->format('Y-m-d H:i:s');
            $update_timeslot->lowest_budget = $post_data['lowest_budget'];
            $update_timeslot->highest_budget = $post_data['highest_budget'];
            $update_timeslot->save(); 

            $this->updateChannelTimeSlotHasPackage([
              'channel_timeslots_id'=>$update_timeslot->id,
              'post_data'=>$post_data
            ]);

      }else{
        foreach($daterange as $date){

            $check_channel_exist = new M_channel_timeslots();
            $check_channel_exist->where('channels_id',$post_data['hide_channel_id'])
            ->where('start_datetime',$date->format('Y-m-d').' '.$startTime)
            ->where('end_datetime',$date->format('Y-m-d').' '.$endTime)
            ->get();

            if($check_channel_exist->result_count() > 0){ 

                // update channel exist 
                $check_channel_exist->name = $post_data['name'];
                $check_channel_exist->description = $post_data['description'];
                $check_channel_exist->lowest_budget = $post_data['lowest_budget'];
                $check_channel_exist->highest_budget = $post_data['highest_budget'];
                $check_channel_exist->save(); 

                $this->updateChannelTimeSlotHasPackage([
                  'channel_timeslots_id'=>$check_channel_exist->id,
                  'post_data'=>$post_data
                ]);

            }else{ 


                // insert data into record
                $channel_timeslots = new M_channel_timeslots();
                $channel_timeslots->channels_id = $post_data['hide_channel_id'];
                $channel_timeslots->name = $post_data['name'];
                $channel_timeslots->description = $post_data['description'];
                $channel_timeslots->start_datetime = $date->format('Y-m-d').' '.$startTime;
                $channel_timeslots->end_datetime = $date->format('Y-m-d').' '.$endTime;
                $channel_timeslots->lowest_budget = $post_data['lowest_budget'];
                $channel_timeslots->highest_budget = $post_data['highest_budget'];
                $channel_timeslots->created_by = $this->agent_data['id'];
                $channel_timeslots->active = 1;
                $channel_timeslots->save(); 

                $this->updateChannelTimeSlotHasPackage([
                  'channel_timeslots_id'=>$channel_timeslots->id,
                  'post_data'=>$post_data
                ]);

            }

            

        }

    }



    echo json_encode([
      'status'=>true,
      'post_data'=>$post_data
  ]);	
}

public function ajaxGetEventForRenderCalendar(){
   $post_data = $this->input->post(); 
   $arrEvent = [];
   $arrEventId = [];

   $this_month = (isset($post_data['select_month']))?$post_data['select_month']:date('m');
   $this_year = (isset($post_data['select_year']))?$post_data['select_year']:date('Y'); 

   $channel_timeslots = new M_channel_timeslots();
   $channel_timeslots->where('MONTH(start_datetime)',$this_month)
   ->where('YEAR(start_datetime)',$this_year)
   ->where('channels_id',@$this->channels_data->id)
   ->get();

   if($channel_timeslots->result_count() > 0){
      foreach ($channel_timeslots as $key => $value) {
    			# code...
         array_push($arrEvent, [
            'groupId'=>$value->id,
            'title'=>$value->name,
            'start'=>date('Y-m-d',strtotime($value->start_datetime)).'T'.date('H:i:s',strtotime($value->start_datetime)),
            'end'=>date('Y-m-d',strtotime($value->end_datetime)).'T'.date('H:i:s',strtotime($value->end_datetime)),
            'extendedProps'=>[
                'lowest_budget'=>$value->lowest_budget,
                'highest_budget'=>$value->highest_budget,
                'title_for_show'=>date('H:i',strtotime($value->start_datetime)).'-'.date('H:i',strtotime($value->end_datetime)).' '.$value->name,
                'channel_timeslot_has_packages'=>$this->getChannelTimeSlotHasPackages([
                  'channel_timeslots_id'=>$value->id
                ])
            ]
        ]);

                //array_push($arrEventId, $value->id);
     }

 }




 echo json_encode([
  'status'=>true,
  'post_data'=>$post_data,
  'events'=>$arrEvent
]);
}

public function ajaxGetAddTimeslotPackageRow(){
    $post_data = $this->input->post(); 

    $arrSelectPackage = $this->getSelectTimeSlotPackage();

    $view = $this->load->view('timeslot/timeslot_package_row',[
        'select_package'=>$arrSelectPackage
    ],true);


    echo json_encode([
        'status'=>true,
        'post_data'=>$post_data,
        'timeslot_package_row'=>$view
    ]);
}




public function createTimeSlot(){
   $this->__createTimeSlot();
}
public function editTimeSlot($id){ 

   $channel_timeslots = new M_channel_timeslots($id);

   if(!$channel_timeslots->id){
      redirect(base_url($this->controller));
  }

  $this->__createTimeSlot($id);

}

public function deleteTimeSlot($id){
   $channel_timeslots = new M_channel_timeslots($id);

   if(!$channel_timeslots->id){
      redirect(base_url($this->controller));
  }

}

private function __createTimeSlot($id = null){ 

   $this->loadValidator();

   $this->loadDateRangePickerStyle();
   $this->loadDateRangePickerScript();

   if($id){
      $this->template->javascript->add(base_url('assets/js/timeslot/edit_timeslot.js'));
  }else{
      $this->template->javascript->add(base_url('assets/js/timeslot/create_timeslot.js'));
  }

  $channel_timeslots = new M_channel_timeslots($id);


  if($this->input->post(NULL,FALSE)){ 

      $duration_txt = $this->input->post('duration'); 
      $exDuration = explode(' - ', $duration_txt);

      $start_datetime = new DateTime($exDuration[0]);
      $end_datetime = new DateTime($exDuration[1]); 

      $channel_timeslots->channels_id = $this->channels_data->id;
      $channel_timeslots->start_datetime = $start_datetime;
      $channel_timeslots->end_datetime = $end_datetime;
      $channel_timeslots->lowest_budget = $this->input->post('lowest_budget');
      $channel_timeslots->highest_budget = $this->input->post('highest_budget');
      $channel_timeslots->active = $this->input->post('active');  

      if($channel_timeslots->save()){ 

         $txtSuccess = ($id)?__('Edit timeslot success','timeslot/create_timeslot'):__('Create timeslot success','timeslot/create_timeslot'); 

         $this->msg->add($txtSuccess,'success');
         redirect($this->uri->uri_string());

     }




    		// print_r($start_datetime);
    		// print_r($end_datetime);

    		// exit;


 } 


 $data = [
  'channel_timeslots'=>$channel_timeslots
];


$this->template->content->view('timeslot/create_timeslot',$data);
$this->template->publish();

}

private function getSelectTimeSlotPackage(){
    $arrReturn = [];

    $timeslot_package = new M_channel_timeslot_packages();
    $timeslot_package->order_by('id','asc')->get();

    foreach ($timeslot_package as $key => $value) {
        # code...
        $arrReturn[$value->id] = $value->name;
    }

    return $arrReturn;

}

private function updateChannelTimeSlotHasPackage($data = []){ 
  $post_data = $data['post_data'];

  // clear all data and insert new record
  $this->db->delete('channel_timeslot_has_packages',[
    'channel_timeslots_id'=>$data['channel_timeslots_id']
  ]);

  foreach ($post_data['timeslot_package'] as $key => $value) {
    # code...
    $channel_timeslot_has_packages = new M_channel_timeslot_has_packages();
    $channel_timeslot_has_packages->channel_timeslots_id = $data['channel_timeslots_id'];
    $channel_timeslot_has_packages->channel_timeslot_packages_id = $value;
    $channel_timeslot_has_packages->budget = $post_data['package_price'][$key];
    $channel_timeslot_has_packages->save();
  }

}

private function getChannelTimeSlotHasPackages($data = []){ 

  $arrReturn = [];


  $channel_timeslot_has_packages = new M_channel_timeslot_has_packages();
  $channel_timeslot_has_packages->where('channel_timeslots_id',$data['channel_timeslots_id'])
  ->get();


  foreach ($channel_timeslot_has_packages as $key => $value) {
    # code...
    array_push($arrReturn, [
        'channel_timeslot_packages_id'=>$value->channel_timeslot_packages_id,
        'budget'=>$value->budget
    ]);
  }

  return $arrReturn;


}

}