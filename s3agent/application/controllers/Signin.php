<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct(){
		parent::__construct();

		$this->load->library(array(
			'Lang_controller',
			'Msg'
		));

		$this->load->helper(array(
			'lang',
			'our',
			'cookie'
		));
	}
	public function index(){

		print_r($this->session->userdata('agent_data'));

		if($this->session->userdata('agent_data')){
			redirect(base_url('dashboard'));
		}

		if($this->input->post(NULL,FALSE)){
			//print_r($this->input->post());exit;
			$user = new M_user();
			$user->where('email',$this->input->post('email'))
			->where('password',sha1($this->input->post('password')))
			->get();

			if($user->id){

				// print_r($user->to_array());exit;
				if($user->active == '0' || $user->owner_channel_id == '0' || $user->ownner_channel_status == '0'){
					$this->msg->add(__('User has been disabled,Please contact administrator','signin'),'error');
					redirect($this->uri->uri_string());
				}

				if($this->input->post('stay_signin')){
						$cookie_value = array(
							'email'=>base64_encode(base64_encode($user->email)),
							'password'=>base64_encode(base64_encode($this->input->post('password')))
						);
						set_cookie(array(
							'name'=>'remember_me_email',
							'value'=>$cookie_value['email'],
							'expire'=>1296000,
							'prefix'=>'_s3agent_'
						));
						set_cookie(array(
							'name'=>'remember_me_password',
							'value'=>$cookie_value['password'],
							'expire'=>1296000,
							'prefix'=>'_s3agent_'
						));

				}else{
						delete_cookie('_s3agent_remember_me_email');
						delete_cookie('_s3agent_remember_me_password');
				}


				$this->session->set_userdata('agent_data',$user->to_array());

				redirect(base_url('dashboard'));


			}else{

				$this->msg->add(__('Email or password incorrect','signin'),'error');
				redirect($this->uri->uri_string());
			}
		}


		$data = array(
			'remember_me'=>$this->getRememberMe()
		);

		//print_r($data);
		$this->load->view('signin',$data);
	}

	public function signout(){
		$this->session->unset_userdata('agent_data');
		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function forgot_password(){


		$data = array(

		);

		$this->load->view('forgot_password',$data);
	}

	private function getRememberMe(){
    	$arReturn = array(
    		'email'=>'',
    		'password'=>''
    	);

    	if(get_cookie('_s3agent_remember_me_email') && get_cookie('_s3agent_remember_me_password')){

    		$username = base64_decode(base64_decode(get_cookie('_s3agent_remember_me_email')));
    		$admin_password = base64_decode(base64_decode(get_cookie('_s3agent_remember_me_password')));
    		
    		$arReturn['email'] = $username;
    		$arReturn['password'] = $admin_password;
    	}
    	return $arReturn;
    }
}
