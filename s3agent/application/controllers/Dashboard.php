<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AgentController.php';
class Dashboard extends AgentController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){
    	//print_r($this->session->userdata('user_data'));
    	$data = array(
    		'advertisement_count'=>$this->getAdvertisementCount()
    	);
    	$this->template->content->view('dashboard/dashboard',$data);
    	$this->template->publish();
    }

    private function getAdvertisementCount(){
    	$advertisement_active = 0;
    	$advertisement_all = 0;

    	$channel_id = $this->agent_data['owner_channel_id'];

    	$query_active = $this->db->select('*')
    	->from('advertisement_campaigns')
    	->where('end_datetime >=',date('Y-m-d H:i:s'))
    	->where('channels_id',$channel_id)
    	->get();

    	$advertisement_active = $query_active->num_rows();

    	$query_all = $this->db->select('*')
    	->from('advertisement_campaigns')
    	->where('channels_id',$channel_id)
    	->get();

    	$advertisement_all = $query_all->num_rows();

    	return array(
    		'advertisement_active'=>$advertisement_active,
    		'advertisement_all'=>$advertisement_all
    	);
    }

}