<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AgentController.php';
class Activity extends AgentController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct() {
                        parent::__construct();
    }

    public function index(){

    }

    public function change_password(){

    	// print_r($this->agent_data);

    	$this->loadValidator();
    	$this->template->javascript->add(base_url('assets/js/activity/change_password.js'));


    	if($this->input->post(NULL,FALSE)){

    		$change_password = $this->db->update('user',[
    			'password'=>sha1($this->input->post('new_password'))
    		],['id'=>$this->agent_data['id']]);

    		//echo $this->db->last_query();exit;

    		if($change_password){
    			$this->msg->add(__('Change password success','activity/change_password'),'success');
    			redirect($this->uri->uri_string());
    		}
    	}

    	$data = array(

    	);

    	$this->template->content->view('activity/change_password',$data);
    	$this->template->publish();

    }

    public function ajaxCheckOldPassword(){
    	$get_data = $this->input->get();

    	$queryCheck = $this->db->select('id,password')
    	->from('user')
    	->where('id',$this->agent_data['id'])
    	->where('password',sha1($get_data['old_password']))
    	->get();

    	if($queryCheck->num_rows() > 0){
    		echo json_encode(array(
    			'valid'=>true
    		));
    	}else{
    		echo json_encode(array(
    			'valid'=>false
    		));
    	}
    }


}