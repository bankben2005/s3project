<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once APPPATH . 'libraries/AgentController.php';
class Advertisement extends AgentController {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public $ads_uploaded_path = "";
	public $ads_image_url = "";
	public function __construct() {
                parent::__construct();
                $this->setAdsUploadedPath();
    }

    public function index(){
    	//print_r($this->session->userdata('user_data'));
    	
    }

    public function s3_campaign_list(){

    	$this->loadDataTableStyle();


    	$this->loadDataTableScript();


    	$data = array(
    		's3_campaign'=>$this->getS3CampaignList()
    	);

    	$this->template->content->view('advertisement/s3_campaign_list',$data);
    	$this->template->publish();

    }

    public function s3_create_advertisement_campaign(){
    	$this->__s3_create_advertisement_campaign();
    }
    public function s3_edit_advertisement_campaign($id){
    	$advertisement_campaigns = new M_advertisement_campaigns();
    	$advertisement_campaigns->where('id',$id)
    	->where('channels_id',$this->agent_data['owner_channel_id'])
    	->get();

    	if(!$advertisement_campaigns->id){
    		redirect($this->controller.'/s3_campaign_list');
    	}
    	$this->__s3_create_advertisement_campaign($id);

    }
    public function s3_delete_advertisement_campaign($id){


    }

    public function delete_campaign($id){
        $advertisement_campaigns = new M_advertisement_campaigns();
        $advertisement_campaigns->where('id',$id)
        ->where('channels_id',$this->agent_data['owner_channel_id'])
        ->get();

        if($advertisement_campaigns->id){

            //print_r($advertisement_campaigns);exit;

            /* delete all campaign_images and campaign_videos */

            /* check this advertisement has advertisement campaign images */

            $queryCheck = $this->db->select('*')
            ->from('advertisement_campaign_images')
            ->where('advertisement_campaigns_id',$advertisement_campaigns->id)
            ->get();

            if($queryCheck->num_rows() > 0){
                /* unlink all image from uploaded folder */

                foreach ($queryCheck->result() as $key => $value) {
                    # code...
                    $image_path = $this->ads_uploaded_path.'advertisement/campaign_images/'.$value->id.'/'.$value->image;

                    //print_r($image_path);exit;
                    if(file_exists($image_path)){
                        unlink($image_path);
                    }

                    $this->db->delete('advertisement_campaign_images',[
                        'id'=>$value->id
                    ]);
                }
                

                /* eof unlink all image from uploaded folder */

            }


            $this->db->delete('advertisement_campaign_videos',[
                'advertisement_campaigns_id'=>$advertisement_campaigns->id
            ]);
            /* eof delete all campaign_images and campaign_videos*/

            if($advertisement_campaigns->delete()){

                $this->msg->add(__('Delete advertisement campaign success','advertisement/s3_campaign_list'),'success');
                redirect($this->controller.'/s3_campaign_list');
            }


            
        }else{
            redirect($this->controller.'/s3_campaign_list');

        }

    }

    public function deleteAdsPopupImage($advertisement_campaigns_id){

    	$advertisement_campaigns = new M_advertisement_campaigns();
        $advertisement_campaigns->where('id',$advertisement_campaigns_id)
        ->where('channels_id',$this->agent_data['owner_channel_id'])
        ->get();

        if($advertisement_campaigns->id){

            /* check file exist and remove file */
            if(file_exists($this->ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image)){
            	unlink($this->ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image);
            }

            if(is_dir($this->ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->id)){
            	rmdir($this->ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->id);
            }

            $advertisement_campaigns->popup_image = '';

            if($advertisement_campaigns->save()){
            	$this->msg->add(__('Delete popup image success','advertisement/s3_create_advertisement_campaign'),'success');
            	redirect($this->controller.'/s3_edit_advertisement_campaign/'.$advertisement_campaigns->id);
            }


        }
    }

    public function s3_setting_image_video($ads_id){
    	$this->loadImageUploadStyle();        
        $this->loadDateRangePickerStyle();

        $this->loadSweetAlert();
        $this->loadValidator();
        $this->loadDateRangePickerScript();
        $this->loadImageUploadScript();

        $this->template->javascript->add(base_url('assets/js/advertisement/set_advertisement_imageandvideo.js'));

        /* check this advertisement campaign */
        $advertisement_campaigns = new M_advertisement_campaigns();
        $advertisement_campaigns->where('id',$ads_id)
        ->where('channels_id',$this->agent_data['owner_channel_id'])
        ->get();



        if(!$advertisement_campaigns->id){
            redirect($this->controller);
        }
        /* eof check this advertisement campaign */


        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;

            $advertisement_campaign_images = new M_advertisement_campaign_images();
            $advertisement_campaign_images->where('advertisement_campaigns_id',$this->input->post('advertisement_campaigns_id'))
            ->where('position',$this->input->post('hide_position'))->get();

            $exDuration = explode(' - ', $this->input->post('duration'));
            $start_datetime = new DateTime($exDuration[0]);
            $end_datetime = new DateTime($exDuration[1]);

            //print_r($this->ads_uploaded_path);exit;
            //print_r($_FILES);exit;


            if($advertisement_campaign_images->id){
                /* if already has record*/
                $advertisement_campaign_images->start_datetime = $start_datetime->format('Y-m-d H:i:s');
                $advertisement_campaign_images->end_datetime = $end_datetime->format('Y-m-d H:i:s');
                
                if($advertisement_campaign_images->save()){
                    if($_FILES['ads_image']['error'] == 0){
                        $this->uploadAdsImage($advertisement_campaign_images);
                    }
                }

                $this->msg->add(__('Edit advertisement image success','advertisemnt/set_advertisement_imageandvideo'),'success');
                redirect($this->uri->uri_string());

                

            }else{

                // print_r($_POST);
                // print_r($_FILES);
                // exit;
                /* create new record */
                $created_ads_images = new M_advertisement_campaign_images();
                $created_ads_images->advertisement_campaigns_id = $this->input->post('advertisement_campaigns_id');
                $created_ads_images->position = $this->input->post('hide_position');
                $created_ads_images->start_datetime = $start_datetime->format('Y-m-d H:i:s');
                $created_ads_images->end_datetime = $end_datetime->format('Y-m-d H:i:s');

                if($created_ads_images->save()){
                    if($_FILES['ads_image']['error'] == 0){
                        $this->uploadAdsImage($created_ads_images);
                    }

                }
                $this->msg->add(__('Create advertisement image success','advertisement/set_advertisement_imageandvideo'),'success');
                redirect($this->uri->uri_string());

            }



        }


        $data = array(
            'ads_campaign'=>$advertisement_campaigns,
            'ads_images'=>$this->getArrAdvertisementImages($advertisement_campaigns)
        );

        //print_r($data['ads_images']);exit;
        //$key = array_search('1', array_column($data['ads_images'], 'position'));

        // echo $key;exit;

        $this->template->content->view('advertisement/set_advertisement_imageandvideo',$data);
        $this->template->publish();
    }

    public function delete_image($ads_campaign_image_id){

        $advertisement_campaign_images = new M_advertisement_campaign_images($ads_campaign_image_id);


        if(!$advertisement_campaign_images->id){
            redirect($this->controller.'/s3_setting_image_video');
        }

        $ads_campaign_id = $advertisement_campaign_images->advertisement_campaigns_id;


        /* delete image and folder image first */
        $folder_image = $this->ads_uploaded_path.'advertisement/campaign_images/'.$advertisement_campaign_images->id;
        $image_path = $folder_image.'/'.$advertisement_campaign_images->image;
        // echo $folder_image."<br>";
        // echo $image_path;
        if(file_exists($image_path)){
            /* unlink image and remove folder */
            unlink($image_path);
            if(is_dir($folder_image)){
                rmdir($folder_image);
            }
            
        }

        if($advertisement_campaign_images->delete()){
            $this->msg->add(__('Delete advertisement image success','advertisement/set_advertisement_imageandvideo'),'success');
            redirect(base_url($this->controller.'/s3_setting_image_video/'.$ads_campaign_id));
        }
        //echo $image_path;
        /* eof delete image and folder image */
    }

    public function ajaxSetAdsVideo(){
        $post_data = $this->input->post();

        $start_datetime = new DateTime($post_data['start_datetime']);

        $advertisement_campaign_videos = new M_advertisement_campaign_videos($post_data['ads_campaign_videos_id']);
        $advertisement_campaign_videos->advertisement_campaigns_id = $post_data['ads_campaign_id'];
        $advertisement_campaign_videos->video_url = $post_data['youtube_url'];
        $advertisement_campaign_videos->start_datetime = $start_datetime->format('Y-m-d H:i:s');
        $advertisement_campaign_videos->save();


        echo json_encode(array(
            'status'=>true,
            'post_data'=>$post_data
        ));
    }

    private function __s3_create_advertisement_campaign($id=null){

    	$this->loadValidator();
    	$this->loadImageUploadStyle();
    	$this->loadDateRangePickerStyle();


    	$this->loadImageUploadScript();
    	$this->loadDateRangePickerScript();

    	if($id){
    		$this->template->javascript->add(base_url('assets/js/advertisement/edit_ads.js'));
    	}else{
    		$this->template->javascript->add(base_url('assets/js/advertisement/create_ads.js'));
    	}

    	$advertisement_campaigns = new M_advertisement_campaigns($id);


    	if($this->input->post(NULL,FALSE)){
    		/* ex duration */
            $exDuration = explode(' - ', $this->input->post('duration'));
            $start_datetime = new DateTime($exDuration[0]);
            $end_datetime = new DateTime($exDuration[1]);
            /* eof ex duration */

            $advertisement_campaigns->name = $this->input->post('name');
            $advertisement_campaigns->description = $this->input->post('description');
            $advertisement_campaigns->start_datetime = $start_datetime->format('Y-m-d H:i:s');
            $advertisement_campaigns->end_datetime = $end_datetime->format('Y-m-d H:i:s');            
            $advertisement_campaigns->active = $this->input->post('active');
            $advertisement_campaigns->channels_id = $this->agent_data['owner_channel_id'];
            $advertisement_campaigns->popup_image_url = $this->input->post('popup_image_url');


            if($advertisement_campaigns->save()){

                /* check upload image */
                if($_FILES['popup_image']['error'] == 0){
                         $this->uploadAdvertisementCampaignImage(array(
                            'advertisement_campaigns'=>$advertisement_campaigns,
                            'segment_id'=>$id
                        ));
                }
               
                /* eof check upload image */


                $txt_success = ($id)?__('Edit advertisement campaign success','advertisement/s3_create_advertisement_campaign'):__('Create advertisement campaign success','advertisement/s3_create_advertisement_campaign');

                if(!$id){
                    $txt_go_to = __('Go to set advertisement image and video','advertisement/s3_create_advertisement_campaign');
                    $txt_go_to = sprintf($txt_go_to,base_url($this->controller.'/s3_setting_image_video/'.$advertisement_campaigns->id));

                }


                $txt_success .= " ".$txt_go_to;

                $this->msg->add($txt_success,'success');
                redirect($this->uri->uri_string());
            }

    	}

    	$data = array(
    		'advertisement_campaigns'=>$advertisement_campaigns,
    		'ads_uploaded_path'=>$this->ads_uploaded_path,
    		'ads_image_url'=>$this->ads_image_url
    	);

    	$this->template->content->view('advertisement/s3_create_advertisement_campaign',$data);
    	$this->template->publish();

    }


    private function getS3CampaignList(){
    	$channel_id = $this->agent_data['owner_channel_id'];
    	//echo $channel_id;exit;
    	$advertisement_campaigns = new M_advertisement_campaigns();
    	$advertisement_campaigns->where('channels_id',$channel_id)->get();

    	return $advertisement_campaigns;


    }

    private function uploadAdvertisementCampaignImage($data = array()){
        $advertisement_campaigns = $data['advertisement_campaigns'];
        $id = $data['segment_id'];


            if(!is_dir($this->ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->getId().'')){
                    mkdir($this->ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->getId().'',0777,true);
            }

            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
            $config['upload_path'] = $this->ads_uploaded_path.'advertisement/popup_images/'.$advertisement_campaigns->getId().'/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000';
            $this->load->library('upload',$config);
                if(!$this->upload->do_upload('popup_image')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->msg->add($error['error'], 'error');

                    if(file_exists($this->ads_uploaded_path.'advertisement/campaign_images/'.$advertisement_campaigns->getId())){
                            rmdir($this->ads_uploaded_path.'advertisement/campaign_images/'.$advertisement_campaigns->getId());
                    }
                    $advertisement_campaigns->delete();

                    if($id){
                        redirect('admin/'.$this->controller.'/editAdvertisementCampaign/'.$id);
                    }else{
                        redirect('admin/'.$this->controller.'/createAdvertisementCampaign');
                    }
            }else{

                        /* unlink older file */
                        $image_path = $this->ads_image_url.'advertisement/popup_images/'.$advertisement_campaigns->id.'/'.$advertisement_campaigns->popup_image;
                        if(file_exists($image_path)){
                            unlink($image_path);
                        }
                        /* eof unlink older file */

                        $data_upload = array('upload_data' => $this->upload->data());

                        $ads_update = new M_advertisement_campaigns($advertisement_campaigns->id);
                        $ads_update->popup_image = $data_upload['upload_data']['file_name'];
                        $ads_update->save();
                        // $employee_update = new M_employee($ads_image->getId());
                        // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                        // $employee_update->save();
            }





    }

    private function setAdsUploadedPath(){
    	switch (ENVIRONMENT) {
    		case 'development':
    			$this->ads_uploaded_path = '../s3remoterating/uploaded/';
    			$this->ads_image_url = 'http://s3remoterating.development/uploaded/';
    		break;
    		case 'testing':

    		break;
    		case 'production':
    			$this->ads_uploaded_path = '../rating/uploaded/';
    			$this->ads_image_url = 'http://s3remoteservice.psi.co.th/rating/uploaded/';
    		break;
    		
    		default:
    			# code...
    		break;
    	}
    }

    private function getArrAdvertisementImages($ads_campaign){

        $arrReturn = array();
        $arrPosition = array();
        //array_push($arrReturn, array());

        foreach ($ads_campaign->advertisement_campaign_images->get() as $key => $value) {
            # code...
            $duration = "";
            $start_datetime = new DateTime($value->start_datetime);
            $end_datetime = new DateTime($value->end_datetime);
            //print_r($value->to_array());
            array_push($arrReturn, array(
                'id'=>$value->id,
                'position'=>$value->position,
                'image'=>$value->image,
                'duration'=>$start_datetime->format('d-m-Y H:i').' - '.$end_datetime->format('d-m-Y H:i')
            ));

            //array_push($arrPosition, $value->position);
        }
        return $arrReturn;

    }

    public function postEditAdsImage(){

        if($this->input->post(NULL,FALSE)){
            //print_r($this->input->post());exit;
            $advertisement_campaign_images = new M_advertisement_campaign_images($this->input->post('hide_ads_image_id'));

            if($advertisement_campaign_images->id){
                $exDuration = explode(' - ', $this->input->post('edit_duration'));
                $start_datetime = new DateTime($exDuration[0]);
                $end_datetime = new DateTime($exDuration[1]);

                $advertisement_campaign_images->start_datetime = $start_datetime->format('Y-m-d H:i');
                $advertisement_campaign_images->end_datetime = $end_datetime->format('Y-m-d H:i');

                if($advertisement_campaign_images->save()){
                    if($_FILES['ads_image']['error'] == 0){
                        $this->uploadEditAdsImage($advertisement_campaign_images);
                    }

                    $this->msg->add(__('Edit advertisement image success','advertisement/set_advertisement_imageandvideo'),'success');
                    redirect(base_url($this->controller.'/s3_setting_image_video/'.$advertisement_campaign_images->advertisement_campaigns_id));
                    //redirect($this->uri->uri_string());

                }
            }

        }


    }

    private function uploadAdsImage($ads_image){
            if(!is_dir($this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId().'')){
                    mkdir($this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId().'',0777,true);
            }
            // clearDirectory($this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId().'/');
                                                     
            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
            $config['upload_path'] = $this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId().'/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000';
            $this->load->library('upload',$config);
                if(!$this->upload->do_upload('ads_image')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->msg->add($error['error'], 'error');

                    if(file_exists($this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId())){
                            rmdir($this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId());
                    }

                    $ads_image->delete();

                    redirect($this->controller.'/s3_setting_image_video');
            }else{
                        $data_upload = array('upload_data' => $this->upload->data());

                        $ads_update = new M_advertisement_campaign_images($ads_image->id);
                        $ads_update->image = $data_upload['upload_data']['file_name'];
                        $ads_update->save();
                        // $employee_update = new M_employee($ads_image->getId());
                        // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                        // $employee_update->save();
            }
    }

    private function uploadEditAdsImage($ads_image){

            $config['file_name'] = md5(strtotime(date('Y-m-d H:i:s')));
            $config['upload_path'] = $this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId().'/';
            $config['allowed_types'] = 'gif|jpg|png|jpeg';
            $config['max_size'] = '10000';
            $this->load->library('upload',$config);
                if(!$this->upload->do_upload('ads_image')){
                    $error = array('error' => $this->upload->display_errors());
                    $this->msg->add($error['error'], 'error');
                    if(file_exists($this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId())){
                            rmdir($this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->getId());
                    }
                    $ads_image->delete();

                    redirect($this->controller.'/s3_setting_image_video');
            }else{

                        /* unlink older file */
                        $image_path = $this->ads_uploaded_path.'advertisement/campaign_images/'.$ads_image->id.'/'.$ads_image->image;
                        if(file_exists($image_path)){
                            unlink($image_path);
                        }
                        /* eof unlink older file */

                        $data_upload = array('upload_data' => $this->upload->data());

                        $ads_update = new M_advertisement_campaign_images($ads_image->id);
                        $ads_update->image = $data_upload['upload_data']['file_name'];
                        $ads_update->save();
                        // $employee_update = new M_employee($ads_image->getId());
                        // $employee_update->picture_profile = $data_upload['upload_data']['file_name'];
                        // $employee_update->save();
            }

    }

}